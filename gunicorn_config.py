from multiprocessing import cpu_count
from os import environ
import os

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

logsfiles = os.path.join(BASE_DIR, 'logs')



def max_workers():
    return cpu_count() + 1



name = "kiosk"
bind = '127.0.0.1:8000'
max_requests = 10000
worker_class = 'gevent'
workers = max_workers()
accesslog = os.path.join(logsfiles,'gunicorn_access.log')
loglevel = 'debug'

# errorlog = os.path.join(logsfiles,'gunicorn_error.log')

# def post_fork(server, worker):
#     from psycogreen.gevent import psyco_gevent
#     psyco_gevent.make_psycopg_green()

# gunicorn -c gunicorn_config.py --log-file=- kiosk.kiosk_wsgi:application

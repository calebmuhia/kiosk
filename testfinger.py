import SocketServer
import time
import random
import json


data = {"enroll_status":"already_enrolled"}

class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    
    def handle(self):
        # self.request is the TCP socket connected to the client
        
        print self.request.recv(1024).strip()
        self.request.sendall(json.dumps(data))
            


if __name__ == "__main__":
    HOST, PORT = "localhost", 8444

    # Create the server, binding to localhost on port 9999
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()

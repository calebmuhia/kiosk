from shop.models import Order, OrderItem
from kioskui.models import BarcodeProduct, NoBarcodeProduct
from datetime import datetime, timedelta, date
from django.db.models import Sum
from decimal import Decimal
from itertools import groupby
import time


def daily():

    today = datetime.strptime("2015-02-27", "%Y-%m-%d")
    lastweek = datetime.strptime("2015-02-1", "%Y-%m-%d")

    data = []
    objects = []

    for order in Order.objects.filter(created__range=[lastweek,today]).filter(status=Order.COMPLETED).order_by('created'):
        order_date = date(order.created.year, order.created.month, order.created.day)
        taxes = order.extraorderpricefield_set.aggregate(value = Sum('value'))['value']
        barcode_unit_cost = order.items.aggregate(barcode_unit_cost=Sum('product__barcodeproduct__unit_cost'))['barcode_unit_cost']
        nobarcode_unit_cost = order.items.aggregate(nobarcode_unit_cost=Sum('product__nobarcodeproduct__unit_cost'))['nobarcode_unit_cost']
        
        if not barcode_unit_cost:
            barcode_unit_cost = 0.00
        if not nobarcode_unit_cost:
            nobarcode_unit_cost= 0.00
        if not taxes:
            taxes = 0.00

        unit_cost = barcode_unit_cost+nobarcode_unit_cost


        

        if not taxes:
            taxes = Decimal('0.00')

        data.append(
            {'date':order_date,
            'order_total':order.order_total,
            'order_subtotal':order.order_subtotal,
            'taxes':float(taxes),
            'unit_cost':unit_cost,
            'profits': float(order.order_total if order.order_total else 0 )-float(unit_cost)
            })



    for k,value in groupby(data, key=lambda x:x["date"]):
        values = list(value)
        

        try:
            ordervalues = list(value)
            total = reduce(lambda x,y:x+y,[item['order_total'] for item in values])
        except:
            total = 0.00    

        
        try:
            
            unit_cost = reduce(lambda x,y:x+y,[item['unit_cost'] for item in values])
        except:
            unit_cost = 0.00


        try: 
             
            taxes = reduce(lambda x,y:float(x)+float(y),[item['taxes'] for item in values])
        except Exception, e:
            taxes = 0.00         

        try:    
            profits = reduce(lambda x,y:x+y,[item['profits'] for item in values])
        except:
            profits = 0.00

         

        print '\n'      

        objects.append({'date':k.strftime('%Y-%m-%d'),'total':total,'taxes':taxes,'unit_cost':unit_cost,'profits':profits})

    print objects    

      

        



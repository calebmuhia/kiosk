from kioskui.models import KioskUser as User, BarcodeProduct
from django.core.urlresolvers import reverse

from selenium_test.test import SeleniumTestCase
from selenium_test.webdriver import CustomWebDriver
from django.conf import settings
import time
import logging
logger = logging.getLogger(__name__)

import subprocess

def sendmessage(message):
    subprocess.Popen(['notify-send', message])
    return

# Make sure your class inherits from your base class
class Auth(SeleniumTestCase):

    def setUp(self):

        # setUp is where you setup call fixture creation scripts
        # and instantiate the WebDriver, which in turns loads up the browser.
        user = User.objects.create_superuser(username='kioskuser',
                                      password='kioskuser',
                                      email='info@lincolnloop.com')
        user.increment_cash_deposits(100)
        user.save()
        product = BarcodeProduct.objects.create(
            name="1/4 Cheeseburgters",
            barcode=68906918067236,
            unit_price = 4,
            unit_cost=2,
            hst=True,
            pst=True,
            active=True)
        product2 = BarcodeProduct.objects.create(
            name="5 hour energy berry",
            barcode=65100,
            unit_price = 4,
            unit_cost=2,
            hst=True,
            pst=True,
            active=True)





        # Instantiating the WebDriver will load your browser
        self.wd = CustomWebDriver()

    def tearDown(self):
        # Don't forget to call quit on your webdriver, so that
        # the browser is closed after the tests are ran
        self.wd.quit()

    # Just like Django tests, any method that is a Selenium test should
    # start with the "test_" prefix.
    def test_transaction(self):
        """
        Django Admin login test
        """
        self.max()
        logger.debug("")
        logger.debug("")
        logger.debug("")



        tests_count = 3
        for i in xrange(tests_count):
            logger.debug("")
            logger.debug("")
            logger.debug("")
        

            logger.debug("<-- STARTING TRANSACTION TEST {0} -->".format(i))
            sendmessage("testing Transaction Completion via account")
            sendmessage("opening browser")
            

            self.open(reverse('kiosk_home'))
            sendmessage("testing going to the home Page")



            self.wd.find_css("#start").click()
            self.wd.find_css("body").send_keys('kioskuser\n')
            sendmessage("testing login in a user")
            


            time.sleep(2)
            user=  self.wd.find_css("#welcome").text
            logger.debug("lOGGED IN USER - {0}".format(user.split('Welcome')[1]))
            balance=  self.wd.find_css("#balance").text

            logger.debug("USER BALANCE- {0}".format(balance.split('Balance :')[1]))
            
            sendmessage("testing barcode product 1")
            logger.debug("SCANNING PRODUCT 1 - 68906918067236 - price 4")

            self.wd.find_css("body").send_keys('68906918067236\n')
            time.sleep(2)
            sendmessage("testing barcode product 2")
            logger.debug("SCANNING PRODUCT 68906918067236")

            logger.debug("SCANNING PRODUCT 65100 - price 4")
            self.wd.find_css("body").send_keys('65100\n')
            time.sleep(2)
            cart_total = self.wd.find_css('#carttotal').text

            logger.debug("CART TOTAL - {0}".format(cart_total))



            time.sleep(2)


            sendmessage("Go to checkout page")

            self.wd.find_css('#id_paynow').click()
            time.sleep(2)

            self.wd.find_css("body").send_keys('kioskuser\n')
            time.sleep(3)
            balance_post=  self.wd.find_css("#balance").text;

            logger.debug("USER BALANCE- {0}".format(balance_post.split('Balance :')[1]))
            




            if "thank you" in self.wd.title.lower():
                sendmessage("Test was successful")
                logger.debug("<-- TEST {0} COMPLETE -->".format(i+1))


            time.sleep(7)    

         













        
        
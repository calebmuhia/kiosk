"""
WSGI config for kiosk project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/howto/deployment/wsgi/
"""

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "kiosk.settings")

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from gevent import monkey
monkey.patch_all()

# if __name__ == '__main__':
#     from socketio.server import SocketIOServer
#     server = SocketIOServer(('', 8081), application, resource="socket.io")
#     server.serve_forever()

from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.conf.urls.static import static
admin.autodiscover()
from api import UserResource
from tastypie.api import Api
v2_api = Api(api_name='v2')
v2_api.register(UserResource())
from socketio import sdjango
sdjango.autodiscover()
from kioskui.views import server_error_500_view
handler500 = server_error_500_view
handler404 = server_error_500_view


urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'kiosk.views.home', name='home'),
    url(r'^admin/', include(admin.site.urls)),

    url(r'', include('kioskui.urls')),
    url(r'^api/', include(v2_api.urls)),
    

    url(r'^dashboard/', include('dashboardui.urls')),
    url(r'^socket\.io', include(sdjango.urls)),
    ## test the 500 view
    # (r'^testing-error-views/500/$', server_error_500_view),
    url(r'^oauth2/', include('provider.oauth2.urls', namespace = 'oauth2')),
    url(r'',include('django_cups.urls')),
    url(r'',include('mobile.api.urls'))


    
)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

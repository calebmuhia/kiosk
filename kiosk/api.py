from tastypie.authentication import Authentication, BasicAuthentication, ApiKeyAuthentication, MultiAuthentication, SessionAuthentication
from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie.exceptions import BadRequest
from tastypie import fields


from django.conf.urls import *
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404
#from haystack.query import SearchQuerySet
from tastypie.resources import ModelResource, Resource
from tastypie.utils import trailing_slash
from tastypie.constants import ALL, ALL_WITH_RELATIONS
from kioskui.models import KioskUser as User
from authenticator import MyBasicAuthentication
from provider.oauth2.models import Client



class UserResource(ModelResource):

    class Meta:
        queryset = User.objects.filter()

        list_allowed_methods = ['post','put']
        detail_allowed_methods = ['post','get', 'put']

        resource_name = "user"

        authentication = MyBasicAuthentication()

        authorization = Authorization()

        excludes = ['password', 'is_staff', 'is_superuser', "date_joined", ]

    








from django.conf import settings
import os

LOGGING = {
    'version': 1,

    # How to format the output
    'formatters': {
        
        'verbose': {
            'format': '%(asctime)s %(levelname)-8s [%(name)s] -- %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },

        'json':{
            'format':'kiosk: { "loggerName":"%(name)s", "asciTime":"%(asctime)s", "lineNo":"%(lineno)d", "time":"%(msecs)d", "levelName":"%(levelname)s", "message":"%(message)s"}',
            

        }
    },

    # Log handlers (where to go)
    'handlers': {
        'logging.handlers.SysLogHandler': {
             'level': 'DEBUG',
             'class': 'logging.handlers.SysLogHandler',
             'facility': 'local7',
             'formatter': 'json',
           },
        
        'console':{
            'level':'ERROR',
            'class':'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'mail_admins': {
            'class': 'django.utils.log.AdminEmailHandler',
            'level': 'ERROR',
             # But the emails are plain text by default - HTML is nicer
            'include_html': True,
        },
        # Log to a text file that can be rotated by logrotate
        'logfile': {
            'class': 'logging.handlers.WatchedFileHandler',
            'level': 'DEBUG',
            'filename': os.path.join(settings.BASE_DIR, "logs/kioskui.log"),
            'formatter': 'json',
        },
        'code_debug': {
            'class': 'logging.handlers.WatchedFileHandler',
            'level': 'DEBUG',
            'filename': os.path.join(settings.BASE_DIR, "logs/debug.log"),
            'formatter': 'verbose',
        },
        'billacceptor_stop_start_debug': {
            'class': 'logging.handlers.WatchedFileHandler',
            'level': 'DEBUG',
            'filename': os.path.join(settings.BASE_DIR, "logs/billacceptor_stop_start_debug.log"),
            'formatter': 'verbose',
        },
        'selenium': {
            'class': 'logging.handlers.WatchedFileHandler',
            'level': 'DEBUG',
            'filename': os.path.join(settings.BASE_DIR, "logs/selenium.log"),
            'formatter': 'verbose',
        },
        'error_file': {
            'class': 'logging.FileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(settings.BASE_DIR,'logs/gunicorn_error.log'),
        },
        'access_file': {
            'class': 'logging.FileHandler',
            'formatter': 'verbose',
            'filename': os.path.join(settings.BASE_DIR,'logs/gunicorn_access.log'),
        },
    },

    # Loggers (where does the log come from)
    'loggers': {
        'root': {
            'handlers': ['mail_admins'],
            'level': 'DEBUG',
        },
        # Again, default Django configuration to email unhandled exceptions
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        # Might as well log any errors anywhere else in Django
        'django': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'kiosk.kioskui': {
            'handlers': ['logfile', 'mail_admins','logging.handlers.SysLogHandler'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propogate': True,
        },
        'kioskui': {
            'handlers': ['logfile', 'mail_admins','logging.handlers.SysLogHandler'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propogate': True,
        },
        'debug': {
            'handlers': ['code_debug'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propogate': True,
        },
        'billacceptor_debug': {
            'handlers': ['billacceptor_stop_start_debug'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propogate': True,
        },
        'dashboadui.middleware': {
            'handlers': ['logfile', 'mail_admins'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propogate': True,
        },
        'kiosk.dashboadui.middleware': {
            'handlers': ['logfile', 'mail_admins'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propogate': True,
        },
        'selenium_test.tests.auth': {
            'handlers': ['selenium', 'mail_admins'],
            'level': 'DEBUG', # Or maybe INFO or DEBUG
            'propogate': True,
        },
        
        'gunicorn.error': {
            'level': 'INFO',
            'handlers': ['error_file'],
            'propagate': True,
        },
        'gunicorn.access': {
            'level': 'INFO',
            'handlers': ['access_file'],
            'propagate': False,
        },
    }
}
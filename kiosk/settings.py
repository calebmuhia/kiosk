
"""
Django settings for kiosk project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))

#custom user
AUTH_USER_MODEL = 'kioskui.KioskUser'

AUTHENTICATION_BACKENDS = ('kioskui.auth.backends.DashboardBackend','django.contrib.auth.backends.ModelBackend')


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS =  ['*']


# Application definition

INSTALLED_APPS = (
    'kioskui',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'shop',
    'south',
    'shop.addressmodel', # The default Address and country models
    'shop_simplecategories',
    'tastypie',
    'dashboardui',
    'django_extensions',
    'provider',
    'provider.oauth2',
    # 'selenium_test',
    'django_cups',
    'corsheaders',

    
)

TEMPLATE_CONTEXT_PROCESSORS = [
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages',
    'kioskui.context_processors.get_cart_items',
    'kioskui.context_processors.checkInternet',
    'dashboardui.context_processors.current_location',
    'dashboardui.context_processors.checkzm',
    'dashboardui.context_processors.company_name',
    



]


MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    #'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'dashboardui.middleware.RedirectForCentralDashMiddleware',
    'dashboardui.middleware.DatabaseRouterMiddleware',
    'dashboardui.middleware.LoginRequiredMiddleware',
    'dashboardui.middleware.DatePickerMiddleware',


)

CORS_ORIGIN_ALLOW_ALL = True

ROOT_URLCONF = 'kiosk.urls'

WSGI_APPLICATION = 'kiosk.wsgi.application'


########### IMPORTANT!! ###########
### DATABASE settings are in local_settings.py
########### IMPORTANT!! ###########

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'



USE_I18N = True

USE_L10N = True

USE_TZ = False


SECRET_KEY = 'b*l(%)87n!-*)2ni%t%4#7cdq8y=jq-t%o4tnrx83$et-jtm=q'


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'
STATIC_ROOT = ''
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, "static"),
    os.path.join(BASE_DIR, "dashboardui/static")
)
TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(BASE_DIR, 'templates'),
    os.path.join(BASE_DIR, 'dashboardui/templates'),

)
ADMINS = (
    #('caleb', 'clbnjoroge@gmail.com'),
)

MANAGERS = ADMINS




# tax modifiers 
SHOP_CART_MODIFIERS = [
       'kioskui.tax_modifiers.HstTaxesOnCartItems',
       'kioskui.tax_modifiers.PstTaxesOnCartItems',
]

SHOP_SHIPPING_BACKENDS = [
]


#payment backend
SHOP_PAYMENT_BACKENDS = [
    'kioskui.payments.pay_from_account.PersonalAccountBackend',
    # 'kioskui.payments.pay_on_payroll.PayRollBackend',
    'kioskui.payments.pay_by_debit_precidia.PrecidiaBackend',
]

SHOP_FORCE_LOGIN = False

SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SESSION_COOKIE_AGE = 21600

TASTYPIE_DEFAULT_FORMATS = ['json']

#selenium settings

SELENIUM_WEBDRIVER = os.path.join(BASE_DIR, 'chromedriver')

# TEST_RUNNER='selenium_test.test_runner.FastTestRunner'





CELERY_TIMEZONE = 'UTC'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'
CELERY_IGNORE_RESULT = True
from datetime import timedelta

# CELERYBEAT_SCHEDULE = {
#     'add-every-10-seconds': {
#         'task': 'kioskui.tasks.internet_on',
#         'schedule': timedelta(seconds=10),
#     },
# }

LOGIN_EXEMPT_URLS = (
    r'^dashboard',
)

LOGIN_ALLOW_URLS = (
    r'^dashboard/login',
    r'^dashboard/get_db_names',
    r'^dashboard/api',
    r'^dashboard/videos'

    
)

try:
    from local_settings import *
except ImportError:
    pass

try:
    from logging import *
except Exception, e:
    print e
    pass



try:
    from zm import settings as zm_settings
    ZmSettings = zm_settings.Settings(globals())
    ZmSettings.set_zm()
except:
    pass    
from django.conf.urls import patterns, include, url


urlpatterns = patterns('django_cups.views',
    url(r'^displayPrintForm/$', 'displayPrintForm', name="displayForm"),
    url(r'^getPrinterslist/$', 'getPrinterslist', name="listPrinters"),
    url(r'^refreshPrinterslist/$', 'refreshPrinterslist'),
    url(r'^favourites/getlist/$', 'getFavouriteslist'),
    url(r'^favourites/add/(?P<printer_id>\d+)$', 'addFavourite'),
    url(r'^favourites/del/(?P<printer_id>\d+)$', 'delFavourite'),
)

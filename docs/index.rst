.. Kioskapp documentation master file, created by
   sphinx-quickstart on Thu Dec 18 08:26:00 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Kioskapp's documentation!
====================================

`Canada Micro Market`_  self service markets are revolutionizing on-premises food services by making healthy convenient food options available in the workplace 24/7. It’s like having a cafeteria with hundreds of items available any time of day. Support your employee’s healthy lifestyle commitment with a micro market in your breakroom.

A typical Canada Micro Market includes these healthy, convenient options:

* Yogurts and milk
* carrots
* Crispy and crunchy snacks
* A wide selection of beverages
* Single cup coffee
* Frozen treats
* General merchandise
* Over the counter medications
* Even office supplies

.. _Canada Micro Market: http://canadamicromarkets.ca/ 


The main documentation for the site is organized into a couple sections:

* :ref:`kiosk-manuals`
* :ref:`dashboard-manuals`


.. _kiosk-manuals:

Kiosk/Frontend Manuals
----------------------
Mainly meant for the User

.. toctree::
    :maxdepth: 2

    pages/frontend/welcomepage
    pages/frontend/cart
    pages/frontend/checkout
    pages/frontend/account
    pages/frontend/missing_keytag_barcode
    pages/frontend/createNewUser
    pages/frontend/emptyCashBox
    pages/frontend/driver_prekitting
    




    



.. _dashboard-manuals:

Dashboard/Backend Manuals
-------------------------
Mainly meant for the Admin

.. toctree::
    :maxdepth: 2

    pages/backend/overview
    pages/backend/dashboard
    pages/backend/transactions
    pages/backend/products
    pages/backend/customers
    pages/backend/inventory
    pages/backend/settings


Database Structure:

.. toctree::
    :maxdepth: 2

    pages/backend/db


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


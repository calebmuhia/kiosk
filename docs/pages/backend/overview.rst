Dashboard Overview
==================

Overview
--------

The dashboard is used by the `Admin` to undertake administrative tasks e.g View sales/transactions, add/edit products, add/edit users etc. The dashboard includes the following functional features:


Features
++++++++

Location Selector
~~~~~~~~~~~~~~~~~

    Mainly used if the dashboard is used for multiple locations. Changing the location will enable the Admin to work on a database for that location.

Date Range Selector
~~~~~~~~~~~~~~~~~~~
    Allows the admin to select the date range which will be used in records query, 

Download PDF/CSV files
~~~~~~~~~~~~~~~~~~~~~~
    Some pages includes a download button for downloading the appropriate records within a set time range.

Upload CSV Files
~~~~~~~~~~~~~~~~

    User when Admin would like to create more than one item (product or user) at a time. Instructions on the csv format is shown on the Upload Modal


Sections
--------

* :doc:`dashboard`
* Transactions
* Products
* Customers
* Inventory
* Settings


    


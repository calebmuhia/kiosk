Transactions
=============

overview
--------
Section Holds three subsections

* :ref:`orders`
* Cash Transactions
* Cash Flow


.. _orders:

Orders
------

Found Under Transactions > Orders

show orders information for the selected Location and time Range.Orders are created when a user decides to :ref:`checkout` items on the :ref:`cart`.

Features
~~~~~~~~

    * `Date Range selector` : query the db for orders within the selected time range;

    * `Pagination` : a subset of the orders is returned for the time range per page, pressing Previous or Next fetch the next or previous queryset.

    * `Search` : capable of searching orders matching name of user, keytag of user, order id within the Date Range

    * `Export List Csv` : Downloads a csv list of orders within the Date Range.

    * `Export Details Csv` : Downloads a csv list of items within orders, Sorted by orders

Fields
~~~~~~

    * `Location`: Location Name
    * `Order id` : Internal order id
    * `Status` : status of the order, Options - ``Completed``, ``Canceled`` or ``Processing`` depending on the  stage of the checkout
    * `Date/Time` : created time
    * `User` : user who checkout the items
    * `Order Total` : total Amount charged to the user, including tax, cog etc
    * `Taxes ` : taxes applied to the order
    * `Payment Method` : method used for completing the Order, default `MY Account` and `Debit Card`


Orders Details
~~~~~~~~~~~~~~

found after clicking on the order id on the Orders page

Shows Details of the order including the items bought and or contents of the debit response

.. _cashtransactions:

Cash Transactions
-----------------

Found Under Dashboard > Transactions > Cash Transactions

Shows a list of cash flow within the system. Captures :

    * `buying events` : when a user buy an item using the account or the debit/credit card
    * `Accounts Updates` : when a user update the account via bills or debit/credit card
    * `Admin Updates` : when admin update users account - account increment or decrement

Features
~~~~~~~~
    * search
    * csv Download
    * datetime selector

Cash Flow
----------

Found Under Dashboard > Transactions > Cash Flow

same as :ref:`cashtransactions` but in filtered manner

Features
~~~~~~~~
    * search
    * csv Download
    * datetime selector








.. _db:

Database Structure
==================

Overview
--------

Structure of databases visualized in an image

.. image:: my_project_visualized.png
    :scale: 50 %
    :alt: alternate text
    :align: right


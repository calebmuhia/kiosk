Home 
=========

overview
---------
Found under Dashboard > Sales .

Features
~~~~~~~~
Changes Location and DateTime range.


The ``Dashboard`` (Home) shows the following.


Sales Graph
~~~~~~~~~~~

A graph of products sold against time, for the time range chosen. the second part of the graph container shows Top Ten Products sold for the time range and Top ten Categories.

Sales Figures
~~~~~~~~~~~~~

Shows the following

* Total Sales : Total order totals including Taxes and COG
* Profits  : total order totals exclusive COG
* Cost of Goods 
* Taxes


Latest Orders
~~~~~~~~~~~~~

Shows 5 latest orders, their status, date/time, user and the order Total.


CashBox
~~~~~~~

The current amount in the bill collector ``CashBox``.


Reports
--------

Cashbox
~~~~~~~

Found under Dashboard > Reports > Cash Box

This Reports shows the current amount on the bill collector ``CashBox``, it also shows the cashbox history 
, including 
* Start Date, 
* End Date (Dates emptied), 
* amounts in the bill Collector, 
* Emptied By 

Products Sold
~~~~~~~~~~~~~~

Found under Dashboard > Reports > Products Sold

Shows the number of items bought per sold product within a time range.

The report can be exported to csv or to pdf

Products Sold
~~~~~~~~~~~~~

Found under Dashboard > Reports > Products Sold

Shows a graph of products sold per day against a day within the time range

Non Moving Stock
~~~~~~~~~~~~~~~~

Found under Dashboard > Reports > Non Moving Stock

Shows products that sold below a certain level within a time range


Missing KeyTags or Barcode
~~~~~~~~~~~~~~~~~~~~~~~~~~~

Found under Dashboard > Reports > Missing Scanned Barcodes

Shows barcodes/keytags that where scanned on the kiosk frontend but are not available within the system








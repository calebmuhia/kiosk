.. _cart:

Cart
====

Overview
---------

The user `Cart` space holds information about :doc:`products` that the user have scanned or selected from the no-barcode :doc:`products` list. The `Cart` will persist until the products on it are checked out or deleted.

Scanning Barcodes
-----------------

Scanning product `barcode` is done by holding the product close to the `barcode-scanner`, barcode first, this will automatically add the product to the user `Cart` Space. 

Remove Product from Cart
------------------------

Done by pressing the `Remove` button for the desired product, this will remove all items scanned for that particular product.

Number of items scanned per Product
-----------------------------------

To buy more than one item per product, the user has to scan the product `barcode` several times to get the desired number of items on the cart, this number will show up on the cart besides the product name.

.. _tax:

Tax
---

Tax may be add to the products `line-total`. The type of tax added may depend on the locality.
Main types of Taxes include

* Goods and services tax/harmonized sales tax (GST/HST)
* Provincial Sales Tax (PST)

The `Admin` should make sure the correct tax applies and the value is set via the admin `dashboard`


Cart Checkout
-------------

`Cart` :doc:`checkout` is done by pressing the `Pay Now` button in the cart page or scanning the user `keytag` on cart page to automatically pay for the cart using user :doc:`account`.



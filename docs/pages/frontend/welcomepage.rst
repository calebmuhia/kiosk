Getting Started
===============

Overview
--------

On the welcome page the user is presented with 5 options, 

* Scan product Barcode to start
* Start Order
* go to My Account
* choose a NOBarcode Items


Scan product Barcode to start
-----------------------------

Scanning the product barcode on the kiosk will add the scanned item to a :doc:`cart` and redirect to it, here the user can add more items.


Start Order
-----------

Present a user with an empty :doc:`cart` where items can be scanned, to add them to the user cart space.


go to MY Account
----------------

Redirects user to a `login-page` there he/she can scan his/her keytag to `login` to the :doc:`account` page.


choose a NoBarcode Items
------------------------

Redirects the user to a list of items that does not have barcodes. These can be oranges, bananas, coffee etc.
The user can add this items to the :doc:`cart` by pressing the product image.









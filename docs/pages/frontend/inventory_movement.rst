Overview
========

The stock Movement Report shows the opening and closing information between dates, information such as purchases made, Scrapped items and order totals. This is useful for tracking stock movements over a set period of time, especially when items are returned.

The Query can be found under; Dashboards -> Query Data -> Stock Movement, select the report select the Start Date, End Date and Location, then click execute.

The Columns

SKU - This is the Item Number of the Product

Item Title - Title of the product

Category - The category for the selected product

OpeningLevel - This is the stock level at the start of the selected date.

OpeningStockValue - Stock Value at the start of the selected date.

Purchaces - This value is the quantity in made in purchases within the selected time period. If a Purchase Order has not been delivered then it will still be displayed in this column

Returns - Quantity of items returned to the selected location, this is based on the date the item was actually returned.

Scrap Item - Quantity of items scrapped. This is based on the date the item was scrapped.

Total - This is the total revenue for the selected time frame for the SKU.

TotalStockCost - The total cost of the products that have sold between the dates

ClosingLevel - The stock level at the end of the selected date

ClosingStockValue - The closing stock value for the stock item.

Q&A
Why is my Stock Value Zero?

The stock value may be zero when the stock value has not been entered when the stock was delivered into the system. When delivering Purchase Orders or Importing Stock, it is a good idea to make sure there is a value for the stock. Also it is good to have the Cost Price for your stock items.

Can I filter by Category?

Yes, to do this there is a filter box underneath the Category Column, start typing the name of the Category and it will start filtering the report.

Can I export this report?

Yes, on the right hand side of the window are two buttons Export to CSV and Export to Excel. The CSV will create a comma delimited file where as the export to excel will create a formatted excel file.
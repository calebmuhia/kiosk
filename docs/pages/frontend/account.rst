My Account
==========

Overview
---------
User can go to his/her account by pressing `My Account` button on the `Welcome` page or `My Account` button that's on top left of all pages. 

The `My Account` present a user with options to update the `account`. User is required to login to have access to this page. On this page the user may:

* Update account balance
* Register Fingerprint
* Update full name and email
* View transaction history


Update Account Balance
-----------------------

This can be done in two ways:

Update account balance via bill collector
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Press `Cash` button in the `My Account` page to activate the bill collector. The bill collector will recognize bills and will automatically update the account with appropriate amount.
The bill collector may sometimes reject some bills if it does not recognize them.

Update account balance via credit/debit card
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

press `Credit/Debit Card` button in the `My Account` page. This will show amount options e.g $5, $10, $20 etc, that will be updated to the account. Once a bill amount is chosen, the kiosk activates the `Pin Pad`. User should follow instructions on the `Pin Pad` after which the account is updated with the amounts chosen and the amounts deducted from the card. If an error occurs, the user should log out and back in to check if the account was updated before attempting again


Registering a Fingerprint
-------------------------

Fingerprints can be used to login into users account as an alternative to the use of a keytag. The user can register the fingerprint by pressing the  `Register Thumb-print` button. The user will be prompted to place the desired finger on the fingerprint reader provided on the kiosk. The system is designed to pick the best fingerprint image, so the user may need to repeat the process a few more time. Same fingerprint should be used. The user can now login into the account by just scanning their fingerprint on the fingerprint reader



View Past Orders History
------------------------

The user can view all order history by pressing the `view Transactions` Button. This will also allow them to see which transactions are `Canceled` or `Completed`.  




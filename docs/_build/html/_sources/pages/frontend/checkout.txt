.. _checkout:

Checkout
========

overview
--------

The `Checkout` page present the user with a number of payment methods:

* :ref:`Pay Via Account`
* :ref:`Pay with Credit/Debit Card`


.. _Pay Via Account:

Pay Via Account
---------------

For this `payment method` to be used, the user must be logged in, scanning the user `keytag` on the checkout will automatically choose payment via the account. User must have enough money on account prior checkout, or the payment will fail. If the user don't have enough money to complete the transaction, he/she can press `My Account` button to add money to the account. visit :doc:`account` Page for more details. 


.. _Pay with Credit/Debit Card:

Pay with Credit/Debit Card
--------------------------

To choose payment via credit/debit card, user should press the `Pay with Credit/Debit Card` button on the `checkout` page. The user should then follow instructions on the `Pin Pad` device provided on the kiosk, enter pin if required and complete the transaction.



Order Payment
-------------

After choosing the payment method, the system will deduct the total amount, which include any :ref:`tax`. The kiosk will then redirect to the `Thank you`, where the user will decide to print the order receipt or logout.

All user orders history is available to the user via the :doc:`account` page.








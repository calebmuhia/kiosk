from kioskui.models import KioskUser as User, BarcodeProduct, NoBarcodeProduct, MissingBarcodeOrKeycard
from mobile.api import *
from mobile.api.authentication import OAuth20Authentication
from datetime import datetime




class BarcodeProductResource(ModelResource):


    class Meta:
        queryset = BarcodeProduct.objects.all()
        list_allowed_methods = ['get' ]
        detail_allowed_methods = ['get' ]

        resource_name = 'product'
        limit=0
        
        authentication = OAuth20Authentication()
        # authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/scan_barcode/(?P<keypress>\w+)%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('scan_barcode'), name="api_scan_barcode"),
              
            ]


    def scan_barcode(self, request, **kwargs):

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        response = {'type':"not a product"}

        keypress  = kwargs['keypress']

        if BarcodeProduct.objects.filter(barcode=keypress).exists():
            try:

                product = BarcodeProduct.objects.get(barcode=keypress)
                logger.info("Adding item '%s' to cart." % product)
                
                response = {
                    "type": "PRODUCT",
                    "name": product.get_name(),
                    "item_id": product.id,
                    "unit_price": float(product.unit_price),
                }##response
            except Exception, e:
                logger.critical("More than 1 item matched this keypress: '%s'" % keypress)
                logger.critical(e)

        if response['type'] == "not a product":
            #send_email_signal.send(sender = None, msg = "Problem detecting when barcode %s was scanned" % keypress )
            MissingBarcodeOrKeycard.objects.create(
                    barcode = keypress,
                    date = datetime.now())
            
            pass

        self.log_throttled_access(request)
        return self.create_response(request, response)      
            


class NoBarcodeProductResource(ModelResource):


    class Meta:
        queryset = NoBarcodeProduct.objects.all()
        list_allowed_methods = ['get' ]
        detail_allowed_methods = ['get' ]

        resource_name = 'noscanproduct'
        limit=0
        
        authentication = OAuth20Authentication()
        # authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True


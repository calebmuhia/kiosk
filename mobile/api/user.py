from kioskui.models import KioskUser as User
from mobile.api import *
from mobile.api.authentication import OAuth20Authentication


class UserResource(ModelResource):

    balance = fields.CharField(attribute='balance', readonly=True)



    class Meta:
        queryset = User.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put','delete' ]

        resource_name = 'user'
        limit=0
        excludes = ["password", "is_staff", 'is_superuser','id', ]
        authentication = OAuth20Authentication()
        # authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

    def authorized_read_list(self, object_list, bundle):
        print '--authorized_read_list--'
        return object_list.filter(id=bundle.request.user.id)    
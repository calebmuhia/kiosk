from mobile.api import *
from shop.models import Cart, CartItem, Product
from mobile.api.authentication import OAuth20Authentication
from shop.util.cart import get_or_create_cart
from django.http import Http404






class CartItemResource(ModelResource):

    class Meta:
        queryset = CartItem.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put','delete' ]

        resource_name = 'cartitem'
        limit=0
        authentication = OAuth20Authentication()
        # authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

    def authorized_read_list(self, object_list, bundle):
        print '--authorized_read_list--'
        return object_list.filter(id=bundle.request.user.id)



class CartResource(ModelResource):

    class Meta:
        queryset = Cart.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put','delete' ]

        resource_name = 'cart'
        limit=0
        authentication = OAuth20Authentication()
        # authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True
    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/get_cartitems%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('get_cartitems'), name="api_get_cartitems"),
            url(r"^(?P<resource_name>%s)/add_item/(?P<product_id>\d+)%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('add_product_to_cart'), name="api_add_item"),
            url(r"^(?P<resource_name>%s)/delete_item/(?P<id>\d+)%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('delete_product_from_cart'), name="api_delete_item"),
              
            ]    

    def authorized_read_list(self, object_list, bundle):
        print '--authorized_read_list--'
        return object_list.filter(id=bundle.request.user.id)

    def get_cartitems(self, request, **kwargs):

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        user = request.user

        

        cart = get_or_create_cart(request)
        cart.update(request)

        

        items = []
        try:
            if cart.total_quantity > 0:
               
                for item in cart.items.all():
                    product = {
                        "cart_item":item.pk,
                        'id':item.product.id,
                        "name":item.product.name,
                        "quantity":str(item.quantity),
                        "price":item.product.unit_price,
                        'total':item.product.unit_price*item.quantity
                         }

                    items.append(product)
        except Exception,e :
                    print e
                    pass
        objects = {'cart_totals': cart.current_total, 'taxes':cart.extra_price_fields}            
        objects.update({'items':items})            

        self.log_throttled_access(request)
        return self.create_response(request, objects)

    def add_product_to_cart(self, request, **kwargs):

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        user = request.user


        product_id = kwargs['product_id']

        
        product = Product.objects.get(pk = product_id)


        quantity = 1

        cart = get_or_create_cart(request, save=True)
        cart_item = cart.add_product(product, quantity)
        
        cart.save()
        cart.update(request)

        
        

        items = []
        try:
            if cart.total_quantity > 0:  
                for item in cart.items.all():
                    product = {
                        "cart_item":item.pk,
                        'id':item.product.id,
                        "name":item.product.name,
                        "quantity":str(item.quantity),
                        "price":item.product.unit_price,
                        'total':item.product.unit_price*item.quantity                    
                         }

                    items.append(product)
        except Exception,e :
                    print e
                    pass

        objects = {'cart_totals': cart.current_total, 'taxes':cart.extra_price_fields}            
        objects.update({'items':items})            
        self.log_throttled_access(request)
        return self.create_response(request, objects)

    def delete_product_from_cart(self, request, **kwargs):

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        user = request.user
        cart = get_or_create_cart(request)
        item_id = kwargs['id']
        print 'item_id', item_id

        

        
        try:
            cart.delete_item(item_id)
            
        except Exception, e:
            pass

        cart.update(request)    

        items = []
        try:
            if cart.total_quantity > 0:
                
                for item in cart.items.all():
                    product = {
                        "cart_item":item.pk,    
                        'id':item.product.id,
                        "name":item.product.name,
                        "quantity":str(item.quantity),
                        "price":item.product.unit_price,
                        'total':item.product.unit_price*item.quantity
                        
                         }

                    items.append(product)
        except Exception,e :
                    print e
                    pass

        objects = {'cart_totals': cart.current_total, 'taxes':cart.extra_price_fields}            
        objects.update({'items':items})                

        self.log_throttled_access(request)
        return self.create_response(request, objects)     

        




                          









from kioskui.models import KioskUser as User
from mobile.api import *
from mobile.api.authentication import OAuth20Authentication
from shop.models import Order, OrderItem, OrderPayment, OrderExtraInfo, Cart
import logging
logger = logging.getLogger(__name__)
from shop.util.cart import get_or_create_cart
from uuid import uuid4
from django.conf import settings 
from decimal import Decimal
from shop.order_signals import completed


class OrderResource(ModelResource):


    class Meta:
        queryset = Order.objects.all().order_by('-created')
        list_allowed_methods = ['get' ]
        detail_allowed_methods = ['get' ]

        resource_name = 'order'
        limit=0
        
        authentication = OAuth20Authentication()
        # authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

        

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/checkout_account%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('checkoutOrderByAccount'), name="api_checkout_account"),
              
            ]

    def dehydrate(self, bundle):

        bundle.data['status'] = bundle.obj.get_status_display()
             
        paymentmethod = bundle.obj.orderpayment_set.filter()[:1]
        if paymentmethod:
            paymentmethod = paymentmethod[0]
            bundle.data['payment_method'] = paymentmethod.payment_method
        else:
            bundle.data['payment_method'] = ''

        taxes = bundle.obj.extraorderpricefield_set.filter()
        

        for tax in taxes:
             bundle.data[tax.label] = tax.value

        return bundle            

    def authorized_read_list(self, object_list, bundle):
        print '--authorized_read_list--'
        return object_list.filter(user=bundle.request.user)


    def checkoutOrderByAccount(self, request, **kwargs):

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)


        response = {}

        """
        Process the transaction
        """
        logger.info("Starting '%s' payment processing... Mobile Account Backend" )

        cart = get_or_create_cart(request)
        cart.update(request)
        the_order = Order.objects.create_from_cart(cart, request)
        

        # add_order_to_request(request, the_order)

        

        order_amount = the_order.order_total

        try:
            check_completed_order.delay(the_order.id)
        except:
            pass    


        #check if we have any generic products in the order

        # check_generic_products(request, order)

        ## Get the user object
        user = request.user
        

        ## generate a transaction number
        transaction_id = uuid4().hex

        ## check the user's account balance
        user_balance = user.balance

        ## if order amount >  user account balance: prompt to add money (via cash, credit etc)
        if order_amount > user_balance:
            logger.info("Insufficient funds. Order amount ('%s') > account balance ('%s')" % (order_amount, user_balance))

            payonpayroll = getattr(settings, 'PAY_ON_PAYROLL', False)

            if not payonpayroll:

                the_order.status = Order.CANCELLED
                the_order.save()

                response = {'status':'error', 'msg':'Insufficient Funds'}



             
#       
        else:   
            ## ... else increment appropriate expense totals
            logger.debug("Incrementing user expense totals...")                                                                                                                 
            user.increment_account_expense(order_amount, the_order.id)
            user.save()                                                                                                                                                                                                                                                                                                                                                                             

            ## mark the amount as payed and save this transaction
            logger.debug("Marking this transaction as paid and save it.")
            
            OrderPayment.objects.create(
                    order=the_order,
                    # How much was paid with this particular transfer
                    amount=Decimal(order_amount),
                    transaction_id=transaction_id,
                    payment_method='MY Account (Via Mobile)')

            order = the_order
            if(order.is_paid()):
                # Set the order status:
                order.status = Order.COMPLETED
                order.save()

                # empty the related cart
                try:
                    cart = Cart.objects.get(pk=order.cart_pk)
                    cart.empty()
                except Cart.DoesNotExist:
                    pass

                completed.send(sender=self, order=order) 

                response = {"status":"success"}

            else:
                response = {"status":"error"}

                order.status = Order.CANCELLED
                order.save()

                
        self.log_throttled_access(request)
        return self.create_response(request, response)        



            
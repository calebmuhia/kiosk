from django.conf.urls import patterns, url,include
from mobile.api.user import UserResource
from mobile.api.cart import CartItemResource,CartResource
from mobile.api.order import OrderResource
from mobile.api.product import BarcodeProductResource, NoBarcodeProductResource

from tastypie.api import Api
v3_api = Api(api_name='v3')

v3_api.register(UserResource())
v3_api.register(CartItemResource())
v3_api.register(CartResource())
v3_api.register(OrderResource())
v3_api.register(BarcodeProductResource())
v3_api.register(NoBarcodeProductResource())








urlpatterns = patterns('',
       url(r'^api/', include(v3_api.urls)),  
)
from mobile.api import *
from kioskui.models import Tax


class TaxResource(ModelResource):

    class Meta:
        queryset = CartItem.objects.all()
        list_allowed_methods = ['get' ]
        detail_allowed_methods = ['get']

        resource_name = 'tax'
        limit=0
        authentication = OAuth20Authentication()
        # authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True



function camelToDash(str) {
  // converts resource urls to format required by api
  return str.replace(/([a-z\d])([A-Z])/g, '$1_$2').toLowerCase();
}

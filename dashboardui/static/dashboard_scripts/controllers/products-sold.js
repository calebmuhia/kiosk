'use strict'

angular.module('dashboardui')

.controller('ProductSoldCtrl', ['$scope', "$rootScope", '$http','$filter','$timeout', function($scope, $rootScope, $http, $filter, $timeout ){
$scope.data = []
    $scope.get_data = function(){

        if(typeof($scope.lastweek)=='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        if(typeof($scope.today)=='object'){
            var today = $filter('date')($scope.today, 'yyyy-MM-dd')
        }
        else{
            var today = $scope.today
        }

        $scope.products_sold_promise = $http.get('/dashboard/api/v1/productmovement/products_sold/?lastweek='+lastweek+'&today='+today)
                .then(function(data){
                    $scope.products_sold = data.data
                    
                }
                    ,function(error){
                        console.log(error)
                    })
        $scope.products_sold_per_day = $http.get('/dashboard/api/v1/productmovement/products_sold_per_day/?lastweek='+lastweek+'&today='+today)
                .then(function(data){
                    $scope.data = data.data
                    console.log($scope.data)
                    
                }
                    ,function(error){
                        console.log(error)
                    })
        $scope.products_sold_per_hour = $http.get('/dashboard/api/v1/productmovement/products_sold_per_hour/?lastweek='+lastweek+'&today='+today)
          .then(function(data){
            $scope.amount_per_hour = []
            $scope.quantity_per_hour = []
              angular.forEach(data.data, function(val, key){
                
                $scope.amount_per_hour.push([key,val.total_amount])
                $scope.quantity_per_hour.push([key,val.total_items])

              })
              
              
          }
              ,function(error){
                  console.log(error)
              })        

    }

    $scope.get_data()

    $rootScope.reload_data = function(){
        $scope.get_data()
    }

    $scope.csv_headersproducts_sold = [
{title:'Location',field:'location'}, 

{title:'ProductName',field:'name'}, 
                      {title:'Upc',field:'barcode'}, 
                      {title:'NumberOfProductsSold',field:'amount'}, 
                      
                      ]

$scope.export_csv_products_sold = function(){

   return $scope.products_sold

}

$scope.download_pdf_products_sold = function(){
  var doc = new jsPDF('p','pt', 'a4', true);
  
doc.setFontType("bolditalic");
doc.setFontSize(9);

var y = 100




doc.text(30, 20, "Products Sold Between "+$filter('date')($scope.lastweek, 'yyyy-MM-dd')+" and"+$filter('date')($scope.today, 'yyyy-MM-dd')  );

  
  doc.cellInitialize();
  doc.cell(10, y, 150, 20, "Location", 0);
  doc.cell(10, y, 150, 20, "Product Name", 0);
  doc.cell(10, y, 80, 20, "Upc", 0);
  doc.cell(10, y, 130, 20, "Number Of Products Sold", 0);
  
  doc.setFontType("normal");
   
  
  angular.forEach($scope.products_sold, function(val, index){


  
  if (((index+1) % 35) == 0 ){

    doc.addPage('a4');
    doc.cellInitialize();
    y=10


  }  
   doc.cell(10, y, 150, 20, val.location, index+1);
  doc.cell(10, y, 150, 20, val.name, index+1);
  doc.cell(10, y, 80, 20, String(val.barcode), index+1);
  doc.cell(10, y, 130, 20, String(val.amount), index+1);
 
    
  })
  
doc.save('Products sold'+'.pdf');




}





}]);
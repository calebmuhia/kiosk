'use strict'

angular.module('dashboardui')

.controller("UserListCtrl", ['$scope','$q','$interval', 'UserService','$timeout', '$modal','NewUser',"$upload",   function($scope, $q,$interval, UserService, $timeout, $modal, NewUser, $upload){
   
    $scope.$scope = $scope;

    $scope.users = []
    $scope.gridOptions = {
        enableSorting: true,
        enableScrollbars:true,
        rowEditWaitInterval:500,
        data : [],

    }
    
     
   $scope.editableInPopup = '<button style="margin:2px" id="editBtn" type="button" class="btn btn-danger btn-xs" ng-click="getExternalScopes().delete_user(row,$event)" >Delete</button> '
   $scope.userDetailLink = '<a href="/dashboard/users/reports/user_details/{{row.entity[col.field]}}" style="cursor:pointer;margin:6px">{{row.entity[col.field]}}</a>'
    
    
  
      $scope.gridOptions.columnDefs= [
        {name:'Location', field:'location', enableCellEdit: false},
        { name:'Keytag', field: 'username',enableCellEdit: false, cellTemplate:$scope.userDetailLink, },
        { name:'First Name', field: 'first_name'},
        { name:'Last Name', field: 'last_name',},
        { name:'Email', field: 'email'},
        { name:'Active', field: 'is_active', type:'boolean'},
        { name:'Cash Box Manager', field: 'is_cashbox_manager', type:'boolean'},

        { name:'Delete',  field:'', cellTemplate:$scope.editableInPopup,  enableCellEdit: false}

      ];
        
        
    
$scope.get_users = function(){
    $scope.users = UserService.user_list(23)
    .then(function(data){
        $scope.gridOptions.data = data.objects
        $scope.meta = data.meta
        

    },
    function(error){

    } )

};
$scope.get_users();

$scope.usersToBeSearched = '';
  $scope.search_user = function(){
    
    if ($scope.usersToBeSearched == ''){
      $scope.msg = "search params are empty"
      return;
    }

    $scope.users=UserService.search_keytag_user($scope.usersToBeSearched)
    .then(function(data){
      $scope.gridOptions.data = data.objects
      $scope.meta = data.meta

    })



  };

$scope.delete_user = function delete_user(row, $event){
   
    var index = $scope.gridOptions.data.indexOf(row.entity)


        $scope.users=UserService.delete_user(row)
        .then(function(data){
          $scope.gridOptions.data.splice(index,1);



        })
    };



      $scope.saveRow = function( rowEntity ) {
        

        
    var promise = UserService.save_user(rowEntity)
    $scope.users = promise
    $scope.gridApi.rowEdit.setSavePromise( $scope.gridApi.grid, rowEntity, promise );
   

  }; 
 
  $scope.gridOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
  };

  $scope.get_next = function(){
        var next_url = $scope.meta.next;
        if (next_url){
            ProductService.next_previous(next_url)
        .then(function(data){
            $scope.gridOptions.data = data.objects
            $scope.meta = data.meta
            
        },
        function(error){

        });
        }
        

    };
    $scope.get_previous = function(){
        var previous_url = $scope.meta.previous;
        if (previous_url){
            UserService.next_previous(previous_url)
        .then(function(data){
            $scope.gridOptions.data = data.objects
            $scope.meta = data.meta
           
        },
        function(error){

        });
        }
        

    };




$scope.csv_headers = [
                      {title:'Location',field:'location'},
                      {title:'FirstName',field:'first_name'}, 
                      {title:'LastName',field:'last_name'},
                      {title:'Email',field:'email'},

                      {title:'Keytag',field:'username'}, 
                      {title:'Active',field:'is_active'}, 
                      ]

// $scope.export_csv = UserService.user_list(0)
//               .then(function(data){
             
//                   return data.objects    
//               });

$scope.export_csv = function(){

      return UserService.user_list(0)
              .then(function(data){
             
                  return data.objects    
              });
      

}



$scope.createNewUser = function (size) {
    $scope.newUser= NewUser;
    

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: 'CreateNewUser',
      size: size,
      resolve: {
        new_user: function () {
          return $scope.newUser;
        }
      }
    });

    modalInstance.result.then(function (userdata) {
      $scope.newUserData = userdata;
      console.log(userdata)
      $scope.users = UserService.save_new_user($scope.newUserData)
      .then(function(data){
        var data_list = []
        data_list.push(data)
        $scope.gridOptions.data = data_list
      
      })
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };




  $scope.uploadNewUsers = function (size) {
    
    

    var modalInstance = $modal.open({
      templateUrl: 'uploadFileContent.html',
      controller: 'UploadFile',
      size: size,
      resolve: {
        
      }
    });

    modalInstance.result.then(function (file) {

     var fileReader = new FileReader();
    fileReader.readAsArrayBuffer(file);
    fileReader.onload = function(e) {

      
        $scope.users = $upload.http({
            url: '/dashboard/users/upload_users_csv/',
            headers: {'Content-Type': file.type},
            data: e.target.result
        }).progress(function(ev) {
            console.log(ev)
        }).success(function(data) {
              $scope.get_users();
            
        }).error(function(data) {
            
        });
    }

    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };





}])

.controller('CreateNewUser', function ($scope, $modalInstance, new_user) {
  $scope.categories = [];

  $scope.new_user = new_user;
  
  

  $scope.ok = function () {
   
    $modalInstance.close($scope.new_user);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
})

.controller('UploadFile', function ($scope, $modalInstance ) {

 $scope.file = null; 
 
 $scope.onFileSelect = function($files) {
    //$files: an array of files selected, each file has name, size, and type.
    
      $scope.file = $files[0];
      
  };
  

  $scope.ok = function () {
   
    $modalInstance.close($scope.file);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
'use strict'

angular.module('dashboardui')

.controller("StockMovement", ['$scope','Crud','$q','$interval','$modal','$filter', '$rootScope','uiGridConstants', function($scope,Crud,$q,$interval, $modal, $filter, $rootScope, uiGridConstants ){
    $scope.$scope = $scope
    $scope.products = []
    $scope.gridOptions = {
        enableSorting: true,
        enableScrollbars:true,
        enableEdit:false,
        data : [],
        enableFiltering:true,

    }

    $scope.gridOptions.columnDefs= [
          { name:'Location', field: 'location', width:80,enableCellEdit: false, enableFiltering:false},
          { name:'Product Name', 
          field: 'product_name',
           width:130,
           enableCellEdit: false,
           filter: {
          condition: function(searchTerm, cellValue) {
            var lower_value = cellValue.toLowerCase()
            var lower_search = searchTerm.toLowerCase()
            return lower_value.indexOf(lower_search) >= 0;
          }}},
          { name:'Barcode', field: 'product_barcode', width:120 ,enableCellEdit: false,
          filter: {
          condition: function(searchTerm, cellValue) {
            var lower_value = cellValue.toLowerCase()
            var lower_search = searchTerm.toLowerCase()
            return lower_value.indexOf(lower_search) >= 0;
          }}},
          { name:'Opening Stock', field: 'pre', enableCellEdit: false,filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        },
        {
          condition: uiGridConstants.filter.LESS_THAN,
          placeholder: 'less than'
        }
      ]},
          { name:'Sold Qty', field: 'sold',enableCellEdit: false,filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        },
        {
          condition: uiGridConstants.filter.LESS_THAN,
          placeholder: 'less than'
        }
      ]},
          { name:'Restocked Qty', field: 'restocked',enableCellEdit: false,filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        },
        {
          condition: uiGridConstants.filter.LESS_THAN,
          placeholder: 'less than'
        }
      ]},
          { name:'Stale Qty', field: 'stale',enableCellEdit: false,filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        },
        {
          condition: uiGridConstants.filter.LESS_THAN,
          placeholder: 'less than'
        }
      ]},
          { name:'Returned Qty', field: 'returns',enableCellEdit: false,filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        },
        {
          condition: uiGridConstants.filter.LESS_THAN,
          placeholder: 'less than'
        }
      ]},
          { name:'Revenue ($)', field: 'revenue',enableCellEdit: false,filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        },
        {
          condition: uiGridConstants.filter.LESS_THAN,
          placeholder: 'less than'
        }
      ]},
          { name:'COG ($)', field: 'cog',enableCellEdit: false,filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        },
        {
          condition: uiGridConstants.filter.LESS_THAN,
          placeholder: 'less than'
        }
      ]},
          { name:'Closing Stock', field: 'post',enableCellEdit: false,filters: [
        {
          condition: uiGridConstants.filter.GREATER_THAN,
          placeholder: 'greater than'
        },
        {
          condition: uiGridConstants.filter.LESS_THAN,
          placeholder: 'less than'
        }
      ]},

          
        ];

    $scope.get_data = function(){
      console.log('getting data')
        
         if(typeof($scope.lastweek)==='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        if(typeof($scope.today)==='object'){
            var today = $filter('date')($scope.today, 'yyyy-MM-dd')
        }
        else{
            var today = $scope.today
        }


        var url = 'productmovement/inventory_quantity_movement/?lastweek='+lastweek+'&today='+today

        $scope.stockmovement = Crud.list(url, undefined)
        .then(function(data){
            $scope.gridOptions.data = data
            $scope.meta = data.meta
            

        },
        function(error){

        } )

    };
    $scope.get_data();

    $rootScope.reload_data = function(){
        $scope.get_data()
    }

    







   
}])


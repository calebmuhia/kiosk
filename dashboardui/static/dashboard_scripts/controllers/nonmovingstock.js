'use strict'

angular.module('dashboardui')

.controller('NonMovingStockCtrl', ['$scope', "$rootScope", '$http','$filter', function($scope, $rootScope, $http, $filter ){
$scope.data = []

$scope.from = 0
$scope.to = 100


$scope.greaterThan = function(prop, val){
    console.log(prop, val)
    return function(item){
      if (item[prop] > val || item[prop] == val) return true;
    }
}


$scope.lessThan = function(prop, val){
    return function(item){
      if (item[prop] < val || item[prop] == val ) return true;
    }
}
    $scope.get_data = function(){

        if(typeof($scope.lastweek)=='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        if(typeof($scope.today)=='object'){
            var today = $filter('date')($scope.today, 'yyyy-MM-dd')
        }
        else{
            var today = $scope.today
        }

        $http.get('/dashboard/api/v1/productmovement/products_sold/?lastweek='+lastweek+'&today='+today)
                .then(function(data){
                    $scope.products_sold = data.data
                    
                }
                    ,function(error){
                        console.log(error)
                    })

    }

    $scope.get_data()

    $rootScope.reload_data = function(){
        $scope.get_data()
    }


}]);
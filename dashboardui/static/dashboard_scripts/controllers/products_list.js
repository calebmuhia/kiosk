'use strict'

angular.module('dashboardui')

.controller("ProductsListCtrl", ['$scope','ProductService', 'CategoryService', '$q','$interval','$modal','NewBarcodeProduct', 'SupplierService','toaster', '$upload', '$http' , function($scope,ProductService,CategoryService, $q,$interval, $modal,NewBarcodeProduct,SupplierService, toaster, $upload, $http ){
    $scope.$scope = $scope

    $scope.delete_products = function(size){
      console.log("deleting all products")
      var r = confirm("Are you sure you want to delete all products");
      if (r == true){
        $scope.promise = $http.get('/dashboard/products/delete_products')
        .then(function(){
          $scope.get_products()
        })
      }
      

    }

    $scope.popups = function(code,title,msg){
           toaster.pop(code, title, msg);
       };

    $scope.initLocationsProducts = function(location){
      $scope.location = location
      console.log(location)
    }
    $scope.products = []
    $scope.gridOptions = {
        enableSorting: true,
        enableScrollbars:true,
        rowEditWaitInterval:500,
        data : [],

    }
    $scope.productsStatus = [
                          {'status':'All', 'id':''},
                          {'status':'Active', 'id':'True'},
                          {'status':'InActive', 'id':'False'},

     ]
     $scope.status = $scope.productsStatus[0]
     $scope.editableInPopup = '<button style="margin:2px" id="editBtn" type="button" class="btn btn-danger btn-xs" ng-click="getExternalScopes().deleteProduct(row,$event)" >Delete</button> '
     $scope.supplierDropdown = '<select ng-input="COL_FIELD" ng-class="\'colt\' + col.uid" ui-grid-edit-dropdown ng-model="MODEL_COL_FIELD" data-ng-options="field[\'id\'] as field[\'name\'] for field in getExternalScopes().suppliers" ng-blur="getExternalScopes().saveRow(row.entity,$event)">'
     $scope.categoryDropdown = '<select ng-input="COL_FIELD" ng-class="\'colt\' + col.uid" ui-grid-edit-dropdown ng-model="MODEL_COL_FIELD" data-ng-options="field[\'id\'] as field[\'name\'] for field in getExternalScopes().categories" ng-blur="getExternalScopes().saveRow(row.entity,$event)">'
    
    SupplierService.Supplier_list()
    .then(function(data){
      $scope.suppliers = data.objects
    })
    CategoryService.category_list()
    .then(function(data){
        $scope.categories = data.objects
        
        $scope.categories.push({id:'all',name:'All'})
        $scope.category = $scope.categories[$scope.categories.length-1]
        
    });

    $scope.gridOptions.columnDefs= [
    { name:'Location', field: 'location', width:130},
          { name:'Name', field: 'name', width:130},
          { name:'Barcode', field: 'barcode', width:120 },
          { name:'Unit Price', field: 'unit_price', type:'float'},
          { name:'Unit Cost', field: 'unit_cost',  type:'number' },
          { name:'Quantity', field: 'quantity',  type:'number', enableCellEdit: false},
          { name:'Min Stock', field: 'min_stock',  type:'number'},
          { name:'Max Stock', field: 'max_stock',  type:'number'},
          { name:'Category', field: 'category',   editType: 'dropdown', enableCellEdit: true,editableCellTemplate: $scope.categoryDropdown, enableCellEditOnFocus: true, cellFilter: 'mapCategories',},
          { name:'Supplier', field: 'supplier', editType: 'dropdown', enableCellEdit: true,editableCellTemplate: $scope.supplierDropdown, enableCellEditOnFocus: true, cellFilter: 'mapSuppliers',},
          { name:'Hst', field: 'hst', type: 'boolean', width:50},
          { name:'Pst', field: 'pst', type: 'boolean', width:50},
          { name:'Active', field:'active', type: 'boolean', width:50},
          { name:'Delete',  field:'', cellTemplate:$scope.editableInPopup,  enableCellEdit: false}

        ];

$scope.get_products = function(){
    $scope.promise = ProductService.products_list(23)
    .then(function(data){
        $scope.gridOptions.data = data.objects
        $scope.meta = data.meta
        

    },
    function(error){

    } )

};
$scope.get_products();

$scope.productsToBeSearched = '';
  $scope.search_product = function(){
    console.log("searching for ", $scope.productsToBeSearched);
    if ($scope.productsToBeSearched == ''){
      $scope.msg = "search params are empty"
      return;
    }

    $scope.promise = ProductService.search_barcode_product($scope.productsToBeSearched)
    .then(function(data){
      if (data.objects.length > 0){
        $scope.gridOptions.data = data.objects
      $scope.meta = data.meta
      }
      else{
        console.log("not found")
        $scope.popups('error', "Error", "Product "+$scope.productsToBeSearched+' Not Found')
      }

      



    })



  };

$scope.deleteProduct = function deleteProduct(row, $event){
   
    var index = $scope.gridOptions.data.indexOf(row.entity)


        $scope.promise = ProductService.deleteProduct(row)
        .then(function(data){
          $scope.gridOptions.data.splice(index,1);
          $scope.popups("success", "Success", "Success Deleting Product")



        })
    };



      $scope.saveRow = function( rowEntity ) {
        

        console.log("saving row", rowEntity)
        

        
    $scope.promise = ProductService.save_product(rowEntity)
    .then(function(data){
      $scope.popups("success", "Success", "Sucess Editing "+rowEntity.name)
    })
    $scope.gridApi.rowEdit.setSavePromise( $scope.gridApi.grid, rowEntity, $scope.promise );
   

  }; 
 
  $scope.gridOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
  };

  //next and previous pages functions

  $scope.get_next = function(){
        var next_url = $scope.meta.next;
        if (next_url){
            $scope.promise = ProductService.next_previous(next_url)
        .then(function(data){
            $scope.gridOptions.data = data.objects
            $scope.meta = data.meta
            
        },
        function(error){

        });
        }
        

    };
    $scope.get_previous = function(){
        var previous_url = $scope.meta.previous;
        if (previous_url){
           $scope.promise= ProductService.next_previous(previous_url)
        .then(function(data){
            $scope.gridOptions.data = data.objects
            $scope.meta = data.meta
           
        },
        function(error){

        });
        }
        

    };


// fetch items for saving into database

$scope.csv_headers = [
{title:'Location',field:'location'}, 

{title:'Description',field:'name'}, 
                      {title:'Upc',field:'barcode'}, 
                      {title:'UnitPrice',field:'unit_price'}, 
                      {title:'UnitCost',field:'unit_cost'}, 
                      {title:'Quantity',field:'quantity'}, 
                      {title:'Category',field:'category.name'}, 
                      {title:'Supplier',field:'supplier'}, 
                      {title:'MinStock',field:'min_stock'}, 
                      {title:'MaxStock',field:'max_stock'},
                      {title:'Hst',field:'hst'},
                       {title:'Pst',field:'pst'}, 
                       {title:'Active',field:'active'}]

$scope.export_csv = function(){
  return ProductService.products_list(0)
              .then(function(data){
                console.log("returning data length", data.objects.length)

                  return data.objects    
              });

}




  $scope.createNewProduct = function (size) {
    $scope.newProduct = NewBarcodeProduct;
   $scope.newProduct.location = $scope.location

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: 'CreateNewProduct',
      size: size,
      resolve: {
        new_product: function () {

          return $scope.newProduct;
        }
      }
    });

    modalInstance.result.then(function (productdata) {
      $scope.newProductData = productdata;
      
      $scope.promise = ProductService.save_new_product($scope.newProductData)
      .then(function(data){
        if($scope.newProductData.locations.indexOf($scope.location) != -1){
        var data_list = []
        data_list.push(data)
        $scope.gridOptions.data = data_list
        $scope.popups("success", "Success", "Success Adding product "+productdata.name+" To Locations "+productdata.locations.join(' '))

      }
      
      })
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };


// opens a new modal for products csv upload
  $scope.uploadNewProducts = function (size) {
    
    

    var modalInstance = $modal.open({
      templateUrl: 'uploadFileContent.html',
      controller: 'UploadFile',
      size: 'lg',
      resolve: {
        
      }
    });

    modalInstance.result.then(function (object) {
    $scope.promise = $upload.http({
            url: '/dashboard/products/upload_products/',
            headers: {'Content-Type': 'text/json'},
            data: object
            
        }).progress(function(ev) {
            console.log(ev)
        }).success(function(data) {

        $scope.popups("success", "Success", "Csv File Submitted To Server For Processing ")
        $scope.get_products()

              
            
        }).error(function(data) {
            
        });
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };







   
}])

.controller('CreateNewProduct', function ($scope, $modalInstance, new_product, CategoryService, $http) {
  $scope.categories = [];
  $scope.selected = [];
$scope.locations = []
$scope.selected.push(new_product.location)


  $scope.new_product = new_product;
  CategoryService.category_list()
  .then(function(data){
    $scope.categories = data.objects
    $scope.new_product.category = $scope.categories[0]
  })
  $http.get('/dashboard/get_db_names/')
    .then(function(data){
        $scope.locations = data.data
        console.log(data)

    })



    var updateSelected = function (action, location) {
        if (action == 'add' & $scope.selected.indexOf(location) == -1) $scope.selected.push(location);
        if (action == 'remove' && $scope.selected.indexOf(location) != -1) $scope.selected.splice($scope.selected.indexOf(location), 1);
        if($scope.selected.length == 0){
          $scope.selectedError = true
        } 
        else{
          $scope.selectedError = false

        }  
    }

    $scope.updateSelection = function ($event, location) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        updateSelected(action, location);

    };

    $scope.selectAll = function ($event) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        for (var i = 0; i < $scope.locations.length; i++) {
            var location = $scope.locations[i];
            updateSelected(action, location);
        }
    };

    $scope.getSelectedClass = function (location) {
        return $scope.isSelected(location) ? 'selected' : '';
    };

    $scope.isSelected = function (location) {
        return $scope.selected.indexOf(location) >= 0;
    };

    //something extra I couldn't resist adding :)
    $scope.isSelectedAll = function () {
        return ($scope.selected.length === $scope.locations.length)
    };



  

  $scope.ok = function () {
    $scope.new_product.locations = $scope.selected
    console.log($scope.new_product)
   
    $modalInstance.close($scope.new_product);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
})

.filter('mapCategories', function( $cacheFactory ) {
      return function(input) {
        var cache = $cacheFactory.get("categories")
        

        if (cache.get(input) != undefined) {
          
          return cache.get(input);
        } else {
          return 'None';
        }
      };
    })

.filter('mapSuppliers', function( $cacheFactory ) {
      return function(input) {
        var cache = $cacheFactory.get("suppliers")
        

        if (cache.get(input) != undefined) {
          
          return cache.get(input);
        } else {
          return 'None';
        }
      };
    })

.controller('UploadFile', function ($scope, $modalInstance, $http ) {

 $scope.file = null; 

  $scope.selected = [];
$scope.locations = []



  
  $http.get('/dashboard/get_db_names/')
    .then(function(data){
        $scope.locations = data.data
        console.log(data)


        $scope.selected = angular.copy(data.data)

    })



    var updateSelected = function (action, location) {
        if (action == 'add' & $scope.selected.indexOf(location) == -1) $scope.selected.push(location);
        if (action == 'remove' && $scope.selected.indexOf(location) != -1) $scope.selected.splice($scope.selected.indexOf(location), 1);
        if($scope.selected.length == 0){
          $scope.selectedError = true
        } 
        else{
          $scope.selectedError = false

        }  
    }

    $scope.updateSelection = function ($event, location) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        updateSelected(action, location);

    };

    $scope.selectAll = function ($event) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        for (var i = 0; i < $scope.locations.length; i++) {
            var location = $scope.locations[i];
            updateSelected(action, location);
        }
    };

    $scope.getSelectedClass = function (location) {
        return $scope.isSelected(location) ? 'selected' : '';
    };

    $scope.isSelected = function (location) {
        return $scope.selected.indexOf(location) >= 0;
    };

    //something extra I couldn't resist adding :)
    $scope.isSelectedAll = function () {
        return ($scope.selected.length === $scope.locations.length)
    };





 
 $scope.onFileSelect = function($files) {
    //$files: an array of files selected, each file has name, size, and type.
    
      $scope.file = $files[0];
      console.log($scope.selected)
      
  };
  

  $scope.ok = function () {

    var fileReader = new FileReader();
    fileReader.readAsBinaryString($scope.file);
    fileReader.onload = function(e) {
    var object = {
      data:e.target.result,
      locations:$scope.selected

    }
    console.log(object)

    $modalInstance.close(object);
  };
}

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
'use strict'

angular.module('dashboardui')

.controller('CashFlowCtrl', ['$scope', '$http','CashFlowService',"$filter","$rootScope", function($scope, $http, CashFlowService, $filter, $rootScope){
$scope.cash = true;
console.log()

    $scope.limitInit = function(limit){
        $scope.limit = limit;
        $scope.get_data()
    }
    $scope.get_data = function(){
        if(typeof($scope.lastweek)==='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        if(typeof($scope.today)==='object'){
            var today = $filter('date')($scope.today, 'yyyy-MM-dd')
        }
        else{
            var today = $scope.today
        }

        

        $scope.promise = CashFlowService.cashflowlist($scope.limit,lastweek, today)
        .then(function(data){
            $scope.cashflow_data = data.objects
            $scope.meta= data.meta
            $scope.sum_amount(data.objects)
           
        },
        function(error){

        });

    };

    $scope.sum_amount = function(cashflow_objects){
        $scope.sum_admin  = 0
        $scope.sum_accountDeposit = 0
        $scope.sum_creditDeposit = 0
        $scope.sum_accountPurcase = 0
        $scope.sum_creditPurcase = 0
        $scope.sum_adminDeducts = 0

        angular.forEach(cashflow_objects, function(value){
            if (value['status']==20){
                $scope.sum_accountDeposit+=parseFloat(value['amount'])
            }
            else if (value['status']==30){
                $scope.sum_creditDeposit+=parseFloat(value['amount'])
            }
            else if (value['status']==40){
                $scope.sum_admin+=parseFloat(value['amount'])
            }
            else if (value['status']==50){
                $scope.sum_accountPurcase+=parseFloat(value['amount'])
            }
            else if (value['status']==60){
                $scope.sum_creditPurcase+=parseFloat(value['amount'])
            }
            else if (value['status']==70){
                $scope.sum_adminDeducts+=parseFloat(value['amount'])
            }
        });
    }

    

    $rootScope.reload_data = function(){
        $scope.get_data()
    }

    $scope.get_next = function(){
        var next_url = $scope.meta.next;
        if (next_url){
            $scope.promise =CashFlowService.next_previous(next_url)
        .then(function(data){
            $scope.cashflow_data = data.objects
            $scope.meta = data.meta
            
        },
        function(error){

        });
        }
        

    }
    $scope.get_previous = function(){
        var previous_url = $scope.order_meta.previous;
        if (previous_url){
            $scope.promise =CashFlowService.next_previous(previous_url)
        .then(function(data){
            $scope.cashflow_data = data.objects
            $scope.meta = data.meta
           
        },
        function(error){

        });
        }
        

    }

    $scope.get_cash = function() {
$scope.cash = true;
$scope.admin = false;
$scope.admindeduct = false;
$scope.credittoaccount = false;
$scope.credittopurchase = false;
$scope.accounttopurchase = false;

    }
    $scope.get_admin = function() {
$scope.cash = false;
$scope.admin = true;
$scope.admindeduct = false;
$scope.credittoaccount = false;
$scope.credittopurchase = false;
$scope.accounttopurchase = false;

    }

    $scope.get_adminDeducts = function() {
$scope.cash = false;
$scope.admin = false;
$scope.admindeduct = true;
$scope.credittoaccount = false;
$scope.credittopurchase = false;
$scope.accounttopurchase = false;

    }
    $scope.get_credittoacccount = function() {
$scope.cash = false;
$scope.admin = false;
$scope.admindeduct = false;
$scope.credittoaccount = true;
$scope.credittopurchase = false;
$scope.accounttopurchase = false;

    }
    $scope.get_credittopurchase = function() {
$scope.cash = false;
$scope.admin = false;
$scope.admindeduct = false;

$scope.credittoaccount = false;
$scope.credittopurchase = true;
$scope.accounttopurchase = false;

    }
    $scope.get_accounttopurchase = function() {
$scope.cash = false;
$scope.admin = false;
$scope.admindeduct = false;

$scope.credittoaccount = false;
$scope.credittopurchase = false;
$scope.accounttopurchase = true;

    }




    


    



}])
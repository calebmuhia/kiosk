'use strict'

angular.module('dashboardui')

.controller('LiveStreamCtrl', ['$scope', function($scope){
    $scope.selected = []
    $scope.get_monitors_id = function(monitors){
        $scope.monitors = monitors
        console.log(monitors)
        $scope.selected.push($scope.monitors[0])
    }


    

    var updateSelected = function (action, category) {
        if (action == 'add' & $scope.selected.indexOf(category) == -1) $scope.selected.push(category);
        if (action == 'remove' && $scope.selected.indexOf(category) != -1) $scope.selected.splice($scope.selected.indexOf(category), 1);
    }

    $scope.updateSelection = function ($event, category) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        updateSelected(action, category);
    };

    $scope.selectAll = function ($event) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        for (var i = 0; i < $scope.new_restock_report.categories.length; i++) {
            var category = $scope.new_restock_report.categories[i];
            updateSelected(action, category);
        }
    };

    $scope.getSelectedClass = function (category) {
        return $scope.isSelected(category) ? 'selected' : '';
    };

    $scope.isSelected = function (category) {
        return $scope.selected.indexOf(category) >= 0;
    };

    //something extra I couldn't resist adding :)
    $scope.isSelectedAll = function () {
        return ($scope.selected.length === $scope.new_restock_report.categories.length)
    };


    
}])
'use strict'

angular.module('dashboardui')

.controller('TaxesCtr', ['$scope', '$http', 'Crud', function($scope, $http, Crud){

    $scope.promise=$http.get('/dashboard/api/v1/taxes/?name=HST')
    .then(function(data){
        console.log(data)
        $scope.hst = data.data.objects[0]
        if (!$scope.hst){
            $scope.hst = {
                name:'HST',
                value:0
            }
        }
        $scope.hst.value = parseInt($scope.hst.value)
    })
    
    $scope.promise=$http.get('/dashboard/api/v1/taxes/?name=PST')
    .then(function(data){
        console.log(data)
        $scope.pst = data.data.objects[0]
        if (!$scope.pst){
            $scope.pst = {
                name:'PST',
                value:0
            }
        }
        $scope.pst.value = parseInt($scope.pst.value)
            
    })

    $scope.save = function(){

        if ($scope.hst.id != undefined){
            $scope.promise = Crud.save_object($scope.hst)
            .then(function(data){

            })
        }
        else{
            $scope.promise = Crud.save_new_object($scope.hst, 'taxes')
            .then(function(data){

            })

        }

        if ($scope.pst.id != undefined){
            $scope.promise = Crud.save_object($scope.pst)
            .then(function(data){

            })
        }
        else{
            $scope.promise = Crud.save_new_object($scope.pst, 'taxes')
            .then(function(data){

            })

        }


    }


}]);
'use strict'

angular.module('dashboardui')

.controller("ViewInventoryRestockCtrl", ['$scope','Crud', '$q','$interval','$modal','$http', '$filter',  function($scope,Crud, $q,$interval, $modal, $http, $filter){

$scope.$scope = $scope
$scope.init = function(report_id){
  $scope.report_id = report_id
  console.log( report_id)
  $scope.get_data()
}
$scope.gridOptions = {
        enableSorting: true,
        enableScrollbars:true,
        rowEditWaitInterval:500,
        data : [],

    }

$scope.gridOptions.columnDefs= [
      {name:'Location', field:'location', enableCellEdit: false},
      { name:'Product Name', field: 'product_name', width:130, enableCellEdit: false},
      { name:'Barcode/Upc', field: 'upc', width:120, enableCellEdit: false },
      { name:'Calculated Restock Qty', field: 'proposed_restock_quantity'},
      { name:'Category', field: 'categories', enableCellEdit: false},
      { name:'Current Qty', field: '', enableCellEdit: false},
      { name:'Sold In Last Week', field: '', enableCellEdit: false},
      

    ]


$scope.prekittingCompleted = function(){
  $scope.restock_report.prekitting_completed=true
  Crud.save_object($scope.restock_report)
}

$scope.save_products = function(){
  
  $scope.save_products_promise=$http.get('/dashboard/inventory/reports/save_products/'+$scope.restock_report.id+'/')
  .then(function(response){
    console.log(response)
    Crud.list("restock_reports/"+$scope.report_id, undefined)
      .then(function(data){
        $scope.restock_report = data
        $scope.update_again=false

      });
  })
}

$scope.saveRow = function( rowEntity ) {
    
    var promise = Crud.save_object(rowEntity)
    $scope.gridApi.rowEdit.setSavePromise( $scope.gridApi.grid, rowEntity, promise );

}; 



$scope.gridOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
};


$scope.get_data = function(){
  Crud.list("restock_reports/"+$scope.report_id, undefined)
  .then(function(data){
    $scope.restock_report = data

  })
  Crud.list("restock_reports_items/?restock_report="+$scope.report_id, undefined)
  .then(function(data){
    $scope.restock_items = data.objects
    $scope.gridOptions.data = data.objects

  })

}

$scope.print_pdf = function(){
  var doc = new jsPDF('p','pt', 'a4', true);
  
doc.setFontType("bolditalic");
doc.setFontSize(9);
var png = $scope.get_barcode()
var y = 100
var modulus = 35



doc.text(30, 20, $filter('uppercase')($scope.restock_report.location)+' - '+$scope.restock_report.name);
doc.addImage(png,'PNG',300,25,200,50)
  
  doc.cellInitialize();
  doc.cell(10, y, 150, 20, "Product Name", 0);
  doc.cell(10, y, 80, 20, "Upc", 0);
  doc.cell(10, y, 60, 20, "Restock Qty", 0);
  doc.cell(10, y, 70, 20, "Category", 0);
  doc.cell(10, y, 60, 20, "Current Qty", 0);
  doc.cell(10, y, 60, 20, "Sold L.Week", 0);
  doc.setFontType("normal");
   
  
  angular.forEach($scope.restock_items, function(val, index){


  
  if (((index+1) % modulus) == 0 ){

    doc.addPage('a4');
    doc.cellInitialize();
    y=10


  }  
   
  doc.cell(10, y, 150, 20, val.product_name, index+1);
  doc.cell(10, y, 80, 20, val.upc, index+1);
  doc.cell(10, y, 60, 20, String(val.proposed_restock_quantity), index+1);
  doc.cell(10, y, 70, 20, val.categories, index+1);
  doc.cell(10,y, 60, 20, "null", index+1);
  doc.cell(10, y, 60, 20, 'null', index+1);
    
  })
  
doc.save($scope.restock_report.name+'.pdf');




}

$scope.get_barcode = function(){
  var bcid='code39';
  var len = 6 
  var value = String($scope.restock_report.id) 
  len = len-value.length
  var text = '4LF'+new Array(len + 1).join( 0 )+value;
  var rot = 'N';
  var scale=2
  var bw = new BWIPJS
  var opts = {}
  opts['parsefnc'] = bw.value(true);
  opts['alttext'] = bw.value(text)

  if (opts.alttext)
    opts.includetext = bw.value(true);

  // Render the bar code
  bw.bitmap(new Bitmap);
  bw.scale(scale, scale);
  bw.push(text);
  bw.push(opts);
  bw.call(bcid);
  var png = bw.bitmap().getPNG(rot);

  return png

}

$scope.csv_headers = [{title:'Product Name',field:'product_name'}, 
                      {title:'Barcode/Upc',field:'upc'}, 
                      {title:'ProposedRestockQuantity',field:'proposed_restock_quantity'}, 
                      {title:'Categories',field:'categories'}, 
                      {title:'CurrentQuantity',field:''}, 
                      {title:'SoldInLastWeek',field:''}, 


                      ]

$scope.export_csv = function(){
  return $scope.restock_items}


  

}]);
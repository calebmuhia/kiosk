'use script'

 angular.module('dashboardui')

 .controller('adminFunctionsCtrl', ['$scope','$rootScope','AdminService','toaster', function($scope, $rootScope,AdminService,toaster){
    $scope.popups = function(code,title,msg){
           toaster.pop(code, title, msg);
       };

    $rootScope.$on('rebootMsg', function(event,msg,location){
        if (msg == 'ok'){
            $scope.popups('success', 'Rebooting', location+' is now rebooting')
        }
        else{
            $scope.popups('error', 'Error', 'Error Rebooting '+location+' \n Maybe the Machine is not ONLINE')

        }
    })

    $rootScope.$on('service_status', function(event,data){
        angular.forEach(data,function(val, key){
            try{
                $scope.locations[key]['current_load'] = val['Current Load']['plugin_output']
                $scope.locations[key]['disk'] = val['Disk']['plugin_output']
                $scope.locations[key]['db'] = val['Mysql Service']['current_state']




            } 
            catch(e){

            }
        
        

        })
    })
    $rootScope.$on('status', function(event,data){
        angular.forEach(data,function(val, key){
            try{
                $scope.locations[key]['current_state'] = val['current_state']
            } 
            catch(e){

            }
        
        

        })

        
        
    })




    var admin = new AdminService()

    $scope.initLocations = function(locations){
        $scope.locations = locations
        admin.set_locations(locations)
        admin.get_status();
    }

    

    $scope.reboot = function(location){

        var r = confirm("Are You Sure You Want To Reboot "+location)
        if (r == true){
            admin.reboot(location)
        }

        

    }

    

     
 }])
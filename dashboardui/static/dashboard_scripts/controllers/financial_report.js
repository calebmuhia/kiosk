'use strict'

angular.module("dashboardui")

.controller('FinancialReportCtrl', ['$scope','$filter','Crud','$rootScope', function($scope,$filter, Crud, $rootScope){

    $scope.get_report = function(){
        if(typeof($scope.lastweek)==='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        if(typeof($scope.today)==='object'){
            var today = $filter('date')($scope.today, 'yyyy-MM-dd')
        }
        else{
            var today = $scope.today
        }

        $scope.promise = Crud.list_date('orders/financial_report/',lastweek, today)
        .then(function(response){
            $scope.report = response

        })




    }
    $scope.get_report()

    $rootScope.reload_data = function(){
        $scope.get_report()
    }





    
}])
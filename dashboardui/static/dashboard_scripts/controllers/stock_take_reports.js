'use script'

 angular.module('dashboardui')

 .controller('StockTakeReports', ['$scope','$rootScope','$http','$modal', function($scope, $rootScope,$http,$modal){
    

    $scope.promise = $http.get("/dashboard/api/v1/productmovement/stock_take_report/")
    .then(function(response){
        $scope.takes = response.data
       
    })

    $scope.view = function(values){
        console.log(values)

        var modalInstance = $modal.open({
          templateUrl: 'view.html',
          controller: 'ViewCtrl',
          size: 'lg',
          resolve: {
            values:function(){
                return values
            }
            
          }
        });
    
        modalInstance.result.then(function (object) {
        
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });

    }

    
    

     
 }])

 .controller('ViewCtrl', ['$modalInstance','values','$scope', function($modalInstance, values,$scope){


    $scope.items = values

    $scope.ok = function(){
        $modalInstance.close()

    }
     
 }])
    


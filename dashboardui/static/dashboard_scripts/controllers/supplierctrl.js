'use strict'

angular.module('dashboardui')

.controller("SupplierCtrl", ['$scope','SupplierService', '$q','$interval','$modal',  function($scope,SupplierService, $q,$interval, $modal){
    
$scope.$scope = $scope
    
$scope.gridOptions = {
    enableSorting: true,
    enableScrollbars:true,
    rowEditWaitInterval:500,
    data : [],

}
$scope.deleteButton = '<button style="margin:2px" id="editBtn" type="button" class="btn btn-danger btn-xs" ng-click="getExternalScopes().deleteSupplier(row,$event)" >Delete</button> '

$scope.gridOptions.columnDefs= [
          { name:'Supplier Name', field: 'name'},
          { name:'Delete',  field:'', cellTemplate:$scope.deleteButton,  enableCellEdit: false}

          

        ];

$scope.get_suppliers = function(){
  SupplierService.Supplier_list()
  .then(function(data){
    $scope.gridOptions.data = data.objects
  })
} ;
$scope.get_suppliers();

$scope.saveRow = function( rowEntity ) {
        

        
    var promise = SupplierService.save_supplier(rowEntity)
    $scope.gridApi.rowEdit.setSavePromise( $scope.gridApi.grid, rowEntity, promise );
   

  }; 
 
  $scope.gridOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
  };


$scope.deleteSupplier = function deleteSupplier(row, $event){
   
    var index = $scope.gridOptions.data.indexOf(row.entity)


        SupplierService.deleteSupplier(row)
        .then(function(data){
          $scope.gridOptions.data.splice(index,1);



        })
    };

$scope.createNewSupplier = function (size) {
    

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: 'CreateNewSupplier',
      size: size,
      resolve: {
        new_Supplier: function () {
          $scope.newSupplier = {
            name:"",
            order:0
          }
          return $scope.newSupplier;
        }
      }
    });

    modalInstance.result.then(function (Supplierdata) {
      $scope.newSupplierData = Supplierdata;
      SupplierService.save_new_supplier($scope.newSupplierData)
      .then(function(data){
      
        $scope.gridOptions.data.push(data)
      
      })
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };    







   
}])

.controller('CreateNewSupplier', function ($scope, $modalInstance, new_Supplier) {
  
  $scope.new_Supplier = new_Supplier
  
  $scope.ok = function () {
   
    $modalInstance.close($scope.new_supplier);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
'use strict'

angular.module('dashboardui')
.controller('HomeCtrl', ['$scope','$http', '$rootScope', '$filter', 'OrderService','Crud',  function($scope,$http, $rootScope, $filter, OrderService, Crud){
	$scope.data = []

	$scope.get_data = function(){
		if(typeof($scope.lastweek)==='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        if(typeof($scope.today)==='object'){
            var today = $filter('date')($scope.today, 'yyyy-MM-dd')
        }
        else{
            var today = $scope.today
        }

		OrderService.group_by_date(lastweek, today)
		.then(function(data){
			$scope.data  = data
		}, function(error){

		});
		

		OrderService.sales(lastweek, today)
		.then(function(data){
			$scope.sales = data
		},
		function(error){

		});

		OrderService.get_orders_latest()
		.then(function(data){
			$scope.latest_orders = data.objects
		},
		function(error){

		});

		Crud.list('orders/top_ten/?lastweek='+lastweek+'&today='+today, undefined)
		.then(function(data){
			$scope.top_ten_categories = data.categories
			$scope.top_ten_products = data.products
		})


		

			
       

	};

	$http.get('/dashboard/api/v1/users/user_total_balances/')
			.then(function(data){
		     
				
				$scope.balances = data.data.balance
				console.log(data.data.balance)
				
				
			},
			function(error){
				console.log(error)
			}
			)

	$http.get('/dashboard/api/v1/cashbox/?emptied')
			.then(function(data){
		     
				
				$scope.cashbox = data.data.objects[0]
				console.log($scope.cashbox)
				
			},
			function(error){
				console.log(error)
			}
			)
	

	$scope.get_data();

	


	$rootScope.reload_data = function(){
		$scope.get_data()
	}

	


	



}])
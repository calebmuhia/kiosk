'use strict'

angular.module('dashboardui')

.controller("NoBarcodeProductsListCtrl", ['$scope','ProductService', 'CategoryService', '$q','$interval','$modal','Crud','toaster', function($scope,ProductService,CategoryService, $q,$interval, $modal, Crud,toaster ){
    
    $scope.$scope = $scope
    $scope.initLocationsProducts = function(location){
      $scope.location = location
      console.log(location)
    }
    $scope.popups = function(code,title,msg){
           toaster.pop(code, title, msg);
       };

    $scope.products = []
    $scope.gridOptions = {
        enableSorting: true,
        enableScrollbars:false,
        rowEditWaitInterval:500,
        data : [],
        rowHeight:'130',
         enableCellSelection: true,
         enableRowSelection: false,
    }

    $scope.imageTemplate = function(){
      return '<img style="margin:10px" src="{{row.entity[col.field]}}" height="100" width="100" lazy-src class="img-responsive img-thumbnail" ng-click="getExternalScopes().alert(row,$event)">'
    }
    $scope.editableInPopup = '<button style="margin:10px" id="editBtn" type="button" class="btn btn-danger" ng-click="getExternalScopes().deleteProduct(row,$event)" >Delete</button> '
    

    
    $scope.productsStatus = [
                          {'status':'All', 'id':''},
                          {'status':'Active', 'id':'True'},
                          {'status':'InActive', 'id':'False'},

     ]
     $scope.status = $scope.productsStatus[0]
    CategoryService.category_list()
    .then(function(data){
        $scope.categories = data.objects
        $scope.gridOptions.columnDefs= [
         { name:'Image', field: 'image', cellTemplate: $scope.imageTemplate() ,width:130, enableCellEdit: false },

        { name:'Name', field: 'name', width:250},
          
          { name:'Unit Price', field: 'unit_price', type:'float'},
          { name:'Unit Cost', field: 'unit_cost', type:'number' },
          { name:'Quantity', field: 'quantity',  type:'number'},
          // { name:'Min Stock', field: 'min_stock', width:70, type:'number'},
          // { name:'Max Stock', field: 'max_stock', width:70, type:'number'},
          { name:'Category', field: 'category',width:100,    editableCellTemplate:  'ui-grid/dropdownEditor',editDropdownValueLabel:'name' ,editDropdownOptionsArray:$scope.categories},
          // { name:'Supplier', field: 'Supplier', type:'number'},
          { name:'Hst', field: 'hst', type: 'boolean'},
          { name:'Pst', field: 'pst', type: 'boolean'},
          { name:'Active', field:'active', type: 'boolean'},
          { name:'Ordering', field:'ordering', type:"number"},
          { name:'Delete',  field:'', cellTemplate:$scope.editableInPopup,  enableCellEdit: false}


        ];
        $scope.categories.push({id:'all',name:'All'})
        $scope.category = $scope.categories[$scope.categories.length-1]
        
    })

$scope.get_products = function(){
    $scope.promise = ProductService.products_list_no_barcode(9)
    .then(function(data){
        $scope.gridOptions.data = data.objects
        $scope.meta = data.meta
        

    },
    function(error){

    } )

}
$scope.get_products();

$scope.productsToBeSearched = '';
  $scope.search_product = function(){
    console.log("searching for ", $scope.productsToBeSearched);
    if ($scope.productsToBeSearched == ''){
      $scope.msg = "search params are empty"
      return;
    }

    $scope.promise = ProductService.search_nobarcode_product($scope.productsToBeSearched)
    .then(function(data){
      if (data.objects.length > 0){
        $scope.gridOptions.data = data.objects
      $scope.meta = data.meta
      }
      else{
        console.log("not found")
        $scope.popups('error', "Error", "Product "+$scope.productsToBeSearched+' Not Found')
      }

      



    })



  };
$scope.getTableStyle= function() {
   var rowHeight=130;
   var headerHeight=45;
   return {
      height: ($scope.gridOptions.data.length * rowHeight + headerHeight) + "px"
   };
};

      $scope.saveRow = function( rowEntity ) {
        

        
    var promise = ProductService.save_product(rowEntity)
    .then(function(data){
      $scope.popups("success", "Success", "Sucess Editing "+rowEntity.name)
    })
    $scope.gridApi.rowEdit.setSavePromise( $scope.gridApi.grid, rowEntity, promise );
   

  }; 
 
  $scope.gridOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
  };

  $scope.deleteProduct = function deleteProduct(row, $event){
    console.log(row)
    var index = $scope.gridOptions.data.indexOf(row.entity)


        ProductService.deleteProduct(row)
        .then(function(data){
          $scope.gridOptions.data.splice(index,1);
          $scope.popups("success", "Success", "Success Deleting Product")



        })
    }

  $scope.get_next = function(){
        var next_url = $scope.meta.next;
        if (next_url){
            ProductService.next_previous(next_url)
        .then(function(data){
            $scope.gridOptions.data = data.objects
            $scope.meta = data.meta
            
        },
        function(error){

        });
        }
        

    }
    $scope.get_previous = function(){
        var previous_url = $scope.meta.previous;
        if (previous_url){
            ProductService.next_previous(previous_url)
        .then(function(data){
            $scope.gridOptions.data = data.objects
            $scope.meta = data.meta
           
        },
        function(error){

        });
        }
        

    }

    

    $scope.CreateNewProduct = function () {
    $scope.newProduct = {name:"",
    unit_price:0.00,
    unit_cost:0.00,
    hst:true,
    pst:true,
    ordering:0,
    active:true,
    category:'',
    image:'',
    location:$scope.location,
    quantity:0};
   $scope.newProduct.location = $scope.location

    var modalInstance = $modal.open({
      templateUrl: 'create_new_product.html',
      controller: 'CreateNewProductCtrl',
      size: 'lg',
      resolve: {
        noBarcodeProduct: function () {

          return $scope.newProduct;
        }
      }
    });

    modalInstance.result.then(function (productdata) {
      $scope.newProductData = productdata;
      
      $scope.promise = Crud.save_new_object($scope.newProductData,'nobarcodeproduct')
      .then(function(data){
        if($scope.newProductData.locations.indexOf($scope.location) != -1){
var data_list = []
        data_list.push(data)
        $scope.gridOptions.data = data_list
        $scope.popups("success", "Success", "Success Adding product "+productdata.name+" To Locations "+productdata.locations.join(' '))

      }
      
      })
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };


}])



.controller("CreateNewProductCtrl",function ($scope, $modalInstance, $http, noBarcodeProduct,CategoryService) {

  

  $scope.noBarcodeProduct = noBarcodeProduct

  $scope.categories = [];
  $scope.selected = [];
$scope.locations = []
$scope.selected.push(noBarcodeProduct.location)


  
  CategoryService.category_list()
  .then(function(data){
    $scope.categories = data.objects
    $scope.noBarcodeProduct.category = $scope.categories[0]
  })
  $http.get('/dashboard/get_db_names/')
    .then(function(data){
        $scope.locations = data.data
        console.log(data)

    })



    var updateSelected = function (action, location) {
        if (action == 'add' & $scope.selected.indexOf(location) == -1) $scope.selected.push(location);
        if (action == 'remove' && $scope.selected.indexOf(location) != -1) $scope.selected.splice($scope.selected.indexOf(location), 1);
        if($scope.selected.length == 0){
          $scope.selectedError = true
        } 
        else{
          $scope.selectedError = false

        }  
    }

    $scope.updateSelection = function ($event, location) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        updateSelected(action, location);

    };

    $scope.selectAll = function ($event) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        for (var i = 0; i < $scope.locations.length; i++) {
            var location = $scope.locations[i];
            updateSelected(action, location);
        }
    };

    $scope.getSelectedClass = function (location) {
        return $scope.isSelected(location) ? 'selected' : '';
    };

    $scope.isSelected = function (location) {
        return $scope.selected.indexOf(location) >= 0;
    };

    //something extra I couldn't resist adding :)
    $scope.isSelectedAll = function () {
        return ($scope.selected.length === $scope.locations.length)
    };

  $scope.onFileSelect = function($files) {
    //$files: an array of files selected, each file has name, size, and type.
    console.log($files)

    
      $scope.file = $files[0];
      $scope.$apply()
      var elem = angular.element("#fileinput");
      elem.trigger("change");

      
      
  };
   

  $scope.ok = function () {

    var fileReader = new FileReader();
    fileReader.readAsDataURL($scope.file);
    
    fileReader.onload = function(e) {
      $scope.noBarcodeProduct.image = e.target.result
      $scope.noBarcodeProduct.locations = $scope.selected

    console.log($scope.noBarcodeProduct)
    $modalInstance.close($scope.noBarcodeProduct);
  }

    
  };

  $scope.cancel = function () {
    
    $modalInstance.dismiss('cancel');
  };
});
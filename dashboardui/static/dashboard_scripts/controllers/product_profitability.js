'use strict'

angular.module('dashboardui')

.controller('ProductProfitability', ['$scope','Crud',"$filter","$rootScope", function($scope,Crud,$filter,$rootScope){
    
    $scope.$scope = $scope
    $scope.gridOptions = {
        enableSorting: true,
        enableScrollbars:true,
        rowEditWaitInterval:500,
        enableFiltering:true,
        data : [],

    }

    $scope.gridOptions.columnDefs= [
    { name:'Location', field: 'location', width:130, enableCellEdit: false},
    { name:'Barcode', field: 'barcode',  enableCellEdit: false, width:130, filter: {
          condition: function(searchTerm, cellValue) {
            var lower_value = cellValue.toLowerCase()
            var lower_search = searchTerm.toLowerCase()
            return lower_value.indexOf(lower_search) >= 0;
          }}},
    { name:'Name', field: 'product_name', enableCellEdit: false, width:130, filter: {
          condition: function(searchTerm, cellValue) {
            var lower_value = cellValue.toLowerCase()
            var lower_search = searchTerm.toLowerCase()
            return lower_value.indexOf(lower_search) >= 0;
          }}},
    { name:'Unit Price', field: 'unit_price', enableCellEdit: false},
    { name:'Unit Cost', field: 'unit_cost', enableCellEdit: false},
    { name:'Sale', field: 'unit_sale', enableCellEdit: false},
    { name:'Total Sale', field: 'total_sales', enableCellEdit: false},
    { name:'Sales Cost', field: 'sales_cost', enableCellEdit: false},

    { name:'Revenue', field: 'revenue', enableCellEdit: false},
    { name:'Cogs', field: 'cogs', enableCellEdit: false},
    { name:'Profit/Loss', field: 'profit_or_loss', enableCellEdit: false},
    { name:'Gross Margin', field: 'gross_margin', enableCellEdit: false},

          

        ];

     $scope.get_data = function(){

        if(typeof($scope.lastweek)=='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        
        if(typeof($scope.today)=='object'){
            var today_object = angular.copy($scope.today)
            

            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }
        else{
            
            var today_object = new Date($scope.today)
           

            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }

        

        $scope.promise= Crud.list_date('orders/product_profitability_report/',lastweek, today)
        .then(function(data){
            $scope.gridOptions.data = data
            
           
        },
        function(error){

        });
        

    };
    $scope.get_data()
    $rootScope.reload_data = function(){
        $scope.get_data()
    }
    
}])


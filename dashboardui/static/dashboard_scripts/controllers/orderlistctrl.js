'use strict'

angular.module('dashboardui')

.controller('OrderListCtrl', ['$scope','$http', '$rootScope', '$filter', 'OrderService', function($scope,$http, $rootScope, $filter, OrderService){
    $scope.data = []

    $scope.get_data = function(){
        if(typeof($scope.lastweek)==='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        if(typeof($scope.today)==='object'){
            var today = $filter('date')($scope.today, 'yyyy-MM-dd')
        }
        else{
            var today = $scope.today
        }

        

        $scope.promise= OrderService.order_list(15,lastweek, today)
        .then(function(data){
            $scope.orders = data.objects
            $scope.order_meta = data.meta
           
        },
        function(error){

        });
        
        OrderService.sales(lastweek, today)
        .then(function(data){
            $scope.sales = data
        },
        function(error){

        });
        

            
       

    };

    
    $scope.get_data();

    $http.get('/dashboard/api/v1/cashbox/?emptied')
            .then(function(data){
             
                
                $scope.cashbox = data.data.objects[0]
                
                
            },
            function(error){
                console.log(error)
            }
            )
    

    

    


    $rootScope.reload_data = function(){
        $scope.get_data()
    }

    $scope.get_next = function(){
        var next_url = $scope.order_meta.next;
        if (next_url){
           $scope.promise = OrderService.next_previous(next_url)
        .then(function(data){
            $scope.orders = data.objects
            $scope.order_meta = data.meta
            
        },
        function(error){

        });
        }
        

    }
    $scope.get_previous = function(){
        var previous_url = $scope.order_meta.previous;
        if (previous_url){
            OrderService.next_previous(previous_url)
        .then(function(data){
            $scope.orders = data.objects
            $scope.order_meta = data.meta
           
        },
        function(error){

        });
        }
        

    }
    $rootScope.csv_list_headers = [
                      {title:'Location',field:'location'},
                      {title:"Order Id", field:'id'},
                      {title:"Status", field:'status'},
                      {title:"DateTime", field:'created'},
                      {title:"User", field:"user"},
                      {title:"Order Total", field:"order_total"},
                      {title:"Gst Total", field:"Gst"},
                      {title:"Pst Total", field:"Pst"},
                      {title:"Payment Method", field:"payment_method"}
                      
                      ]
    $rootScope.csv_details_headers = [
                      {title:'Location',field:'location'},
                      {title:"Order Id", field:'order_id'},
                      {title:"Status", field:'status'},
                      {title:"DateTime", field:'created'},
                      {title:"Product Upc", field:"barcode"},
                      {title:"Product Name", field:"product_name"},
                      {title:"Category", field:"category"},
                      {title:"Quantity", field:"quantity"},
                      {title:"Unit Price", field:"unit_price"},
                      {title:"Total Price", field:"total_price"},
                      {title:"Unit Cost", field:"unit_cost"},
                      {title:"Total Cost", field:"total_cost"},
                      {title:"Gst", field:"gst"},
                      {title:"Pst", field:"pst"},
                      {title:"Payment Method", field:"payment_method"},







 

                      
                      ]
                  

    $rootScope.export_csv_list = function(){
        console.log("called")
        if(typeof($scope.lastweek)=='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        
        if(typeof($scope.today)=='object'){
            var today_object = $scope.today

            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }
        else{
            
            var today_object = new Date($scope.today)


            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }


        

        return OrderService.order_list(0,lastweek, today)
        .then(function(data){
            angular.forEach(data.objects, function(val, index){
                val.status = $filter('status')(val.status)
                val.created = $filter('date')(val.created, "yyyy/MM/dd HH:mm")
            })
        return data.objects
            
           
        });
        
        

            
       

    };

    $rootScope.export_csv_details = function(){
        console.log("called")
        if(typeof($scope.lastweek)=='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        
        if(typeof($scope.today)=='object'){
            var today_object = $scope.today

            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }
        else{
            
            var today_object = new Date($scope.today)


            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }


        
        var url = '/dashboard/api/v1/orders/order_details/?lastweek='+lastweek+'&today='+today
        return $http.get(url)
        .then(function(data){
            
        return data.data
            
           
        });
        
        

            
       

    };

    


    



}])
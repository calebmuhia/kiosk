'use strict'

angular.module('dashboardui')

.controller("CategoryCtrl", ['$scope','Crud', '$q','$interval','$modal',  function($scope,Crud, $q,$interval, $modal){
    
$scope.$scope = $scope
    
$scope.gridOptions = {
    enableSorting: true,
    enableScrollbars:true,
    rowEditWaitInterval:500,
    data : [],

}
$scope.deleteButton = '<button style="margin:2px" id="editBtn" type="button" class="btn btn-danger btn-xs" ng-click="getExternalScopes().deleteCategory(row,$event)" >Delete</button> '

$scope.gridOptions.columnDefs= [
          { name:'Name', field: 'name'},
          { name:'Ordering', field: 'order'},
          { name:'Parent Category', field: 'parent_category', enableCellEdit: false},
          { name:'Delete',  field:'', cellTemplate:$scope.deleteButton,  enableCellEdit: false}

          

        ];

$scope.get_categories = function(){
  Crud.list('categories','categories')
  .then(function(data){
    $scope.gridOptions.data = data.objects
  })
} ;
$scope.get_categories();

$scope.saveRow = function( rowEntity ) {
        

        
    var promise = Crud.save_object(rowEntity)
    $scope.gridApi.rowEdit.setSavePromise( $scope.gridApi.grid, rowEntity, promise );
   

  }; 
 
  $scope.gridOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
  };


$scope.deleteCategory = function deleteCategory(row, $event){
   
    var index = $scope.gridOptions.data.indexOf(row.entity)


        Crud.delete_object(row)
        .then(function(data){
          $scope.gridOptions.data.splice(index,1);



        })
    };

$scope.createNewCategory = function (size) {
    

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: 'CreateNewCategory',
      size: size,
      resolve: {
        new_category: function () {
          $scope.newCategory = {
            name:"",
            order:0
          }
          return $scope.newCategory;
        }
      }
    });

    modalInstance.result.then(function (categorydata) {
      $scope.newCategoryData = categorydata;
      Crud.save_new_object('categories', $scope.newCategoryData)
      .then(function(data){
      
        $scope.gridOptions.data.push(data)
      
      })
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };    







   
}])

.controller('CreateNewCategory', function ($scope, $modalInstance, new_category) {
  
  $scope.new_category = new_category
  
  $scope.ok = function () {
   
    $modalInstance.close($scope.new_category);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
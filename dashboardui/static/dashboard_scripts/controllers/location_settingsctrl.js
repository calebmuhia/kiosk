'use strict'

angular.module('dashboardui')

.controller('LocationCtr', ['$scope', '$http', 'Crud', function($scope, $http, Crud){

    
    $scope.weekdays = [1,2,3,4,5,6,7]
    $scope.hours = []
    
    $scope.promise=$http.get('/dashboard/api/v1/locationinfo/')
    .then(function(data){
        for(var i=0;i<25;i++){
            $scope.hours.push(i)
        }
        console.log(data.data.objects[0])
        $scope.location = data.data.objects[0]
        if(!$scope.location){
            $scope.location={}
            $scope.location.weekday_from=1
            $scope.location.weekday_to=5
            $scope.location.from_hour=7
            $scope.location.to_hour=19


        }
        
    })
    
    

    $scope.save = function(){
        console.log($scope.location)

        if ($scope.location.id != undefined){
            $scope.promise = Crud.save_object($scope.location)
            .then(function(data){
                console.log(data)
                $scope.location = data

            })
        }
        else{
            $scope.promise = Crud.save_new_object($scope.location, 'locationinfo')
            .then(function(data){
                $scope.location = data
                console.log(data)

            })

        }

        


    }


}]);
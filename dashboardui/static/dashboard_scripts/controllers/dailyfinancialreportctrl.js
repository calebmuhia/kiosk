'use strict'

angular.module('dashboardui')

.controller('DailyFinancialReport', ['$scope','$filter','Crud','$rootScope', function($scope,$filter, Crud, $rootScope){

$scope.get_data = function(){
        if(typeof($scope.lastweek)=='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        
        if(typeof($scope.today)=='object'){
            var today_object = $scope.today
            // today_object.setDate(today_object.getDate()+1)

            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }
        else{
            
            var today_object = new Date($scope.today)
            // today_object.setDate(today_object.getDate()+1)

            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }

        

        
        
        $scope.promise= Crud.list_date('orders/daily_financial_report/',lastweek, today)
        .then(function(data){
            $scope.all_dates = data
            $scope.order_total = 0.00
            $scope.order_subtotal = 0.00
            $scope.taxes = 0.00
            $scope.unit_cost = 0.00
            $scope.profits = 0.00
            angular.forEach(data, function(value, index){
                 
                    $scope.order_total += parseFloat(value.total)
                    $scope.taxes += parseFloat(value.taxes)
                    $scope.unit_cost += parseFloat(value.unit_cost)
                    $scope.profits += parseFloat(value.profits)


                 

            })
           
        },
        function(error){

        });
        

            
       

    };
    $scope.get_data();
    $rootScope.reload_data = function(){
        $scope.get_data()
    }
    
}])

.directive("sparkLineChart", function () {

    return {

        restrict: "E",

        scope: {

            data: "@"

        },

        compile: function (tElement, tAttrs, transclude) {

            tElement.replaceWith("<span>" + tAttrs.data + "</span>");

            return function (scope, element, attrs) {

                attrs.$observe("data", function (newValue) {
                    

                    element.html(newValue);

                    element.sparkline('html', { type: 'line', width: '96%', height: '80px', barWidth: 11, barColor: 'blue' });

                });

            };

        }

    };

})

.directive("easyPieChart", function () {

    return {

        restrict: "A",

        scope: {

            'data-percent': "@"

        },

        compile: function (tElement, tAttrs, transclude) {

            

            return function (scope, element, attrs) {

                attrs.$observe("data-percent", function (newValue) {
                    
                    

                    // element.html(newValue);

                    element.easyPieChart({
                        animate: 2000,
                        trackColor: "#515151",
                        scaleColor: "#515151",
                        lineCap: 'butt',
                        lineWidth: 20,
                        barColor: function(percent) {
                            percent /= 100;
                            return "rgb(" + Math.round(255 * (1-percent)) + ", " + Math.round(255 * percent) + ", 0)";
                        },
                        size: 90



     
    });;

                });

            };

        }

    };

})
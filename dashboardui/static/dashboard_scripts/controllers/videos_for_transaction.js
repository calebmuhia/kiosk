'use strict'

angular.module('dashboardui')

.controller('VideoForTrasactionCtrl', ['$scope','$sce','$modal','$http', function($scope, $sce, $modal,$http){

    $scope.get_events = function(events){
        $scope.events = events
        angular.forEach($scope.events, function(val, indx){
            var frames = val.frames
            var len = 5

            var value = String(frames)
            var middle = String(parseInt(frames/2)) 
            len = 5-middle.length
            
            val.start_image = val.starttime+"/00001-capture.jpg"
            val.middle_image = val.starttime+'/'+new Array(len + 1).join(0)+parseInt(frames/2)+'-capture.jpg'
            len = 5-value.length
            val.end_image = val.starttime+'/'+new Array(len + 1).join(0)+frames+'-capture.jpg'


        })
        console.log($scope.events)
        $scope.videos_showing= false
        $scope.images_showing = true

        $scope.video = events[0]
        $scope.config = {
                sources: [
                {src:$sce.trustAsResourceUrl('/events/'+$scope.video.monitorid+'/'+$scope.video.starttime+'/Event-'+$scope.video.id+'-r1-s1.mp4'), type: "video/mp4"}

                    ],
                tracks: [
                    
                ],
                
                theme: "/static/video/bower-videogular-themes-default/videogular.css",
                plugins: {
                    poster: ""
                }
            };

            console.log($scope.config)
            $scope.get_next_previous()

    }

    $scope.get_next_previous = function(){
        $http.get('/dashboard/videos/checkNextPrevious/?event_id='+$scope.video.id)
        .then(function(data){
            var url = "/dashboard/videos/event_step/?event_id=";
            if(data.data.next_event != null){
                $scope.next_event = url+data.data.next_event
                $scope.next_url_css = 'btn-success'
            }
            else{
                $scope.next_event = '#'
                $scope.next_url_css = 'btn-default disabled'
            }

            if(data.data.previous_event != null){
                $scope.previous_event = url+data.data.previous_event
                $scope.previous_url_css = 'btn-success'
            }
            else{
                $scope.previous_event = '#'
                $scope.previous_url_css = 'btn-default disabled'
            }
        })

    }
    




    $scope.show_videos = function(){
        $scope.videos_showing= true
        $scope.images_showing = false
        $scope.get_video()

    }


    $scope.get_video = function(){
        $scope.url = '/events/'+$scope.video.monitorid+'/'+$scope.video.starttime+'/Event-'+$scope.video.id+'-r1-s1.mp4'
        $scope.video_name = 'Event-'+$scope.video.id+'-Camera-'+$scope.video.monitorid+'.mp4'


        $scope.config.sources=[{src:$sce.trustAsResourceUrl($scope.url), type: "video/mp4"},]
        
        
    }
    $scope.set_time = function(){
        var modalInstance = $modal.open({
          templateUrl: '/static/views/set_time.html',
          controller: 'SetTime',
          size: 'lg',
          resolve: {
            object:function(){
                return $scope.video
            }
            
          }
        });
    
        modalInstance.result.then(function (object) {
        
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });

    }

    
    
    

    
}])

.controller('SetTime', ['$scope','object','$modalInstance','$timeout','$filter','$http', function($scope, object, $modalInstance,$timeout, $filter, $http){
   

    var s = object.starttime.split('/')
    console.log(s)
    
    var d = new Date('20'+s[0],s[1]-1,s[2],s[3],s[4],s[5],0);
    console.log(d)

    $scope.date = $filter('date')(d, 'yyyy/MM/dd');
    $scope.mytime = d;


    $scope.get_search_url = function(){
        var d = new Date($scope.date)
        d.setHours($scope.mytime.getHours())
        d.setMinutes($scope.mytime.getMinutes())
        var selectedtime = $filter('date')(d, 'yyyy/MM/dd H:mm:ss')
        console.log(selectedtime)
        $http.get('/dashboard/videos/check_for_video/?search_time='+selectedtime)
        .then(function(data){
            if (data.data.events != null){
                window.location = '/dashboard/videos/event_step/?event_id='+data.data.events
            }
            else{
                $scope.error = true
            }
        })
    }





    $scope.open = function() {
   
    $timeout(function() {
      $scope.opened = true;
    });
  };
  


    $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
  $scope.format = $scope.formats[1];

  $scope.hstep = 1;
  $scope.mstep = 1;

  $scope.options = {
    hstep: [1, 2, 3],
    mstep: [1, 5, 10, 15, 25, 30]
  };

  $scope.ismeridian = false;
  $scope.toggleMode = function() {
    $scope.ismeridian = ! $scope.ismeridian;
  };


  
  $scope.changed = function () {
    
  };

  $scope.clear = function() {
    $scope.mytime = null;
  };


    



    
}])


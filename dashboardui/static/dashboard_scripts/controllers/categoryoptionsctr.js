'use strict'

angular.module('dashboardui')

.controller("CategoryOptionsCtrl", ['$scope','Crud', '$q','$interval','$modal', function($scope,Crud, $q,$interval, $modal){
    
$scope.$scope = $scope
    
$scope.gridOptions = {
    enableSorting: true,
    enableScrollbars:true,
    rowEditWaitInterval:500,
    data : [],

}
Crud.list("categories", "categories")
    .then(function(data){
        $scope.categories = data.objects
        $scope.category = $scope.categories[$scope.categories.length-1]
        
    });
$scope.deleteButton = '<button style="margin:2px" id="editBtn" type="button" class="btn btn-danger btn-xs" ng-click="getExternalScopes().deleteCategoryOption(row,$event)" >Delete</button> '
$scope.categoryDropdown = '<select  ng-class="\'colt\' + col.uid" ui-grid-edit-dropdown ng-model="row.entity.category" data-ng-options="field.name for field in getExternalScopes().categories">'
    
$scope.gridOptions.columnDefs= [
          { name:'Category Name', field: 'category.id', editType: 'dropdown', enableCellEdit: true,editableCellTemplate: $scope.categoryDropdown, enableCellEditOnFocus: true, cellFilter: 'mapCategories',},
          { name:'Days Of Product', field: 'days_of_product', type:"number"},
          { name:'Hard Max', field: 'hard_max', type:"number"},
          { name:'Round To Case', field: 'round_to_case', type:"boolean"},

          { name:'Delete',  field:'', cellTemplate:$scope.deleteButton,  enableCellEdit: false}

          

        ];

$scope.get_categories_options = function(){
  Crud.list("categories_options")
  .then(function(data){
    $scope.gridOptions.data = data.objects
  })
} ;
$scope.get_categories_options();

$scope.saveRow = function( rowEntity ) {
  
        

        
    var promise = Crud.save_object(rowEntity)
    $scope.gridApi.rowEdit.setSavePromise( $scope.gridApi.grid, rowEntity, promise );
   

  }; 
 
  $scope.gridOptions.onRegisterApi = function(gridApi){
    //set gridApi on scope
    $scope.gridApi = gridApi;
    gridApi.rowEdit.on.saveRow($scope, $scope.saveRow);
  };


$scope.deleteCategoryOption= function deleteCategoryOption(row, $event){
   
    var index = $scope.gridOptions.data.indexOf(row.entity)


        Crud.delete_object(row)
        .then(function(data){
          $scope.gridOptions.data.splice(index,1);



        })
    };

$scope.createNewCategoryOption = function (size) {
    

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: 'CreateNewCategoryOption',
      size: size,
      resolve: {
        new_category: function () {
          $scope.newCategoryOption = {
            category:{},
            days_of_product:7,
            hard_max:0,
            round_to_case:false

          }
          return $scope.newCategoryOption;
        }
      }
    });

    modalInstance.result.then(function (categoryoptiondata) {
      $scope.newCategoryOptionData = categoryoptiondata;
      
      Crud.save_new_object($scope.newCategoryOptionData,"categories_options")
      .then(function(data){
      
        $scope.gridOptions.data.push(data)
      
      })
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };    







   
}])

.controller('CreateNewCategoryOption', function ($scope, $modalInstance, new_category, Crud) {
  $scope.categories = [];
  $scope.new_category = new_category
  Crud.list("categories", 'categories')
  .then(function(data){
    $scope.categories = data.objects
    $scope.new_category.category = {
      name:"",
    }
    
  })
  
  
  
  $scope.ok = function () {
   
    $modalInstance.close($scope.new_category);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
})
.filter('mapCategories', function( $cacheFactory ) {
      return function(input) {
        var cache = $cacheFactory.get("categories")

        if (cache.get(input) != undefined) {
         
          
          return cache.get(input);
        } else {
          return 'None';
        }
      };
    })
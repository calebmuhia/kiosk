'use strict'

angular.module('dashboardui')

.controller('CashBoxCtr', ['$scope', '$http', function($scope, $http){

    $http.get('/dashboard/api/v1/cashbox/?emptied=false')
    .then(function(data){
        console.log(data)
        $scope.current_cashbox = data.data.objects[0]
    })
    
    $http.get('/dashboard/api/v1/cashbox/?emptied=true')
    .then(function(data){
        console.log(data)
        $scope.history_cashbox = data.data.objects
    })


}]);
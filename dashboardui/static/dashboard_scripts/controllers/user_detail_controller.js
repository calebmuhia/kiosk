'use strict'

angular.module("dashboardui")
.controller("UserDetailsCtr", ["$scope", "UserService", "OrderService","CashFlowService", function($scope, UserService, OrderService, CashFlowService){

    $scope.init = function(user_id, username,  balance, total_deposits, total_expenses, total_deducts,pay_by_payroll){
        $scope.user_id = user_id;
        $scope.balance = balance;
        $scope.total_deposits = total_deposits;
        $scope.total_expenses = total_expenses;
        $scope.total_deducts = total_deducts;
        $scope.username = username;
        var lower =  pay_by_payroll == 'True' ? true:false;
        $scope.pay_by_payroll = lower;
        console.log($scope.pay_by_payroll);
        $scope.get_data();
    }

    $scope.save_payment = function(){
        console.log($scope.pay_by_payroll)
        $scope.paymentpromise = UserService.save_payment($scope.user_id, $scope.pay_by_payroll)
        .then(function(data){
            console.log(data.objects)
            

        })

    }


    $scope.get_data = function(){

        CashFlowService.usercashflowlist(25,$scope.username)
        .then(function(data){
            $scope.cashflow_data = data.objects
            $scope.meta= data.meta
            
           
        },
        function(error){

        });

        OrderService.user_order_list(25, $scope.username)
    .then(function(data){
        $scope.orders = data.objects
        $scope.order_meta = data.meta
    })

    }
    $scope.increase_balance_value = 0.00;
    $scope.decrease_balance_value = 0.00;

    $scope.increment_user_balance  =  function(){
        UserService.increase_user_balance($scope.user_id, $scope.increase_balance_value)
        .then(function(data){
            console.log(data.objects)
            if (data.objects.status == "success"){
                $scope.balance = data.objects.balance;
                $scope.total_deposits = data.objects.total_deposits;
                $scope.total_deducts = data.objects.total_deducts;
            }
            $scope.increase = false
        })

    }
    $scope.decrement_user_balance  =  function(){
        UserService.decrease_user_balance($scope.user_id, $scope.decrease_balance_value)
        .then(function(data){
            console.log(data.objects)
            if (data.objects.status == "success"){
                $scope.balance = data.objects.balance;
                $scope.total_deposits = data.objects.total_deposits;
                $scope.total_deducts = data.objects.total_deducts;
            }
            $scope.decrease = false
        })

    }
    

    $scope.get_next_orders = function(){
        var next_url = $scope.order_meta.next;
        if (next_url){
            OrderService.next_previous(next_url)
        .then(function(data){
            $scope.orders = data.objects
            $scope.order_meta = data.meta
            
        },
        function(error){

        });
        }
        

    }
    $scope.get_previous_orders = function(){
        var previous_url = $scope.order_meta.previous;
        if (previous_url){
            OrderService.next_previous(previous_url)
        .then(function(data){
            $scope.orders = data.objects
            $scope.order_meta = data.meta
           
        },
        function(error){

        });
        }
        

    }


    $scope.get_next_cashflow = function(){
    var next_url = $scope.meta.next;
    if (next_url){
        CashFlowService.next_previous(next_url)
    .then(function(data){
        $scope.cashflow_data = data.objects
        $scope.meta = data.meta
        
    },
    function(error){

    });
    }
    

}
    $scope.get_previous_cashflow = function(){
        var previous_url = $scope.order_meta.previous;
        if (previous_url){
            CashFlowService.next_previous(previous_url)
        .then(function(data){
            $scope.cashflow_data = data.objects
            $scope.meta = data.meta
           
        },
        function(error){

        });
        }
        

    }








}])
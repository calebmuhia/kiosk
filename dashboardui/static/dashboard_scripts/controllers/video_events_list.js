'use strict'

angular.module('dashboardui')

.controller('EventListCtrl', ['$scope','$rootScope','Crud','$filter','$http', function($scope,$rootScope,Crud,$filter,$http ){

    $scope.cause_filters = [{label:'All',id:"all"},{label:'Scan',id:'scanning_items'}, {label:'Door Sensor',id:'door-trigger'},{label:'No Orders (Door)',id:'no_orders'}]
    $scope.cause_filter = $scope.cause_filters[0]
    $scope.params  = {}
    $scope.CrudOps = function(url, params){
        var api_prefix = '/dashboard/api/v1/'

        return $http({ url:api_prefix+url, params:params, method:"GET"}
           )
            .then(function(data){
                
                
   
                return data.data
   
            },
            function(error){
                console.log(error)
            }
            )
    }

    $scope.filter_by_cause = function(cause){
        $scope.params['causes'] = cause.id
        $scope.get_data($scope.params)


    }



    $scope.get_data = function(params){
        if(typeof($scope.lastweek)=='object'){
            var lastweek = $filter('date')($scope.lastweek, 'yyyy-MM-dd')
        }
        else{
            var lastweek = $scope.lastweek
        }
        
        if(typeof($scope.today)=='object'){
            var today_object = $scope.today
            today_object.setDate(today_object.getDate()+1)

            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }
        else{
            
            var today_object = new Date($scope.today)
            today_object.setDate(today_object.getDate()+1)

            var today = $filter('date')(today_object, 'yyyy-MM-dd')
        }

        var param = {'starttime__gte':lastweek,'starttime__lte':today,limit:15}

        angular.forEach(param, function(val, key){
            params[key] = val

        })




        $scope.promise = $scope.CrudOps('events', params )
        .then(function(data){
            $scope.events = data.objects
            $scope.events_meta = data.meta
        })


    }
    $scope.get_data($scope.params)

    $rootScope.reload_data = function(){
        $scope.get_data($scope.params)
    }

    $scope.get_next = function(){
        var next_url = $scope.events_meta.next;
        if (next_url){
           $scope.promise = Crud.next_previous(next_url)
        .then(function(data){
            $scope.events = data.objects
            $scope.events_meta = data.meta
            
        },
        function(error){

        });
        }
        

    }
    $scope.get_previous = function(){
        var previous_url = $scope.events_meta.previous;
        if (previous_url){
            Crud.next_previous(previous_url)
        .then(function(data){
            $scope.events = data.objects
            $scope.events_meta = data.meta
           
        },
        function(error){

        });
        }
        

    }
    
}])
'use strict'

angular.module('dashboardui')

.controller("InventoryRestockCtrl", ['$scope','Crud', '$q','$interval','$modal','$http',  function($scope,Crud, $q,$interval, $modal, $http){

$scope.placeholderapiurl = "choices_placeholder"    



$scope.get_choices_placeholders = function(){
  Crud.list($scope.placeholderapiurl)
  .then(function(data){
    $scope.placeholders = data.objects
  })
} ;
$scope.get_choices_placeholders();

$scope.get_reports = function(){
  Crud.list('restock_reports')
  .then(function(data){
    $scope.reports = data.objects
  })
}
$scope.get_reports()

$scope.run_restock_report = function(placeholder_id){
  $http.get('/dashboard/inventory/reports/create_restock_report/'+placeholder_id+'/')
  .then(function(response){
    window.location = response.data.url
    // console.log(response.data.url)
  })
}


$scope.delete_choices_placeholders= function(placeholder){
  console.log(placeholder)
   
    var index = $scope.placeholders.indexOf(placeholder)


        Crud.delete_object_none_grid(placeholder)
        .then(function(data){
          $scope.placeholders.splice(index,1);



        })
    };

$scope.createRestockReport = function (size) {
    

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: 'CreateNewRestockReport',
      size: size,
      resolve: {
        new_restock_report: function () {
          $scope.restock_report = {
            placeholder:{
                name:''
            },
            categories:[]
          }
          return $scope.restock_report;
        }
      }
    });

    modalInstance.result.then(function (newReportData) {
      
      Crud.save_new_object(newReportData.placeholder,$scope.placeholderapiurl)
      .then(function(data){
        var objects = []
        delete data["csrfmiddlewaretoken"]

        angular.forEach(newReportData.categories, function(val, index){
            var object = {
                place_holder:data,
                category:val
            }
            objects.push(object)
        })
        var createddata = {
            objects:objects
        }
        Crud.save_new_object_batch(createddata, 'category_choices')
        .then(function(categorydata){
            console.log(categorydata)
            $scope.placeholders.unshift(data)
        })
      })
      
      
     
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  }; 


  $scope.EditRestockReport = function (size,report) {
    

    var modalInstance = $modal.open({
      templateUrl: 'edit_restock_report.html',
      controller: 'EditRestockReport',
      size: size,
      resolve: {
        restock_report: function () {
          $scope.edit_restock_report = {
            placeholder:report,
            categories:[]
          }
          return $scope.edit_restock_report;
        }
          
       
      }
    });

    modalInstance.result.then(function () {

    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  };   
  
}])

.controller('EditRestockReport', function ($scope, $modalInstance, restock_report, Crud) {
    $scope.restock_report = restock_report

   
   $scope.selected = [];
   
   Crud.list("category_choices/?placeholder="+$scope.restock_report.placeholder.id, undefined)
       .then(function(category_choices_data){
          $scope.category_choices_data = category_choices_data
          console.log(category_choices_data)
          Crud.list("categories",undefined)
         .then(function(data){
             $scope.restock_report.categories = data.objects
             angular.forEach(category_choices_data.objects, function(category_choice, index){
              angular.forEach($scope.restock_report.categories, function(category,index ){
                if (category_choice.category.id===category.id){
                  $scope.selected.push(category)
                }
              })
           })
         
       });
        
   })

 




    var updateSelected = function (action, category) {
        if (action == 'add' & $scope.selected.indexOf(category) == -1)
          {
            $scope.selected.push(category);

            var place_holder = angular.copy($scope.restock_report.placeholder)
            delete place_holder.location

            
            var object = {
              place_holder:place_holder,
              category:category

            }
            Crud.save_new_object(object, "category_choices")
            .then(function(response){
              console.log(response)

            })
            
          }
        
        
        if (action == 'remove' && $scope.selected.indexOf(category) != -1) {

          $scope.selected.splice($scope.selected.indexOf(category), 1);
          angular.forEach($scope.category_choices_data.objects, function(data, index){
              if (data.category.id == category.id){
                Crud.delete_object_none_grid(data)
                .then(function(response){
                  var index = $scope.category_choices_data.objects.indexOf(data)
                  $scope.category_choices_data.objects.splice(index,1);

                })
                
              }
            })
        }

    }
    
    $scope.updateSelection = function ($event, category) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        updateSelected(action, category);
        
    };

    $scope.selectAll = function ($event) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        for (var i = 0; i < $scope.restock_report.categories.length; i++) {
            var category = $scope.restock_report.categories[i];
            updateSelected(action, category);
        }
    };

    $scope.getSelectedClass = function (category) {
        return $scope.isSelected(category) ? 'selected' : '';
    };

    $scope.isSelected = function (category) {
        return $scope.selected.indexOf(category) >= 0;
    };

    //something extra I couldn't resist adding :)
    $scope.isSelectedAll = function () {
        return ($scope.selected.length === $scope.restock_report.categories.length)
    };

    $scope.savePlaceholderName = function(){
      var object = angular.copy($scope.restock_report.placeholder)
      Crud.save_object(object)
      .then(function(data){
        console.log(data)
      })
    }
    







  
  $scope.ok = function () {



   
    $modalInstance.close();
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
})


.controller('CreateNewRestockReport', function ($scope, $modalInstance, new_restock_report, Crud) {
    $scope.new_restock_report = new_restock_report
   Crud.list("categories",undefined)
   .then(function(data){
     $scope.new_restock_report.categories = data.objects
     
   });



$scope.selected = [];

    var updateSelected = function (action, category) {
        if (action == 'add' & $scope.selected.indexOf(category) == -1) $scope.selected.push(category);
        if (action == 'remove' && $scope.selected.indexOf(category) != -1) $scope.selected.splice($scope.selected.indexOf(category), 1);
    }

    $scope.updateSelection = function ($event, category) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        updateSelected(action, category);
    };

    $scope.selectAll = function ($event) {
        var checkbox = $event.target;
        var action = (checkbox.checked ? 'add' : 'remove');
        for (var i = 0; i < $scope.new_restock_report.categories.length; i++) {
            var category = $scope.new_restock_report.categories[i];
            updateSelected(action, category);
        }
    };

    $scope.getSelectedClass = function (category) {
        return $scope.isSelected(category) ? 'selected' : '';
    };

    $scope.isSelected = function (category) {
        return $scope.selected.indexOf(category) >= 0;
    };

    //something extra I couldn't resist adding :)
    $scope.isSelectedAll = function () {
        return ($scope.selected.length === $scope.new_restock_report.categories.length)
    };

  
  $scope.ok = function () {

    $scope.new_restock_report.categories = $scope.selected

   
    $modalInstance.close($scope.new_restock_report);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
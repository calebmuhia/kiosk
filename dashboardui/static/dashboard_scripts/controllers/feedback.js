'use strict'

angular.module('dashboardui')

.controller('FeedbackCtr', ['$scope', '$rootScope','$http','$modal','$cookies',function($scope,$rootScope,$http, $modal,$cookies){
   
   $scope.get_feedback = function(){
    $scope.promise = $http.get('/dashboard/api/v1/feedback/')
    .then(function(response){
        $scope.msgs = response.data.objects
        $scope.meta = response.data.meta
    })


   }

   $scope.get_feedback()

   $scope.viewfeedback = function(feedback){
    var modalInstance = $modal.open({
         templateUrl: 'viewfeedback.html',
         controller: 'ViewFeedback',
         size: 'lg',
         resolve: {
           feedbackobject:function(){
               return feedback
           }
           
         }
       });
   
       modalInstance.result.then(function (object) {
        object.archived = true
        object['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        $http.put(object.resource_uri, object)
       
       }, function () {
         console.log('Modal dismissed at: ' + new Date());
       });

   }

}])

.controller('ViewFeedback', ['$scope','feedbackobject','$modalInstance', function($scope, feedbackobject,$modalInstance){

    $scope.msg = feedbackobject

    $scope.ok = function(){
        $modalInstance.close($scope.msg)
    }
    
}])


'use strict'

angular.module("dashboardui")

.controller('IframeVideosCtrl', ['$scope','$sce', function($scope,$sce){

    $scope.locationList = [
    {id:$sce.trustAsResourceUrl('http://li701-9.members.linode.com:9141/dashboard/videos/'),name:'Grenville'},
    {id:$sce.trustAsResourceUrl('http://li701-9.members.linode.com:9010/dashboard/videos/'),name:'301'},
    {id:$sce.trustAsResourceUrl('http://li701-9.members.linode.com:9011/dashboard/videos/'),name:'302'},
    {id:$sce.trustAsResourceUrl('http://li701-9.members.linode.com:9014/dashboard/videos/'),name:'Albany'},
    {id:$sce.trustAsResourceUrl('http://li701-9.members.linode.com:9145/dashboard/videos/'),name:'501'},
    {id:$sce.trustAsResourceUrl('http://li701-9.members.linode.com:9142/dashboard/videos/'),name:'CentralWire'}
    ]
    $scope.selected_location = $scope.locationList[0] 
    
}])
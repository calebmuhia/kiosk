'use strict'

angular.module('dashboardui')

.controller('PayRollPaymentsCtrl', ['$scope','Crud','$filter', function($scope, Crud, $filter){

    $scope.get_data = function(){
        $scope.dataPromise = Crud.list('payrolls', undefined)
        .then(function(data){
            $scope.meta = data.meta;
            $scope.payments = data.objects
            console.log(data)

        })
    }
    $scope.get_data()


    $scope.csv_headers = [
                      {title:'Keytag',field:'username'},
                      {title:'First Name',field:'first_name'},
                      {title:'LastName',field:'last_name'},
                      {title:'email',field:'email'},          
                      {title:'Amount', field:"amount"}

                      ]
                  

    $scope.export_csv_list = function(){
            return Crud.list('users/payrollPaymentReport/', undefined)
        .then(function(data){
          console.log(data)
            var users = data
            angular.forEach(users, function(value, index){
              var expense = (parseFloat(value.account_expenses)+parseFloat(value.credit_card_expenses)+parseFloat(value.debit_card_expenses))
              var deposits = parseFloat(value.cash_deposits)+ parseFloat(value.credit_card_deposits)+ parseFloat(value.debit_card_deposits)+parseFloat(value.admin_deposits)
              var balance = deposits - expense - parseFloat(value.admin_deducts)
              value.amount =Math.abs(balance)
              console.log(deposits, expense, value.admin_deducts)
            })
            $scope.get_data()

            return users

        })
        
            
           
        };

    
}])
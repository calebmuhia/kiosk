'use strict'

angular.module('dashboardui')

.controller('DatetimeCtr', ['$scope', '$rootScope',function($scope,$rootScope){
	$scope.dateInit = function(today, lastweek){
		$scope.today = today
		$scope.lastweek = lastweek

	}



  $scope.open_today = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.today_opened = true;
    $scope.lastweek_opened = false;
  };
  $scope.open_lastweek = function($event) {
    $event.preventDefault();
    $event.stopPropagation();

    $scope.lastweek_opened = true;
    $scope.today_opened = false;

  };

  $scope.dateOptions = {
    formatYear: 'yy',
    startingDay: 1
  };

 


 

}]);
angular.module('dashboardui')
  .filter('weekdays', function() {


    return function(input) {

        var weekdays = {
        '1':"Monday",
        '2':'Tuesday',
        "3":"Wednesday",
        '4':"Thursday",
        '5':"Friday",
        '6':'Saturday',
        '7':'Sunday'
    }
      var out = weekdays[input];
      return out;
    };
  })

  .filter('hoursfilter', function() {


    return function(input) {
              
      var out = ("0" + input).slice(-2);  
      out = out+':00'
        
      return out;
    };
  })

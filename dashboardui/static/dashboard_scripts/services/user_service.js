'use strict'

angular.module('dashboardui')

.factory('UserService', ["$http",'$q','$cookies', function($http, $q, $cookies){

    var exports = {}
    

    exports.user_list = function(limit){
        return $http.get('/dashboard/api/v1/users/?limit='+limit)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )
    };

    exports.search_keytag_user = function(usersToSearch){
        return $http.get('/dashboard/api/v1/users/search/?q='+usersToSearch)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )
    };
    

    
    exports.next_previous = function(url){
        return $http.get(url)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }
    exports.save_user = function(row){

        row['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(row)

        return $http.put(row.resource_uri, row)
        .then(function(response) {
                        if (typeof response.data === 'object') {
                            console.log(response)
                            return response.data;
                        } else {
                            // invalid response
                            console.log(response)
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }


    exports.save_new_user = function(data){

        data['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        

        return $http.post('/dashboard/api/v1/users/', data)
        .then(function(response) {
                        if (typeof response.data === 'object') {
                            console.log(response)
                            return response.data;
                        } else {
                            // invalid response
                            console.log(response)
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }


    exports.delete_user = function(row){

        row['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(row.entity)

        return $http.delete(row.entity.resource_uri)
        .then(function(response) {
            
                        if (response.status === 204 ) {
                            
                            return response.data;
                        } else if (response.status === 404) {
                            // invalid response
                            
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    } 
    exports.save_payment = function(user, value){

       return $http.get('/dashboard/api/v1/users/pay_by_payroll/'+user+'/?value='+value)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }

    exports.increase_user_balance = function(user, value){

       return $http.get('/dashboard/api/v1/users/increase_balance/'+user+'/?value='+value)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }


    exports.decrease_user_balance = function(user, value){

       return $http.get('/dashboard/api/v1/users/decrease_balance/'+user+'/?value='+value)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }
    exports.upload_user_csv = function(file){

       return $http.uploadFile({
              url:'/dashboard/users/upload_users_csv/',
              file:file        
       })
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }



    return exports


}])

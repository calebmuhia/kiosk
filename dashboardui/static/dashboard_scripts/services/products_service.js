'use strict'

angular.module('dashboardui')

.factory('ProductService', ["$http",'$q','$cookies', function($http, $q, $cookies){

    var exports = {}

    exports.products_list = function(limit){
        return $http.get('/dashboard/api/v1/barcodeproduct/?limit='+limit)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )
    };
    exports.search_barcode_product = function(productToSearch){
        return $http.get('/dashboard/api/v1/barcodeproduct/search/?q='+productToSearch)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )
    };

    exports.search_nobarcode_product = function(productToSearch){
        return $http.get('/dashboard/api/v1/nobarcodeproduct/search/?q='+productToSearch)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )
    };
    exports.products_list_no_barcode = function(limit){
        return $http.get('/dashboard/api/v1/nobarcodeproduct/?limit='+limit)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )
    };

    
    exports.next_previous = function(url){
        return $http.get(url)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }
    exports.save_product = function(row){

        row['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(row)

        return $http.put(row.resource_uri, row)
        .then(function(response) {
                        if (typeof response.data === 'object') {
                            console.log(response)
                            return response.data;
                        } else {
                            // invalid response
                            console.log(response)
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }

    exports.save_new_product = function(data){

        data['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(data)

        return $http.post("/dashboard/api/v1/barcodeproduct/", data)
        .then(function(response) {
                        if (typeof response.data === 'object') {
                            console.log(response)
                            return response.data;
                        } else {
                            // invalid response
                            console.log(response)
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }

    exports.deleteProduct = function(row){

        row['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(row.entity)

        return $http.delete(row.entity.resource_uri)
        .then(function(response) {
            
                        if (response.status === 204 ) {
                            
                            return response.data;
                        } else if (response.status === 404) {
                            // invalid response
                            
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    } 



    return exports


}])

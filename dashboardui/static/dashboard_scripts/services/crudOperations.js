'use strict'

angular.module('dashboardui')

.factory('Crud', ["$http", "$cookies", "$q","$cacheFactory", function($http, $cookies, $q, $cacheFactory){

    var exports = {}

    var api_prefix = '/dashboard/api/v1/'

    
    
    exports.list = function(url,sender){
        
        return $http.get(api_prefix+url)
            .then(function(data){
                if (sender!=undefined){
                    

                    if ($cacheFactory.get(sender) === undefined){
                    var cache = $cacheFactory(sender)
                angular.forEach(data.data.objects, function(val, index){
                    
                    cache.put(val.id, val.name)
                });
                }

                }
                
   
                return data.data
   
            },
            function(error){
                console.log(error)
            }
            )
    };
    exports.list_date = function(url,lastweek,today){
        
        return $http.get(api_prefix+url+'?lastweek='+lastweek+'&today='+today)
            .then(function(data){
                
                
   
                return data.data
   
            },
            function(error){
                console.log(error)
            }
            )
    };
    
    exports.next_previous = function(url){
        return $http.get(url)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }

    exports.save_object = function(row){

        row['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        
        // row.id = [-1]
        console.log(row.resource_uri)

        return $http.put(row.resource_uri, row)
        .then(function(response) {
                        if (typeof response.data === 'object') {
                            console.log(response)
                            return response.data;
                        } else {
                            // invalid response
                            console.log(response)
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }

    exports.save_new_object = function(data, url){

        data['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(data)

        return $http.post(api_prefix+url+'/', data)
        .then(function(response) {
                        if (typeof response.data === 'object') {
                            console.log(response)
                            return response.data;
                        } else {
                            // invalid response
                            console.log(response)
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }

    exports.save_new_object_batch = function(data, url){

        // data['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(data)

        return $http.patch(api_prefix+url+'/', data)
        .then(function(response) {
                        if (typeof response.data === 'object') {
                            console.log(response)
                            return response.data;
                        } else {
                            // invalid response
                            console.log(response)
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }

    exports.delete_object = function(row){

        row['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(row.entity)

        return $http.delete(row.entity.resource_uri)
        .then(function(response) {
            
                        if (response.status === 204 ) {
                            
                            return response.data;
                        } else if (response.status === 404) {
                            // invalid response
                            
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }
    exports.delete_object_none_grid = function(object){

        object['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        

        return $http.delete(object.resource_uri)
        .then(function(response) {
            
                        if (response.status === 204 ) {
                            
                            return response.data;
                        } else if (response.status === 404) {
                            // invalid response
                            
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }

    return exports


}])



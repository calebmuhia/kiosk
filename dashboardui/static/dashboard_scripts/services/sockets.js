'use strict'

angular.module('dashboardui')
.factory('CheckConnectionSocket', function (socketFactory) {



  var myIoSocket = io.connect('http://localhost/test',{
              transports: ['xhr-polling', 'jsonp-polling']
            });

  var socket = socketFactory({
    ioSocket: myIoSocket
  });
  socket.forward('test-message')
  

  return socket;
});


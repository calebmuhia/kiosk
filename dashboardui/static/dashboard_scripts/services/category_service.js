'use strict'

angular.module('dashboardui')

.factory('CategoryService', ["$http", "$cookies", "$q","$cacheFactory", function($http, $cookies, $q, $cacheFactory){

    var exports = {}

    
    exports.category_list = function(){
        return $http.get('/dashboard/api/v1/categories/')
            .then(function(data){

                

                if ($cacheFactory.get("categories") === undefined){
                    var cache = $cacheFactory("categories")
                angular.forEach(data.data.objects, function(val, index){
                    
                    cache.put(val.id, val.name)
                });
                }
                   
                return data.data

                
            },
            function(error){
                console.log(error)
            }
            )
    };
    
    exports.next_previous = function(url){
        return $http.get(url)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }

    exports.save_category = function(row){

        row['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        
        // row.id = [-1]

        return $http.put(row.resource_uri, row)
        .then(function(response) {
                        if (typeof response.data === 'object') {
                            console.log(response)
                            return response.data;
                        } else {
                            // invalid response
                            console.log(response)
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }

    exports.save_new_category = function(data){

        data['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(data)

        return $http.post("/dashboard/api/v1/categories/", data)
        .then(function(response) {
                        if (typeof response.data === 'object') {
                            console.log(response)
                            return response.data;
                        } else {
                            // invalid response
                            console.log(response)
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }

    exports.deleteCategory = function(row){

        row['csrfmiddlewaretoken'] = $cookies['csrftoken'];
        console.log(row.entity)

        return $http.delete(row.entity.resource_uri)
        .then(function(response) {
            
                        if (response.status === 204 ) {
                            
                            return response.data;
                        } else if (response.status === 404) {
                            // invalid response
                            
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        console.log(response)
                        return $q.reject(response.data);
                    });

    }

    return exports


}])



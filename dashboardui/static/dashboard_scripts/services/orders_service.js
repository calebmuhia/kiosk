'use strict'

angular.module('dashboardui')

.factory('OrderService', ["$http", function($http){

    var exports = {}

    exports.group_by_date = function(date1, date2){

        return $http.get('/dashboard/api/v1/orders/group_by_date/?lastweek='+date1+'&today='+date2)
            .then(function(data){
                console.log(data.data)
             
                
                return data.data
            },
            function(error){
                return 'error'
            }
            )

    } 

    exports.sales = function(date1, date2){

        return $http.get('/dashboard/api/v1/orders/sales/?lastweek='+date1+'&today='+date2)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    };

    exports.user_order_list = function(limit, username){
        return $http.get('/dashboard/api/v1/orders/?limit='+limit+'&username='+username)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )
    };
    exports.order_list = function(limit, date2, date1){
        return $http.get('/dashboard/api/v1/orders/?limit='+limit+'&date1='+date1+'&date2='+date2)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )
    };
    exports.get_orders_latest = function(){
        return $http.get('/dashboard/api/v1/orders/?limit=5')
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )


    }
    exports.next_previous = function(url){
        return $http.get(url)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }

    return exports


}])
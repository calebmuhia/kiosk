'use strict'

angular.module('dashboardui')

.factory('CashFlowService', ["$http", function($http){

    var exports = {}

    exports.cashflowlist = function(limit,date2, date1){
        return $http.get('/dashboard/api/v1/usercashflow/?limit='+limit+'&date1='+date1+'&date2='+date2)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }
    exports.usercashflowlist = function(limit,username){
        return $http.get('/dashboard/api/v1/usercashflow/?limit='+limit+'&username='+username)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }

    exports.next_previous = function(url){
        return $http.get(url)
            .then(function(data){
             
                
                return data.data
                
            },
            function(error){
                console.log(error)
            }
            )

    }

    return exports


}])
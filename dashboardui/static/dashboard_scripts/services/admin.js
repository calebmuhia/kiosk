'use strict'

angular.module('dashboardui')

.factory('AdminService', ['$http','$q','$rootScope','$interval','$sce', function($http, $q,$rootScope, $interval,$sce){

    var AdminService = function(){
        this.locations = []
    }

    AdminService.prototype.set_locations = function(locations){
        this.locations = locations
        
    }

    AdminService.prototype.reboot = function(location){
        console.log('rebooting '+location)

        $http.get('/dashboard/admin/reboot/'+location)
        .then(function(data){
            
            if (data.data == 'null'){
                $rootScope.$broadcast('rebootMsg', 'error', location)
            }
            else{
                $rootScope.$broadcast('rebootMsg', 'ok', location)
            }

        })

    }
    var get_services_status = function(){
        var url = 'http://104.236.23.19:4567/_status/_hostgroup/ubuntu-servers/_service';
        $http({
    method: 'GET',
    url: url,
    headers: {'Content-Type':'text/json'} //or whatever type
 })
            .success(function(data){
                $rootScope.$broadcast('service_status', data)
                
            })


        

    }

    var get_status =function(){
        var url = 'http://104.236.23.19:4567/_status/_state';
        $http({
    method: 'GET',
    url: url,
    headers: {'Content-Type':'text/json'} //or whatever type
 })
            .success(function(data){
                $rootScope.$broadcast('status', data)
            })


        

    }

    AdminService.prototype.get_status = function(){
        get_status()
        get_services_status()
        $interval(function(){
            get_status()
            get_services_status()
        }, 30000)
        
    }

    return AdminService
    
}])
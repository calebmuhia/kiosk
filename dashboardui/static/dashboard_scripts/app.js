'use strict'

angular.module('dashboardui', [
    'ngSanitize',
    
    'ngResource',
    'ngRoute',
    'ngCookies',
    
    'ui.bootstrap',
    // 'toaster',
    'ui.grid',
    'ui.grid.resizeColumns',
    'ui.grid.edit',
     'ui.grid.rowEdit', 
     'ui.grid.cellNav',
     'ngCsv',
     'btford.socket-io',
     'angularFileUpload',
     'cgBusy',
     'toaster',
     "com.2fdevs.videogular",
    "com.2fdevs.videogular.plugins.controls",
    "com.2fdevs.videogular.plugins.overlayplay",
    "com.2fdevs.videogular.plugins.poster"
     
    ])
.value("NewBarcodeProduct", {
    name:'',
    barcode:'',
    quantity:0,
    category:null,
    supplier:null,
    unit_price:0.00,
    unit_cost:0.00,
    active:true,
    hst:false,
    pst:false,
    min_stock:2,
    max_stock:6,

})
.value("NewUser", {
    username:'',
    first_name:'',
    last_name:'',
    email:'',
    

})

.config(['$resourceProvider', '$interpolateProvider','$httpProvider', function ($resourceProvider, $interpolateProvider, $httpProvider) {
       // Don't strip trailing slashes from calculated URLs
       $resourceProvider.defaults.stripTrailingSlashes = false;
       $httpProvider.defaults.useXDomain = true;

      //Remove the header used to identify ajax call  that would prevent CORS from working
      delete $httpProvider.defaults.headers.common['X-Requested-With'];

     }])
.run( function run( $http, $cookies,$anchorScroll  ){

    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    $http.defaults.headers.put['X-CSRFToken'] = $cookies['csrftoken'];

    
})

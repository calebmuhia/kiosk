'use strict'

angular.module('dashboardui')

.directive('chart', function(){
    return{
        restrict: 'EA',
        link: function(scope, elem, attrs){

            var chart = null,
                // setup plot
		    options = {
		        xaxis: {
          mode: "time",
          minTickSize: [1, "day"]
      },
      yaxis: {
         min: 0
      },
		        colors: ['#8bceeb'],
		        series: {
		            lines: {
		                lineWidth: 1,
		                fill: true,
		                fillColor: {
		                    colors: [{
		                        opacity: 0.5
		                    }, {
		                        opacity: 0.2
		                    }]
		                },
		                steps: false
		
		            }
		        }
		    };
		
		    
                    
            var data = scope[attrs.ngModel];  

                 
            
            // If the data changes somehow, update it in the chart
            scope.$watch('data', function(v){
            	
                 if(!chart){
                    chart = $.plot(elem, [v], options);;
                    elem.show();
                }else{
                    chart.setData([v]);
                    chart.setupGrid();
                    chart.draw();
                }
            });
        }
    };
});


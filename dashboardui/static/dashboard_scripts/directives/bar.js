'use strict'

angular.module('dashboardui')

.directive('barchart', function(){
    return{
        restrict: 'EA',
        link: function(scope, elem, attrs){

            var chart = null;
                // setup plot
            var options = {
                    grid: {
                        hoverable: true
                    },
                    tooltip: true,
                    tooltipOpts: {
                        //content: '%x - %y',
                        //dateFormat: '%b %y',
                        defaultTheme: false
                    },
                    xaxis: {
                        mode: "time",
                        minTickSize: [1, "day"]
                    },
                    yaxes: {
                        tickFormatter: function (val, axis) {
                            return "$" + val;
                        },
                        max: 1200
                    },

        
                };

            // If the data changes somehow, update it in the chart
            scope.$watch('data', function(v){
                var data = [{
                    label: "Sales Per Day",
                    data: v,
                    bars: {
                        show: true,
                        align: "center",
                        barWidth: 30 * 30 * 60 * 1000
                    }
                }]
                console.log(data)
                
                 if(!chart){
                    chart = $.plot(elem, data, options);;
                    elem.show();
                }else{
                    chart.setData(data);
                    chart.setupGrid();
                    chart.draw();
                }
            });
        }
    };
})

.directive('barChartQuantity', function(){
    return{
        restrict: 'EA',
        link: function(scope, elem, attrs){

            
            var ticks = []
            for(var i=0; i<25; i++){
                ticks.push(i)
            }
                
            var chart = null;
                // setup plot
            var options = {
                    grid: {
                        hoverable: true
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '%y items',
                        //dateFormat: '%b %y',
                        defaultTheme: false
                    },
                    xaxis: {
                        ticks: ticks,
                        show:true,
                        min:0,
                        max:24,
                      
                        autoscaleMargin : 0.2,
                        title: 'Hours',

                        
                        tickFormatter: function(n){ return n+'hr'; }
                    },
                    yaxis : {
                        show:true,
                        min : 0,
                        autoscaleMargin : 0.1,
                        title: 'Products'
                    },
                    HtmlText: true,
                        legend: {
                            show:true,
                            position: 'nw'
                    },

                    
                    

        
                };

            // If the data changes somehow, update it in the chart
            scope.$watch('quantity_per_hour', function(v){
                console.log(v)
                var data = [{
                    title: 'Number of product bought against hour for date:',
                    label: "Sales Per hour",
                    data: v,
                    bars : {
                        show : true,
                        horizontal : false,
                        shadowSize : 0,
                        barWidth : 0.5
                    },
                    markers: {
                        show: true,
                        position: 'ct',
                        labelFormatter: function(obj){
                            return data[obj.index][1]+" items";
                        }
                    },
                    mouse : {
                        track : true,
                        relative : true
                    },
                }]
                
                
                 if(!chart){
                    chart = $.plot(elem, data, options);;
                    elem.show();
                }else{
                    chart.setData(data);
                    chart.setupGrid();
                    chart.draw();
                }
            });
        }
    };
})

.directive('barChartAmounts', function(){
    return{
        restrict: 'EA',
        link: function(scope, elem, attrs){

            
            var ticks = []
            for(var i=0; i<25; i++){
                ticks.push(i)
            }
                
            var chart = null;
                // setup plot
            var options = {
                    grid: {
                        hoverable: true
                    },
                    tooltip: true,
                    tooltipOpts: {
                        content: '$%y',
                        //dateFormat: '%b %y',
                        defaultTheme: false
                    },
                    xaxis: {
                        ticks: ticks,
                        show:true,
                        min:0,
                        max:24,
                      
                        autoscaleMargin : 0.2,
                        title: 'Hours',

                        
                        tickFormatter: function(n){ return n+'hr'; }
                    },
                    yaxis : {
                        show:true,
                        min : 0,
                        autoscaleMargin : 0.1,
                        title: 'Products'
                    },
                    HtmlText: true,
                        legend: {
                            show:true,
                            position: 'nw'
                    },

                    
                    

        
                };

            // If the data changes somehow, update it in the chart
            scope.$watch('amount_per_hour', function(v){
                console.log(v)
                var data = [{
                    title: 'Amount in $ of product bought each hour for date:',
                    label: "Sales Per hour",
                    data: v,
                    bars : {
                        show : true,
                        horizontal : false,
                        shadowSize : 0,
                        barWidth : 0.5
                    },
                    markers: {
                        show: true,
                        position: 'ct',
                        labelFormatter: function(obj){
                            return data[obj.index][1]+" items";
                        }
                    },
                    mouse : {
                        track : true,
                        relative : true
                    },
                }]
                
                
                 if(!chart){
                    chart = $.plot(elem, data, options);;
                    elem.show();
                }else{
                    chart.setData(data);
                    chart.setupGrid();
                    chart.draw();
                }
            });
        }
    };
});


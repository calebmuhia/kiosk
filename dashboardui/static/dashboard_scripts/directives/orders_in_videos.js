'use strict'

angular.module('dashboardui')
.directive('orderInVideo', ['$http', function($http){
    // Runs during compile
    return {
        // name: '',
        // priority: 1,
        // terminal: true,
        scope: {video:'=video'}, // {} = isolate, true = child, false/undefined = no change
        // controller: function($scope, $element, $attrs, $transclude) {},
        // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
        restrict: 'E', // E = Element, A = Attribute, C = Class, M = Comment
        // template: '',
        templateUrl: '/static/views/order_in_video.html',
        // replace: true,
        transclude: true,
        // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
        link: function(scope, iElm, iAttrs, controller) {
            

            scope.$watch('video', function(video){
                $http.get('/dashboard/videos/get_orders_in_video/?event_id='+video)
                .then(function(data){
                    console.log(data)
                    scope.orders = data.data
                })
            })
            
        }
    };
}]);
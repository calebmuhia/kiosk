'use strict'
angular.module("dashboardui")
.directive('ngUnique', ['$http', function (async) {
  return {
    require: 'ngModel',
    link: function (scope, elem, attrs, ctrl) {
      elem.on('keyup', function (evt) {
        scope.$apply(function () {
          var val = elem.val();
          var req = { "username":val}
          var ajaxConfiguration = { method: 'GET', url: '/dashboard/api/v1/barcodeproduct/barcode_exists/?q='+val };
          async(ajaxConfiguration)
            .success(function(data, status, headers, config) {
              ctrl.$setValidity('unique', data.status);
              

            });
          });
        });
      }
    }
  }
])

.directive('ngUniqueUser', ['$http', function (async) {
  return {
    require: 'ngModel',
    link: function (scope, elem, attrs, ctrl) {
      elem.on('keyup', function (evt) {
        scope.$apply(function () {
          var val = elem.val();
          
          var ajaxConfiguration = { method: 'GET', url: '/dashboard/api/v1/users/user_exists/?q='+val };
          async(ajaxConfiguration)
            .success(function(data, status, headers, config) {
              ctrl.$setValidity('unique', data.status);
              

            });
          });
        });
      }
    }
  }
])

.directive('ngUniqueCategoryOption', ['$http', function (async) {
  return {
    require: 'ngModel',
    scope: {ngModel:'='},
    link: function (scope, elem, attrs, ctrl) {

      elem.on('change', function (evt) {
        scope.$apply(function () {
          var val = scope.ngModel;
          var ajaxConfiguration = { method: 'GET', url: '/dashboard/api/v1/categories_options/category_option_exists/?q='+val.id };
          async(ajaxConfiguration)
            .success(function(data, status, headers, config) {
              console.log(data.status)
              ctrl.$setValidity('unique', data.status);

            });
          });
        });
      }
    }
  }
])

.directive('correctFileType', ['$http', function (async) {
  return {
    require: 'ngModel',
    scope: {ngModel:'='},
    link: function (scope, elem, attrs, ctrl) {
      var value = attrs.ngModel
      console.log(value)

      elem.on('change', function (evt) {
        scope.$apply(function () {
          var val = elem.val();
          var found = val.match(/\.(jpg|jpeg|png|gif)$/)
          console.log(found, val)
          if(!val.match(/\.(jpg|jpeg|png|gif)$/)){
            console.log("file is not image")
            ctrl.$setValidity('is_image', false);

          }
          else{
            ctrl.$setValidity('is_image', true);

          }

          

          
          });
        });
      }
    }
  }
])



'use strict'

angular.module('dashboardui')

.directive('piechart', function(){
    return{
        restrict: 'EA',
        require:'ngModel',
        link: function(scope, elem, attrs, ngModel){

            var chart = null,
                // setup plot
            options = {
                colors: ["#be8181", "#81ad81", "#a37bce", "#5dc2ed", "#d0de5d","#be8181", "#81ad81", "#a37bce", "#5dc2ed", "#d0de5d"],
                series: {
                    pie: {
                        show: true,
                        label: {
                            show: false,
                            formatter: function(label, series){
                                return '<div style="font-size:10px">'+label+'%</div>';
                            },
                            threshold: 0.1
                        }
                    }
                },
                legend: {
                    show: true,
                    noColumns: 1, // number of colums in legend table
                    labelFormatter: null, // fn: string -> string
                    labelBoxBorderColor: "#888", // border color for the little label boxes
                    container: null, // container (as jQuery object) to put legend in, null means default on top of graph
                    position: "ne", // position of default legend container within plot
                    margin: [-200, 0], // distance from grid edge to default legend container within plot
                    backgroundOpacity: 1// set to 0 to avoid background
                },
                grid: {
                    hoverable: true,
                    clickable: true
                }
            };
        
            
                    
            var data = scope[attrs.ngModel];  
            console.log(data)
// var data = [
//     { label: "IE",  data: 19.5, color: "#4572A7"},
//     { label: "Safari",  data: 4.5, color: "#80699B"},
//     { label: "Firefox",  data: 36.6, color: "#AA4643"},
//     { label: "Opera",  data: 2.3, color: "#3D96AE"},
//     { label: "Chrome",  data: 36.3, color: "#89A54E"},
//     { label: "Other",  data: 0.8, color: "#3D96AE"}
// ];
                 
            
            // If the data changes somehow, update it in the chart

            scope.$watch(attrs.ngModel, function(v){

                console.log(v)
                if (v != undefined){
                   if(!chart){
                    chart = $.plot(elem, v, options);;
                    elem.show();
                }else{
                    chart = $.plot(elem, v, options);;
                    elem.show();
                } 
                }

                
                 
            });
        }
    };
});

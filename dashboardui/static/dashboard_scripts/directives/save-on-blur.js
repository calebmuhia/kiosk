'use strict'
angular.module('dashboardui')
.directive('saveOnBlur', ['$http', function($http){

    return function(scope, element, attr){
        element.bind('blur', function(event){
            scope.$apply(attr.saveOnBlur)

        })
    }
}])
.directive('saveOnChange', ['$http', function($http){

    return function(scope, element, attr){
        element.bind('change', function(event){
            scope.$apply(attr.saveOnChange)

        })
    }
}])
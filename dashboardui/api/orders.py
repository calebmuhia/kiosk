from dashboardui.api import *
from shop.models import Order, OrderItem
from django.conf.urls import patterns, url,include

from itertools import groupby
import time
from datetime import datetime, date, timedelta
from django.db.models import Sum, Q
from kioskui.models import QuantityMovement, Tax, BarcodeProduct, KioskUser as User
from django.conf import settings
from django.utils.timezone  import make_naive, get_default_timezone
import sys

from decimal import Decimal
from datetime import datetime,timedelta

import logging
logger= logging.getLogger(__name__)

try:
    from zm.models import Monitors, Events
except:
    pass








def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class OrderResource(ModelResource):




    class Meta:
        queryset = Order.objects.all().order_by('-created')
        list_allowed_methods = ['get', ]
        detail_allowed_methods = ['get', ]

        resource_name = 'orders'
        
        filtering={
        'created':ALL_WITH_RELATIONS,
        'user':ALL_WITH_RELATIONS,
        'username':ALL,
        'date1':ALL,
        'date2':ALL,
        }

        

    def dehydrate(self, bundle):
        try:

            bundle.data['user'] = bundle.obj.user.username if bundle.obj.user else None
        except:
            bundle.data['user'] = None
            
        paymentmethod = bundle.obj.orderpayment_set.filter()[:1]
        if paymentmethod:
            paymentmethod = paymentmethod[0]
            bundle.data['payment_method'] = paymentmethod.payment_method
        else:
            bundle.data['payment_method'] = ''

        taxes = bundle.obj.extraorderpricefield_set.filter()
        

        for tax in taxes:
             bundle.data[tax.label] = tax.value

        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = ''  

        if getattr(settings, 'INCLUDE_ZM', False):
            try:
                camera4 = Monitors.objects.get(name__icontains = "Camera4")
                created_time = bundle.obj.created
                print created_time
                events = Events.objects.filter(starttime__range = [created_time-timedelta(seconds = 60), created_time+timedelta(seconds = 60)]).exists()

                if events:
                    print events
                    bundle.data['event'] = True
                else:
                    bundle.data['event'] = None   
            except Exception, e:
                print e
                bundle.data['event'] = None     



   
        return bundle   

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/group_by_date%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('group_by_date'), name="api_group_by_date"),
            url(r"^(?P<resource_name>%s)/sales%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('sales'), name="api_sales"),
            url(r"^(?P<resource_name>%s)/top_ten%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('topTenProductCategories'), name="api_top_ten"),
            url(r"^(?P<resource_name>%s)/order_details%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('order_details'), name="order_details"),
            url(r"^(?P<resource_name>%s)/financial_report%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('financial_report'), name="financial_report"),
            
            url(r"^(?P<resource_name>%s)/all_locations%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('all_locations'), name="api_all_locations"),
            url(r"^(?P<resource_name>%s)/top_products%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('top_products'), name="api_top_products"),
            url(r"^(?P<resource_name>%s)/daily_financial_report%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('daily_financial_report'), name="daily_financial_report"),
            url(r"^(?P<resource_name>%s)/product_profitability_report%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('productProfitabilityReport'), name="productProfitabilityReport"),
            url(r"^(?P<resource_name>%s)/user_profitability_report%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('UserProfitabilityReport'), name="userProfitabilityReport"),

        ]

    def group_by_date(self, request, **kwargs):

        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except Exception, e:
            print e
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        objects = []

        data = [(date(order.created.year, order.created.month, order.created.day),order.order_total)for order in Order.objects.filter(created__range=[lastweek,today]).filter(status=Order.COMPLETED).order_by('created')]
        
        objects = [[time.mktime(k.timetuple())*1000,reduce(lambda x,y:x+y,[item[1] for item in value])] for k,value in groupby(data, key=lambda x:x[0])] 

        self.log_throttled_access(request)
        return self.create_response(request, objects) 

    def topTenProductCategories(self, request, **kwargs):

        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        objects = {}
        products_objects = []
        categories_objects = []
        category_object = []
        
        
        


        data = QuantityMovement.objects.filter(
            Q(created__range = [lastweek, today]) & Q(status=QuantityMovement.product_bought)
            ).values('slug', 'product_name', 'amount', 'product_barcode')
        data = sorted(data, key=lambda x:x['slug'])

        for product, values in groupby(data, key=lambda x:x["slug"]):
            categories = Category.objects.filter(products__slug = product)

            for category in categories:
                
                categories_objects.append(category.name)
                    

            products_group = {}
            
            groups = list(values)
            products_objects.append({
            'label':groups[0]['product_name'],
            'data':int(reduce(lambda x,y:int(x)+int(y),[value['amount'] for value in groups]))
            })        
            

        categories = sorted(categories_objects, key=lambda x:x)

        try:

            for category_name, items in groupby(categories, key=lambda x:x):
                category_object.append({'label':category_name, 'data':len(list(items))})
        except Exception, e:
            print e 

        products_objects = sorted(products_objects, key=lambda x:x['data'], reverse=True)           
        category_object = sorted(category_object, key=lambda x:x['data'], reverse=True)   
        objects.update({
            'products':products_objects[:10],
            'categories':category_object[:10]
        })    

        self.log_throttled_access(request)
        return self.create_response(request, objects)     



    def sales(self, request, **kwargs):
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        objects = {}    

        orders = Order.objects.filter(created__range=[lastweek,today]).filter(status=Order.COMPLETED).order_by('created')

        totals = orders.aggregate(order_total=Sum('order_total'),order_subtotal=Sum('order_subtotal'))
        taxes = orders.aggregate(taxes=Sum('extraorderpricefield__value'))['taxes']
        
        barcode_unit_cost = orders.aggregate(barcode_unit_cost=Sum('items__product__barcodeproduct__unit_cost'))['barcode_unit_cost']
        nobarcode_unit_cost=orders.aggregate(nobarcode_unit_cost=Sum('items__product__nobarcodeproduct__unit_cost'))['nobarcode_unit_cost']
        if not barcode_unit_cost:
            barcode_unit_cost = 0.00
        if not nobarcode_unit_cost:
            nobarcode_unit_cost= 0.00
        unit_cost = barcode_unit_cost+nobarcode_unit_cost    
            

        objects.update({
            'order_total':totals['order_total'],
            'order_subtotal':totals['order_subtotal'],
            'taxes':taxes,
            'unit_cost':unit_cost,
            'profits': float(totals['order_total'] if totals['order_total'] else 0 )-float(unit_cost)
            })

        self.log_throttled_access(request)
        return self.create_response(request, objects) 

    def order_details(self, request, **kwargs):
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        objects = []
        try:
            location = request.session['db'] 

        except:
            location = '' 


        orders = Order.objects.filter(created__range=[lastweek,today]).order_by('-created')
        
        for order in orders:

            card_number = ''
            try:

                payment_method = getattr(order.orderpayment_set.filter()[:1].get(), "payment_method", "" )

            except Exception as e:
                #print e

                payment_method = ''
            
            orderitems = OrderItem.objects.filter(order = order)
            
            taxes = Tax.objects.filter()
            tax_objects = {}
            for tax in taxes:
                tax_objects[tax.name] = tax.value


            for item in orderitems:
                result = {}
                print "order item"
                try:
                    categories = " ".join([category.name for category in item.product.categories.all()])
                except:
                    categories = ''    


                try:
                    result = {
                    'location':location,
                    'order_id':order.id,
                    'status':order.get_status_display(),
                    'created':order.created.strftime("%d/%m/%Y %I:%M %p"),
                    'barcode':getattr(item.product, "barcode", None),
                    'product_name':repr(item.product_name).replace("u'", "").replace("'", ""),
                    'category':categories,
                    'quantity':item.quantity,
                    'unit_cost':item.product.unit_cost,
                    'total_cost':item.product.unit_cost*item.quantity,
                    'total_price':item.line_total,
                    'unit_price':item.product.unit_price,
                    'gst':str(tax_objects['HST'] if item.product.hst else 0),
                    'pst':str(tax_objects['PST'] if item.product.pst else 0),
                    'payment_method':payment_method,
                    }
                except Exception, e:
                    print e
                    
                    continue
                objects.append(result)    

        self.log_throttled_access(request)
        return self.create_response(request, objects)   

    def financial_report(self, request, **kwargs):
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        try:
            location = request.session['db'] 

        except:
            location = '' 

        objects = {}     


        orders = Order.objects.filter(created__range=[lastweek,today]).order_by('-created').filter(status=Order.COMPLETED)

        order_credit = orders.filter(orderpayment__payment_method='Debit CARD')
        print 'orders', order_credit
        order_account = orders.filter(orderpayment__payment_method='My Account')

        credit_sales_without_tax =  order_credit.aggregate(Sum("order_subtotal"))["order_subtotal__sum"]
        credit_sales_with_tax =  order_credit.aggregate(Sum("order_total"))["order_total__sum"]

        account_sales_without_tax =  order_account.aggregate(Sum("order_subtotal"))["order_subtotal__sum"]
        account_sales_with_tax =  order_account.aggregate(Sum("order_total"))["order_total__sum"]

        orderItemsCredit_count = OrderItem.objects.filter(order__in = [order.id for order in order_credit ]).aggregate(Sum("quantity"))["quantity__sum"]
        orderItemsAccount_count = OrderItem.objects.filter(order__in = [order.id for order in order_account ]).aggregate(Sum("quantity"))["quantity__sum"]
        credit_transactions = OrderItem.objects.filter(order__in = [order.id for order in order_credit ]).count()
        account_transactions = OrderItem.objects.filter(order__in = [order.id for order in order_account ]).count()



        objects["credit_sales_without_tax"] = credit_sales_without_tax
        objects["credit_sales_with_tax"] = credit_sales_with_tax
        objects["account_sales_without_tax"] = account_sales_without_tax
        objects["account_sales_with_tax"] = account_sales_with_tax
        objects["orderItemsCredit_count"] = orderItemsCredit_count
        objects["orderItemsAccount_count"] = orderItemsAccount_count
        objects["credit_transactions"] = credit_transactions
        objects["account_transactions"] =  account_transactions
        objects["total_sales"] = orders.aggregate(Sum("order_total"))["order_total__sum"]
        objects["total_sales_witout_tax"] = orders.aggregate(Sum("order_subtotal"))["order_subtotal__sum"]
  
        self.log_throttled_access(request)
        return self.create_response(request, objects)

    def all_locations(self, request, **kwargs): 
    
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        databases =  getattr(settings, "DATABASES", "")
        if "All" in databases:
            del databases["All"]
        else:
            pass

               
        database_list = [db_name for db_name in databases]

        if "default" in database_list:
            del database_list[database_list.index("default")]

        if "Demo" in database_list:
            del database_list[database_list.index("Demo")]    

        if "zm_db" in database_list:
            del database_list[database_list.index("zm_db")]    


        objects = [] 


        
        for db in database_list:

            try:
                json_responses = []
                location_object = {}

                total_qty = BarcodeProduct.objects.using(db).aggregate(Sum("quantity"))["quantity__sum"]
                total_max = BarcodeProduct.objects.using(db).aggregate(Sum("max_stock"))["max_stock__sum"]
                
                orders = Order.objects.using(db).filter(status=Order.COMPLETED).filter(created__range=[lastweek, today]).order_by('created')

                data = [(date(order.created.year, order.created.month, order.created.day),order.order_total) for order in orders]
        
                graph = [str(int(reduce(lambda x,y:x+y,[item[1] for item in value]))) for k,value in groupby(data, key=lambda x:x[0])] 

                
                totals = orders.aggregate(order_total=Sum('order_total'),order_subtotal=Sum('order_subtotal'))
                taxes = orders.aggregate(taxes=Sum('extraorderpricefield__value'))['taxes']
        
                barcode_unit_cost = orders.aggregate(barcode_unit_cost=Sum('items__product__barcodeproduct__unit_cost'))['barcode_unit_cost']
                nobarcode_unit_cost= orders.aggregate(nobarcode_unit_cost=Sum('items__product__nobarcodeproduct__unit_cost'))['nobarcode_unit_cost']
                if not barcode_unit_cost:
                    barcode_unit_cost = 0.00
                if not nobarcode_unit_cost:
                    nobarcode_unit_cost= 0.00
                unit_cost = barcode_unit_cost+nobarcode_unit_cost  

                percent_stock =total_qty*100/total_max  
            

                location_object[db] = {
                    'order_total':totals['order_total'] if totals['order_total'] else 0.00,
                    'order_subtotal':totals['order_subtotal'] if totals['order_subtotal'] else 0.00,
                    'taxes':taxes if taxes else 0.00,
                    'unit_cost':unit_cost,
                    'profits': float(totals['order_total'] if totals['order_total'] else 0 )-float(unit_cost),
                    'percent_stock':percent_stock,
                    "graph":','.join(graph),

                    }
                    
                objects.append(location_object)    



            except Exception, e:
                raise

        self.log_throttled_access(request)
        return self.create_response(request, objects)  


    def top_products(self, request, **kwargs):
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        databases =  getattr(settings, "DATABASES", "")

        if "All" in databases:
            del databases["All"]
        else:
            pass

        

        database_list = [db_name for db_name in databases]

        if "default" in database_list:
            del database_list[database_list.index("default")]

        if "Demo" in database_list:
            del database_list[database_list.index("Demo")]    

        if "zm_db" in database_list:
            del database_list[database_list.index("zm_db")]  
        objects = []

        for db in database_list:

            orders = Order.objects.using(db).filter(status=Order.COMPLETED).filter(created__range=[lastweek, today]).order_by('created')

            orderitems = OrderItem.objects.using(db).filter(order__in = orders).values('product_name','quantity')

            items = sorted(orderitems, key=lambda x:x['product_name'])

            group = [[key,reduce(lambda x,y:x+y,[item['quantity'] for item in value])] for key, value in groupby(items, lambda x:x["product_name"])]
            for item in group:
                objects.append(item)

        items = sorted(objects, key=lambda x:x[0])

        groups = [[key,reduce(lambda x,y:x+y,[item[1] for item in value])] for key, value in groupby(items, lambda x:x[0])]       
        items = sorted(groups, key=lambda x:x[1], reverse=True)
        clear_objects = []
        for item in items[:10]:
            clear_objects.append({'label':item[0],'data':item[1]})

        self.log_throttled_access(request)
        return self.create_response(request, clear_objects)

    def productProfitabilityReport(self, request, **kwargs):    
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        objects = []

        orders = Order.objects.filter(created__range = [lastweek, today]).filter(status = Order.COMPLETED)
        

        products = BarcodeProduct.objects.filter()

        for product in products:

            total_sales = 0
            unit_sale = 0
            sales_cost = 0

            orderItems = OrderItem.objects.filter(order__in = orders).filter(product_reference = product.id)

            for orderItem in orderItems:
                total_sales += orderItem.line_total
                unit_sale += orderItem.quantity


            sales_cost = unit_sale * product.unit_cost
            revenue = unit_sale * product.unit_price
            cogs = product.unit_cost * unit_sale
            profit_or_loss = revenue -Decimal(str(cogs))
            gross_margin = 0
            if profit_or_loss == 0:
                gross_margin = 0
            else:
                gross_margin = (100 * profit_or_loss ) / revenue




            objects.append({
                "barcode":product.barcode,
                "location":request.session["db"],
                "product_name":product.name,
                "unit_price" : product.unit_price,
                "unit_cost": product.unit_cost,
                "unit_sale": unit_sale,
                "total_sales":total_sales,
                "sales_cost" : sales_cost,
                "revenue":revenue,
                "cogs":cogs,
                "profit_or_loss":round(profit_or_loss, 2),
                "gross_margin":round(gross_margin, 4)
            })

        self.log_throttled_access(request)
        return self.create_response(request, objects)

    def UserProfitabilityReport(self, request, **kwargs):    
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request) 
        
        objects = []

        users = User.objects.filter().exclude(Q(first_name__icontains = "test") | Q(last_name__icontains = "test") | Q(username__icontains = "test"))
        user_details = {}




        for user in users:

            total_sales = 0
            unit_sale = 0
            sales_cost = 0
            list_id = []
            unit_price = Decimal('0.00')
            unit_cost = 0.00
            revenues = 0
            cogs = 0
            profit_or_loss = 0.00
            gross_margin = 0.00


            orderItems = OrderItem.objects.filter(order__in = [order.id for order in Order.objects.filter(user = user).filter(created__range =  [lastweek, today]).filter(status=Order.COMPLETED)])
            
            products_list = []

            for orderItem in orderItems:
                total_sales += orderItem.line_total
                unit_sale += orderItem.quantity

                try:
                    product = orderItem.product

                    unit_cost = product.unit_cost
                    unit_price = product.unit_price

                    sales_cost += unit_cost*orderItem.quantity

                    profit_or_loss += float(orderItem.line_total) - unit_cost

                except Exception, e:
                    exc_type, exc_value, exc_traceback = sys.exc_info()
                    print e, exc_traceback.tb_lineno
                    logger.debug(e, exc_traceback.tb_lineno)
                    continue

            if profit_or_loss == 0:
                pass
            else:
                gross_margin = (100 * profit_or_loss ) / float(total_sales)


            objects.append({
                "location":request.session["db"],
                        "customer_name":'%s %s' % (user.first_name, user.last_name),
                        "user_name":user.username,
                        "total_sales":total_sales,
                        "sales_cost" : round(sales_cost,2),
                        "profit_or_loss":round(profit_or_loss, 2),
                        "gross_margin":round(gross_margin, 4),
                        "unit_sale":unit_sale



                    })

        numbers = {}  
        for order in Order.objects.filter(user = None).filter(status = Order.COMPLETED).filter(created__range =  [lastweek, today]):


            total_sales = 0
            unit_sale = 0
            sales_cost = 0
            list_id = []
            unit_price = Decimal('0.00')
            unit_cost = 0.00
            revenues = 0
            cogs = 0
            profit_or_loss = 0.00
            gross_margin = 0.00
            number = None
            try:
                extrainfor = order.extra_info.get()

                lines = extrainfor.text.split("\n")

                for line in lines:
                    if "ssl_card_number" in line:
                        line = line.split("=")

                        number = line[1][-4:]
            except Exception, e:

                number = None

            print number
            if not number:
                continue

            else:
                
                
                orderItems = OrderItem.objects.filter(order = order)

                for orderItem in orderItems:
                    total_sales += orderItem.line_total
                    print "total_sales"
                    unit_sale += orderItem.quantity

                    try:
                        product = orderItem.product

                        unit_cost = product.unit_cost
                        unit_price = product.unit_price

                        sales_cost += unit_cost*orderItem.quantity

                        profit_or_loss += float(orderItem.line_total) - unit_cost

                    except Exception, e:
                        exc_type, exc_value, exc_traceback = sys.exc_info()
                        print e, exc_traceback.tb_lineno
                        logger.debug(e, exc_traceback.tb_lineno)

                        continue

                if profit_or_loss == 0:
                    pass
                else:
                    gross_margin = (100 * profit_or_loss ) / float(total_sales)
                if number in numbers:
                    


                    numbers[number]["total_sales"] += total_sales
                    numbers[number]["sales_cost"] += round(sales_cost,2)
                    numbers[number]["profit_or_loss"]+=round(profit_or_loss, 2)
                    numbers[number]["gross_margin"]+=round(gross_margin, 4)
                    numbers[number]["unit_sale"]+=unit_sale

                else:
                    numbers[number]= {
        
                        "total_sales":total_sales,
                        "sales_cost" : round(sales_cost,2),
                        "profit_or_loss":round(profit_or_loss, 2),
                        "gross_margin":round(gross_margin, 4),
                        "unit_sale":unit_sale
                    }

        for number in numbers:
            objects.append({
                "location":request.session["db"],
                "customer_name":None,
                "user_name":number,
                "total_sales":number["total_sales"],
                "sales_cost" : number["sales_cost"],
                "profit_or_loss":number["profit_or_loss"],
                "gross_margin":number["gross_margin"],
                "unit_sale":number["unit_sale"]
                })         


        self.log_throttled_access(request)
        return self.create_response(request, objects)             



    
    def daily_financial_report(self, request, **kwargs):    
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        data = []
        objects = []

        for order in Order.objects.filter(created__range=[lastweek,today]).filter(status=Order.COMPLETED).order_by('created'):
            order_date = date(order.created.year, order.created.month, order.created.day)
            taxes = order.extraorderpricefield_set.aggregate(value = Sum('value'))['value']
            barcode_unit_cost = order.items.aggregate(barcode_unit_cost=Sum('product__barcodeproduct__unit_cost'))['barcode_unit_cost']
            nobarcode_unit_cost = order.items.aggregate(nobarcode_unit_cost=Sum('product__nobarcodeproduct__unit_cost'))['nobarcode_unit_cost']
            
            if not barcode_unit_cost:
                barcode_unit_cost = 0.00
            if not nobarcode_unit_cost:
                nobarcode_unit_cost= 0.00
            if not taxes:
                taxes = 0.00

            unit_cost = barcode_unit_cost+nobarcode_unit_cost


            

            if not taxes:
                taxes = Decimal('0.00')

            data.append(
                {'date':order_date,
                'order_total':order.order_total,
                'order_subtotal':order.order_subtotal,
                'taxes':float(taxes),
                'unit_cost':unit_cost,
                'profits': float(order.order_total if order.order_total else 0 )-float(unit_cost)
                })



        for k,value in groupby(data, key=lambda x:x["date"]):
            values = list(value)
            

            try:
                
                total = reduce(lambda x,y:x+y,[item['order_total'] for item in values])
            except:
                total = 0.00    

            
            try:
                
                unit_cost = reduce(lambda x,y:x+y,[item['unit_cost'] for item in values])
            except:
                unit_cost = 0.00


            try: 
                 
                taxes = reduce(lambda x,y:float(x)+float(y),[item['taxes'] for item in values])
            except Exception, e:
                taxes = 0.00         

            try:    
                profits = reduce(lambda x,y:x+y,[item['profits'] for item in values])
            except:
                profits = 0.00

             

            print '\n'      

            objects.append({'date':k.strftime('%Y-%m-%d'),'total':total,'taxes':taxes,'unit_cost':unit_cost,'profits':profits}) 
        
        self.log_throttled_access(request)
        return self.create_response(request, objects)

    
    def build_filters(self, filters=None):
        """
        tag is not a proper field attr for image, to filter by tag, we need to 
        create a custom filter  
        """

        if filters is None:
            filters = {}

        orm_filters = super(OrderResource, self).build_filters(filters)

        if 'username' in filters:


            query = filters['username']
            # we will create a queryset for the tags using Q objects
            # https://docs.djangoproject.com/en/dev/topics/db/queries/#complex-lookups-with-q-objects
            qset = (Q(user__username=query))

            # update the filters
            orm_filters.update({'user': qset})

        if 'date1' in filters and 'date2' in filters:
            date1 = filters['date1']
            date2 = filters['date2']

            today = datetime.strptime(date1, "%Y-%m-%d")
            lastweek = datetime.strptime(date2, "%Y-%m-%d" )
            today = today+timedelta(hours=23)

            qset_date =(Q(created__range=[lastweek,today]))
            orm_filters.update({'dates': qset_date})    

       

        return orm_filters

    def apply_filters(self, request, applicable_filters):
        '''
          apply the custom filter
        '''

        if 'user' in applicable_filters:
            # pop out the custom filter
            custom = applicable_filters.pop('user')

        else:

            custom = None

        if "dates" in applicable_filters:
            dates = applicable_filters.pop('dates')

        else:
            dates = None        

            

        semi_filtered = super(OrderResource, self).apply_filters(
            request, applicable_filters)
        # print semi_filtered

        if custom:
            semi_filtered = semi_filtered.filter(custom).distinct()

        if dates:
            semi_filtered = semi_filtered.filter(dates).distinct()


           

        return semi_filtered  







             






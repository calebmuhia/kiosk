from kioskui.models import CashBox
from dashboardui.api import *

# Create your models here.
def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class CashBoxResource(ModelResource):


    class Meta:
        queryset = CashBox.objects.all().order_by('-created')
        list_allowed_methods = ['get', ]
        detail_allowed_methods = ['get', ]

        resource_name = 'cashbox'
        limit=0
        filtering={
        'emptied':ALL_WITH_RELATIONS

        }
    def dehydrate(self, bundle):
        bundle.data['emptied_by'] = bundle.obj.emptied_by.username if bundle.obj.emptied_by else None

        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = ''
            
        return bundle
        
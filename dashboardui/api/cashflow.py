from kioskui.models import UserCashFlow
from dashboardui.api import *
from django.db.models import Q 
from datetime import datetime, timedelta
# Create your models here.
def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class UserCashFlowResource(ModelResource):


    class Meta:
        queryset = UserCashFlow.objects.all().order_by('-date')
        list_allowed_methods = ['get', ]
        detail_allowed_methods = ['get', ]

        resource_name = 'usercashflow'
        limit=0
        filtering = {
        'date1': ALL,
        'date2':ALL,
        'username':ALL
        }

    def dehydrate(self, bundle):
        bundle.data["user"] = bundle.obj.user.username if bundle.obj.user else None
        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = '' 

        return bundle  

    def build_filters(self, filters=None):
        """
        tag is not a proper field attr for image, to filter by tag, we need to 
        create a custom filter  
        """

        if filters is None:
            filters = {}

        orm_filters = super(UserCashFlowResource, self).build_filters(filters)

        if 'username' in filters:

            query = filters['username']
            # we will create a queryset for the tags using Q objects
            # https://docs.djangoproject.com/en/dev/topics/db/queries/#complex-lookups-with-q-objects
            qset = (Q(user__username=query))

            # update the filters
            orm_filters.update({'user': qset})

        if 'date1' in filters and 'date2' in filters:
            date1 = filters['date1']
            date2 = filters['date2']

            today = datetime.strptime(date1, "%Y-%m-%d")
            lastweek = datetime.strptime(date2, "%Y-%m-%d" )
            today = today+timedelta(hours=23)

            qset_date =(Q(date__range=[lastweek,today]))
            orm_filters.update({'dates': qset_date, "date1":date1, "date2":date2})    

       

        return orm_filters

    def apply_filters(self, request, applicable_filters):
        '''
          apply the custom filter
        '''

        if 'user' in applicable_filters:
            # pop out the custom filter
            custom = applicable_filters.pop('user')

        else:

            custom = None

        if "dates" in applicable_filters:
            dates = applicable_filters.pop('dates')
            date1 = applicable_filters.pop('date1')
            date2 = applicable_filters.pop('date2')
            request.session['last_week'] = date2
            request.session['today'] = date1

        else:
            dates = None     

            

        semi_filtered = super(UserCashFlowResource, self).apply_filters(
            request, applicable_filters)
        # print semi_filtered

        if custom:
            semi_filtered = semi_filtered.filter(custom).distinct()


        if dates:
            semi_filtered = semi_filtered.filter(dates).distinct()    

           

        return semi_filtered      
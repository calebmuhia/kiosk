from zm.models import Events, Monitors
from dashboardui.api import *
from shop.models import Order, OrderItem
from datetime import datetime, timedelta


# Create your models here.
def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class EventsResource(ModelResource):


    class Meta:
        queryset = Events.objects.all().order_by('-starttime')
        list_allowed_methods = ['get', ]
        detail_allowed_methods = ['get', ]

        resource_name = 'events'
        limit=0
        filtering={
        'starttime':ALL_WITH_RELATIONS,
        'causes':ALL_WITH_RELATIONS

        }


    def dehydrate(self, bundle):
        try:
            bundle.data['camera'] = Monitors.objects.get(id=bundle.obj.monitorid).name
        except Exception, e:
            bundle.data['camera'] = None
        

        
        videosOrders = []
        try:
            orders = Order.objects.filter(created__range = [bundle.obj.starttime, bundle.obj.starttime+timedelta(minutes=1)])

            

            for order in orders:
                videosOrders.append({
                    'id':order.id,
                'time':order.created.strftime('%Y/%m/%d %H:%M:%S'),
                'user':order.user.username if order.user else None,
                'status': order.get_status_display(),
                'items':[item.product_name for item in OrderItem.objects.filter(order = order)]
                })
        except Exception, e:
            raise BadRequest(e)        

        bundle.data['orders'] = videosOrders
    
        return bundle


    def build_filters(self, filters=None):
        """
        tag is not a proper field attr for image, to filter by tag, we need to 
        create a custom filter  
        """

        if filters is None:
            filters = {}

        orm_filters = super(EventsResource, self).build_filters(filters)

        if 'causes' in filters:


            query = filters['causes']
            # we will create a queryset for the tags using Q objects
            # https://docs.djangoproject.com/en/dev/topics/db/queries/#complex-lookups-with-q-objects
            

            # update the filters
            orm_filters.update({'causes': query})

        print orm_filters    

       

        return orm_filters

    def apply_filters(self, request, applicable_filters):
        '''
          apply the custom filter
        '''

        if 'causes' in applicable_filters:
            # pop out the custom filter
            custom = applicable_filters.pop('causes')

        else:

            custom = None

            

        semi_filtered = super(EventsResource, self).apply_filters(
            request, applicable_filters)

        print semi_filtered
        # print semi_filtered

        

        if custom:
            print 'custom',custom
            if custom == 'all': 
                pass

            elif custom == 'no_orders':
                events = semi_filtered.filter(cause='door-trigger')
                filtered_events = []
                for event in events:
                    orders = Order.objects.filter(created__range=[event.starttime, event.starttime+timedelta(seconds = 60)]).exists()
                    if not orders:
                        filtered_events.append(event)
                semi_filtered = filtered_events 
                       
            elif custom == 'door-trigger': 
                print "this is the door-trigger"
                events = semi_filtered.filter(cause='door-trigger')
                semi_filtered = events

            
                    

           

        return semi_filtered    
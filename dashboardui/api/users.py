from dashboardui.api import *

from django.conf.urls import patterns, url,include

from itertools import groupby
import time
from datetime import datetime, date, timedelta
from django.db.models import Sum, Q
from shop.models import Product
from kioskui.models import KioskUser as User, UserCashFlow,PayRollPaymentReport,PayRollItem
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

import re
from decimal import Decimal

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:
        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]


def get_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.
    '''
    query = None # Query to search for every search term
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query  




def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class UserResource(ModelResource):



    class Meta:
        queryset = User.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put','delete' ]

        resource_name = 'users'
        limit=0
        excludes = ["password", "is_staff", 'is_superuser', ]
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('search_users'), name="api_search_user"),
            url(r"^(?P<resource_name>%s)/pay_by_payroll/(?P<user_id>\d+)/" % (self._meta.resource_name), self.wrap_view('save_payment'), name="api_pay_by_payroll"),
            url(r"^(?P<resource_name>%s)/increase_balance/(?P<user_id>\d+)/" % (self._meta.resource_name), self.wrap_view('increase_balance'), name="api_increase_balance"),
            url(r"^(?P<resource_name>%s)/decrease_balance/(?P<user_id>\d+)/" % (self._meta.resource_name), self.wrap_view('decrease_balance'), name="api_decrease_balance"),
            url(r"^(?P<resource_name>%s)/user_exists%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('user_exists'), name="api_user_exists"),
            url(r"^(?P<resource_name>%s)/upload_users_csv%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('uploadUsersCsv'), name="api_upload_users_csv"),
            url(r"^(?P<resource_name>%s)/payrollPaymentReport%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('payrollPaymentReport'), name="api_payrollPaymentReport"),
            url(r"^(?P<resource_name>%s)/user_total_balances%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('user_total_balances'), name="api_user_total_balances"),
            
            ]

    def increase_balance(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        value = request.GET.get('value', '')

        user_id = kwargs["user_id"]


        
        if value:
            try:
                user = User.objects.get(id = user_id)
                user.increment_admin_deposits(value)
                user.save()
                objects = {"status":"success","balance":user.balance, "total_deposits":user.total_deposits, 'total_deducts':user.admin_deducts}
                



            except Exception, e:
                print e
                objects = {"status":"failed"}

        else:
            objects = {"status":"failed"}


       

        

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list) 

    def decrease_balance(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        value = request.GET.get('value', '')

        user_id = kwargs["user_id"]


        
        if value:
            try:
                user = User.objects.get(id = user_id)
                user.increment_admin_deducts(value)
                user.save()
                objects = {"status":"success","balance":user.balance, "total_deposits":user.total_deposits, 'total_deducts':user.admin_deducts}
 
            except Exception, e:
                objects = {"status":"failed"}

        else:
            objects = {"status":"failed"}

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list) 

    def save_payment(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        value = request.GET.get('value', '')

        user_id = kwargs["user_id"]
        objects = {}

        if value:
            try:
                user = User.objects.get(id = user_id)
                user.pay_by_payroll = True if value == 'true' else False
                user.save()
                objects = {"status":"success"}
 
            except Exception, e:
                objects = {"status":"failed"}

        else:
            objects = {"status":"failed"}

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)     



    def search_users(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        q = request.GET.get('q', '')
        query = get_query(q, ["first_name","last_name", "username"])
        
        print query

        users = User.objects.filter(query)

        objects = []

        for result in users:
            bundle = self.build_bundle(obj=result, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list) 

    def obj_create(self, bundle, request=None, **kwargs):
        data = bundle.data
        try:

            bundle.obj = User.objects.create_user(data['username'], data['email'], data['username'])
            bundle.obj.first_name = data['first_name']
            bundle.obj.last_name = data['last_name']

            bundle.obj.save()

        except IntegrityError as e:
            print e
            raise BadRequest("bad request")

        return bundle   

    def user_exists(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        q = request.GET.get('q', '')
        

        users = User.objects.filter(username=q).exists()
        objects = {}

        if users:
            objects.update({"status":False})
        else:
            objects.update({"status":True})    

        self.log_throttled_access(request)
        return self.create_response(request, objects)

    def user_total_balances(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        
        

        users_balances = Decimal("0.00")
        for user in User.objects.filter():
            users_balances+=user.balance
        objects = {}

        
        objects.update({"balance":users_balances})
            

        self.log_throttled_access(request)
        return self.create_response(request, objects)    
    def dehydrate(self, bundle):
        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = ''   

        return bundle


    def payrollPaymentReport(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        q = request.GET.get('q', '')
        

        users = User.objects.filter()
        
        objects = []

        if users:

            payment = PayRollPaymentReport.objects.create()

            for result in users:
                if result.balance < 0:
                    item = PayRollItem.objects.create(
                        payment = payment,
                        user = result,
                        user_keytag = result.username,
                        amount = abs(result.balance)
                        )

                    bundle = self.build_bundle(obj=result, request=request)
                    bundle = self.full_dehydrate(bundle)
                    objects.append(bundle)

                    result.increment_admin_deposits(abs(result.balance)) 
                    result.save()

           

        self.log_throttled_access(request)
        return self.create_response(request, objects)             

class PayrollResource(ModelResource):
    class Meta:
        queryset = PayRollPaymentReport.objects.all().order_by("-created")
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put','delete' ]

        resource_name = 'payrolls'
        limit=0
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

    

                     


        
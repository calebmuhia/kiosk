from dashboardui.api import *
from kioskui.models import *
from django.conf.urls import patterns, url,include


from itertools import groupby
import time
from datetime import datetime, date, timedelta
from django.db.models import Sum, Q



def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class ChoicesPlaceHolderResource(ModelResource):

    class Meta:
        queryset = ChoicesPlaceHolder.objects.all().order_by('-created')
        list_allowed_methods = ['get', 'post', 'put', 'delete']
        detail_allowed_methods = ['get', 'post','put', 'delete']
        resource_name = 'choices_placeholder'
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

    def dehydrate(self, bundle):
        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = ''   

        return bundle    

class RestockCategoryChoiceResource(ModelResource):
    place_holder = fields.ForeignKey("dashboardui.api.inventory.ChoicesPlaceHolderResource", "place_holder")
    category = fields.ForeignKey("dashboardui.api.products.CategoryResource", "category", full=True)

    class Meta:
        queryset = RestockCategoryChoice.objects.all()
        list_allowed_methods = ['get', 'post', 'patch', 'put', 'delete']
        detail_allowed_methods = ['get', 'post', 'patch', 'put', 'delete']
        resource_name = 'category_choices'
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

        filtering={
        'placeholder':ALL
        }

    def build_filters(self, filters=None):
        """
        placeholder is not a proper field attr for category_choices, to filter by placeholder, we need to 
        create a custom filter  
        """

        if filters is None:
            filters = {}

        orm_filters = super(RestockCategoryChoiceResource, self).build_filters(filters)

        if 'placeholder' in filters:


            query = filters['placeholder']
            # we will create a queryset for the tags using Q objects
            # https://docs.djangoproject.com/en/dev/topics/db/queries/#complex-lookups-with-q-objects
            qset = (Q(place_holder__id=query))

            # update the filters
            orm_filters.update({'placeholder': qset})

       

        return orm_filters

    def apply_filters(self, request, applicable_filters):
        '''
          apply the custom filter
        '''

        if 'placeholder' in applicable_filters:
            # pop out the custom filter
            custom = applicable_filters.pop('placeholder')

        else:

            custom = None

            

        semi_filtered = super(RestockCategoryChoiceResource, self).apply_filters(
            request, applicable_filters)
        # print semi_filtered

        if custom:
            semi_filtered = semi_filtered.filter(custom).distinct()

        return semi_filtered
    def dehydrate(self, bundle):
        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = ''   

        return bundle         


class RestockInventoryReportResource(ModelResource):

    class Meta:
        queryset = RestockInventoryReport.objects.all().order_by('-created')
        list_allowed_methods = ['get', 'post', 'put', 'delete']
        detail_allowed_methods = ['get', 'post','put', 'delete']
        resource_name = 'restock_reports'
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True
    def dehydrate(self, bundle):
        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = ''   

        return bundle    

class RestockInventoryReportitemsResource(ModelResource):
    
    class Meta:
        queryset = RestockInventoryReportitems.objects.all()
        list_allowed_methods = ['get', 'post', 'patch', 'put', 'delete']
        detail_allowed_methods = ['get', 'post', 'patch', 'put', 'delete']
        resource_name = 'restock_reports_items'
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True
        limit = 0

        filtering={
        'restock_report':ALL
        }
        
    def dehydrate(self, bundle):
        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = ''   

        return bundle    

    def build_filters(self, filters=None):
        """
        placeholder is not a proper field attr for category_choices, to filter by placeholder, we need to 
        create a custom filter  
        """

        if filters is None:
            filters = {}

        orm_filters = super(RestockInventoryReportitemsResource, self).build_filters(filters)

        if 'restock_report' in filters:


            query = filters['restock_report']
            # we will create a queryset for the tags using Q objects
            # https://docs.djangoproject.com/en/dev/topics/db/queries/#complex-lookups-with-q-objects
            qset = (Q(report__id=query))

            # update the filters
            orm_filters.update({'restock_report': qset})

       

        return orm_filters

    def apply_filters(self, request, applicable_filters):
        '''
          apply the custom filter
        '''

        if 'restock_report' in applicable_filters:
            # pop out the custom filter
            custom = applicable_filters.pop('restock_report')

        else:

            custom = None

            

        semi_filtered = super(RestockInventoryReportitemsResource, self).apply_filters(
            request, applicable_filters)
        # print semi_filtered

        if custom:
            semi_filtered = semi_filtered.filter(custom).distinct()

        return semi_filtered             






from kioskui.models import Feedback
from dashboardui.api import *

# Create your models here.
def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None


class FeedbackResource(ModelResource):


    class Meta:
        queryset = Feedback.objects.all().order_by('-date')
        list_allowed_methods = ['get', 'put']
        detail_allowed_methods = ['get','put' ]
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT

        resource_name = 'feedback'
        limit=0
        filtering={
        'emptied':ALL_WITH_RELATIONS

        }
    def dehydrate(self, bundle):


        

        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = ''

        
            
        return bundle
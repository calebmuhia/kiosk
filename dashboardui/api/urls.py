from django.conf.urls import patterns, url,include
from dashboardui.api.orders import OrderResource
from dashboardui.api.cashbox import CashBoxResource
from dashboardui.api.products import ProductResource, BarcodeProductResource, NoBarcodeProductResource, ProductMovementResource,CategoryResource, SupplierResource, ExtraCategoryOptionsResource

from dashboardui.api.cashflow import UserCashFlowResource
from dashboardui.api.users import UserResource, PayrollResource
from dashboardui.api.inventory import ChoicesPlaceHolderResource, RestockCategoryChoiceResource, RestockInventoryReportResource, RestockInventoryReportitemsResource
from dashboardui.api.settings import TaxesResource, LocationResource
from dashboardui.api.videos import EventsResource
from dashboardui.api.feedback import FeedbackResource
from tastypie.api import Api
v1_api = Api(api_name='v1')
v1_api.register(OrderResource())
v1_api.register(CashBoxResource())
v1_api.register(ProductResource())
v1_api.register(BarcodeProductResource())
v1_api.register(NoBarcodeProductResource())
v1_api.register(ProductMovementResource())
v1_api.register(UserCashFlowResource())
v1_api.register(CategoryResource())
v1_api.register(SupplierResource())

v1_api.register(UserResource())
v1_api.register(ExtraCategoryOptionsResource())
v1_api.register(ChoicesPlaceHolderResource())
v1_api.register(RestockCategoryChoiceResource())
v1_api.register(RestockInventoryReportResource())
v1_api.register(RestockInventoryReportitemsResource())
v1_api.register(TaxesResource())
v1_api.register(LocationResource())
v1_api.register(PayrollResource())
v1_api.register(EventsResource())
v1_api.register(FeedbackResource())











urlpatterns = patterns('',
       url(r'^api/', include(v1_api.urls)),  
)
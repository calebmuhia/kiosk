from tastypie.authentication import Authentication, BasicAuthentication, ApiKeyAuthentication, MultiAuthentication, SessionAuthentication
from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie.exceptions import BadRequest
from tastypie import fields


from django.conf.urls import *
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404
#from haystack.query import SearchQuerySet
from tastypie.resources import ModelResource, Resource
from tastypie.utils import trailing_slash


from tastypie.constants import ALL, ALL_WITH_RELATIONS

from shop_simplecategories.models import Category


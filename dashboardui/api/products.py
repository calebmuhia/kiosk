from dashboardui.api import *

from django.conf.urls import patterns, url,include

from itertools import groupby
import time
from datetime import datetime, date, timedelta
from django.db.models import Sum, Q
from shop.models import Product
from kioskui.models import BarcodeProduct, NoBarcodeProduct, QuantityMovement, Supplier, ExtraCategoryFields
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned 
from shop_simplecategories.models import Category
import re
from db_routes.pin import *
import base64, os, json
from kiosk import settings
from shop.models import Order, OrderItem



def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class ProductResource(ModelResource):


    class Meta:
        queryset = Product.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put' ]

        resource_name = 'products'
        limit=0

def normalize_query(query_string,
                    findterms=re.compile(r'"([^"]+)"|(\S+)').findall,
                    normspace=re.compile(r'\s{2,}').sub):
    ''' Splits the query string in invidual keywords, getting rid of unecessary spaces
        and grouping quoted words together.
        Example:
        >>> normalize_query('  some random  words "with   quotes  " and   spaces')
        ['some', 'random', 'words', 'with quotes', 'and', 'spaces']
    '''
    return [normspace(' ', (t[0] or t[1]).strip()) for t in findterms(query_string)]


def get_query(query_string, search_fields):
    ''' Returns a query, that is a combination of Q objects. That combination
        aims to search keywords within a model by testing the given search fields.
    '''
    query = None # Query to search for every search term
    terms = normalize_query(query_string)
    for term in terms:
        or_query = None # Query to search for a given term in each field
        for field_name in search_fields:
            q = Q(**{"%s__icontains" % field_name: term})
            if or_query is None:
                or_query = q
            else:
                or_query = or_query | q
        if query is None:
            query = or_query
        else:
            query = query & or_query
    return query  


class BarcodeProductResource(ModelResource):


    class Meta:
        queryset = BarcodeProduct.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put', 'delete' ]

        resource_name = 'barcodeproduct'
        limit=0
        excludes=['bottle_deposit','case_qty', 'case_upc', 'demand', 'demand_two', 'last_modified', 'lead_time',] 
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('search_products'), name="api_search_products"),
            url(r"^(?P<resource_name>%s)/barcode_exists%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('barcode_exists'), name="api_barcode_exists"),
            url(r"^(?P<resource_name>%s)/handle_keypress%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('handle_keypress'), name="api_handle_keypress"),
            
            ]

    def barcode_exists(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        q = request.GET.get('q', '')
        

        products = BarcodeProduct.objects.filter(barcode=q).exists()
        objects = {}

        if products:
            objects.update({"status":False})
        else:
            objects.update({"status":True})    

        

        

        
        self.log_throttled_access(request)
        return self.create_response(request, objects) 

    def search_products(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        q = request.GET.get('q', '')
        query = get_query(q, ["name", "barcode"])
        print query

        products = BarcodeProduct.objects.filter(query)

        objects = []

        for result in products:
            bundle = self.build_bundle(obj=result, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list)
    def handle_keypress(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        q = _get_parameter(request, 'q')

        product_object = "Not a product"
        try:

            item = BarcodeProduct.objects.get(barcode=q)
            product_object = {
                "type": "PRODUCT",
                "name": item.get_name(),
                "item_id": item.id,
                "barcode":item.barcode
                }

        except:

            product_object = "Not a product"        

        self.log_throttled_access(request)
        return self.create_response(request, product_object)        

    def dehydrate(self, bundle):
        try:
            bundle.data['category'] = bundle.obj.categories.get().id
        except ObjectDoesNotExist:
            bundle.data['category'] = ''
        except MultipleObjectsReturned:
            category = bundle.obj.categories.filter()[0]
            print category
            bundle.obj.categories.clear()
            bundle.obj.categories.add(category)
            bundle.obj.save()
            bundle.data['category'] = bundle.obj.categories.get().id

        try:
            bundle.data["supplier"] = bundle.obj.supplier.id
        except:
            bundle.data["supplier"] = ''

        try:
            bundle.data['location'] = bundle.request.session['db'] 

        except:
            bundle.data['location'] = '' 

                 


        return bundle 

    def hydrate(self, bundle):
        category = bundle.data['category']
        if category:
            product = bundle.obj
            product.categories.clear()
            category = Category.objects.get(id = category)
            product.categories.add(category)

        supplier = bundle.data['supplier']
        if supplier:
            product = bundle.obj
            try:
                if supplier != product.supplier.id:
                    supplier = Supplier.objects.get(id = supplier)
                    product.supplier = supplier
                    product.save
            except:
                supplier = Supplier.objects.get(id = supplier)
                product.supplier = supplier
                product.save        



        return bundle  

    def obj_create(self, bundle, request=None, **kwargs):
        data = bundle.data
        current_location = bundle.request.session['db']
        try:
            for location in data['locations']:
                print location
                pinDb(location)

                bundle.obj = BarcodeProduct.objects.create(
                    name = data["name"],
                    barcode = data["barcode"],
                    quantity = data["quantity"],
                    unit_price = data["unit_price"],
                    unit_cost = data["unit_cost"],
                    min_stock = data["min_stock"],
                    max_stock = data["max_stock"],
                    hst = data["hst"],
                    pst = data["pst"],
                    active = data["active"]
                    )
                
                try:
                    category_name = data['category']['name']
                   
                    category = Category.objects.get(name = category_name)
                    print category
                    product.categories.add(category)
                except Exception, e:
                    print e
                    pass
                  

                 

        except Exception as e:
            print e
            raise BadRequest(e)   

        pinDb(current_location)      

        return bundle      






class NoBarcodeProductResource(ModelResource):


    class Meta:
        queryset = NoBarcodeProduct.objects.all()
        list_allowed_methods = ['get','post','put']
        detail_allowed_methods = ['get','post','put', 'delete' ]

        resource_name = 'nobarcodeproduct'
        limit=0
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

    def hydrate(self, bundle):
        category = bundle.data['category']
        if category:
            product = bundle.obj
            product.categories.clear()
            category = Category.objects.get(id = category)
            product.categories.add(category)


        return bundle  

    def dehydrate(self, bundle):
        try:
            bundle.data['category'] = bundle.obj.categories.get().id
        except ObjectDoesNotExist:
            bundle.data['category'] = ''
        except MultipleObjectsReturned:
            category = bundle.obj.categories.filter()[0]
            print category
            bundle.obj.categories.clear()
            bundle.obj.categories.add(category)
            bundle.obj.save()
            bundle.data['category'] = bundle.obj.categories.get().id

        return bundle
    def prepend_urls(self):    
        return [
            url(r"^(?P<resource_name>%s)/search%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('search_products'), name="api_search_no_barcode_products"),
            ]    

    def obj_create(self, bundle, request=None, **kwargs):
        data = bundle.data
        current_location = bundle.request.session['db']
        image = bundle.data["image"]
        ext = {
                'png':'data:image/png;base64,',
                'jpg':'data:image/jpeg;base64,'
            }

        fileext = 'jpg'

        if ext['png'] in image:
            image = image.replace("data:image/png;base64,", "")
            fileext = 'png'

        elif ext['jpg'] in image:
            image = image.replace("data:image/jpeg;base64,", "")
            fileext = 'jpg'
        image_path = os.path.join(settings.MEDIA_ROOT,'products_with_no_barcodes/{0}.{1}'.format(datetime.now().strftime('%d%m%Y%H%M'),fileext))    
        try:
            f = open(image_path, "wb")
            f.write(base64.decodestring(image))
            f.close()


        except Exception as e:
            print e
        try:
            for location in data['locations']:
                print location
                pinDb(location)

                bundle.obj = NoBarcodeProduct.objects.create(
                    name = data["name"],
                    quantity = data["quantity"],
                    unit_price = data["unit_price"],
                    unit_cost = data["unit_cost"], 
                    hst = data["hst"],
                    pst = data["pst"],
                    active = data["active"],
                    ordering = data["ordering"],
                    image=image_path.split('media/')[1],
                    )
                
                try:
                    category_name = data['category']['name']
                   
                    category = Category.objects.get(name = category_name)
                    print category
                    product.categories.add(category)
                except Exception, e:
                    print e
                    pass
                  

                 

        except Exception as e:
            print e
            raise BadRequest(e, data)   

        pinDb(current_location)      

        return bundle     

    def search_products(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        q = request.GET.get('q', '')
        query = get_query(q, ["name"])
        print query

        products = NoBarcodeProduct.objects.filter(query)

        objects = []

        for result in products:
            bundle = self.build_bundle(obj=result, request=request)
            bundle = self.full_dehydrate(bundle)
            objects.append(bundle)

        object_list = {
            'objects': objects,
        }

        self.log_throttled_access(request)
        return self.create_response(request, object_list) 

           
         

class CategoryResource(ModelResource):
    class Meta:
        queryset = Category.objects.all()
        list_allowed_methods = ['get','post','put', 'delete' ]
        detail_allowed_methods = ['get','post','put', 'delete' ]

        resource_name = 'categories'
        limit=0
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

class ExtraCategoryOptionsResource(ModelResource):
    category = fields.ForeignKey("dashboardui.api.products.CategoryResource","category", full=True  )
    class Meta:
        queryset = ExtraCategoryFields.objects.all()
        list_allowed_methods = ['get','post','put', 'delete' ]
        detail_allowed_methods = ['get','post','put', 'delete' ]

        resource_name = 'categories_options'
        limit=0
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/category_option_exists%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('category_option_exists'), name="api_category_option_exists"),
            
            ]    


    def category_option_exists(self, request, **kwargs):
        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        # Do the query
        q = request.GET.get('q', '')
        

        option = ExtraCategoryFields.objects.filter(category__id=q).exists()
        objects = {}

        if option:
            objects.update({"status":False})
        else:
            objects.update({"status":True})    

        self.log_throttled_access(request)
        return self.create_response(request, objects)   



class SupplierResource(ModelResource):
    class Meta:
        queryset = Supplier.objects.all()
        list_allowed_methods = ['get','post','put', 'delete' ]
        detail_allowed_methods = ['get','post','put', 'delete' ]

        resource_name = 'suppliers'
        limit=0
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True
    

class ProductMovementResource(ModelResource):


    class Meta:
        queryset = QuantityMovement.objects.all()
        list_allowed_methods = ['get','post','put' ]
        detail_allowed_methods = ['get','post','put' ]

        resource_name = 'productmovement'
        limit=0

        filtering = {
        'created':ALL,
        'status':ALL
        }

    def prepend_urls(self):
        return [
            url(r"^(?P<resource_name>%s)/products_sold%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('products_sold'), name="api_products_sold"),
            url(r"^(?P<resource_name>%s)/products_sold_per_day%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('products_sold_per_day'), name="api_products_sold"),
            url(r"^(?P<resource_name>%s)/inventory_quantity_movement%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('inventory_quantity_movement'), name="inventory_quantity_movement"),
            url(r"^(?P<resource_name>%s)/products_sold_per_hour%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('products_sold_per_hour'), name="products_sold_per_hour"),
            url(r"^(?P<resource_name>%s)/stock_take%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('stockTake'), name="stockTake"),
            url(r"^(?P<resource_name>%s)/stock_take_report%s$" % (self._meta.resource_name, trailing_slash()), self.wrap_view('stockTakeReports'), name="stockTakeReports"),
            

        ]

    def products_sold(self, request, **kwargs):
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except Exception, e:
            print e
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        try:
            location = request.session['db'] 

        except:
            location = ''    

        today = today+timedelta(hours=23)

        

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        objects = []



        data = QuantityMovement.objects.filter(
            Q(created__range = [lastweek, today]) & Q(status=QuantityMovement.product_bought)
            ).values('slug', 'product_name', 'amount', 'product_barcode')
        data = sorted(data, key=lambda x:x['slug'])

        for product, values in groupby(data, key=lambda x:x["slug"]):
            
            group = {}
            groups = list(values)
            group.update({
            'location':location,    
            'slug':product,
            'name':groups[0]['product_name'],
            'barcode':groups[0]['product_barcode'],
            'amount':reduce(lambda x,y:int(x)+int(y),[value['amount'] for value in groups])
            

            })        
            objects.append(group)

        self.log_throttled_access(request)
        return self.create_response(request, objects) 

    def products_sold_per_day(self, request, **kwargs):
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except Exception, e:
            print e
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)

        data = [(date(product.created.year, product.created.month, product.created.day)
                ,product.amount) for product in QuantityMovement.objects.filter(
                Q(created__range = [lastweek, today]) & Q(status=QuantityMovement.product_bought)
                ).order_by('created')]
        print data
      
        objects = [[time.mktime(k.timetuple())*1000,reduce(lambda x,y:int(x)+int(y),[item[1] for item in value])] for k,value in groupby(data, key=lambda x:x[0])] 
        
        self.log_throttled_access(request)
        return self.create_response(request, objects)

    def products_sold_per_hour(self, request, **kwargs):

        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except:
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)
        objects = {}
        
        days_between = today-lastweek




        for i in range(0, days_between.days+1):
            
            hours = 23

            for hour_number in range(0, hours+1):

                list_of = []
                total_items = 0
                total_amount = 0
                for orders in Order.objects.filter(created__range = [lastweek+timedelta(days = i)+timedelta(hours = hour_number), lastweek+timedelta(days = i)+timedelta(hours = hour_number+1)]).filter(status = Order.COMPLETED):
                    list_of.append(orders.id)

                
                for item in OrderItem.objects.filter(order__in = list_of):
                    total_items += item.quantity
                    total_amount += item.line_total

                if hour_number in objects:
                    objects[hour_number]["total_items"] +=int(total_items)
                    objects[hour_number]["total_amount"] +=int(total_amount)

                else:


                    objects[hour_number] = {
                        "total_items":int(total_items),
                        "total_amount": int(total_amount)
                    }

        self.log_throttled_access(request)
        return self.create_response(request, objects)                
    

    def inventory_quantity_movement(self, request, **kwargs):
        try:
            today = datetime.strptime(_get_parameter(request, 'today'), "%Y-%m-%d")
            lastweek = datetime.strptime(_get_parameter(request, 'lastweek'), "%Y-%m-%d")
            request.session['last_week'] = _get_parameter(request, 'lastweek')
            request.session['today'] = _get_parameter(request, 'today')
        except Exception, e:
            print e
            today = datetime.strptime(request.session['today'], "%Y-%m-%d")
            lastweek = datetime.strptime(request.session['last_week'], "%Y-%m-%d")

        today = today+timedelta(hours=23)

        self.method_check(request, allowed=['get'])
        self.is_authenticated(request)
        self.throttle_check(request)


        logs = QuantityMovement.objects.filter(Q(created__range = [lastweek, today])).order_by("-created")
        try:
            location = request.session['db'] 

        except:
            location = ''     

        products = BarcodeProduct.objects.all()
        objects = []
        for product in products:

            log = logs.filter(product_barcode = product.barcode)
            if len(log) > 0:
                pre = log.reverse()[0].pre_quantity
                post = log[0].post_quantity
                print str(post)+', '+str(pre)+", "+product.barcode

                sold = sum([int(p.amount) for p in log.filter(status = QuantityMovement.product_bought)])
                returns = sum([int(p.amount) for p in log.filter(status = QuantityMovement.manual_adjustment_returns)])
                stale = sum([int(p.amount) for p in log.filter(status = QuantityMovement.manual_adjustment_stale)])
                restocked = sum([int(p.amount) for p in log.filter(status = QuantityMovement.Warehouse_to_kiosk_reports)])

            else:
                pre = post = product.quantity
                sold = 0
                stale = 0
                returns = 0
                restocked = 0

            objects.append({
                'location':location,
                "product_barcode":product.barcode,
                "product_name":product.name,
                'sold':sold,
                'stale':stale,
                'returns' : returns,
                'restocked':restocked,
                'pre':pre,
                'post':post,
                'revenue':product.unit_price*sold,
                'cog':product.unit_cost*sold

                })

        self.log_throttled_access(request)
        return self.create_response(request, objects)    
        

    def stockTake(self, request, **kwargs):

        self.method_check(request, allowed=['post',])
        self.is_authenticated(request)
        self.throttle_check(request)

        current_location = request.session['db']

        data = json.loads(request.body)

        pinDb(data['location'])

        response = {}

        try:
            product = BarcodeProduct.objects.get(barcode = data['barcode'])
            if 'quantity' in data:

                quantity_logging = QuantityMovement.objects.create(
                            product_name = product.name,
                            product_barcode = getattr(product, "barcode", None),
                            created = datetime.now(),
                            pre_quantity = product.quantity,
                            post_quantity = int(data['quantity']),
                            status = QuantityMovement.stock_take,
                            slug = product.slug,
                            amount = int(data['quantity'])

                        )

                product.quantity = int(data['quantity'])
                product.save()
                

            if 'stale' in data:
                quantity_logging = QuantityMovement.objects.create(
                            product_name = product.name,
                            product_barcode = getattr(product, "barcode", None),
                            created = datetime.now(),
                            pre_quantity = product.quantity,
                            post_quantity = product.quantity-int(data['stale']),
                            status = QuantityMovement.manual_adjustment_stale,
                            slug = product.slug,
                            amount = int(data['stale'])


                        )
                product.quantity -= int(data['stale'])
                product.save()
                

            if 'return' in data:

                quantity_logging = QuantityMovement.objects.create(
                            product_name = product.name,
                            product_barcode = getattr(product, "barcode", None),
                            created = datetime.now(),
                            pre_quantity = product.quantity,
                            post_quantity = product.quantity-int(data['return']),
                            status = QuantityMovement.manual_adjustment_returns,
                            slug = product.slug,
                            amount = int(data['return'])


                        )
                
                product.quantity -= int(data['return'])
                product.save()
                
            if 'add' in data:
                quantity_logging = QuantityMovement.objects.create(
                            product_name = product.name,
                            product_barcode = getattr(product, "barcode", None),
                            created = datetime.now(),
                            pre_quantity = product.quantity,
                            post_quantity = product.quantity+int(data['add']),
                            status = QuantityMovement.manual_adjustment_error_add,
                            slug = product.slug,
                            amount = int(data['add'])


                        )
                
                product.quantity += int(data['add'])
                product.save()
                
            if 'sub' in data:

                quantity_logging = QuantityMovement.objects.create(
                            product_name = product.name,
                            product_barcode = getattr(product, "barcode", None),
                            created = datetime.now(),
                            pre_quantity = product.quantity,
                            post_quantity = product.quantity-int(data['sub']),
                            status = QuantityMovement.manual_adjustment_error_sub,
                            slug = product.slug,
                            amount = int(data['sub'])


                        )
                quantity_logging.save()
                product.quantity -= int(data['sub'])
                product.save()

            response = {'status':'ok'}    




        except Exception, e:
            response = {'status':'error', 'msg':e}  

        pinDb(current_location)    


        self.log_throttled_access(request)
        return self.create_response(request, response)     



    def stockTakeReports(self, request, **kwargs):

        self.method_check(request, allowed=['get',])
        self.is_authenticated(request)
        self.throttle_check(request)

        objects = []

        products = QuantityMovement.objects.filter(status__in = [QuantityMovement.stock_take,QuantityMovement.manual_adjustment_returns,QuantityMovement.manual_adjustment_stale, QuantityMovement.manual_adjustment_returns,QuantityMovement.manual_adjustment_stale,QuantityMovement.manual_adjustment_error_add, QuantityMovement.manual_adjustment_error_sub ]).order_by('-created')
        data = [{"product_name":product.product_name,"pre_quantity":product.pre_quantity,"post_quantity":product.post_quantity,"created":product.created.strftime('%Y-%m-%d'),"datetime":product.created,"status":product.get_status_display(),"amount":product.amount} for product in products]
        
        
        for item, value in groupby(data, key=lambda x:x["created"]):

            values = list(value)
            objects.append({item:values})



        
        self.log_throttled_access(request)
        return self.create_response(request, objects)    
            



                 
        
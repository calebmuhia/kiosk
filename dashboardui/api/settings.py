from dashboardui.api import *
from kioskui.models import *
from django.conf.urls import patterns, url,include


from itertools import groupby
import time
from datetime import datetime, date, timedelta
from django.db.models import Sum, Q



def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class TaxesResource(ModelResource):

    class Meta:
        queryset = Tax.objects.all()
        list_allowed_methods = ['get', 'post', 'put', ]
        detail_allowed_methods = ['get', 'post','put', ]
        resource_name = 'taxes'
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True
        filtering={
        "name":ALL
        }

class LocationResource(ModelResource):

    class Meta:
        queryset = LocationInfo.objects.all()
        list_allowed_methods = ['get', 'post', 'put', ]
        detail_allowed_methods = ['get', 'post','put', ]
        resource_name = 'locationinfo'
        authentication = Authentication()
        authorization = Authorization() # THIS IS IMPORTANT
        always_return_data = True
        filtering={
        "name":ALL
        }    
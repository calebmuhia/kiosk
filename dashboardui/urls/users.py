from django.conf.urls import patterns, url
from dashboardui.views.users import UserList, UserDetails

urlpatterns = patterns('',
    url(r'^$',UserList.as_view(), name="users"),
    url(r'^reports/user_details/(?P<username>\w+)$', UserDetails.as_view(), name="users_users_details"),
    url(r'^upload_users_csv', UserList.as_view(action='uploadUsersCsv'), name="upload_users_csv"),
    


    

    
)
from django.conf.urls import patterns, url
from dashboardui.views.settings import Settings

urlpatterns = patterns('',
    url(r'^$',Settings.as_view(), name="taxes_settings"),
    url(r'^reports/location_settings/$',Settings.as_view(template_name="dashboardui/settings/location_settings.html"), name="location_settings"),

     
)
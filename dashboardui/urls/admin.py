from django.conf.urls import patterns, url
from dashboardui.views.admin import AdminFunctions

urlpatterns = patterns('',
    url(r'^$', AdminFunctions.as_view(), name="admin_functions"),
    url(r'^reboot/(?P<location>[-\w ]+)/', AdminFunctions.as_view(action='reboot'), name="kiosk_reboot"),

    

)
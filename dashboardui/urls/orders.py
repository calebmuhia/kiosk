from django.conf.urls import patterns, url
from dashboardui.views.orders import OrderList, CashFlow, OrderDetail, UsersOnPayrolls

urlpatterns = patterns('',
    url(r'^$',OrderList.as_view(), name="orders"),
    url(r'^order_detail/(?P<order_id>\d+)/', OrderDetail.as_view(), name="orders_order_detail"),
    url(r'^reports/cash_flow_list/$', CashFlow.as_view(
        template_name="dashboardui/orders/cash_flow_list.html")
        , name="orders_cash_flow_list"),
    url(r'^reports/cash_flow_details/$', CashFlow.as_view(
        template_name="dashboardui/orders/cash_flow_details.html")
        , name="orders_cash_flow_details"),
    
    url(r'^reports/payroll', UsersOnPayrolls.as_view(), name="usersonpayroll"),
    url(r'^items/(?P<payment_id>\d+)', UsersOnPayrolls.as_view(action="getItems", template_name="dashboardui/orders/items.html"), name="getItems"),


    
)
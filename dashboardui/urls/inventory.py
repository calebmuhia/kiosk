from django.conf.urls import patterns, url
from dashboardui.views.inventory import *

urlpatterns = patterns('',
    url(r'^$',InventoryRestock.as_view(), name="inventory_restock"),
    url(r'^reports/category_options/', ExtraCategoryOptions.as_view(), name="inventory_extra_category_options"),
    url(r'^reports/create_restock_report/(?P<placeholder_id>\d+)/', InventoryRestock.as_view(action="create_restock_report"), name="create_restock_report"),
    url(r'^reports/view_restock_report/(?P<report_id>\d+)/', ViewInventoryRestockReport.as_view(), name="view_restock_report"),
    url(r'^reports/driver_prekitting/(?P<report_id>\d+)/', ViewInventoryRestockReport.as_view(template_name="dashboardui/inventory/driver_prekitting.html"), name="driver_prekitting"),
    url(r'^reports/save_products/(?P<report_id>\d+)/', ViewInventoryRestockReport.as_view(action="save_reportitems_quantity"), name="save_reportitems_quantity"),
    url(r'^reports/stock_movement/', StockMovement.as_view(), name="stock_movement"),
    url(r'^reports/stock_take_reports/', StockMovement.as_view(template_name="dashboardui/inventory/stock_take_reports.html"), name="stock_take_reports"),

    

    

    
)
# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from dashboardui.views.db_routes import ChangeDatabases
from django.views.generic import RedirectView




urlpatterns = patterns('',
    url(r'^$', RedirectView.as_view(pattern_name='home')),
    #include urls located on the dashboardui.urls.home
    (r'^home/', include('dashboardui.urls.home')),
    #include urls located on the dashboardui.urls.orders
    (r'^orders/', include('dashboardui.urls.orders')),
    #include urls located on the dashboardui.urls.products
    (r'^products/', include('dashboardui.urls.products')),
    #include urls located on the dashboardui.urls.users
    (r'^users/', include('dashboardui.urls.users')),
    #include urls located on the dashboardui.urls.inventory
    (r'^inventory/', include('dashboardui.urls.inventory')),
    #include urls located on the dashboardui.urls.settings
    (r'^settings/', include('dashboardui.urls.settings')),
    #include urls located on the api.urls directory
    (r'', include('dashboardui.api.urls')),
    #include urls located on the dashboardui.urls.vidoes directory

    (r'^videos/', include('dashboardui.urls.videos')),
    (r'^admin/', include('dashboardui.urls.admin')),
    (r'^feedback/', include('dashboardui.urls.feedback')),






    url(r'^change_database/$', ChangeDatabases.as_view(), name = "change_database"),
    url(r'^get_db_names/$', 'dashboardui.views.db_routes.getLocationNames', name = "get_db_names"),

    url(r'^login/$',
        'dashboardui.views.db_routes.dashboard_login',
        {'template_name': 'dashboardui/login.html'},
        name='auth_login'),

    )
from django.conf.urls import patterns, url
from dashboardui.views.products import ProductList, CategoryList

urlpatterns = patterns('',
    url(r'^$',ProductList.as_view(template_name='dashboardui/products/products_list.html'), name="products"),
    url(r'^nobarcode/$',ProductList.as_view(template_name='dashboardui/products/nobarcodeproducts_list.html'), name="no_barcode_products"),
    url(r'^upload_products/$',ProductList.as_view(action="uploadProductsCsv"), name="upload_products"),
    url(r'^delete_products/$',ProductList.as_view(action="delete_all_products"), name="delete_all_products"),


    url(r'^reports/categories/$', CategoryList.as_view()
        , name="category_list"),
    url(r'^reports/suppliers/$', CategoryList.as_view(template_name="dashboardui/products/suppliers.html")
        , name="suppliers_list"),
    

    
)
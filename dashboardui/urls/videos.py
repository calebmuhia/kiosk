__author__ = "caleb"
from django.conf.urls import patterns, url
from dashboardui.views.videos import LiveStream, VideoForTransaction,EventStep, IframeVideos,EventList
from dashboardui.views.orders import OrderList

urlpatterns = patterns('',
    url(r'^$',LiveStream.as_view(), name="livestream"),
    url(r'^video_for_transaction/(?P<order_id>\d+)$',VideoForTransaction.as_view(), name="video_for_transaction"),
    url(r'^event_step/',EventStep.as_view(), name="event_step"),
    url(r'^check_for_video/',EventStep.as_view(action="check_for_videos"), name="check_for_videos"),
    url(r'^checkNextPrevious/',EventStep.as_view(action="checkNextPrevious"), name="checkNextPrevious"),
    url(r'^get_orders_in_video/',EventStep.as_view(action="checkOrders"), name="get_orders_in_video"),


    url(r'^all$',IframeVideos.as_view(), name="iframezm"),
    url(r'^orders$',OrderList.as_view(), name="orders_videos"),
    url(r'^event_list$',EventList.as_view(), name="event_list"),

    


    



    
    


    

    
)
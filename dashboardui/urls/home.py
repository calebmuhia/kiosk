from django.conf.urls import patterns, url
from dashboardui.views.home import Home, ProductsSold, CashBoxView, NonMovingStock, MissingBarcodes,FinancialReport

urlpatterns = patterns('',
    url(r'^$', Home.as_view(), name="home"),
    url(r'^reports/cashbox/$', CashBoxView.as_view(), name="home_reports_cashbox"),
    url(r'^reports/products_sold/$', ProductsSold.as_view(), name="home_reports_products_sold"),
    url(r'^reports/non_moving_stock/$', NonMovingStock.as_view(), name="home_reports_non_moving_stock"),
    url(r'^reports/missing/$', MissingBarcodes.as_view(), name="home_reports_missing_barcodes"),
    url(r'^reports/financialreport/$', FinancialReport.as_view(), name="home_reports_financial_report"),
    url(r'^all_locations/$', FinancialReport.as_view(template_name="dashboardui/home/all_locations.html"), name="all_locations"),
    url(r'^reports/daily_financial_report/$', FinancialReport.as_view(template_name="dashboardui/home/daily_financial_report.html"), name="home_reports_daily_financial_report"),
    url(r'^reports/product_profitability/$', FinancialReport.as_view(template_name="dashboardui/home/product_profitability.html"), name="home_reports_product_profitability"),  
    url(r'^reports/user_profitability/$', FinancialReport.as_view(template_name="dashboardui/home/user_profitability.html"), name="home_reports_user_profitability"),


)
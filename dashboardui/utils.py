__author__ = 'caleb'

import paramiko, getpass
from django.conf import settings


SSH_SETTINGS = {
    'Grenville': {
        'USERNAME': 'test',
        'PASSWORD': 'test88',
        'URL': '10.8.1.154',
    },
    '3M-301': {
        'USERNAME': 'test',
        'PASSWORD': 'test88',
        'URL': '10.8.1.10',
    },
    '3M-302': {
        'USERNAME': 'test',
        'PASSWORD': 'test88',
        'URL': '10.8.1.11',
    },
    '3M-501': {
        'USERNAME': 'kioskuser',
        'PASSWORD': 'C@nn0tg01n',
        'URL': '10.8.1.45',
    },
    '3M-502': {
        'USERNAME': 'kioskuser',
        'PASSWORD': 'C@nn0tg01n',
        'URL': '10.8.1.46',
    },
    'Albany': {
        'USERNAME': 'test',
        'PASSWORD': 'test88',
        'URL': '10.8.1.14',
    },
    
    'CentralWire': {
        'USERNAME': 'user',
        'PASSWORD': 'C@nn0tg01n',
        'URL': '10.8.1.42',
    },
    'northHala': {
        'USERNAME': 'test',
        'PASSWORD': 'test88',
        'URL': '10.8.0.182',
    },
    'southHala': {
        'USERNAME': 'test',
        'PASSWORD': 'test88',
        'URL': '10.8.0.178',
    },
    'HGS Belleville': {
        'USERNAME': 'user',
        'PASSWORD': 'C@nn0tg01n',
        'URL': '10.8.0.146',
    },

}

def ssh_connection(user, host, port=22, password=None, key_filename=None):
    """
    with password='' you will be prompted for a password when the script runs
    """
    

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    if password == '':
        # ask for one on stdin
        password = getpass.getpass('Password for %s@%s: ' % (user, host))
    try:    
        ssh.connect(host, port=port, username=user, password=password, key_filename=key_filename, timeout=1)
        # custom attributes
        ssh.user = user
        if user == 'root':
            ssh.homedir = '/root'
        else:
            ssh.homedir = '/home/%s' % user
        ssh.password = password
        ssh.use_sudo = False
        msg = 'Connected to %s@%s \n' % (user, host)
        
        return ssh
    except:
        return None    

def run_remote(ssh, cmd, check_exit_status=True, verbose=True):
    chan = ssh.get_transport().open_session()
    stdin = chan.makefile('wb')
    stdout = chan.makefile('rb')
    stderr = chan.makefile_stderr('rb')
    processed_cmd = cmd
    if ssh.use_sudo:
        processed_cmd = 'sudo -S bash -c "%s"' % cmd.replace('"', '\\"')
    msg = 'processing : %s \n' % processed_cmd
    
    chan.exec_command(processed_cmd)
    if stdout.channel.closed is False: # If stdout is still open then sudo is asking us for a password
        stdin.write('%s\n' % ssh.password)
        stdin.flush()
    result = {
        'stdout': [],
        'stderr': [],
    }
    exit_status = chan.recv_exit_status()
    result['exit_status'] = exit_status
    def print_output():
        for line in stdout:
            result['stdout'].append(line.replace('\n',''))
            
        for line in stderr:
            result['stderr'].append(line)
            
    if check_exit_status and exit_status != 0:
        print_output()
        print 'non-zero exit status (%d) when running "%s"' % (exit_status, cmd)
        exit(exit_status)
    if verbose:
        print processed_cmd
        print_output()
    return result

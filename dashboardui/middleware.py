from datetime import datetime, timedelta
__author__ = 'caleb'
import redis
r = redis.StrictRedis(host='localhost', port=6379, db=0)
from db_routes.pin import *
import logging
logger = logging.getLogger(__name__)
from os import environ
from django.conf import settings
from django.core.urlresolvers import reverse
from re import compile

from django.http import HttpResponseRedirect
from django.contrib.sessions.models import Session
from kioskui.models import KioskUser as User
from django.contrib.auth import login, authenticate

class DatePickerMiddleware(object):

    def process_request(self, request):

        try:
            
            today = request.session["today"]
            last_week = request.session['last_week']

            # if (today!=datetime.today().strftime("%Y-%m-%d")):
            #     self.set_dates(request)


        except Exception, e:
            # logger.debug(e)

            self.set_dates(request)

    def set_dates(self, request):
        today_date = datetime.now()

        week = timedelta(days = 7)
        last_week = today_date - week

        request.session['last_week'] = last_week.strftime("%Y-%m-%d")
        request.session['today'] = today_date.strftime("%Y-%m-%d")

class DatabaseRouterMiddleware(object):

    def process_request(self, request):
        
        try:
            db = request.session["db"]
            old_db = request.session['old_db']
            print db, old_db
            try:
                today = request.session["today"]
                last_week = request.session['last_week']
            except:
                today = None
                last_week = None  


            if db != old_db:


                authenticated_user = self.get_authenticated_user(request)
                pinDb(db)
                user = authenticate(username = authenticated_user)
                if user:
                    login(request, user)

                request.session['old_db'] = db
                request.session['db'] = db

                request.session['last_week'] = last_week
                request.session['today'] = today   

                

            else:

                pinDb(db)

        except Exception, e:
            print e
            db = "default"
            request.session["db"] = db
            request.session['old_db'] = db
            pinDb(db)

    def get_authenticated_user(self, request):
        
        if 'sessionid' in request.COOKIES:
            print "sessionid found"
            
            s = Session.objects.get(pk=request.COOKIES['sessionid'])
            if '_auth_user_id' in s.get_decoded():
                user = User.objects.get(id=s.get_decoded()['_auth_user_id'])

                return user.username

            else:
                return None
        else:
            return None  

class RedirectForCentralDashMiddleware(object):

    def process_request(self, request):

        if getattr(settings,"CENTRAL_DASH", None):
            
            path = request.path_info.lstrip('/')
            
            

            ALLOW_URLS = (
                r'^dashboard',
                r'^accounts/logout'

            )
           
            ALLOWED_URLS = [compile(expr) for expr in ALLOW_URLS]

            if not any(m.match(path) for m in ALLOWED_URLS):
                return HttpResponseRedirect(reverse("home"))







EXEMPT_URLS = [compile(settings.LOGIN_URL.lstrip('/'))]
ALLOW_URLS = [compile(settings.LOGIN_URL.lstrip('/'))]

if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [compile(expr) for expr in settings.LOGIN_EXEMPT_URLS]

if hasattr(settings, 'LOGIN_ALLOW_URLS'):
    ALLOW_URLS += [compile(expr) for expr in settings.LOGIN_ALLOW_URLS]

class LoginRequiredMiddleware(object):
    """
    Middleware that requires a user to be authenticated to view any page other
    than LOGIN_URL. Exemptions to this requirement can optionally be specified
    in settings via a list of regular expressions in LOGIN_EXEMPT_URLS (which
    you can copy from your urls.py).

    Requires authentication middleware and template context processors to be
    loaded. You'll get an error if they aren't.
    """
    def process_request(self, request):
        assert hasattr(request, 'user'), "The Login Required middleware\
         requires authentication middleware to be installed. Edit your\
         MIDDLEWARE_CLASSES setting to insert\
         'django.contrib.auth.middlware.AuthenticationMiddleware'. If that doesn't\
         work, ensure your TEMPLATE_CONTEXT_PROCESSORS setting includes\
         'django.core.context_processors.auth'."

        # pin = this_thread_is_pinned()
        # print "pin ",pin 

        # if pin =='albany':
        #     pass  

        if not request.user.is_authenticated():
            path = request.path_info.lstrip('/')
            # pin_this_thread("default")


            if any(m.match(path) for m in EXEMPT_URLS) and not any(m.match(path) for m in ALLOW_URLS) :
                return HttpResponseRedirect(reverse("auth_login")+"?next=/"+path)            
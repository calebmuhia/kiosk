# -*- coding: utf-8 -*-
from django.conf import settings

import socket, paramiko, cmd
import getpass
from dashboardui.utils import SSH_SETTINGS, run_remote, ssh_connection
from dashboardui.views import *



class AdminFunctions(ExtraActionMixin):
    template_name='dashboardui/admin/admin_functions.html'
    
    def get(self, request, *args, **kwargs):
    
        ctx = self.get_context_data(**kwargs)
        locations = {location:{} for location in SSH_SETTINGS}


        
        ctx.update({'locations':json.dumps(locations)})
    
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
    
        return render_to_response(self.template_name, ctx)

    def reboot(self, request, *args, **kwargs):
        
        location = self.kwargs['location'] 

        location_settings =  SSH_SETTINGS[location]
        conn = ssh_connection(location_settings['USERNAME'], location_settings['URL'], password=location_settings['PASSWORD'])
        if not conn:
            return HttpResponse(json.dumps(None))
        conn.use_sudo = True

        command = "/sbin/reboot -f > /dev/null 2>&1 &"

        result = run_remote(conn, command)

        

        return HttpResponse(json.dumps(result))

        













# def restart_machine(host):
#    msg = 'Initializing ssh connection \n'
#    p['ssh-channel'].trigger('log', msg)
#    conn = ssh_connection(host['USERNAME'], host['URL'], password=host['PASSWORD'])
#    conn.use_sudo = True
#    result = run_remote(conn, 'ls')
   
   
#    p['ssh-channel'].trigger('log', result['stdout'])

#    p['ssh-channel'].trigger('log','Done')    

#    conn.close()
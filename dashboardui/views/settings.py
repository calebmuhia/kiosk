from dashboardui.views import *
from datetime import datetime, timedelta
from django.db.models import Q, Sum


import logging
logger = logging.getLogger(__name__)

class Settings(TemplateView):
    """
    Settings for taxes 
    """
    template_name = 'dashboardui/settings/taxes_settings.html'

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)
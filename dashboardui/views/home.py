from dashboardui.views import *

import logging
logger = logging.getLogger(__name__)




class Home(GetDatetimeMixin):
    template_name = 'dashboardui/home/home.html'

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)  

        return render_to_response(self.template_name, ctx ) 

class CashBoxView(GetDatetimeMixin):
    template_name = 'dashboardui/home/cashbox.html'

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)  

        return render_to_response(self.template_name, ctx ) 

        
class ProductsSold(GetDatetimeMixin):
    template_name = 'dashboardui/home/products_sold.html'

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)  

        return render_to_response(self.template_name, ctx ) 


class NonMovingStock(GetDatetimeMixin):
    template_name = 'dashboardui/home/nonmovingstock.html'

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)  

        return render_to_response(self.template_name, ctx ) 


class MissingBarcodes(TemplateView):
    template_name = 'dashboardui/home/missingbarcode.html'
    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs) 

        missingbarcode = MissingBarcodeOrKeycard.objects.filter().order_by('-date') 
        ctx.update({
            'missing':missingbarcode
            })

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)

class FinancialReport(GetDatetimeMixin):

    template_name='dashboardui/home/financial_report.html'
            
    def get(self, request, *args, **kwargs):
    
        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
    
        return render_to_response(self.template_name, ctx)    

            
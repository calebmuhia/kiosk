from dashboardui.views import *

import logging
logger = logging.getLogger(__name__)


class FeedBackView(TemplateView):
    template_name = 'dashboardui/feedback/feedback.html'
    
    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs) 

        

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)
from dashboardui.views import *

import logging
logger = logging.getLogger(__name__)
from django.utils import simplejson
from django.core.serializers.json import DjangoJSONEncoder
import decimal
from django.db.models.base import ModelState

class OrderList(GetDatetimeMixin):
    template_name = 'dashboardui/orders/order_list.html'

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)

        return render_to_response(self.template_name, ctx)

class OrderDetail(TemplateView):
    template_name = 'dashboardui/orders/order_detail.html'

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)
        order = Order.objects.get(id=self.kwargs['order_id'])
        ctx.update({
            'order':order
            })
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)


        
class CashFlow(ExtraActionMixin, GetDatetimeMixin):

    

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)

        return render_to_response(self.template_name, ctx)

class UsersOnPayrolls(ExtraActionMixin):
    template_name = 'dashboardui/orders/users_on_payroll.html'
    
    def get(self,request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx) 


    def getItems(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)

        payment_id = self.kwargs['payment_id']

        items = PayRollItem.objects.filter(payment = payment_id).prefetch_related('user_set').values('id','amount','user_keytag','user__first_name','user__last_name','user__email')
        ctx.update({'items':json.dumps(items, cls=DateTimeEncoder)})

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name,ctx )
                 

class DateTimeEncoder(json.JSONEncoder):
    def default(self, obj):
        objects = []
        for item in obj:
            objcopy = {}
            for key in item:
                
                
                if isinstance(item[key], decimal.Decimal):
                    objcopy[key] = float(item[key])

                else :
                    objcopy[key] = item[key]    

            objects.append(objcopy) 

        print objects    

        return objects           
                        




        
           

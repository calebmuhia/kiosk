__author__ = 'caleb'
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_GET
from django.http import HttpResponse, HttpResponseRedirect
from kioskui.models import *
from django.contrib.auth import login, authenticate

from decimal import Decimal

from shop.models.ordermodel import Order, OrderItem, OrderPayment, OrderExtraInfo
from datetime import datetime

import json
import urlparse

class ExtraActionMixin(TemplateView):
    action = None

    def dispatch(self, request, *args, **kwargs):
        """
        Submitting form works only for "GET" and "POST".
        If `action` is defined use it dispatch request to the right method.
        """
        if not self.action:
            return super(ExtraActionMixin, self).dispatch(request, *args,
                **kwargs)
        if self.action in self.http_method_names:
            handler = getattr(self, self.action, self.http_method_not_allowed)
        else:
            handler = getattr(self, self.action, None)
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

class GetDatetimeMixin(TemplateView):

    def get_context_data(self, **kwargs):
        ctx = super(GetDatetimeMixin, self).get_context_data(**kwargs)
        

        ctx["last_week"] = self.request.session['last_week']
        ctx["today"] = self.request.session['today'] 

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)


        return ctx
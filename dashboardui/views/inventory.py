from dashboardui.views import *
from datetime import datetime, timedelta
from django.db.models import Q, Sum


import logging
logger = logging.getLogger(__name__)

class ExtraCategoryOptions(TemplateView):
    """
    crud functions for 
    """
    template_name = 'dashboardui/inventory/category_options.html'

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)

        
class InventoryRestock(ExtraActionMixin):
    """
    create/edit restock inventory choice place holders
    manually run restock inventory
    view past inventory reports
    """
    template_name = 'dashboardui/inventory/inventory_restock.html'

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)


    def create_restock_report(self, request, *args, **kwargs):
        """
        view used to create restock inventory report, from this report client can prekitt, 
        and update the products with the new quantity
        """ 
        msg = []

        placeholder_id = self.kwargs['placeholder_id']
        week = timedelta(days=7)
        dateToday = datetime.today()
        last_week_date = dateToday -week

        # we will restock again in for days
        time_to_next_restock = 4

        # used for bottles and cans, 
        case_qty = 12

        #get the category place holder
        placeholder = ChoicesPlaceHolder.objects.get(id=placeholder_id)

        #create a inventory report placeholder
        report = RestockInventoryReport.objects.create(
            name = "%s report created on %s" % (placeholder.name, datetime.today().strftime('%c')),
            created=datetime.today()
            )

        # fetch list of category id associated with the report 
        category_list = [choice.category.id for choice in RestockCategoryChoice.objects.filter(place_holder = placeholder)]

        # fetch the distinct products associated the category list (we only include Barcod products)?
        products = BarcodeProduct.objects.filter(active = True).filter(categories__in = category_list).distinct()
        msg.append(products.count())
        extra_category_options = None
        categories_fields = False
        for product in products:

            # calculate the demand (we assume demand is the quantity bought over a week)
            quantity_movements = QuantityMovement.objects.filter(Q(product_barcode=product.barcode)&\
                Q(created__range=[last_week_date,dateToday+timedelta(days=1)])&\
                Q(status=QuantityMovement.product_bought))
            demand = quantity_movements.aggregate(demand = Sum("amount"))["demand"]
            
            if not demand:
                demand = 0
            msg.append({"demand":demand, "product":product.barcode})    
            try:
                category = product.categories.all()[0]
                extra_category_options = ExtraCategoryOptions.objects.get(category=category)
                categories_fields = True
                round_to_case = extra_category_options.round_to_case
            except:
                round_to_case = False
                extra_category_options = None

            msg.append({'categories_fields':categories_fields, 'product':product.barcode})    

            try:

                trigger_qty = (int(time_to_next_restock)*int(demand)/7)+product.min_stock
                msg.append({'trigger_qty':trigger_qty, 'product':product.barcode})


                if product.quantity < trigger_qty or product.quantity == trigger_qty:
                    if not categories_fields:
                        if product.quantity < 0:
                            refill_qty = product.max_stock-0
                        else:
                            refill_qty = product.max_stock-product.quantity
                    else:
                        if product.quantity < 0:
                            refill_qty = product.max_stock-0
                        else:
                            refill_qty = product.max_stock-product.quantity

                        if round_to_case:
                            new_restock_quantity = 0

                            if refill_qty == case_qty:
                                new_restock_quantity = case_qty

                            elif refill_qty < 0.5*case_qty:
                                new_restock_quantity = refill_qty

                            elif refill_qty > 0.5*case_qty:
                                new_restock_quantity = round((refill_qty/case_qty)+0.5)*case_qty
                            refill_qty = new_restock_quantity
                else:
                    refill_qty = 0
                    continue
            except Exception, e:
                print e
                msg.append({"error":e,"line": 130})
                continue

            try:
                if refill_qty == 0 or refill_qty < 0:
                    msg.append({"refill_qty":str(refill_qty), "product":product.barcode})
                    continue

                
                reportitems = RestockInventoryReportitems.objects.create(
                    product_name = repr(product.name).replace("u'","").replace("'", ""),
                    proposed_restock_quantity=refill_qty,
                    upc= product.barcode,
                    categories = category.name,
                    report= report
                )
                

            except Exception, e:
                #logger.info(e)
                print e, "exeption"
                msg.append({"error":e,"line": 150})

                continue

        response={
        'url':'/dashboard/inventory/reports/view_restock_report/'+str(report.id)+'/'
        }

        return HttpResponse(json.dumps(response)) 

class ViewInventoryRestockReport(ExtraActionMixin):
    template_name="dashboardui/inventory/view_inventory_report.html"

    def get(self, request, *args, **kwargs):
        report_id = self.kwargs["report_id"]

        ctx = self.get_context_data(**kwargs)
        ctx.update({'report_id':report_id})

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)

    def save_reportitems_quantity(self, request, *args, **kwargs):
        report_id = self.kwargs["report_id"]
        report = RestockInventoryReport.objects.get(id=report_id)
        reportitems = RestockInventoryReportitems.objects.filter(report=report)
        


        for item in reportitems:
            
            try:
                product = BarcodeProduct.objects.get(barcode=item.upc)
                
                product.quantity_increment(item.proposed_restock_quantity, QuantityMovement.Warehouse_to_kiosk_reports)
                product.save()
                msg.append(product.quantity)
            except Exception, e:
                
                continue

        report.isUpdated=True
        report.save()
        response={"status":'ok'}

        return HttpResponse(json.dumps(response))         



                               
class StockMovement(GetDatetimeMixin):
    template_name = 'dashboardui/inventory/stockmovement.html'

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)    



            







    











        

from dashboardui.views import *
import csv
import StringIO
from kioskui.models import KioskUser as User




class UserList(ExtraActionMixin):
    template_name = "dashboardui/users/user_list.html"

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)  

    def uploadUsersCsv(self, request, *args, **kwargs):

        data = self.request.body


        csvReader = csv.DictReader(StringIO.StringIO(data))

        for row in csvReader:
            print row
            try:
                try:
                    balance = row['Balance'].replace("$", '')
                except Exception, e:
                    print "error1 is", e
                    balance = 0

                print balance        
                           
                new_user = User.objects.create_user(row['Keytag'], row['Email'], row['Keytag'])
                new_user.first_name = row['FirstName']
                new_user.last_name = row['LastName']
                new_user.is_active = row['Active']

                new_user.save()
                new_user.increment_admin_deposits(balance)
                new_user.save()
            except Exception, e:
                print "error2 is", e
                continue
                

        

        return HttpResponse(json.dumps({"status":"ok"}))

    



class UserDetails(TemplateView):
    template_name = "dashboardui/users/user_details.html"

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        username= self.kwargs['username']

        user = KioskUser.objects.get(username = username)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        ctx.update({"user":user})    

        return render_to_response(self.template_name, ctx)   






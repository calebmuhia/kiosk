from dashboardui.views import *

import logging
logger = logging.getLogger(__name__)
from kiosk import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import never_cache
from django.contrib.auth import REDIRECT_FIELD_NAME, login as auth_login, logout as auth_logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.sites.models import get_current_site
import re


# Create your models here.
def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class ChangeDatabases(TemplateView):

    template_name = None

    def get_context_data(self, **kwargs):
        ctx = super(ChangeDatabases, self).get_context_data(**kwargs)
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return ctx

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)



        current_path = _get_parameter(self.request, "current_path")

        location = _get_parameter(self.request, "locations")
        print location

        
        
        if location:
            self.request.session["db"] = location
        else:
            self.request.session["db"] = "default"

        if current_path:
            return HttpResponseRedirect(current_path)

        else:
            return HttpResponseRedirect(reverse("home"))

               


                

def getLocationNames(request):
    db = [db for db in getattr(settings, 'DATABASES', '')] 

    if 'zm_db' in db:
        db.pop(db.index('zm_db'))

    return HttpResponse(json.dumps(db)) 





@csrf_exempt
@never_cache
def dashboard_login(request, template_name='dashboardui/login.html',
          redirect_field_name=REDIRECT_FIELD_NAME,
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.REQUEST.get(redirect_field_name, '')

    if request.method == "POST":
        form = authentication_form(data=request.POST)
        if form.is_valid():
            netloc = urlparse.urlparse(redirect_to)[1]

            # Use default setting if redirect_to is empty
            if not redirect_to:
                redirect_to = settings.LOGIN_REDIRECT_URL

            # Security check -- don't allow redirection to a different
            # host.
            elif netloc and netloc != request.get_host():
                redirect_to = settings.LOGIN_REDIRECT_URL

            # Okay, security checks complete. Log the user in.
            #make sure that the user has permissions
            user = form.get_user()
            if user.is_active and user.is_superuser and user.is_staff:
                auth_login(request, form.get_user())

            else:
                form = authentication_form(request)
                request.session.set_test_cookie()

                current_site = get_current_site(request)
                error = "You do not have permission to view pages"

                context = {
                    'form': form,
                    'error':error,
                    redirect_field_name: redirect_to,
                    'site': current_site,
                    'site_name': current_site.name,
                }
                context.update(extra_context or {})
                return render_to_response(template_name, context,
                                          context_instance=RequestContext(request, current_app=current_app))



            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    request.session.set_test_cookie()

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    context.update(extra_context or {})
    return render_to_response(template_name, context,
                              context_instance=RequestContext(request, current_app=current_app))          
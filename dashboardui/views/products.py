from dashboardui.views import *

import logging
logger = logging.getLogger(__name__)
from dashboardui.tasks import add_products
import json

class ProductList(ExtraActionMixin):
    

    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)
        
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)

    def uploadProductsCsv(self, request, *args, **kwargs):

        data = self.request.body

        add_products(data)

        return HttpResponse(json.dumps("ok"))

    def delete_all_products(self,request,*args, **kwargs):
    
        products = BarcodeProduct.objects.all()
        for product in products:
            product.delete()

        return HttpResponse(json.dumps("ok"))        



        


        

class CategoryList(GetDatetimeMixin):
    template_name = "dashboardui/products/categories.html"
    def get(self, request, *args, **kwargs):

        ctx = self.get_context_data(**kwargs)

        return render_to_response(self.template_name, ctx)



             

from zm.models import Events, Monitors
from dashboardui.views import *
from datetime import datetime, timedelta

def _get_parameter(request, name):
    '''
    Helper: abstraction to recover paremeters from POST or GET
    either
    '''

    if request.POST:
        try:
            return request.POST[name]
        except KeyError:
            return None
    if request.GET:
        try:
            return request.GET[name]
        except KeyError as e:
            return None

class LiveStream(TemplateView):
    """
    shows live streams thro the cameras
    """
    template_name = 'dashboardui/videos/live_stream.html'

    def get(self, request, *args, **kwargs):

        monitors = [{'name':monitor.name,'id':int(monitor.id)} for monitor in Monitors.objects.filter()]

        ctx = self.get_context_data(**kwargs)
        ctx.update({'monitors':json.dumps(monitors)})

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)


class VideoForTransaction(TemplateView):
    """
    query events recored for a transaction
    """

    template_name='dashboardui/videos/video_for_transaction.html'
    
    def get(self, request, *args, **kwargs):
    
        ctx = self.get_context_data(**kwargs)
        order_id = self.kwargs['order_id']
        order = Order.objects.get(id=order_id)
        monitors = [monitor.id for monitor in Monitors.objects.filter(name__in = ["Camera4","camera5", "screen"])]
        events = Events.objects.filter(starttime__range = [order.created-timedelta(seconds = 60), order.created+timedelta(seconds = 60)]).filter(monitorid__in=monitors).values('id','monitorid','frames','starttime','cause' )
        
        ctx.update({'events':json.dumps(events, cls=IntEncoder),'order':order})
    
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
    
        return render_to_response(self.template_name, ctx)

class EventStep(ExtraActionMixin):
    

    template_name = 'dashboardui/videos/video_for_transaction.html'
    
    def get(self, request, *args, **kwargs):
    
        ctx = self.get_context_data(**kwargs)
        

        
        
        event_id = _get_parameter(self.request,'event_id')
        events = None
        order = None
        monitors = [monitor.id for monitor in Monitors.objects.filter(name__in = ["Camera4","camera5", "screen",'Camera2'])]
        
        if event_id:
            event = Events.objects.get(id = event_id)  
            events = Events.objects.filter(starttime__range = [event.starttime-timedelta(seconds = 60), event.starttime+timedelta(seconds = 60)]).filter(monitorid__in=monitors).values('id','monitorid','frames','starttime','cause' )


        else:
            try:
                events = Events.objects.filter(monitorid__in=monitors).order_by('-starttime')[:3].values('id','monitorid','frames','starttime','cause')
                print 'events', events
            except Exception, e:
                print e
                pass    

        ctx.update({'events':json.dumps(events, cls=IntEncoder),'order':order})    
        
    
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
    
        return render_to_response(self.template_name, ctx)

    def check_for_videos(self,request,*args,**kwargs):
        ctx = self.get_context_data(**kwargs)
        search_time = _get_parameter(self.request,'search_time')
        events = None

        if search_time:
            date = datetime.strptime(search_time, '%Y/%m/%d %H:%M:%S')
            count = 0
            monitors = [monitor.id for monitor in Monitors.objects.filter(name__in = ["Camera4"])]

            while True:
                count += 1

                if count == 5:
                    break
                
                events = Events.objects.filter(starttime__range = [date-timedelta(seconds = 30*count), date+timedelta(seconds = 30*count)]).filter(monitorid__in=monitors)
                print events
                if events:
                    events = events[0].id
                    break
        if not events:
            events = None            

        return HttpResponse(json.dumps({'events':events}))  

    def checkNextPrevious(self,request,*args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        event_id = _get_parameter(self.request,'event_id')
        monitors = [monitor.id for monitor in Monitors.objects.filter(name__in = ["Camera4"])]
        next_event = None
        previous_event = None

        if event_id:
            event = Events.objects.get(id=event_id)
            try:
                next_event = event.get_next_by_starttime(monitorid__in = monitors)
                next_event = int(next_event.id)
            except:
                pass

            try:
                previous_event = event.get_previous_by_starttime(monitorid__in = monitors)
                previous_event = int(previous_event.id)
            except:
                pass         

        return HttpResponse(json.dumps({'next_event':next_event, 'previous_event':previous_event})) 

    def checkOrders(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        event_id = _get_parameter(self.request,'event_id')

        videosOrders = []

        if event_id:
            event = Events.objects.get(id = event_id)
            orders = Order.objects.filter(created__range = [event.starttime, event.starttime+timedelta(minutes=1)])

            for order in orders:
                videosOrders.append({
                    'id':order.id,
                'time':order.created.strftime('%Y/%m/%d %H:%M:%S'),
                'user':order.user.username if order.user else None,
                'status': order.get_status_display(),
                'items':[item.product_name for item in OrderItem.objects.filter(order = order)]
                })

        
        return HttpResponse(json.dumps(videosOrders))        


        



class IframeVideos(TemplateView):

    template_name='dashboardui/videos/iframeview.html'
    
    def get(self, request, *args, **kwargs):
    
        ctx = self.get_context_data(**kwargs)
        
        
    
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
    
        return render_to_response(self.template_name, ctx)

class EventList(GetDatetimeMixin):

    template_name='dashboardui/videos/event_list.html'
    
    def get(self, request, *args, **kwargs):
    
        ctx = self.get_context_data(**kwargs)
        
        
    
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
    
        return render_to_response(self.template_name, ctx)


class IntEncoder(json.JSONEncoder):
    def default(self, obj):
        print obj
        objects = []
        for item in obj:
            objcopy = {}
            for key in item:
                if isinstance(item[key],datetime):
                    objcopy[key] = item[key].strftime("%y/%m/%d/%H/%M/%S")
                    objcopy['date'] = item[key].strftime("%y/%m/%d")
                    objcopy['time'] = item[key].strftime("%H:%M:%S")


                else:
                    try:

                        objcopy[key] = int(item[key])
                    except:
                        objcopy[key] = str(item[key])
                        

            objects.append(objcopy) 

        print objects    

        return objects





from socketio.namespace import BaseNamespace
from socketio.sdjango import namespace
from redis import StrictRedis
redis = StrictRedis(host='localhost', port=6379, db=0)
import json

@namespace('/test')
class TailNamespace(BaseNamespace):
    def listener(self):
        
        # Emit the backlog of messages
        
        messages = redis.lrange('test', 0, -1)
        self.emit('test-message', ''.join(messages))

        self.pubsub.subscribe('test-channel')
        self.pubsub.subscribe('checkdb-channel')

        for m in self.pubsub.listen():
            
            
            if m['type'] == 'message':
                if m['channel']=='test-channel':
                    self.emit('test-message', m['data'].replace("'", '"'))
                elif m['channel']=='checkdb-channel':
                    self.emit('checkdb-message', m['data'].replace("'", '"'))    

    def on_subscribe(self):
        self.pubsub = redis.pubsub()
        self.spawn(self.listener)
import redis
r = redis.StrictRedis(host='localhost', port=6379, db=0)
from django.conf import settings

def current_location(request):

    try:
        location = r.get('db')

        if location:
            return {"location_name":location}

        else:
            return {"location_name":""}

    except:
        return {"location_name":""}

def checkzm(request):

    if getattr(settings, 'INCLUDE_ZM', False):
        return {"zm":True}

    else:
        return {'zm':False} 

def company_name(request):

    if getattr(settings, 'LOCATION', False):
        return {"company":settings.LOCATION}
    else:
        return ''    

               

          
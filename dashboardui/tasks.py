from __future__ import absolute_import

from celery import shared_task
import redis, StringIO, csv, json
r = redis.StrictRedis(host='localhost', port=6379, db=0)
from db_routes.pin import pinDb
from kioskui.models import BarcodeProduct
from shop_simplecategories.models import Category


@shared_task
def add():

    locations_status = 100
    r.rpush('test', locations_status)
    r.set('locations', locations_status)
    r.publish('test-channel',locations_status)
    r.delete('test')



@shared_task
def add_products(dataobject):
    objects = json.loads(dataobject)

    try:
        for location in objects['locations']:
            print "this is the location", location
            csvReader = csv.DictReader(StringIO.StringIO(objects["data"]))
 
            for data in csvReader:
                print data
                try:
                    
                    pinDb(location)
                    if BarcodeProduct.objects.filter(barcode = data['Upc']).exists():
                        product = BarcodeProduct.objects.get(barcode = data['Upc'])
                        product.name = data["Description"]
                        product.quantity = data["Quantity"]
                        product.unit_price = data["UnitPrice"]
                        product.unit_cost = data["UnitCost"]
                        product.min_stock = data["MinStock"]
                        product.max_stock = data["MaxStock"]
                        product.hst = data["Hst"]
                        product.pst = data["Pst"]
                        product.active = data["Active"]
                        product.save()
                        print "product {0} Updated on location {1}".format(product.name, location)
                    else:
                        product = BarcodeProduct.objects.create(
                            name = data["Description"],
                            barcode = data["Upc"],
                            quantity = data["Quantity"],
                            unit_price = data["UnitPrice"],
                            unit_cost = data["UnitCost"],
                            min_stock = data["MinStock"],
                            max_stock = data["MaxStock"],
                            hst = data["Hst"],
                            pst = data["Pst"],
                            active = data["Active"]
                            )
                        print "product {0} created on location {1}".format(product.name, location)  
                    
                    try:
                        category_name = data['Category']
                        if Category.objects.filter(name = category_name).exists():
                            category = Category.objects.get(name = category_name)
                        else:
                            category = Category.objects.create(name = category_name)

                        product.categories.clear()
                        product.categories.add(category)
                        product.save()


                    except Exception, e:
                        print e
                        pass
                except Exception, e:
                    print e 
                    continue

    except Exception as e:
        print e
           

    
        

    

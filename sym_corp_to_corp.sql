--
-- Nodes
--
insert into sym_node_group (node_group_id, description)
values ('users', 'central users data');

insert into sym_node_group (node_group_id, description)
values ('corp', 'Root Amazon');

insert into sym_node_group (node_group_id, description)
values ('store', 'Kiosk');


insert into sym_node_group_link (source_node_group_id, target_node_group_id, data_event_action)
values ('corp', 'users', 'P');
insert into sym_node_group_link (source_node_group_id, target_node_group_id, data_event_action)
values ('users', 'corp', 'W');

insert into sym_node_group_link (source_node_group_id, target_node_group_id, data_event_action)
values ('store', 'corp', 'P');
insert into sym_node_group_link (source_node_group_id, target_node_group_id, data_event_action)
values ('corp', 'store', 'W');




insert into sym_node (node_id, node_group_id, external_id, sync_enabled)
values ('111', 'users', '111', 1);
insert into sym_node_identity values ('111');

--
-- Channels
--

insert into sym_router
(router_id,source_node_group_id,target_node_group_id,router_type,create_time,last_update_time)
values('corp_2_users', 'corp', 'users', 'default',current_timestamp, current_timestamp);

insert into sym_router
(router_id,source_node_group_id,target_node_group_id,router_type,create_time,last_update_time)
values('users_2_corp', 'users', 'corp', 'default',current_timestamp, current_timestamp);

insert into sym_router
(router_id,source_node_group_id,target_node_group_id,router_type,create_time,last_update_time)
values('corp_2_store', 'corp', 'store', 'default',current_timestamp, current_timestamp);

insert into sym_router
(router_id,source_node_group_id,target_node_group_id,router_type,create_time,last_update_time)
values('store_2_corp', 'store', 'corp', 'default',current_timestamp, current_timestamp);




insert into sym_channel
(channel_id, processing_order, max_batch_size, enabled, description)
values('item', 1, 100000, 1, 'item');


   

insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('addressmodel_address','addressmodel_address','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('addressmodel_address','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('addressmodel_address','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('addressmodel_country','addressmodel_country','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('addressmodel_country','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('addressmodel_country','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('auth_group','auth_group','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('auth_group','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('auth_group','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('auth_group_permissions','auth_group_permissions','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('auth_group_permissions','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('auth_group_permissions','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('auth_permission','auth_permission','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('auth_permission','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('auth_permission','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('django_admin_log','django_admin_log','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_admin_log','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_admin_log','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('django_content_type','django_content_type','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_content_type','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_content_type','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('django_cups_favouriteprinter','django_cups_favouriteprinter','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_cups_favouriteprinter','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_cups_favouriteprinter','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('django_cups_printer','django_cups_printer','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_cups_printer','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_cups_printer','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('django_session','django_session','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_session','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('django_session','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('djcelery_crontabschedule','djcelery_crontabschedule','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_crontabschedule','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_crontabschedule','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('djcelery_intervalschedule','djcelery_intervalschedule','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_intervalschedule','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_intervalschedule','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('djcelery_periodictask','djcelery_periodictask','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_periodictask','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_periodictask','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('djcelery_periodictasks','djcelery_periodictasks','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_periodictasks','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_periodictasks','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('djcelery_taskstate','djcelery_taskstate','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_taskstate','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_taskstate','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('djcelery_workerstate','djcelery_workerstate','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_workerstate','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('djcelery_workerstate','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_barcodeproduct','kioskui_barcodeproduct','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_barcodeproduct','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_barcodeproduct','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_cashbox','kioskui_cashbox','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_cashbox','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_cashbox','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_choicesplaceholder','kioskui_choicesplaceholder','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_choicesplaceholder','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_choicesplaceholder','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_extracategoryfields','kioskui_extracategoryfields','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_extracategoryfields','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_extracategoryfields','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_kioskuser','kioskui_kioskuser','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_kioskuser','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_kioskuser','store_2_corp',100,current_timestamp,current_timestamp);

insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_kioskuser','corp_2_users',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_kioskuser','users_2_corp',100,current_timestamp,current_timestamp); 


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_kioskuser_groups','kioskui_kioskuser_groups','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_kioskuser_groups','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_kioskuser_groups','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_kioskuser_user_permissions','kioskui_kioskuser_user_permissions','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_kioskuser_user_permissions','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_kioskuser_user_permissions','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_locationinfo','kioskui_locationinfo','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_locationinfo','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_locationinfo','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_missingbarcodeorkeycard','kioskui_missingbarcodeorkeycard','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_missingbarcodeorkeycard','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_missingbarcodeorkeycard','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_nobarcodeproduct','kioskui_nobarcodeproduct','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_nobarcodeproduct','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_nobarcodeproduct','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_printingobject','kioskui_printingobject','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_printingobject','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_printingobject','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_quantitymovement','kioskui_quantitymovement','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_quantitymovement','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_quantitymovement','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_restockcategorychoice','kioskui_restockcategorychoice','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_restockcategorychoice','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_restockcategorychoice','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_restockfrequency','kioskui_restockfrequency','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_restockfrequency','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_restockfrequency','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_restockinventoryreport','kioskui_restockinventoryreport','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_restockinventoryreport','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_restockinventoryreport','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_restockinventoryreportitems','kioskui_restockinventoryreportitems','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_restockinventoryreportitems','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_restockinventoryreportitems','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_supplier','kioskui_supplier','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_supplier','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_supplier','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_tax','kioskui_tax','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_tax','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_tax','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('kioskui_usercashflow','kioskui_usercashflow','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_usercashflow','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('kioskui_usercashflow','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('oauth2_accesstoken','oauth2_accesstoken','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('oauth2_accesstoken','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('oauth2_accesstoken','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('oauth2_client','oauth2_client','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('oauth2_client','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('oauth2_client','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('oauth2_grant','oauth2_grant','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('oauth2_grant','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('oauth2_grant','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('oauth2_refreshtoken','oauth2_refreshtoken','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('oauth2_refreshtoken','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('oauth2_refreshtoken','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_cart','shop_cart','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_cart','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_cart','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_cartitem','shop_cartitem','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_cartitem','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_cartitem','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_extraorderitempricefield','shop_extraorderitempricefield','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_extraorderitempricefield','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_extraorderitempricefield','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_extraorderpricefield','shop_extraorderpricefield','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_extraorderpricefield','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_extraorderpricefield','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_order','shop_order','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_order','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_order','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_orderextrainfo','shop_orderextrainfo','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_orderextrainfo','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_orderextrainfo','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_orderitem','shop_orderitem','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_orderitem','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_orderitem','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_orderpayment','shop_orderpayment','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_orderpayment','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_orderpayment','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_product','shop_product','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_product','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_product','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_simplecategories_category','shop_simplecategories_category','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_simplecategories_category','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_simplecategories_category','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('shop_simplecategories_category_products','shop_simplecategories_category_products','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_simplecategories_category_products','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('shop_simplecategories_category_products','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('south_migrationhistory','south_migrationhistory','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('south_migrationhistory','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('south_migrationhistory','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('tastypie_apiaccess','tastypie_apiaccess','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('tastypie_apiaccess','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('tastypie_apiaccess','store_2_corp',100,current_timestamp,current_timestamp);


insert into sym_trigger
(trigger_id,source_table_name,channel_id,last_update_time,create_time)
values('tastypie_apikey','tastypie_apikey','item',current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('tastypie_apikey','corp_2_store',100,current_timestamp,current_timestamp);


insert into sym_trigger_router
(trigger_id,router_id,initial_load_order,last_update_time,create_time)
values('tastypie_apikey','store_2_corp',100,current_timestamp,current_timestamp);



update sym_trigger set sync_on_update=1, sync_on_insert=1, sync_on_delete=1, sync_on_incoming_batch=1, last_update_time=current_timestamp;








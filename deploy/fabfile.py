from fabric.api import *

env.user = "test"
env.password = "test88"


def all():

    env.hosts = ['10.8.0.7','10.8.1.11','10.8.1.14','10.8.1.154','10.8.1.42','10.8.0.178','10.8.0.182','10.8.0.146','10.8.0.45','10.8.1.10']

def dev():
    env.hosts = ['10.8.0.7']

def m301():
    env.hosts = ['10.8.1.10']

def m302():
    env.hosts = ['10.8.1.11'] 

def m501():
    env.hosts = ['10.8.0.45']

def grenville():
    env.hosts = ['10.8.1.154']

def albany():
    env.hosts = ['10.8.1.14']

def centralwire():
    env.hosts = ['10.8.1.42']   

def south():
    env.hosts = ['10.8.0.178']

def north():
    env.hosts = ['10.8.0.182'] 

def hsg():
    env.hosts = ['10.8.0.146'] 	 	



def pull():
    with cd("/home/test/code/kiosk"):
        run("git pull origin master")
        env.password = "test88"
        run("sudo service kiosk stop")
        run("sudo service kiosk start")

def change_git_url():
    
    with cd("/home/test/code/kiosk"):
        run("git remote set-url origin git@bitbucket.org:refuel247/kiosk.git")

	


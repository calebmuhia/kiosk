#!/usr/bin/python

import MySQLdb

def IterChunks(sequence, chunk_size):
    res = []
    for item in sequence:
        res.append(item)
        if len(res) >= chunk_size:
            yield res
            res = []
    if res:
        yield res  # yield the last, incomplete, portion

def lookahead(iterable):
    it = iter(iterable)
    last = it.next() # next(it) in Python 3
    for val in it:
        yield last, False
        last = val
    yield last, True

def get_user():
    old_db = MySQLdb.connect("localhost","root","voodoo","kioskapp")

    old_db_cursor = old_db.cursor()

    old_db_cursor.execute("select * from auth_user;")

    datas = old_db_cursor.fetchall()
    insert_st = "INSERT INTO kioskui_kioskuser(id,password,last_login,is_superuser,username,first_name,last_name,email,is_staff,is_active,date_joined) VALUES "



    for data,  last in lookahead(datas):

        
        insert_st += "('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')".format(data[0],data[5],data[9].strftime('%Y-%m-%d %H:%M:%S'),data[8],data[1],data[2].replace("O'", "0"),data[3].replace("O'", "0"),data[4],data[6],data[7],data[10].strftime('%Y-%m-%d %H:%M:%S'))
        if last:
            insert_st+=';'
        else:
            insert_st+=','

    print insert_st        
            
    

    # disconnect from server
    old_db.close()


    new_db = MySQLdb.connect("localhost","root","voodoo","kiosk")


    new_db_cursor = new_db.cursor()

    try:
        new_db_cursor.execute("SET foreign_key_checks = 0;")
        
        new_db_cursor.execute(insert_st)
        new_db.commit()
        new_db_cursor.execute("SET foreign_key_checks = 1;")

    except MySQLdb.Error , e:
        print "Error %d: %s" % (e.args[0],e.args[1])
        new_db.rollback()

    new_db.close() 

def get_user_profile():
    old_db = MySQLdb.connect("localhost","root","voodoo","kioskapp")

    old_db_cursor = old_db.cursor(MySQLdb.cursors.DictCursor)

    old_db_cursor.execute("select id from auth_user;")

    data = old_db_cursor.fetchallDict()

    new_db = MySQLdb.connect("localhost","root","voodoo","voodoony")


    new_db_cursor = new_db.cursor()

    
    for row in data:
        try:
            id = row['id']
            old_db_cursor.execute("select * from kioskui_userprofile where user_id={0}".format(id))
            user_profile = old_db_cursor.fetchone()
            
            try:
                insert_st = "UPDATE kioskui_kioskuser SET location='{0}',company='{1}',cash_deposits='{2}',admin_deposits=0,credit_card_deposits='{3}',debit_card_deposits='{4}',account_expenses='{5}',credit_card_expenses='{6}',debit_card_expenses='{7}'".format(user_profile['location'],user_profile['company'],user_profile['cash_deposits'],user_profile['credit_card_deposits'],user_profile['debit_card_deposits'],user_profile['account_expenses'],user_profile['credit_card_expenses'],user_profile['debit_card_expenses'])
            except:
                continue

            insert_st+="where id={0} ;".format(id)
            print insert_st
            
            
            try:
                new_db_cursor.execute("SET foreign_key_checks = 0;")
                
                new_db_cursor.execute(insert_st)
                new_db.commit()
                new_db_cursor.execute("SET foreign_key_checks = 1;")

            except MySQLdb.Error , e:
                print "Error %d: %s" % (e.args[0],e.args[1])
                new_db.rollback()

        except MySQLdb.Error , e:
            print "Error %d: %s" % (e.args[0],e.args[1])  

    old_db.close()
    new_db.close()



def get_order():
    old_db = MySQLdb.connect("localhost","root","voodoo","kioskapp")

    old_db_cursor = old_db.cursor(MySQLdb.cursors.DictCursor)

    old_db_cursor.execute("select * from shop_order;")

    datas = old_db_cursor.fetchallDict()
    insert_st = "INSERT INTO shop_order(id,user_id,status,order_subtotal,order_total,shipping_address_text,billing_address_text,created,modified,cart_pk) VALUES "



    for data,  last in lookahead(datas):

        insert_st += "('{0}','{1}','{2}','{3}','{4}',NULL,NULL,'{5}','{6}',NULL)".format(data['id'],data['user_id'],int('{0}0'.format(data['status'])),data['order_subtotal'],data['order_total'],data['created'].strftime('%Y-%m-%d %H:%M:%S'),data['modified'].strftime('%Y-%m-%d %H:%M:%S'))
        
        if last:
            insert_st+=';'
        else:
            insert_st+=','
            
    

    # disconnect from server
    old_db.close()


    new_db = MySQLdb.connect("localhost","root","voodoo","kiosk")

    new_db_cursor = new_db.cursor()

    try:
        new_db_cursor.execute("SET foreign_key_checks = 0;")
        new_db_cursor.execute(insert_st)
        new_db.commit()
        new_db_cursor.execute("SET foreign_key_checks = 1;")


    except MySQLdb.Error , e:
        print "Error %d: %s" % (e.args[0],e.args[1])
        new_db.rollback()

    new_db.close()  

def get_order_item():
    old_db = MySQLdb.connect("localhost","root","voodoo","kioskapp")

    old_db_cursor = old_db.cursor()

    old_db_cursor.execute("select * from shop_orderitem;")

    datas = old_db_cursor.fetchall()
    insert_st = "INSERT INTO shop_orderitem(id,order_id,product_reference,product_name,unit_price,quantity,line_subtotal,line_total,product_id) VALUES "



    for data,  last in lookahead(datas):
       

        
        insert_st += "('{0}','{1}','{2}','{3}','{4}','{5}',{6},'{7}','{8}')".format(data[0],data[1],data[2],data[3].replace("'",""),data[4],data[5],data[6],data[7],data[8] if data[8] else 'NULL' )
        if last:
            insert_st+=';'
        else:
            insert_st+=','
            
    

    # disconnect from server
    old_db.close()


    new_db = MySQLdb.connect("localhost","root","voodoo","kiosk")

    new_db_cursor = new_db.cursor()

    try:
        new_db_cursor.execute("SET foreign_key_checks = 0;")
        new_db_cursor.execute(insert_st)
        new_db.commit()
        new_db_cursor.execute("SET foreign_key_checks = 1;")


    except MySQLdb.Error , e:
        print "Error %d: %s" % (e.args[0],e.args[1])
        new_db.rollback()

    new_db.close()

def get_orderpayment():
    
    old_db = MySQLdb.connect("localhost","root","voodoo","kioskapp")

    old_db_cursor = old_db.cursor()

    old_db_cursor.execute("select * from shop_orderpayment;")

    datas = old_db_cursor.fetchall()
    
    new_db = MySQLdb.connect("localhost","root","voodoo","kiosk")

    new_db_cursor = new_db.cursor()


    for row in tuple(IterChunks(datas,1000)):
        insert_st = "INSERT INTO `shop_orderpayment` VALUES "
        for data, last in lookahead(row):

            insert_st += "('{0}','{1}','{2}','{3}','{4}')".format(data[0],data[1],data[2],data[3],data[4])
            if last:
                insert_st+=';'
            else:
                insert_st+=','

        
        
        try:
            new_db_cursor.execute("SET foreign_key_checks = 0;")
            new_db_cursor.execute(insert_st)
            new_db.commit()
            new_db_cursor.execute("SET foreign_key_checks = 1;")


        except MySQLdb.Error , e:
            print "Error %d: %s" % (e.args[0],e.args[1])
            new_db.rollback()

    # disconnect from server
    old_db.close()

    new_db.close()

def get_taxes():
    old_db = MySQLdb.connect("localhost","root","voodoo","kioskapp")

    old_db_cursor = old_db.cursor()

    old_db_cursor.execute("select * from shop_extraorderpricefield;")

    datas = old_db_cursor.fetchall()
    
    new_db = MySQLdb.connect("localhost","root","voodoo","kiosk")

    new_db_cursor = new_db.cursor()


    for row in tuple(IterChunks(datas,1000)):
        insert_st = "INSERT INTO `shop_extraorderpricefield` VALUES "
        for data, last in lookahead(row):

            insert_st += "('{0}','{1}','{2}','{3}','{4}','{5}')".format(data[0],data[1],data[2],data[3],'{}',data[4])
            if last:
                insert_st+=';'
            else:
                insert_st+=','

        
        
        try:
            new_db_cursor.execute("SET foreign_key_checks = 0;")
            new_db_cursor.execute(insert_st)
            new_db.commit()
            new_db_cursor.execute("SET foreign_key_checks = 1;")


        except MySQLdb.Error , e:
            print "Error %d: %s" % (e.args[0],e.args[1])
            new_db.rollback()

    # disconnect from server
    old_db.close()

    new_db.close()

def get_orderextrainfo():
    old_db = MySQLdb.connect("localhost","root","voodoo","kioskapp")

    old_db_cursor = old_db.cursor()

    old_db_cursor.execute("select * from shop_orderextrainfo;")

    datas = old_db_cursor.fetchall()
    
    new_db = MySQLdb.connect("localhost","root","voodoo","kiosk")

    new_db_cursor = new_db.cursor()


    for row in tuple(IterChunks(datas,1000)):
        insert_st = "INSERT INTO `shop_orderextrainfo` VALUES "
        for data, last in lookahead(row):

            insert_st += '("{0}","{1}","{2}")'.format(data[0],data[1],data[2])
            if last:
                insert_st+=';'
            else:
                insert_st+=','

        
        
        try:
            new_db_cursor.execute("SET foreign_key_checks = 0;")
            new_db_cursor.execute(insert_st)
            new_db.commit()
            new_db_cursor.execute("SET foreign_key_checks = 1;")


        except MySQLdb.Error , e:
            print "Error %d: %s" % (e.args[0],e.args[1])
            new_db.rollback()

    # disconnect from server
    old_db.close()

    new_db.close()

def get_shopproduct():
    old_db = MySQLdb.connect("localhost","root","voodoo","kioskapp")

    old_db_cursor = old_db.cursor()

    old_db_cursor.execute("select * from shop_product;")

    datas = old_db_cursor.fetchall()
    
    new_db = MySQLdb.connect("localhost","root","voodoo","kiosk")

    new_db_cursor = new_db.cursor()


    for row in tuple(IterChunks(datas,1000)):
        insert_st = "INSERT INTO `shop_product` VALUES "
        for data, last in lookahead(row):

            insert_st += '("{0}","6","{1}","{2}","{3}","{4}","{5}","{6}")'.format(data[0],data[2].replace('"',''),data[3],data[4],data[5],data[6],data[7])
            if last:
                insert_st+=';'
            else:
                insert_st+=','

        
        
        try:
            new_db_cursor.execute("SET foreign_key_checks = 0;")
            new_db_cursor.execute(insert_st)
            new_db.commit()
            new_db_cursor.execute("SET foreign_key_checks = 1;")


        except MySQLdb.Error , e:
            print "Error %d: %s" % (e.args[0],e.args[1])
            new_db.rollback()

    # disconnect from server
    old_db.close()

    new_db.close()

def get_kioskbarcodeproduct():
    old_db = MySQLdb.connect("localhost","root","voodoo","kioskapp")

    old_db_cursor = old_db.cursor(MySQLdb.cursors.DictCursor)

    old_db_cursor.execute("select * from kioskui_barcodeproduct;")

    datas = old_db_cursor.fetchallDict()
    
    new_db = MySQLdb.connect("localhost","root","voodoo","kiosk")

    new_db_cursor = new_db.cursor()


    for row in tuple(IterChunks(datas,1000)):
        insert_st = "INSERT INTO kioskui_barcodeproduct(product_ptr_id,barcode,quantity,unit_cost,min_stock,max_stock,pst,hst) VALUES  "
        for data, last in lookahead(row):

            insert_st += '("{0}","{1}","{2}","{3}","{4}","{5}","{6}","{7}")'.format(data['product_ptr_id'],data['barcode'],data['quantity'],data['unit_cost'],data['min_stock'],data['max_stock'],data['pst'],data['hst'])
            if last:
                insert_st+=';'
            else:
                insert_st+=','

        
        
        try:
            new_db_cursor.execute("SET foreign_key_checks = 0;")
            new_db_cursor.execute(insert_st)
            new_db.commit()
            new_db_cursor.execute("SET foreign_key_checks = 1;")


        except MySQLdb.Error , e:
            print "Error %d: %s" % (e.args[0],e.args[1])
            new_db.rollback()

    # disconnect from server
    old_db.close()

    new_db.close()







import sys
if __name__ == '__main__':

    if len(sys.argv) == 1:
        print "Usage python migrate <option> \n"




        print """user
get_user_profile
get_order
get_order_item
get_orderpayment
get_taxes
get_orderextrainfo
get_shopproduct
get_kioskbarcodeproduct"""

    elif 'get_user' in sys.argv[1]:
        get_user()

    elif 'get_profile' in sys.argv[1]:
        get_user_profile()
    
    elif 'get_order' in sys.argv[1]:
        get_order()  
    elif 'order_item' in sys.argv[1]:
        get_order_item() 
    elif 'orderpayment' in sys.argv[1]:
        get_orderpayment()
    elif 'get_taxes' in sys.argv[1]:
        get_taxes() 
    elif 'orderextrainfo' in sys.argv[1]:
        get_orderextrainfo()    
    elif 'get_shopproduct' in sys.argv[1]:
        get_shopproduct() 
    elif 'get_kioskbarcodeproduct' in sys.argv[1]:
        get_kioskbarcodeproduct()
                                



        
    




#/bin/bash
#Precondition Apache, ImageMagick, Perl
#Usage sh screenStream.sh <frequency> <framewidth> <location>
#<location> This should be a folder in apache such that the image screenShot.jpg is hosted from apache
#<framewidth> The amount of pixels across a frame will be after being resized
#<frquency> input values are in microseconds 1000 = 1 millisecond 1000000 = 1 seconds so for 4fps the value would be 250000
while (sleep 1 &&  import -window root -resize 640 /home/test/code/kiosk/scripts/screenshots/images/screenShot.jpg >> /home/test/code/kiosk/scripts/logs/screenshots.log 2>&1 ) &
do
  wait $!
done

#! /bin/bash
#0-59/1 * * * * /bin/bash /home/test/code/kiosk/scripts/check_zmtrigger.sh
debug_out=/dev/null
logger_opts="-t $0"
if [ "$debug_out" = "/dev/stdout" ]
then
        logger_opts="$logger_opts -s"
fi
echo 'begin checking...' | logger $logopts
        
CHECK_RET=$(ps -ef | grep "zmtrigger.pl" | grep -v "grep" | wc -l)
echo $CHECK_RET | logger $logopts
#CHECK_RET = $?
echo $CHECK_RET
if [[ $CHECK_RET == 0 ]] # none exist
        then
                  echo 'not running, restarting' | logger $logopts
        cmd='/usr/bin/perl -wT /usr/bin/zmtrigger.pl &'
        eval $cmd
fi
        

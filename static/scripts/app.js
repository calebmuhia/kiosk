'use strict'

angular.module('kioskui', [
    
    'ngResource',
    'ngRoute',
    'ngCookies',
    'ngKeypad',
    'ngDraggable',
    'ui.bootstrap',
    'toaster'
    // 'toaster',
    ])

.config(['$resourceProvider', function ($resourceProvider) {
       // Don't strip trailing slashes from calculated URLs
       $resourceProvider.defaults.stripTrailingSlashes = false;
     }])
.run( function run( $http, $cookies,$anchorScroll  ){

    // For CSRF token compatibility with Django
    $http.defaults.headers.post['X-CSRFToken'] = $cookies['csrftoken'];
    $anchorScroll.yOffset = 50;   // always scroll by 50 extra pixels
})


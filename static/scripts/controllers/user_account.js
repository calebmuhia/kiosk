'use strict'

angular.module('kioskui')

.controller('UserAccount', ['$scope','User', '$rootScope','$modal', function($scope, User, $rootScope,$modal){
    $rootScope.counter = 90

    $scope.init_user = function(user_id){
        console.log(user_id)
        $scope.user_object = User.get({id:user_id}, function(data){
            $scope.user = data
            

        });




    }
    $scope.element = null
    var self = this;
        var selectedInputIndex = 0;

        /**
         * This example show how to toggle locked/unlocked
         * state of the keypad.
         */  
        $scope.toggleKeypadLock =  function(){
            toggleLock();
            //Toggling KeyPad Locking state, second argument is Keypad ID
            $scope.$emit(Keypad.TOGGLE_LOCKING, "numeric");
        }



        /**
         * This example show how to toggle open/close
         * state of the keypad.
         */  
        $scope.toggleKeypadOpening =  function(){
            //Toggling KeyPad Locking state, second argument is Keypad ID
            $scope.$emit(Keypad.TOGGLE_OPENING, "numeric");
        }


        /**
         * This example show how to toggle open/close
         * state of the keypad and set a position at the same time.
         */  
        $scope.toggleKeypadPosition =  function(){
            var params = {
                position:{
                    x:600,
                    y:70
                }
            };
            //Toggling KeyPad Locking state, second argument is Keypad ID, third is the param object.
            $scope.$emit(Keypad.TOGGLE_OPENING, "numeric",params);
        }


        /**
         * This example show how to listen for the KEY_PRESSED event thrown
         * by the keypad and do what you need to do with it.
         */  
        $scope.listenedString = "";

        $scope.$on(Keypad.KEY_PRESSED, function(event,data){
            $scope.listenedString += data;
      
            $scope.$apply();
        });







        //DEMO APPLICATION CODE NOT WORTH READING 

        $scope.opened = false;
        $scope.openLabel = "Open Keypad"
        $scope.editing = false

        $scope.$on(Keypad.OPENED, function(event,id){
            $scope.openLabel = "Close Keypad";
            $scope.opened = true;
            
            if(!$scope.$$phase){
                $scope.$apply();
            }
        });

        


        $scope.$on(Keypad.MODIFIER_KEY_PRESSED, function(event,key,id){
           

            switch(key){
                case "ENTER":
                    $scope.savefield();
                    
                    break;
                case "CAPS":
                   console.log('caps loc')
                    break;
                case 'DELETE':
                    $scope.listenedString = $scope.listenedString.substring(0, $scope.listenedString.length - 1);  
                    $scope.$digest();  
            }   
        });


        function findInputInInputs(input,inputs){
            var foundIndex = 0;
            inputs.each(function(index){
                if($(this).is(input.eq(0))){
                    foundIndex = index;
                }
            });

            return foundIndex;
        }


        $scope.locked = true;
        $scope.lockLabel = "Lock Keypad"

        function toggleLock(){
            $scope.locked = !$scope.locked;
            
            if($scope.locked){
                $scope.lockLabel = "Lock Keypad"
            }else{
                $scope.lockLabel = "Unlock Keypad"
            }
        }
        

        $scope.editfield = function(element_id){
            $scope.element = element_id

           
            
            if($scope.opened == false && $scope.editing==false ) {
                $scope.$emit(Keypad.TOGGLE_OPENING, "numeric");
            }
            


            if (element_id=='id_first_name'){
                $scope.editing = true
                $scope.editfirstname=!$scope.editfirstname
                $scope.listenedString = $scope.user.first_name
                

            }
            if (element_id=='id_last_name'){
                $scope.editing = true
                $scope.editlastname=!$scope.editlastname
                $scope.listenedString = $scope.user.last_name
               
            }
            if (element_id=='id_email'){
                $scope.editing = true
                $scope.editemail=!$scope.editemail
                $scope.listenedString = $scope.user.email
                
            }

        }

        $scope.save_user = function(){
            $scope.user_object = User.update($scope.user)
        }

        $scope.savefield = function(){
           
             var element_id = $scope.element
             
             if ($scope.opened==true) {$scope.$emit(Keypad.TOGGLE_OPENING, "numeric");};
            
           

            if (element_id=='id_first_name'){
                
                $scope.editfirstname=!$scope.editfirstname
                $scope.user.first_name = $scope.listenedString
                $scope.listenedString = ''
                $scope.save_user()
                $scope.editing = false
                if(!$scope.$$phase){
                $scope.$apply();
            }

                
            }
            if (element_id=='id_last_name'){
                
                $scope.editlastname=!$scope.editlastname
                $scope.user.last_name = $scope.listenedString
                $scope.listenedString = ''
                $scope.save_user()
                $scope.editing = false
                if(!$scope.$$phase){
                $scope.$apply();
            }
            }
            if (element_id=='id_email'){
              
                $scope.editemail=!$scope.editemail
                $scope.user.email = $scope.listenedString
                $scope.listenedString = ''
                $scope.save_user()
                $scope.editing = false
                if(!$scope.$$phase){
                $scope.$apply();
            }
            }

        }



$scope.connect_app = function(){

    

    var modalInstance = $modal.open({
      templateUrl: 'connect_app.html',
      controller: 'ConnectApp',
      size: 'lg',
      resolve: {
        username:function(){
            return $scope.user.username
        }
        
      }
    });

    modalInstance.result.then(function (object) {
    
    }, function () {
      console.log('Modal dismissed at: ' + new Date());
    });
  

    

}      


}])

.controller('ConnectApp', ['$scope', '$http', '$modalInstance', function($scope, $http,$modalInstance){


$scope.get_barcode = function(data){
  var bcid='qrcode';
  var len = 6 
  var text = angular.toJson(data);
  var rot = 'N';
  var scale=2
  var bw = new BWIPJS
  var opts = {}
  opts['parsefnc'] = bw.value(true);
  opts['alttext'] = bw.value(text)

  if (opts.alttext)
    opts.includetext = bw.value(true);

  // Render the bar code
  bw.bitmap(new Bitmap);
  bw.scale(scale, scale);
  bw.push(text);
  bw.push(opts);
  bw.call(bcid);
  var png = bw.bitmap().getPNG(rot);

  return png


} 

$http.get("/authenticate_app/")
.success(function(data){
  var png = $scope.get_barcode(data);
   
$scope.dataURI = 'data:image/png;base64,' + btoa(png); 

})






$scope.ok = function(){
    $modalInstance.close()
}

}])
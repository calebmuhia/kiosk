'use strict'

angular.module('kioskui')

.controller('ScanCtr', ['$scope','$rootScope', '$http','$cookies', 'transformRequestAsFormPost','$timeout', '$modal','toaster', function($scope, $rootScope, $http, $cookies, transformRequestAsFormPost, $timeout, $modal, toaster){

    $scope.init = function(cart_quantity){
        $scope.csrftoken = $cookies.csrftoken
        console.log($cookies.csrftoken)
        $scope.cart_quantity = cart_quantity
    }
    $scope.outoftime = false
    $scope.popups = function(code,title,msg){
           toaster.pop(code, title, msg);
       };
    $rootScope.counter = 22;

    $scope.cart_delete = function(){
        $http.get("/shop/cart/delete").success(function(){
            
        })

    }
    $rootScope.onTimeout = function(){
        $rootScope.counter-=1;
        if ($rootScope.counter==0){
            if ($scope.cart_quantity > 0){
                if ($scope.outoftime == false)
            {
                $scope.cart_delete()
               $rootScope.notifyLogout()
               $scope.outoftime = true

                
            }
            else{
              window.location = "/accounts/logout"  
            }
            }

            else{
                window.location = "/accounts/logout"

            }

        }
        if($rootScope.counter<10){
            $scope.timerClass = 'danger'
        }
        else{
            $scope.timerClass = ''
        }
        mytimeout = $timeout($rootScope.onTimeout,1000);
    }
    var mytimeout = $timeout($rootScope.onTimeout,1000);

    $rootScope.stop = function(){
        $timeout.cancel(mytimeout);
    }
    $rootScope.start = function(){
        $timeout($rootScope.onTimeout, 1000);
    }
    $rootScope.restart = function(){
        $rootScope.counter = 18;
        $timeout($rootScope.onTimeout, 1000);

    }

    $rootScope.notifyLogout = function(){
        $rootScope.counter = 5

        var modalInstance = $modal.open({
              templateUrl: 'logout.html',
              size: 'lg',
              
                
              }
            );
        
    }




    var keypress_input = ''
    $scope.keypressFun = function(event, view){
        
       var string_from_charcode = String.fromCharCode(event.which);
            if(event.which != 13) {
                keypress_input = keypress_input + string_from_charcode;


                console.log("keypress input: " + keypress_input);
            } else {
                event.preventDefault();
                
                var keypress_url = '/keypress/?keypress='+keypress_input
                $http.get(keypress_url).success(function(response){
                    $rootScope.counter += 3

                    if (response.error == undefined){
                        if (response.type == "USER"){

                          if (response.logged_in == 'false'){
                                 
                            

                            $scope.items = response.items
                            console.log($scope.items.length)

                            if($scope.items.length > 0){
                                console.log(window.location.href.indexOf('cart'))

                                if (window.location.href.indexOf('cart') != -1){
                                    window.location = '/auto_checkout/'
                                }
                                else{
                                    $scope.open = function (size) {

                                    var modalInstance = $modal.open({
                                      templateUrl: 'myModalContent.html',
                                      controller: 'ModalInstanceCtrl',
                                      size: size,
                                      resolve: {
                                        items: function () {
                                          return $scope.items;
                                        }
                                      }
                                    });

                                    modalInstance.result.then(function (selectedItem) {
                                      $scope.selected = selectedItem;
                                      window.location = "/auto_checkout/"
                                    }, function () {
                                      window.location = "/accounts/logout"
                                    });
                            }

                            $scope.open()

                                }
                                

                            }
                            else{
                                if (window.location.href.split('?')[1] == "next=/auto_checkout/"){
                                window.location = "/auto_checkout/";
                            }
                            else if (window.location.href.split('?')[1] == "next=/checkout/"){
                                window.location = "/checkout/";
                            }
                            else if (window.location.href.split('?')[1] == "next=/user_account/"){
                                // help users loggin to their accounts
                                window.location = "/user_account/";
                            }
                            else{window.location = "/cart";}
                            }

                            }

                            else if(response.logged_in == 'true'){

                            if (window.location.href.split('?')[1] == "next=/auto_checkout/"){
                                window.location = "/auto_checkout/";
                            }
                            else if (window.location.href.split('?')[1] == "next=/checkout/"){
                                window.location = "/checkout/";
                            }
                            else if (window.location.href.split('?')[1] == "next=/user_account/"){
                                // help users loggin to their accounts
                                window.location = "/user_account/";
                            }
                            else{window.location = "/cart";}

                            }
                    }

                    else if (response.type == "PRODUCT") {

                            var item_data = {
                                "add_item_id":response.item_id,
                                'csrfmiddlewaretoken':$cookies.csrftoken
                            };
                            console.log($.param(item_data))

                            console.log(response.add_to_cart_url);
                            $http({
                                method:'post',
                                url:response.add_to_cart_url,
                                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                                data: $.param(item_data)
                            }).success(function(data) {
                                console.log(data)
                                 window.location = "/cart/";
                                if (data == 'Ok<br />') { // FIXME: extremely fragile, but crucial, response-string checking (!!!!!)
                                    console.log('product added to cart')
                                    window.location = "/cart/"; // FIXME: not ajax, but works for now.
                                }//if
                            });

                        }//if-else

                    else if (response.type=="redirect"){
                        window.location = response.url
                    }   

                    else{
                        $scope.popups('error', "Error", "Barcode/Keytag "+keypress_input+' Scanned Not Found, \n Admin Has been Notified')
                    } 
                    
                }});
keypress_input = '';

            }
    }
    
}]);

angular.module('kioskui').controller('ModalInstanceCtrl', function ($scope, $modalInstance, items) {

  $scope.items = items;
  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});
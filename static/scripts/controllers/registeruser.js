'use strict'

angular.module("kioskui")
.controller("RegisterUserCtr", ["$scope", "$http","$cookies","$rootScope", function($scope, $http, $cookies, $rootScope){
    $rootScope.counter = 60


    $scope.init_register = function(keypress){
    	
    	$scope.userObject = {
    	'keypress':keypress,
    	'full_name':"",
    	'csrfmiddlewaretoken':$cookies.csrftoken
    	
    }
    console.log($scope.userObject)

    }
    
    $scope.register_user = function(){
    	$scope.register = true
    	$scope.first_name = true
    	$scope.toggleKeypadOpening()
    	

    }
    $scope.cancel = function(){
    	window.location = "/cart"
    }

    $scope.complete = function(){
    	$scope.userObject.full_name = $scope.listenedString
    	if ($scope.userObject.full_name == '') {
            console.log("failed")
            alert("Please Enter Atleast One Name")
            return 

    	}
    	

    	$http({
            method:'post',
            url:'/register_user/create_user/',
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $scope.userObject
        }).then(function(data){
        	console.log(data)
        	window.location = "/user_account"

        },function(error){
        	console.log(error)

        } 

        )
    	
    	
    }

    var self = this;
        var selectedInputIndex = 0; 

 
        $scope.toggleKeypadOpening =  function(){
            
            $scope.$emit(Keypad.TOGGLE_OPENING, "first_name");
        }


        
        $scope.toggleKeypadPosition =  function(){
            var params = {
                position:{
                    x:600,
                    y:70
                }
            };
            
            $scope.$emit(Keypad.TOGGLE_OPENING, "first_name",params);
        }
 
        $scope.listenedString = "";

        $scope.$on(Keypad.KEY_PRESSED, function(event,data){
        	if ($scope.caps){
        		data = data.toUpperCase()
            	
            }
            $scope.listenedString += data;

      
            $scope.$apply();
        });

        $scope.$on(Keypad.MODIFIER_KEY_PRESSED, function(event,data){
        	switch(data){
        		case "DELETE":
        		      $scope.listenedString = $scope.listenedString.slice(0, $scope.listenedString.length-1)
        		      break;
        		case "CAPS":
                      $scope.caps = $scope.caps==true ? false : true
                      console.log($scope.caps)
                      break;
                case "SPACE":
                	$scope.listenedString += ' '
                	break;
                      
        	}
            
      
            $scope.$apply();
        });



}])
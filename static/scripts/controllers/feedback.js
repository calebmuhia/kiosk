'use strict'

angular.module("kioskui")

.controller('FeedbackCtrl', ['$scope','$http',"$modal", function($scope,$http,$modal){


$scope.feedback = function(){
    var modalInstance = $modal.open({
          templateUrl: 'feedback.html',
          controller: 'ModalFeedbackCtrl',
          size: 'lg',

        });
    
        modalInstance.result.then(function (feedback) {
            $http.post('/api/v4/feedback/', feedback)

        
        }, function () {
          console.log('Modal dismissed at: ' + new Date());
        });
}    
}])

.controller('ModalFeedbackCtrl', ['$scope','$http', '$modalInstance','$rootScope',  function($scope, $http, $modalInstance, $rootScope){

    $scope.feedback = {
        text:'',
        name:''     
    }
    $rootScope.counter = 60



    var selectedInputIndex = 0;

        /**
         * This example show how to toggle locked/unlocked
         * state of the keypad.
         */  
        $scope.toggleKeypadLock =  function(){
            toggleLock();
            //Toggling KeyPad Locking state, second argument is Keypad ID
            $scope.$emit(Keypad.TOGGLE_LOCKING, "numeric");
        }



        /**
         * This example show how to toggle open/close
         * state of the keypad.
         */  
        $scope.toggleKeypadOpening =  function(){
            //Toggling KeyPad Locking state, second argument is Keypad ID
            $scope.$emit(Keypad.TOGGLE_OPENING, "numeric");
        }
        $scope.selectedItem = ''

        $scope.selected = function(element){
            
                $scope.selectedItem = element
                if (element == 'text'){
                $scope.listenedString = $scope.feedback.text;

                }
                else if (element == 'name'){
                $scope.listenedString = $scope.feedback.name;
                }

        }


        /**
         * This example show how to toggle open/close
         * state of the keypad and set a position at the same time.
         */  
        $scope.toggleKeypadPosition =  function(){
            console.log("keypad open")
            var params = {
                position:{
                    x:406,
                    y:88
                }
            };
            //Toggling KeyPad Locking state, second argument is Keypad ID, third is the param object.
            $scope.$emit(Keypad.TOGGLE_OPENING, "numeric",params);
        }


        /**
         * This example show how to listen for the KEY_PRESSED event thrown
         * by the keypad and do what you need to do with it.
         */  
        $scope.listenedString = "";

        $scope.$on(Keypad.KEY_PRESSED, function(event,data){
            $scope.listenedString += data;

            if($scope.selectedItem == 'text'){
                $scope.feedback.text = $scope.listenedString
                $scope.listenedString = $scope.feedback.text

            }
            else if($scope.selectedItem == 'name'){
                $scope.feedback.name = $scope.listenedString
                $scope.listenedString = $scope.feedback.name

            }

            $scope.$apply();


        });





        //DEMO APPLICATION CODE NOT WORTH READING 

        $scope.opened = false;
        $scope.openLabel = "Open Keypad"

        $scope.$on(Keypad.OPENED, function(event,id){
            $scope.openLabel = "Close Keypad";
            $scope.opened = true;
            
            if(!$scope.$$phase){
                $scope.$apply();
            }
        });

        $scope.$on(Keypad.CLOSED, function(event,id){
            $scope.openLabel = "Open Keypad";
            $scope.opened = false;

            if(!$scope.$$phase){
                $scope.$apply();
            }
        });


        $scope.$on(Keypad.MODIFIER_KEY_PRESSED, function(event,key,id){
            var focusedInput = $('a[data-ng-keypad-input]:focus'),
                inputs = $('a[data-ng-keypad-input]'),
                foundIndex = findInputInInputs(focusedInput,inputs),
                index = 0;

            switch(key){
                case "PREVIOUS":
                    if(!focusedInput.length){
                        index = inputs.length-1;
                    }else{
                        if(foundIndex===0){
                            index = inputs.length-1;
                        }else{
                            index = foundIndex-1;
                        }
                    }
                    inputs.eq(index).focus();
                    break;
                case "NEXT":
                    if(focusedInput.length){
                        if(foundIndex===inputs.length-1){
                            index = 0;
                        }else{
                            index = foundIndex+1;
                        }
                    }
                    inputs.eq(index).focus();
                    break;
                case "SPACE":
                    $scope.listenedString+= ' ';
                    
                    break;    
                case "ENTER":
                    $scope.toggleKeypadOpening();
                    
                    break;
                case "CAPS":
                   console.log('caps loc')
                    break;
                case 'DELETE':
                    $scope.listenedString = $scope.listenedString.substring(0, $scope.listenedString.length - 1);  
                    $scope.$digest();    

            }   
        });


        function findInputInInputs(input,inputs){
            var foundIndex = 0;
            inputs.each(function(index){
                if($(this).is(input.eq(0))){
                    foundIndex = index;
                }
            });

            return foundIndex;
        }


        $scope.locked = true;
        $scope.lockLabel = "Lock Keypad"

        function toggleLock(){
            $scope.locked = !$scope.locked;
            
            if($scope.locked){
                $scope.lockLabel = "Lock Keypad"
            }else{
                $scope.lockLabel = "Unlock Keypad"
            }
        }
    



   $scope.ok = function () {
    $modalInstance.close($scope.feedback);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };

    
}])
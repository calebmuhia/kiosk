'use strict'

angular.module('kioskui')

.controller('KeyInCtr', ['$scope','$rootScope','$http','$cookies', function($scope, $rootScope,$http, $cookies){
            
      $scope.number_write =function(x)
        {
            console.log(x)
            var text_box = angular.element("#number");
            console.log(text_box)
            if(x>=0 && x<=9)
            {
                if(isNaN(text_box.val()))
                   text_box.val(0);

                text_box.val(text_box.val()+x);
            }

        }

        $scope.number_clear= function()
        {
            angular.element("#number").val('')
        }

        function number_c()
        {
            var inputString = angular.element("#number").val();
            var shortenedString = inputString.substr(0,(inputString.length -1));
            angular.element("#number").val(shortenedString);

        }
    

    $scope.enter_code = function(){
        console.log("called")

        var barcode = angular.element("#number").val()

        var url = "/keypress/?keypress="+barcode

        $http.get(url).success(function(response){
            console.log(response)
    if (response == "Neither a username nor a product"){

                       toastr.error(" <button type='button' id='okBtn' class='nice large blue radius button'>Ok</button>", "We have detected a problem while scanning this product. \nThe problem has been reported to our customer service desk. \nIf you require further assistance please call \n613-240-5599.\n", "ERROR")


                    }
                    else if(response.error == undefined){

                        if(response.type=="PRODUCT"){

                            var item_data = {
            "add_item_id":response.item_id,
            'csrfmiddlewaretoken':$cookies.csrftoken
        };
        

        
        $http({
            method:'post',
            url:"/shop/cart/",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: $.param(item_data)
        }).success(function(data) {
            console.log(data)
             window.location = "/cart/";
            if (data == 'Ok<br />') { // FIXME: extremely fragile, but crucial, response-string checking (!!!!!)
                console.log('product added to cart')
                window.location = "/cart/"; // FIXME: not ajax, but works for now.
            }//if
        });

                        }
                    }
        


        });


    	

    }
    
}])

'use strict'

angular.module('kioskui')

.controller('CheckoutDebitCtr', ['$scope','PayByDebitService', '$timeout', '$rootScope', function($scope, PayByDebitService, $timeout, $rootScope){
$rootScope.counter = 90
var completeTransaction = function(amount){
//     window.onbeforeunload = function (event) {
//   var message = 'Sure you want to leave?';
//   if (typeof event == 'undefined') {
//     event = window.event;
//   }
//   if (event) {
//     event.returnValue = message;
//   }
//   return message;
// }

         PayByDebitService.completeTransaction()
                
                .then(function(data) {
                    console.log(data)
                    // promise fulfilled
                    if (data.status==="success") {
                        console.log('success')
                        $scope.error = 'success';
                        $scope.msg = data.msg
                        $scope.response_status = "success"
                        $timeout(function() {
                            window.location = data.url
                        }, 3000);
                        
                        

                        
                        
                    } else {
                        $scope.error = true
                        $scope.msg = data.msg
                        $scope.response_status = "danger"
                        
                    }
                }, function(error) {
                    console.log("cannot access resource")
                    $scope.error = true
                        $scope.msg = "Server Error Logout to fix"
                        $scope.response_status = "danger"
                    
                });
}


$scope.init = function(amount, is_online){
    
    console.log("data", amount)
    $scope.amount = amount
    $scope.is_online = is_online
    if (is_online==="False"){
        console.log('is_online', is_online)
        $scope.error = true
        console.log("cannot access resource")
                    $scope.error = true
                        $scope.msg = "Due to a network connectivity issue, payment via the debit terminal is currently UNAVAILABLE.  Please call  (613) 784 0192 to restore."

                        $scope.response_status = "danger"
    }
    else{
        completeTransaction()
    }
    
    
   
}

$scope.tryagain = function(){

    if ($scope.is_online === 'False'){
        window.location.reload();
    }
    else{
        $scope.error = false
    completeTransaction()
    }
    
}




}] );
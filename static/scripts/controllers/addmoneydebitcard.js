'use strict'

angular.module('kioskui')

.controller('DebitCtr', ['$scope','DebitService','$rootScope', function($scope, DebitService, $rootScope){

   $rootScope.counter = 90

$scope.initializePinpad = function(amount){
         DebitService.updateAccount(amount)

                
                .then(function(data) {
                    // promise fulfilled
                    if (data.status==="success") {
                        console.log('success')
                        $scope.error = 'success';
                        $scope.msg = data.msg
                        $scope.response_status = "success"
                        $scope.balance = data.balance
                        angular.element('#balance').html("Balance : $"+data.balance) 

                        
                        
                    } else {
                        $scope.error = true
                        $scope.msg = data.msg
                        $scope.response_status = "danger"
                        
                    }
                }, function(error) {
                    console.log("cannot access resource")
                    $scope.error = true
                        $scope.msg = "Server Error Logout to fix"
                        $scope.response_status = "danger"
                    
                });
}


$scope.init = function(amount,is_online){
    $scope.amount = amount
    

    console.log("data", amount)
    $scope.amount = amount
    $scope.is_online = is_online
    if (is_online==="False"){
        console.log('is_online', is_online)
        $scope.error = true
        console.log("cannot access resource")
        $scope.error = true
        $scope.msg = "Due to a network connectivity issue, payment via the debit terminal is currently UNAVAILABLE.  Please call  (613) 784 0192 to restore."

        $scope.response_status = "danger"
    }
    else{
        $scope.initializePinpad($scope.amount)
    }
}

$scope.tryagain = function(){
    $scope.error = false
    
    if ($scope.is_online === 'False'){
        window.location.reload();
    }
    else{
        $scope.error = false
    $scope.initializePinpad($scope.amount)
    }
}




}] );
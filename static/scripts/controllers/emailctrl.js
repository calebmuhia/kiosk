'use strict'
angular.module("kioskui")
.controller('EmailCtrl', ['$scope','$rootScope','$http', function($scope,$rootScope,$http){
    $rootScope.counter = 40
    $scope.initEmail = function(email){
        
        $scope.email = email;
        $scope.edit=false
        $scope.listenedString = email
    }
    $scope.editEmail = function(){
        $scope.edit=true;

        $scope.$emit(Keypad.TOGGLE_OPENING, "numeric");
       
    }

    $scope.savefield = function(){
           
             
             
             if ($scope.opened==true) {$scope.$emit(Keypad.TOGGLE_OPENING, "numeric");};
             console.log($scope.listenedString)

             $scope.email = $scope.listenedString
             

             angular.element("#id_email").val($scope.email)
$scope.edit = false;
$scope.$apply();






            } 

    $scope.element = null
    var self = this;
        var selectedInputIndex = 0;

        /**
         * This example show how to toggle locked/unlocked
         * state of the keypad.
         */  
        $scope.toggleKeypadLock =  function(){
            toggleLock();
            //Toggling KeyPad Locking state, second argument is Keypad ID
            $scope.$emit(Keypad.TOGGLE_LOCKING, "numeric");
        }



        /**
         * This example show how to toggle open/close
         * state of the keypad.
         */  
        $scope.toggleKeypadOpening =  function(){
            //Toggling KeyPad Locking state, second argument is Keypad ID
            $scope.$emit(Keypad.TOGGLE_OPENING, "numeric");
        }


        /**
         * This example show how to toggle open/close
         * state of the keypad and set a position at the same time.
         */  
        $scope.toggleKeypadPosition =  function(){
            var params = {
                position:{
                    x:600,
                    y:70
                }
            };
            //Toggling KeyPad Locking state, second argument is Keypad ID, third is the param object.
            $scope.$emit(Keypad.TOGGLE_OPENING, "numeric",params);
        }


        /**
         * This example show how to listen for the KEY_PRESSED event thrown
         * by the keypad and do what you need to do with it.
         */  
        $scope.listenedString = "";

        $scope.$on(Keypad.KEY_PRESSED, function(event,data){
            console.log(data)
            $scope.listenedString += data;
      
            $scope.$apply();
        });







        //DEMO APPLICATION CODE NOT WORTH READING 

        $scope.opened = false;
        $scope.openLabel = "Open Keypad"
        $scope.editing = false

        $scope.$on(Keypad.OPENED, function(event,id){
            $scope.openLabel = "Close Keypad";
            $scope.opened = true;
            
            if(!$scope.$$phase){
                $scope.$apply();
            }
        });

        


        $scope.$on(Keypad.MODIFIER_KEY_PRESSED, function(event,key,id){
           

            switch(key){
                case "ENTER":
                    $scope.savefield();
                    
                    break;
                case "CAPS":
                   console.log('caps loc')
                    break;
                case 'DELETE':
                    $scope.listenedString = $scope.listenedString.substring(0, $scope.listenedString.length - 1);  
                    $scope.$digest();  
            }   
        });


        

        $scope.locked = true;
        $scope.lockLabel = "Lock Keypad"

        function toggleLock(){
            $scope.locked = !$scope.locked;
            
            if($scope.locked){
                $scope.lockLabel = "Lock Keypad"
            }else{
                $scope.lockLabel = "Unlock Keypad"
            }
        }

        $scope.sendToPrinter = function(){
            $http.get('/printingviaprinter').success(function(){
                window.location = '/accounts/logout'
            })
        }

    
}])
'use strict'

angular.module('kioskui')

.controller('CashCtr', ['$scope','CashService','$rootScope',function($scope, CashService, $rootScope){
    $rootScope.counter = 90

$scope.updateaccount = function(user){

         CashService.updateAccount(user)
                
                .then(function(data) {
                    console.log(data)
                    // promise fulfilled
                    if (data.status==="success") {
                        console.log('success')
                        $scope.error = 'success';
                        $scope.msg = data.msg
                        $scope.response_status = "success"
                        $scope.balance = data.balance
                        $scope.amount = data.amount
                        console.log(data.amount)
                        angular.element('#balance').html("Balance : $"+data.balance) 

                        
                        
                    } else {
                        $scope.error = true
                        $scope.msg = data.msg
                        $scope.response_status = "danger"
                        
                    }
                }, function(error) {
                    console.log("cannot access resource")
                    $scope.error = true
                        $scope.msg = "Server Error Logout to fix"
                        $scope.response_status = "danger"
                    
                });
}


$scope.init = function(user){
    console.log("data", user)
    $scope.user = user
    $scope.updateaccount(user)
}

$scope.tryagain = function(){
    $scope.error = false
    $scope.updateaccount($scope.user)
}




}] );
'use strict'

angular.module('kioskui')

.controller("LogoutCtrl", ["$scope", "$modal", "$http", function($scope, $modal, $http){

    $scope.logoutInit = function(cart_quantity){
    	$scope.cart_quantity = cart_quantity

    }

    





    $scope.logout = function(){
    	if ($scope.cart_quantity > 0){
    		$scope.open = function (size) {

                                    var modalInstance = $modal.open({
                                      templateUrl: 'logoutModal.html',
                                      controller: 'ModalInstanceLogoutCtrl',
                                      size: size,
                                      resolve: {
                                        items: function () {
                                          return $scope.cart_quantity;
                                        }
                                      }
                                    });

                                    modalInstance.result.then(function (selectedItem) {
                                      
                                      window.location = "/checkout"
                                    }, function () {
                                      
                                    });
                            }

                            $scope.open()
    	}
    	else{
    		window.location = "/accounts/logout"
    	}

    }



}]);

angular.module('kioskui').controller('ModalInstanceLogoutCtrl', function ($scope, $modalInstance, items, $http) {

  $scope.items = items;
  $scope.returned = true
    $scope.returnitems = function(){
    	$scope.returned = false
    	console.log("called")
    	
    }

    $scope.cart_delete = function(){
    	$http.get("/shop/cart/delete").success(function(){
    		
    		window.location = '/accounts/logout'
    	})

    }
  

  $scope.ok = function () {
    $modalInstance.close();
  };
  

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
});


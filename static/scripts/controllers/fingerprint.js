'use strict'

angular.module('kioskui')

.controller('Fingerprintctr', ['$scope', '$http',"FingerprintService","$rootScope", function($scope, $http, FingerprintService,$rootScope){
      
      $rootScope.counter = 60;
      var fp

      $scope.initFingerprint = function(username){
        fp = new FingerprintService(username)
        $scope.enroll();
        
        

      }

      $scope.enroll = function(){
        $scope.enroll_started = true
        $scope.enroll_status = fp.enroll_finger()
        .then(function(response){
            $scope.enroll_started = false
            console.log(response.data)

            if (response.data['status'] != undefined){
                $scope.status = response.data['status']
            }

            if (!response){
                $scope.msg = 'Enrollment Failed' 
                $scope.alert = 'danger'

            }

 
            
            else if(response.data['status']=='already_enrolled'){

                $scope.msg = 'Thumbprint is already Registered!'
                $scope.alert = 'warning'
            }
            else if (response.data['status']=='failed'){
                $scope.msg = 'Failed fingerprint registration!'
                $scope.alert = 'danger'
            }
            else if (response.data['status'] == 'success'){
                $scope.msg = 'Succesfull fingerprint registration!'
                $scope.alert = 'success'
            }
            console.log(response.data['status'])


        })

      }
      


    

}]);
'use strict'

var app = angular.module('kioskui')
app.factory('DebitService', ['$http', '$q', function($http, $q){

    return {
        updateAccount: function(amount){
            return $http.get('/add_cash_by_debit_card/updateaccount/'+amount+'/')
                    .then(function(response) {
                        if (typeof response.data === 'object') {
                            return response.data;
                        } else {
                            // invalid response
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        return $q.reject(response.data);
                    });
        }
    }


}])

app.factory('CashService', ['$http', '$q', function($http, $q){

    return {
        updateAccount: function(username){
            return $http.get('/insert_bills/updateaccount/'+username+'/')
                    .then(function(response) {
                        if (typeof response.data === 'object') {
                            return response.data;
                        } else {
                            // invalid response
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        return $q.reject(response.data);
                    });
        }
    }


}])
app.factory('PayByDebitService', ['$http', '$q', function($http, $q){

    return {
        completeTransaction: function(){
            return $http.get('/shop/pay/precidia_payment/make_payment/')
                    .then(function(response) {
                        if (typeof response.data === 'object') {
                            return response.data;
                        } else {
                            // invalid response
                            return $q.reject(response.data);
                        }

                    }, function(response) {
                        // something went wrong
                        return $q.reject(response.data);
                    });
        }
    }


}])
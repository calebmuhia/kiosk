 'use strict'
angular.module('kioskui')
.factory('User', ['$resource','$rootScope', function ($resource,$rootScope) {


            return $resource('/api/v2/user/:id',
                { id: '@id' },
                {
                    update: { method: 'PUT' },
                    get: {isArray: false, method: 'GET'},
                    query: {method:'GET', params:{}},
                    delete: { method: 'DELETE' },
                    post: { method: 'POST' }
                });
        }]);
  
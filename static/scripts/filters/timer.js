angular.module('kioskui')
  .filter('timer', function() {
    return function(input) {
      var out = ("0" + input).slice(-2);
      return out;
    };
  })
  
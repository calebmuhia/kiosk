angular.module('dashboardui')
  .filter('status', function() {


    return function(input) {
        var status_codes = {
        '10':'Processing',
        '20':'Confirming',
        '30':'Confirmed',
        '40':'Completed',
        '50':'Shipped',
        '60':'Canceled'
    }
      var out = status_codes[input];
      return out;
    };
  })
  
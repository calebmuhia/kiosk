Deployment checklist
====================

* check timeout options on nginx (6 minutes), will prevent nginx 403 timeout error
* check nginx custom error pages, they refresh the browser after 5 secs, 
* ensure django debug mode is off in production
* ensure the right precidia/elavon creds
* check touch screen calibration
* update fingerprint db, if available
* check billacceptor and right usb port
* ensure right taxes on the machine (there should only two taxes (PST and HST), not two of both)
* check on symmetric ds (ensure correct id and properties file)
* check on correct machine dates and correct time zone


Machine Update checklist
========================
* ensure django debug mode is off in production
* do at least two transactions test to check for any code errors after code update




__author__ = 'caleb'
import redis
from db_routes.pin import is_dbPinned
r = redis.StrictRedis(host='localhost', port=6379, db=0)



class DbRouter(object):
    """A router to control all database operations on models in
    the myapp application"""

    def db_for_read(self, model, **hints):
        """Send reads to slaves in round-robin unless this thread is "stuck" to
        the master."""

        if model._meta.db_table == 'django_session':
            return 'default'


        if is_dbPinned() == "All" or is_dbPinned() == "none":

            return None

        else:
            return is_dbPinned()

    def db_for_write(self, model, **hints):


        if model._meta.db_table == 'django_session':
            return 'default'

        if is_dbPinned() == "All" or is_dbPinned() == "none":

            return None

        else:
            return is_dbPinned()

    def allow_relation(self, obj1, obj2, **hints):
        return True

        

    def allow_syncdb(self, db, model):

        return True


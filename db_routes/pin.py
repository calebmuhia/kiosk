__author__ = 'caleb'
import redis
r = redis.StrictRedis(host='localhost', port=6379, db=0)

__all__ = ['is_dbPinned', 'pinDb', 'unpinDb']


def is_dbPinned():
    """
    return seleted db or default if no db is selected  
    """
    db = r.get('db')
    return db


def pinDb(db):
    """set a db to route requests"""
    
    r.set('db', db)


def unpinDb():
    """
    route all db request to the default
    """

    r.set('db', 'default')
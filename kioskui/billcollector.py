from django.conf import settings
import os
import logging
logger = logging.getLogger("billacceptor_debug")
import subprocess


class BillCollector(object):

    def __init__(self):
        self.passwd = getattr(settings,'BILL_COLLECTOR_PASSWD','phoneboth87\n')

        self.billcollectprocess = getattr(settings,'BILL_COLLECTOR_PROCESS','billacceptor')

    def start_bill_collector(self):
        
        cmd = 'sudo service {0} restart'.format(self.billcollectprocess)

        p = subprocess.Popen(cmd.split(), stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = p.communicate(self.passwd) 

        logger.debug("starting the billacceptor, output '{0}', err '{1}'".format(out, err))
        
        

    def stop_bill_collector(self):
        
        cmd = 'sudo service {0} stop'.format(self.billcollectprocess)

        p = subprocess.Popen(cmd.split(), stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        out, err = p.communicate(self.passwd) 

        logger.debug("stoping the billacceptor, output '{0}', err '{1}'".format(out, err))
        








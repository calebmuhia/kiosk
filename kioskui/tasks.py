from __future__ import absolute_import
import redis,time
r = redis.StrictRedis(host='localhost', port=6379, db=0)
from celery.utils.log import get_task_logger
from celery.decorators import periodic_task
from celery.task.schedules import crontab
from datetime import timedelta
from celery import shared_task
from shop.models import Order
import logging
logger = logging.getLogger(__name__)





import urllib2

# A periodic task that will run every minute (the symbol "*" means every)
# @periodic_task(run_every=(timedelta(seconds=2)))
@shared_task
def internet_on():  
    try:
        response=urllib2.urlopen('http://74.125.228.100',timeout=1)
        r.set('internet', True)
    except urllib2.URLError as err:
        r.set('internet', False)
    print r.get('internet')    


@shared_task
def check_completed_order(order_id):
    logger.info('order {0} is being processed'.format(order_id))

    time.sleep(300)

    try:
        order = Order.objects.get(id = order_id)
        if(order.status == Order.PROCESSING):
            logger.info('order Marked as Processing')
            if(order.is_paid()):
                logger.info('Order is paid for, marking ad completed')

                order.status = Order.COMPLETED
                order.save()
            else:
                logger.info('Order is not paid for, marking as Cancelled')
                order.status = Order.CANCELED
                order.save()
    except Exception, e:
        logger.info('Error :', e)
        pass            
                    

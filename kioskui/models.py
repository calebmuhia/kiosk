from django.db import models
from django.db.models import Q
from shop.util.fields import CurrencyField
from django.contrib.auth.models import AbstractUser, AbstractBaseUser
from shop.models import Product
from decimal import Decimal
from shop.models import Order, OrderItem
from kiosk.settings import AUTH_USER_MODEL as USER_MODEL
from django.dispatch import receiver
from django.db.models.signals import post_save
from datetime import datetime
from django.utils.text import slugify
import itertools

from kiosk.settings import AUTH_USER_MODEL
from shop.order_signals import completed
from django.utils.translation import ugettext_lazy as _
from shop_simplecategories.models import Category
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned
from shop.models import Product

# Create your models here.

class Supplier(models.Model):
    """
    Model to hold Supplier information
    """
    name = models.CharField(max_length=255, unique=True, default="default")
    def __unicode__(self):
        return self.name


class KioskUser(AbstractUser):

    """
     user class holding user information like balance and the like
    """

    ## company/location details
    location = models.CharField(max_length=255, blank=True, null=True)
    company = models.CharField(max_length=255, blank=True, null=True)

    #############################################
    ############# DEPOSIT TOTALS ################
    
    ## a running total of the CASH amount the user has deposited into his/her account
    cash_deposits = CurrencyField(verbose_name=('Cash Deposits'))

    ## a running total of the CASH amount the user has deposited into his/her account
    admin_deposits = CurrencyField(verbose_name=('Admin Deposits'))

    ## a running total of the CREDIT-CARD amount the user has deposited into his/her account
    credit_card_deposits = CurrencyField(verbose_name=('Credit-Card Deposits'))

    ## a running total of the DEBIT-CARD amount the user has deposited into his/her account
    debit_card_deposits = CurrencyField(verbose_name=('Debit-Card Deposits'))
    ############# DEPOSIT TOTALS ################
    #############################################

    #############################################
    ############# EXPENSE TOTALS ################
    

    ## a running total of the user's ACCOUNT expenditure
    account_expenses = CurrencyField(verbose_name=('Account Expenses'))

    ## a running total of the user's ACCOUNT expenditure
    admin_deducts= CurrencyField(verbose_name=('Admin Deducts'))

    ## a running total of the user's CREDIT-CARD expenditure
    credit_card_expenses = CurrencyField(verbose_name=('Credit-Card Expenses'))

    ## a running total of the user's DEBIT-CARD expenditure
    debit_card_expenses = CurrencyField(verbose_name=('Debit-Card Expenses'))
    ############# EXPENSE TOTALS ################
    #############################################
    account_pin = models.IntegerField(default=0, verbose_name=('Account Pin'))
    account_id = models.CharField(max_length=100, verbose_name=('Account Id'), default='')
    is_cashbox_manager = models.BooleanField(default=False, verbose_name='CashBox Manager Status')
    # pay by payroll
    # pay_by_payroll = models.BooleanField(default=False, verbose_name='Pay by payroll?')


    

    def __unicode__(self):
        return '"%s %s" (%s)' %(
                self.first_name,
                self.last_name,
                self.username,
          
        )


    def get_name(self):
        if self.first_name:
            return self.first_name
        else:
            return self.username
                

    ##__unicode__()


    def _balance(self):
#        return self.total_deposits - self.total_expenses    ## WRONG (!!) Incorrectly deducts credit- and debit- expenses from total balance (when it shouldn't)
        return self.total_deposits - self.account_expenses - self.admin_deducts
    ##_balance()
    balance = property(_balance)


    def _total_deposits(self):
        return self.cash_deposits\
             + self.credit_card_deposits\
             + self.debit_card_deposits\
             +self.admin_deposits
    ##_total_deposits()
    total_deposits = property(_total_deposits)


    def _account_number(self):
        return self.user.username[-5:]
    account_number = property(_account_number)


    def _total_expenses(self):
        return self.account_expenses\
            + self.credit_card_expenses\
            + self.debit_card_expenses
            
    ##_total_expenses()
    total_expenses = property(_total_expenses)


    def increment_cash_deposits(self, bill_value):
        self.cash_deposits += Decimal(bill_value)
        UserCashFlow.objects.create(
            user=self,
            status = UserCashFlow.CASHDEPOSIT,
            amount = bill_value,
            pre = self.balance,
            post = self.balance + bill_value,
            description = 'Update balance from billacceptor'
            )
        cashbox = CashBox()
        cashbox.add_to_current_cashbox(bill_value, self)

    def increment_admin_deposits(self, bill_value):
        
        UserCashFlow.objects.create(
            user=self,
            status = UserCashFlow.ADMINDEPOSIT,
            amount = bill_value,
            pre = self.balance,
            post = self.balance + Decimal(bill_value),
            description = 'Increase balance from admin'
            )
        self.admin_deposits += Decimal(bill_value)    
           
    ##increment_admin_deposits()

    def increment_credit_card_deposits(self, bill_value, extra_field=None):
        UserCashFlow.objects.create(
            user=self,
            status = UserCashFlow.CREDITDEPOSIT,
            amount = bill_value,
            pre = self.balance,
            post = self.balance + bill_value,
            description = 'Update balance from Credit Card',
            extra_field = extra_field
            )
        self.credit_card_deposits += Decimal(bill_value)



    ##increment_credit_card_deposits()

    def increment_admin_deducts(self, bill_value):
        UserCashFlow.objects.create(
            user=self,
            status = UserCashFlow.ADMINDEDUCT,
            amount = bill_value,
            pre = self.balance,
            post = self.balance - Decimal(bill_value),
            description = 'Decrease balance from admin',
            
            )
        self.admin_deducts += Decimal(bill_value)



    ##increment_credit_card_deposits()


    def increment_account_expense(self, order_amount, order_id=None):
        UserCashFlow.objects.create(
            user=self,
            status = UserCashFlow.ACCOUNTEXPENSES,
            amount = order_amount,
            pre = self.balance,
            post = self.balance - order_amount,
            description = 'Bought products via Account',
            order_id = order_id
            )
        self.account_expenses += Decimal(order_amount)
        
    ##increment_account_expense()

    def increment_credit_card_expense(self, order_amount, extra_field=None, order_id=None):
        self.credit_card_expenses += Decimal(order_amount)

        UserCashFlow.objects.create(
            user=self,
            status = UserCashFlow.CREDITEXPENSES,
            amount = order_amount,
            pre = self.balance,
            post = self.balance,
            description = 'Bought products via Credit Card',
            extra_field = extra_field,
            order_id = order_id
            )
        self.account_expenses += Decimal(order_amount)



    ##increment_credit_card_expense    
## KioskUser

class PayRollPaymentReport(models.Model):
    created = models.DateTimeField(auto_now_add=True)

class PayRollItem(models.Model):
    payment = models.ForeignKey(PayRollPaymentReport, related_name='payrollpayment')
    amount = CurrencyField()
    user_keytag = models.CharField(default='',max_length=100 )
    user = models.ForeignKey(USER_MODEL, blank=True, null=True)

# class OrdersOnPayRoll(models.Model):
#     """
#      this model will hold data for paid and and unpaid orders by payroll
#     """
#     INPROGRESS = 1
#     INVOICED = 2
#     PAID = 3
#     SUPPENDED = 4

#     STATUS = (
#         (INPROGRESS,'inprogress'),
#         (INVOICED,'invoiced'),
#         (PAID,'paid'),
#         (SUPPENDED,'SUPPENDED')
#         )

#     user = models.ForeignKey(USER_MODEL, related_name='payrolluser',null=True,blank=True)
#     user_keytag = models.CharField(verbose_name='User Keytag', max_length=256)
#     created = models.DateTimeField(auto_now_add=True)
#     modified = models.DateTimeField(auto_now=True)
#     amount = CurrencyField(default=0.00)
#     is_paid = models.BooleanField(default=False)
#     extra_info = models.TextField(null=True, blank=True, default = '', max_length=15000)
#     date_paid = models.DateTimeField(null=True, blank=True)
#     status = models.IntegerField(default=INPROGRESS, choices=STATUS, verbose_name='Status')

#     def __unicode__(self):
#         return "{0}".format(self.user_keytag)

#     def new_payment(self,user):
#         payment = OrdersOnPayRoll.objects.create(
#             user = user,
#             user_keytag = user.username,
#             amount = Decimal(0.00),
#             extra_info = 'initial amount 0.00 at {0} \n'.format(datetime.today()),
#             )
#         return payment
#     def get_current_payment(self, user):
#         if OrdersOnPayRoll.objects.filter(user = user).exists():
#             try:
#                 return OrdersOnPayRoll.objects.get(Q(user=user) & Q(is_paid=False))
#             except MultipleObjectsReturned:
#                 for payment in OrdersOnPayRoll.objects.filter(user=user):
#                     payment.is_paid = True
#                     payment.save()
#                 payment = OrdersOnPayRoll.objects.filter(user=user).order_by('-created')[0]
#                 payment.is_paid = False
#                 payment.save()
#                 return payment
#         else:
#             payment = self.new_payment(user)
#             return payment

#     def update_current_payment(self, user, amount, order_id):
#         payment = self.get_current_payment(user)
#         payment.amount += Decimal(amount)
#         payment.extra_info+= "amount {0} for order {1} added\n".format(amount, order_id)
#         payment.save()

#     def mark_as_payed(self, user):
#         payment = self.get_current_payment(user)
#         payment.is_paid = True
#         payment.save()
#         self.new_payment(user)




class UserCashFlow(models.Model):
    """
    this table will hold all users cashflow 
    information for every balance activity
    """

    INITIAL = 10
    CASHDEPOSIT = 20
    CREDITDEPOSIT = 30
    ADMINDEPOSIT = 40
    ACCOUNTEXPENSES = 50
    CREDITEXPENCES = 60
    ADMINDEDUCT = 70

    STATUS_CODES = (
        (INITIAL, "initial"),
        (CASHDEPOSIT, 'cash_deposit'),
        (CREDITDEPOSIT, 'credit_deposit'),
        (ADMINDEPOSIT, 'admin_deposit'),
        (ACCOUNTEXPENSES, 'account_expenses'),
        (CREDITEXPENCES, 'credit_expenses'),
        (ADMINDEDUCT, 'admin_deducts')
        )

    user = models.ForeignKey(USER_MODEL, blank=True, null=True,
            verbose_name='User')
    status = models.IntegerField(choices=STATUS_CODES, default=INITIAL,
            verbose_name='Status')
    pre = CurrencyField(verbose_name='Pre Balance', blank=True, null=True)
    post = CurrencyField(verbose_name='Post Balance', blank=True, null=True)
    order_id = models.CharField(verbose_name='Order id', blank=True, null=True, max_length=10, default='')


    date = models.DateTimeField(verbose_name="Date", auto_now_add=True)
    modified  = models.DateTimeField(verbose_name='modified', auto_now=True)
    amount = CurrencyField(verbose_name='Amount')
    extra_field = models.CharField(verbose_name="Extra Field", max_length=256, blank=True, null=True, default='')
    description = models.TextField(verbose_name="Description", max_length=256, blank=True, null=True, default='')
    def __unicode__(self):
        return "User < {0} > - | - Pre Balance < {1} > - | - Post Balance < {2} > - | - On {3}".format(
            self.user.username if self.user else None, self.pre, self.post, self.date.strftime('%c'))    


@receiver(post_save, sender=KioskUser)
def user_saved(sender=None, instance=None,**kwargs):
    user = instance
    print("Request finished!")
    if kwargs["created"]:
        cashflow = UserCashFlow.objects.create(
            user=user, 
            status = UserCashFlow.INITIAL,
            pre = 0.00,
            post = 0.00,
            amount = 0.00,
            description = 'initial'
            )
    # if user.pay_by_payroll and not OrdersOnPayRoll.objects.filter().exists():
    #     payment = OrdersOnPayRoll()
    #     payment.new_payment(user)




class BarcodeProduct(Product):
    """
    Model class for items WITH barcodes
    """
    supplier = models.ForeignKey(Supplier, verbose_name='Supplied by', null=True, blank=True)
    barcode = models.CharField(max_length=255, unique=True,)
    quantity = models.IntegerField(default=0)
    unit_cost = models.FloatField(verbose_name='Unit Cost', default=0)
    case_upc = models.CharField(verbose_name='Case UPC', blank=True, null=True, max_length=255)
    case_qty = models.IntegerField(verbose_name='Case Quantity', default=0)
    min_stock = models.IntegerField(verbose_name='Minimum Stock', default=0 , blank=True, null=True)
    max_stock = models.IntegerField(verbose_name='Maximum Stock', default=0 , blank=True, null=True)
    demand =  models.IntegerField(default=0 , blank=True, null=True)
    demand_two = models.IntegerField(verbose_name='Demand Two', default=0, blank=True, null=True)
    lead_time = models.FloatField(default=0.0, blank=True, null=True)
    bottle_deposit = models.BooleanField(default=False)
    pst = models.BooleanField(default=False, verbose_name="Provincial Sales Tax")
    hst = models.BooleanField(default=False, verbose_name="Harmonized Sales Tax")

    def save(self, *args, **kwargs):
        if not self.pk:


            max_length = BarcodeProduct._meta.get_field('slug').max_length
            self.slug = orig =  slugify(unicode(self.name))[:max_length]

            for x in itertools.count(1):
                if not BarcodeProduct.objects.filter(slug=self.slug).exists():
                    break

                self.slug = unicode("%s-%d" % (orig[:max_length - len(str(x)) - 1], x))        


        super(BarcodeProduct, self).save(*args, **kwargs)

    def quantity_decrement(self, amount, status):
        QuantityMovement.objects.create(
            product_name = self.name,
            product_barcode = self.barcode,
            amount = amount,
            pre_quantity = self.quantity,
            post_quantity = self.quantity - amount,
            status = status,
            slug = self.slug
            )
        self.quantity -= amount
        self.save()

    def quantity_increment(self, amount, status):
        QuantityMovement.objects.create(
            product_name = self.name,
            product_barcode = self.barcode,
            amount = amount,
            pre_quantity = self.quantity,
            post_quantity = self.quantity + amount,
            status = status,
            slug = self.slug
            )
        self.quantity += amount
        self.save()    



    STATUS = (
        ('ACTIVE', 'active'),
        ('INACTIVE', 'inactive'),
        ('DISCONTINUED', 'discontinue'),
        ('BACKODRED', 'backordred'),

    )
    class Meta:
        ordering = ["name"]
        verbose_name = "Products WITH barcodes"
        verbose_name_plural = "Products WITH barcodes"
    ##class Meta


    def __unicode__(self):
        return "<%s  |  %s>" % (self.name, self.barcode)
    ##__unicode__()

    def delete_product(self):
        self.delete()

class NoBarcodeProduct(Product):
    """
    Model class for items WITHOUT barcodes (fruits, coffee, etc)
    """

    image = models.ImageField(upload_to="products_with_no_barcodes/", default='products_with_no_barcodes/default.jpg')
    quantity = models.IntegerField(default=0)
    ordering = models.IntegerField(default=0)

    unit_cost = models.FloatField(verbose_name='Unit Cost', default=0)
    pst = models.BooleanField(default=False, verbose_name="Provincial Sales Tax")
    hst = models.BooleanField(default=False, verbose_name="Harmonized Sales Tax")

    def save(self, *args, **kwargs):
        if not self.pk:
            max_length = NoBarcodeProduct._meta.get_field('slug').max_length
            self.slug = orig =  slugify(unicode(self.name))[:max_length]

            for x in itertools.count(1):
                if not Product.objects.filter(slug=self.slug).exists():
                    break

                self.slug = unicode("%s-%d" % (orig[:max_length - len(str(x)) - 1], x))        


        super(NoBarcodeProduct, self).save(*args, **kwargs)

    def quantity_decrement(self, amount, status):
        print "found product ", amount
        QuantityMovement.objects.create(
            product_name = self.name,
            product_barcode = None,
            amount = amount,
            pre_quantity = self.quantity,
            post_quantity = self.quantity - amount,
            status = status,
            slug = self.slug
            )
        self.quantity =self.quantity-amount
        self.save()  

    def quantity_increment(self, amount, status):
        print "found product ", amount
        QuantityMovement.objects.create(
            product_name = self.name,
            product_barcode = None,
            amount = amount,
            pre_quantity = self.quantity,
            post_quantity = self.quantity + amount,
            status = status,
            slug = self.slug
            )
        self.quantity = self.quantity+amount
        self.save()      




    class Meta:
        ordering = ["ordering"]
        verbose_name = "Products WITHOUT barcodes"
        verbose_name_plural = "Products WITHOUT barcodes"
##class NoBarcodeProduct()


def postOrderCompleteProcess(sender, **kwargs):
    """
    decrment the product quantity when a product is bought and record into product quantity
    """
    order = kwargs['order']
    print order

    for item in OrderItem.objects.filter(order = order.id):
        try:
            product = Product.objects.get(id=item.product_reference)
            product.quantity_decrement(item.quantity, QuantityMovement.product_bought)

        except Exception, e:
            print e
            continue

# a signal that is displatched when an order is completed
completed.connect(postOrderCompleteProcess, dispatch_uid="order_completed")              



class Tax(models.Model):

    """
    our cart modifies ie tax
    """
    name = models.CharField(max_length=255, verbose_name='Tax Name')
    value = models.CharField(max_length=255, verbose_name='Tax Value')


    def __unicode__(self):
        return "{0}{1} {2} tax".format(self.value, '%', self.name)

    class Meta:
        verbose_name='Tax'
        verbose_name_plural = 'Taxes'


class MissingBarcodeOrKeycard(models.Model):
    """
    captures scanned user keytags or product barcode
    """

    date = models.DateTimeField(default=datetime.now)
    barcode = models.CharField(default='', max_length=100 )

    def __unicode__(self):
        return '%s scanned at %s' % (self.barcode, self.date.strftime("%c"))

    def send_email_to_admin(self):
        pass    

class CashBox(models.Model):
    """
    keeps track of cashbox changes and history
    """
    start_date = models.DateTimeField(null=True, blank=True,)
    end_date = models.DateTimeField(null=True, blank=True)
    amount = CurrencyField(verbose_name=('Amount'), default=0.00)
    emptied = models.BooleanField(default=False, )
    created = models.DateTimeField(default=datetime.today(),  null=True, blank=True)
    emptied_at = models.DateTimeField(null=True, blank=True)
    emptied_by = models.ForeignKey(AUTH_USER_MODEL,  null=True, blank=True)
    extra_info = models.TextField(null=True, blank=True, default = '', max_length=15000)

    def __unicode__(self):

        if self.emptied:
            return "started at %s to %s and is emptied %s" % (self.start_date.strftime('%c'),self.end_date.strftime('%c'),self.emptied)
        else:
            return "Cash Box Not emptied"

    def current_cashbox(self):
        try:
            current_cashbox = CashBox.objects.get(emptied = False)
        except:
            current_cashbox = self.new_cashbox()
        return current_cashbox    

    def new_cashbox(self):

        current_cashbox = CashBox.objects.create(
            amount = Decimal('0.00'),
            start_date = datetime.today(),
            created = datetime.today(),
            extra_info = 'initial amount 0.00 at {0} user None \n'.format(datetime.today())  
            )
        return current_cashbox


    def add_to_current_cashbox(self, amount, user):
        current_cashbox = self.current_cashbox()
        current_cashbox.amount+=amount
        current_cashbox.extra_info += "amount {0} at {1} by {2} \n".format(amount, datetime.today(),user.username)
        current_cashbox.save()

    def empty_cashbox(self, user):
        current_cashbox = self.current_cashbox()
        current_cashbox.emptied = True
        current_cashbox.emptied_by = user
        current_cashbox.emptied_at = datetime.today()
        current_cashbox.end_date = datetime.today()
        current_cashbox.save()

        self.new_cashbox()

class QuantityMovement(models.Model):
    """
    use to keep track of products quantity changes
    """

    Warehouse_to_kiosk_reports=10
    manual_adjustment_under=20
    manual_adjustment_over=30
    manual_adjustment_returns=40
    manual_adjustment_stale=50
    manual_adjustment_error_add=60
    manual_adjustment_error_sub=70
    product_bought=80
    stock_take = 90

    STATUS_CODES = (
        (Warehouse_to_kiosk_reports,_("warehouse to kiosk")),
        (manual_adjustment_under,_("manual adjustment under")),
        (manual_adjustment_over,_("manual adjustment over")),
        (manual_adjustment_returns,_("manual adjustment returns")),
        (manual_adjustment_stale,_("manual adjustment stale")),
        (manual_adjustment_error_add,_("manual adjustment error add")),
        (manual_adjustment_error_sub,_("manual adjustment error sub")),
        (product_bought,_("product bought")),
        (stock_take,_("Stock Take")),

    )

    product_name = models.CharField(default = "", max_length = 100)
    slug = models.SlugField(default = '', max_length=100)
    product_barcode = models.CharField(default = "", max_length = 100, blank=True, null=True)
    pre_quantity = models.CharField(default = "", max_length = 100)
    post_quantity = models.CharField(default = "", max_length = 100)
    amount = models.CharField(default = "", max_length = 100, )
    created = models.DateTimeField(auto_now_add = True)
    status = models.IntegerField(default = "", max_length = 100, choices = STATUS_CODES)
    
    def __unicode__(self):
        return "product %s quantity changed at %s" % (self.product_name, self.created.strftime("%c"))

    class Meta:
        verbose_name = 'Product Quantity Movement'
        verbose_name_plural = 'Product Quantity Movement'
        



class LocationInfo(models.Model):
    Monday = 1
    Tuesday = 2
    Wednesday = 3
    Thursday = 4
    Friday = 5
    Saturday = 6
    Sunday = 7

    WEEKDAYS = (
      (Monday, _("Monday")),
      (Tuesday, _("Tuesday")),
      (Wednesday, _("Wednesday")),
      (Thursday, _("Thursday")),
      (Friday, _("Friday")),
      (Saturday, _("Saturday")),
      (Sunday, _("Sunday")),
    )
    location_name = models.CharField(default = "", max_length=255)
    address = models.CharField(default = "", max_length=255, blank=True, null=True)
    contact_name = models.CharField(default = "", max_length=255, blank=True, null=True)
    contact_number = models.CharField(default = "", max_length=255, blank=True, null=True)
    weekday_from = models.IntegerField(choices=WEEKDAYS, default=Monday, blank=True, null=True)
    weekday_to = models.IntegerField(choices=WEEKDAYS, default=Sunday, blank=True, null=True)
    from_hour = models.IntegerField(default=8, blank=True, null=True)
    to_hour = models.IntegerField(default=11, blank=True, null=True)
    notes = models.TextField(default = "", max_length=255, blank=True, null=True)

    def __unicode__(self):
        return "Location %s" % self.location_name
       

    class Meta:
        verbose_name = "Location Information"
        verbose_name_plural = "Location Information"


class RestockFrequency(models.Model):
    restock_frequency = models.IntegerField(default=0)

    def __unicode__(self):
        return "Restock frequency"

    class Meta:
        verbose_name = 'Restock frequency'
        verbose_name_plural = "Restock frequencies"        


class ExtraCategoryFields(models.Model):
    category = models.ForeignKey(Category, related_name= "category_field", unique=True) 
    days_of_product = models.IntegerField(default = 7)
    hard_max = models.IntegerField(default=0, null=True, blank=True)
    round_to_case = models.BooleanField(default=False)

    def __unicode__(self):
        return "category %s" % self.category.name

    class Meta:
        verbose_name = 'Category Fields'
        verbose_name_plural = "category fields"

#post save signal for extra category option
@receiver(post_save, sender=Category)
def category_saved(sender=None, instance=None,**kwargs):
    category = instance

    if kwargs["created"]:
        try:
            options = ExtraCategoryFields.objects.create(
                category = category
                )
        except:
            pass  


class ChoicesPlaceHolder(models.Model):
    """
    holds choices, settings for a inventory report 
    """
    

    name = models.CharField(max_length=256, default="")
    slug = models.SlugField(max_length=254, default="", unique=True)
    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)
    schedule = models.BooleanField(default=False)
    scheduled_date = models.DateTimeField(blank=True, null=True)
    email = models.EmailField(blank=True, null=True)

    def save(self, *args, **kwargs):
        if not self.pk:


            max_length = ChoicesPlaceHolder._meta.get_field('slug').max_length
            self.slug = orig =  slugify(unicode(self.name))[:max_length]

            for x in itertools.count(1):
                if not ChoicesPlaceHolder.objects.filter(slug=self.slug).exists():
                    break

                self.slug = unicode("%s-%d" % (orig[:max_length - len(str(x)) - 1], x))        


        super(ChoicesPlaceHolder, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.name


    class Meta:
        verbose_name = "Choices Place Holder"
        verbose_name_plural = "Choices Place Holders"




class RestockCategoryChoice(models.Model):
    place_holder = models.ForeignKey(ChoicesPlaceHolder, related_name='place_holder')
    category = models.ForeignKey(Category)



    def __unicode__(self):
        return "category %s for the place_holder %s" % (self.category.name, self.place_holder.name)


    class Meta:
        verbose_name = "Restock Category Choice"
        verbose_name_plural = "Restock Category Choices"   


class RestockInventoryReport(models.Model):
    """
     report that is used to request for inventory restock from sources e.g. warehouse
    """
    name = models.CharField(max_length=256,default="", null=False)
    created = models.DateTimeField(auto_now_add=True)
    prekitting_completed = models.BooleanField(default=False)
    isUpdated = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name



class RestockInventoryReportitems(models.Model):
    product_name = models.CharField(max_length=256)
    upc   = models.CharField(max_length=255)
    proposed_restock_quantity = models.IntegerField()
    categories = models.CharField(max_length=100, default='')
    report = models.ForeignKey(RestockInventoryReport)

    def __unicode__(self):
        return self.product_name        


class PrintingObject(models.Model):
    card = models.CharField(max_length=100, default="")
    email = models.EmailField(max_length=200, default="")


class Feedback(models.Model):
    name = models.CharField(blank=True, null=True, max_length=256)
    date = models.DateTimeField(auto_now_add=True)
    text = models.TextField()
    archived = models.BooleanField(default=False)






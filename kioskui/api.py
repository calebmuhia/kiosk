from tastypie.authentication import Authentication, BasicAuthentication, ApiKeyAuthentication, MultiAuthentication, SessionAuthentication
from tastypie.authorization import Authorization, DjangoAuthorization
from tastypie.exceptions import BadRequest
from tastypie import fields


from django.conf.urls import *
from django.core.paginator import Paginator, InvalidPage
from django.http import Http404
#from haystack.query import SearchQuerySet
from tastypie.resources import ModelResource, Resource
from tastypie.utils import trailing_slash

from tastypie.constants import ALL, ALL_WITH_RELATIONS
from kioskui.authentication import MyBasicAuthentication
from shop.models import Order

from kioskui.models import Feedback

# from core.authorization import Authorization



class OrdersResource(ModelResource):

    class Meta:

        queryset = Order.objects.all()
        list_allowed_methods = ['get',]
        detail_allowed_methods = ['get',]

        resource_name = 'orders'
        authentication = MyBasicAuthentication()
        authorization = Authorization()

    def get_object_list(self, request):
        
        return super(OrdersResource, self).get_object_list(request).filter(user=request.user)


class FeedbackResource(ModelResource):

    class Meta:

        queryset = Feedback.objects.all()
        list_allowed_methods = ['post', 'get']
        detail_allowed_methods = ['post', 'get']
        allowed_methods = ['post', 'get']


        resource_name = 'feedback'
        authentication = Authentication()
        authorization = Authorization()





    

    

# -*- coding: utf-8 -*-
import logging
from shop.models import Order
from shop.util.cart import get_or_create_cart
from shop.util.order import add_order_to_request
# from kioskui.utils import check_generic_products

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.conf.urls import patterns, url
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from kioskui.tasks import check_completed_order

from uuid import uuid4
from django.conf import settings 
# from kioskui.models import OrdersOnPayRoll


class PersonalAccountBackend(object):
    """
    Derived from https://github.com/divio/django-shop/blob/master/shop/payment/backends/pay_on_delivery.py
    """
    backend_name = "MY ACCOUNT"
    url_namespace = "personal-account"
    name = url_namespace
    
    def __init__(self, shop):
        self.shop = shop
        # This is the shop reference, it allows this backend to interact with
        # it in a tidy way (look ma', no imports!)

    @method_decorator(login_required)
    def personal_account_view(self, request):
        """
        Process the transaction
        """

        logger.info({"msg":"Starting '{0}' payment processing...".format(self.backend_name), "user":getattr(request.user, 'username', None)})

        ## Get the order object

        logger.info({"msg":"Get or Create cart", "user":getattr(request.user, 'username', None)})
        
        cart = get_or_create_cart(request)
        cart.update(request)

        
        order = Order.objects.create_from_cart(cart, request)
        logger.info({"msg":"Order '{0}' Created from Cart".format(order.id), "user":getattr(request.user, 'username', None)})

        request = request
        add_order_to_request(request, order)

        the_order = self.shop.get_order(request)

        order_amount = self.shop.get_order_total(the_order)

        logger.info({"msg":"Order amount '{0}' will be charged from account".format(order_amount), "user":getattr(request.user, 'username', None)})

        try:
            check_completed_order.delay(the_order.id)
        except:
            pass    


        #check if we have any generic products in the order

        # check_generic_products(request, order)

        ## Get the user object
        user = request.user
        

        ## generate a transaction number
        transaction_id = uuid4().hex

        ## check the user's account balance
        user_balance = user.balance

        ## if order amount >  user account balance: prompt to add money (via cash, credit etc)
        if order_amount > user_balance:
            
            logger.info({"msg":"Insufficient funds. Order amount ('{0}') > account balance ('{1}')".format(order_amount, user_balance), "user":getattr(request.user, 'username', None)})


            payonpayroll = getattr(settings, 'PAY_ON_PAYROLL', False)

            if not payonpayroll:
                logger.info({"msg":"payonpayroll is not active canceling order due to insufficient_funds", "user":getattr(request.user, 'username', None)})
                

                the_order.status = Order.CANCELLED
                the_order.save()

                insufficient_funds_url = reverse('insufficient_funds')
                return HttpResponseRedirect(insufficient_funds_url)

             
#       
           
        ## ... else increment appropriate expense totals
        
        logger.info({"msg":"Incrementing user expense totals...", "user":getattr(request.user, 'username', None)})

        user.increment_account_expense(order_amount, the_order.id)
        user.save()   
        post_user_balance = user.balance                                                                                                                                                                                                                                                                                                                                                                          

        ## mark the amount as payed and save this transaction
        
        logger.info({"msg":"Marking this transaction as paid and save it.", "user":getattr(request.user, 'username', None)})


        try:
            self.shop.confirm_payment(
                the_order, self.shop.get_order_total(the_order), transaction_id,
                self.backend_name, save=True)
        except Exception, e:
            logger.critical({"msg":"Error while saving order '{0}'".format(e), "user":getattr(request.user, 'username', None)})
            

        printing_args = {
                'order_id':the_order.id,
                'account':True
                }
        request.session["printing_args"] = printing_args  

        logger.info({'msg':"pre user balance '{0}', post user balance '{1}', difference '{2}'".format(user_balance, post_user_balance,user_balance-post_user_balance ), "user":getattr(request.user, 'username', None)})

        if (user_balance-post_user_balance)!=order_amount:
            logger.critical({'msg':'money deducted from user account does not match the order amount','Order amount':order_amount, "amount charged":(user_balance-post_user_balance), "user":getattr(request.user, 'username', None)} )
            

        ## redirect to the next step in this checkout process
        return HttpResponseRedirect(self.shop.get_finished_url())
    ##personal_account_view()

    def get_urls(self):
        urlpatterns = patterns('',
            url(r'^$', self.personal_account_view, name=self.name),
        )
        return urlpatterns
    ##get_urls()

##class PersonalAccountBackend()

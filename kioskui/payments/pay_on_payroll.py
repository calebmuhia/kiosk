# -*- coding: utf-8 -*-
import logging
from shop.models import Order
from shop.util.cart import get_or_create_cart
from shop.util.order import add_order_to_request
# from kioskui.utils import check_generic_products

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.conf.urls import patterns, url
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from kioskui.tasks import check_completed_order
from kioskui.models import OrdersOnPayRoll

from uuid import uuid4


class PayRollBackend(object):
    """
    Derived from https://github.com/divio/django-shop/blob/master/shop/payment/backends/pay_on_delivery.py
    """
    backend_name = "PayRoll"
    url_namespace = "payroll"
    name = url_namespace
    
    def __init__(self, shop):
        self.shop = shop
        # This is the shop reference, it allows this backend to interact with
        # it in a tidy way (look ma', no imports!)

    @method_decorator(login_required)
    def pay_roll_view(self, request):
        """
        Process the transaction
        """
        logger.info("Starting '%s' payment processing..." % self.backend_name)

        ## Get the order object

        cart = get_or_create_cart(request)
        cart.update(request)
        order = Order.objects.create_from_cart(cart, request)
        request = request
        add_order_to_request(request, order)

        the_order = self.shop.get_order(request)

        order_amount = self.shop.get_order_total(the_order)
        try:
            check_completed_order.delay(the_order.id)
        except:
            pass    


        
        user = request.user
        

        ## generate a transaction number
        transaction_id = uuid4().hex

        ## check the user's account balance
        user_balance = user.balance

        ## Check if user is eligible to this payment
        if not user.pay_by_payroll:
            logger.info("Cancelling order due to User forbiden to use payment method. Order amount ('%s')" % (order_amount))
            insufficient_funds_url = reverse('insufficient_funds')
            return HttpResponseRedirect(insufficient_funds_url)
#            return HttpResponseRedirect(self.shop.get_cancel_url())
        ##if

        ## ... increment payroll deducts
        logger.debug("increment amount to be deducted from payroll")                                                                                                                 
        payment =  OrdersOnPayRoll()
        payment.update_current_payment(user,order_amount,the_order.id)                                                                                                                                                                                                                                                                                                                                                                          

        ## mark the amount as payed and save this transaction
        logger.debug("Marking this transaction as paid and save it.")
        try:


            self.shop.confirm_payment(
                the_order, self.shop.get_order_total(the_order), transaction_id,
                self.backend_name, save=True)
        except Exception, e:
            logger.debug(e) 

        printing_args = {
                'order_id':the_order.id,
                'account':True
                }
        request.session["printing_args"] = printing_args  
            

        ## redirect to the next step in this checkout process
        return HttpResponseRedirect(self.shop.get_finished_url())
    ##personal_account_view()

    def get_urls(self):
        urlpatterns = patterns('',
            url(r'^$', self.pay_roll_view, name=self.name),
        )
        return urlpatterns
    ##get_urls()

##class PersonalAccountBackend()

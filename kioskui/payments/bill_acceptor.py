# -*- coding: utf-8 -*-
'''
Created on 2012-02-20

@author: saidimu
'''

from kioskui.billcollector import BillCollector
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
import time

import json


class BillAcceptor(object):
    """
- This class takes care of communication with the bill-acceptor REST server
- No django here at all, just a plain vanilla python module
- this module will be called by a django view
"""

    ###############################################################################################
    ############# https://github.com/kiosksolutions/bill_acceptor/blob/master/README ##############
    ###############################################################################################

    OK_KEY = "Ok"

    ## GET requests
    DEVICE_STATUS = "/machineStatus"
    DEVICE_STATUS_KEY = "DeviceStatus"

    STACK_BILL = "/escrowStack"
    STACK_BILL_KEY = ""

    RETURN_BILL = "/escrowReturn"
    RETURN_BILL_KEY = ""

    ## POST requests
    GET_BILL_VALUE = "/getBill"
    AUTO_STACK = "/setAutoStack"



    ###############################################################################################
    ############# https://github.com/kiosksolutions/bill_acceptor/blob/master/README ##############
    ###############################################################################################

    def __init__(self):
        """
        """


        self.bill_acceptor_url = "http://localhost:1984/"

        self.billcollector = BillCollector()
        self.billcollector.start_bill_collector()

        logger.info({"msg":"starting the BillAcceptor"})


    ##__init__()

    def sendCommand(self, command=None, data=None):
        """
        Send a command to the bill acceptor REST server (default command is DEVICE_STATUS)
        """
        from subprocess import Popen, PIPE, STDOUT
        logger.info({"msg":"Sleep for 5 secs to let the billacceptor start and get ready"})
        
        time.sleep(5)
        


        if not command:
            command = self.DEVICE_STATUS
            logger.debug("Command: '%s'" % command)
        ##if

        ## send GET commands (so far only GET_BILL_VALUE and AUTO_STACK are POST commands)

        try:

            if (command != self.GET_BILL_VALUE) and (command != self.AUTO_STACK):
                final_url = self.bill_acceptor_url + command
                logger.debug("REST endpoint: '%s'" % final_url)

                cmd = "curl %s" % final_url
                logger.debug("Command-line: '%s'" % cmd)

                for i in xrange(15):
                    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True)
                    self.bill_acceptor_response = p.stdout.read()
                    if "data" in self.bill_acceptor_response or "timeout" in self.bill_acceptor_response:
                        break

                    error =p.stderr.read()

                    if error:
                        logger.info("error happened '{0}' try '{1}'".format(error, i))
                    time.sleep(1) 

                logger.info({"msg":"Stop the BillAcceptor"})
                self.billcollector.stop_bill_collector()
                logger.info({"msg":"Return the response","response":self.bill_acceptor_response})

                return self.bill_acceptor_response


            ## send POST commands (so far only GET_BILL_VALUE and AUTO_STACK are POST commands)
            elif command == self.GET_BILL_VALUE:
                if not data:
                    return None
                ##if

                payload = {
                    "data": data,
                }

                logger.debug("/getBill payload: %s" % payload)

                final_url = self.bill_acceptor_url + command
                logger.debug("REST endpoint: '%s'" % final_url)

                request_data = json.dumps(payload)

                cmd = "curl %s -X POST -H 'Content-Type: application/json' -d '%s'" % (final_url, request_data)
                logger.debug("Command-line: '%s'" % cmd)

                for i in xrange(15):
                    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True)
                    self.bill_acceptor_response = p.stdout.read()
                    logger.info(type(self.bill_acceptor_response))
                    if "data" in self.bill_acceptor_response or "timeout" in self.bill_acceptor_response:
                        break

                    error =p.stderr.read()

                    if error:
                        logger.info("error happened '{0}' try '{1}'".format(error, i))
                    time.sleep(1)    

                logger.info({"msg":"Stoping the BillAcceptor"})
                self.billcollector.stop_bill_collector()
                logger.info({"msg":"Return the response","response":self.bill_acceptor_response})

                return self.bill_acceptor_response

            ## send POST commands (so far only GET_BILL_VALUE and AUTO_STACK are POST commands)

            elif command == self.AUTO_STACK:
                if not data:
                    return None
                ##if

                payload = {
                    "AutoStack": data,
                }##

                logger.debug("/setAutoStack payload: %s" % payload)

                final_url = self.bill_acceptor_url + command
                logger.debug("REST endpoint: '%s'" % final_url)

                request_data = json.dumps(payload)

                cmd = "curl %s -X POST -H 'Content-Type: application/json' -d '%s'" % (final_url, request_data)
                logger.debug("Command-line: '%s'" % cmd)

                for i in xrange(15):
                    p = Popen(cmd, shell=True, stdin=PIPE, stdout=PIPE, stderr=PIPE, close_fds=True)
                    self.bill_acceptor_response = p.stdout.read()
                    if "data" in self.bill_acceptor_response or "timeout" in self.bill_acceptor_response:
                        break

                    error =p.stderr.read()

                    if error:
                        logger.info("error happened '{0}' try '{1}'".format(error, i))
                    time.sleep(1) 

                logger.info({"msg":"Stop the BillAcceptor"})
                self.billcollector.stop_bill_collector()
                logger.info({"msg":"Return the response","response":self.bill_acceptor_response})

                return self.bill_acceptor_response

        except Exception, e:
            logger.info({"msg":"Stop the BillAcceptor"})
            self.billcollector.stop_bill_collector()
            logger.info({"msg":"error happened", "error":e})
            return None
                

        ##if-elif

    ##sendCommand()

    def process_response(self, response=None, command=None):
        """
"""
        if (not response) or (not command):
            return None
        ##if

        try:
            json_response = json.loads(response)
        except Exception, e:
            logger.debug(e)
            return None
        ##try-except

        if command == self.DEVICE_STATUS:
            return json_response.get(self.DEVICE_STATUS_KEY, default="")

        elif command == self.STACK_BILL:
            return json_response.get(self.STACK_BILL_KEY, default="")

        elif command == self.RETURN_BILL:
            return json_response.get(self.RETURN_BILL_KEY, default="")

        elif command == self.GET_BILL_VALUE:
            
            return json_response

        elif command == self.AUTO_STACK:
            return json_response.get(self.OK_KEY, default="")

        ##if-elif

    ##process_response()

##class BillAcceptor()

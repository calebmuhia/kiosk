# -*- coding: utf-8 -*-
import os
import subprocess
import time

from kiosk import settings
import shutil

__author__ = 'caleb'
import logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
import socket
import time

import requests
import xml.etree.ElementTree as ET
from kioskui.payments.xmltodict import etree_to_dict


class PrecidiaGateway(object):
    """
    - Communicates with payment gateway Precidia
    - No django here at all, just a plain vanilla python module
    - Called by the django-shop payment backend
    """
    # this are input variables
    Command = 0
    Input = 1
    Id = 2
    ClientId = 3
    Amount = 4
    KeepAlive = 5
    ClientMAC = 6

    DEBUG = False

    HOST= getattr(settings, 'PRECIDIA_HOST', '192.168.1.111')
    PORT = 9998

    def __init__(self,order_amount):

        default_input = """
                            <PLRequest> 
                            <Command>CCSALE</Command> 
                            <Input>EXTERNAL</Input> 
                            <Id>1000</Id>                                
                            <ClientId>01</ClientId>
                            <Amount>0.00</Amount> 
                            <KeepAlive>Y</KeepAlive> 
                            <ClientMAC>place passphrase here</ClientMAC> 
                            </PLRequest>
                        """


        self.debit_body_input = ET.fromstring(default_input)
        # for t in self.debit_body_input:
        #     #print t

        self.debit_body_input[self.ClientMAC].text = settings.PRECIDIA_PARAPHASE
        self.debit_body_input[4].text = str(order_amount)
        self.debit_body_output = {}
    def set_amount_to_authorize(self, amount):
        """
        """
        ## FIXME: handle invalid amounts better than this (!!)
        if not amount:
            logger.info({"msg":"Invalid 'amount': ''. Setting to 0.0."})

            logger.warn()
            amount = 0.0
            ##if

        

        self.debit_body_input[self.Amount] = str(amount)

        return True

    def additional_input(self,input=None):
        attr_num = 31
        try:
            if input:
                for index, item in enumerate(input):
                    self.debit_body_input.update({
                        attr_num+index:item
                    })

            #print self.debit_body_input
        except:
            pass
        return True

    ##set_amount_to_authorize()



    def authorize_transaction(self, ):
        """
        this is the body that sends and receives the data from the payment getway

        """

        

        time.sleep(1)


        # self.debit_body_input[self.Id] = '12221122211'
        
        

        request_message = ET.tostring(self.debit_body_input)
        #print request_message
        
        request_response = ''
        if self.DEBUG:
            tree = ET.ElementTree()
            tree.parse('/home/caleb/projects/servomax1/debit_branch/kioskapp/kioskui/payments/approved.xml')
            root = tree.getroot()
            self.format_request_output(root)
            logger.debug("return response %s" % request_response)
            return self.debit_body_output

        try:

            logger.info({"msg":"Try Connecting to precidia server, Host {0}, Port {1}".format(self.HOST, self.PORT)})

            s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            s.connect((self.HOST,self.PORT))
            start_time = time.time()
            data = ''
            s.send(request_message)
            data = []
            while True:
                
                request_response = s.recv(1024)

                msg = request_response
            
                data.append(msg)
                if '</PLResponse>' in msg:
                    break


                
            s.close()
            end_time = time.time()
            logger.info({"msg":"Received response from precidia, time taken {0}".format(end_time-start_time)})

            start_time = time.time()
            request_response = ''.join(data)

            # #print (data)

            root = ET.fromstring(request_response)
            # #print "root", root

            self.format_request_output(root)

            end_time = time.time()
            logger.info({"msg":"Processed Precidia Response {0}".format(end_time-start_time)})



        except Exception , e:
            logger.critical({"msg":'error Connecting to precidia server {0}'.format(e)})
            #print e
            
            return False

        
        logger.info({"msg":'Returing precidia response to backend', "response":request_response})    


        return self.debit_body_output

    ##authorize_transaction()



    def is_transaction_approved(self, portal_output=None):
        """
        Returns True if the transaction in this transaction_receipt object has been approved
        i.e. return transaction_receipt.get('Approved', None)

        """

        if not portal_output:
            portal_output = self.debit_body_output

        

        approval_tag = portal_output['Result']
        #print "approval_tag", approval_tag

        if int(approval_tag) == 'APPROVED':
            return True

        else:
            return False


    ##is_transaction_approved()

    def set_transaction_reference(self):

        # self.debit_body_input[self.Id] = '12221122211'

        return True

    def get_authorization_code(self, portal_output=None):
        """
        Returns the ID associated with this transaction
        """
        if not portal_output:
            portal_output = self.debit_body_output

        authorization_code = portal_output['PLResponse']["Authorization"]

        return authorization_code

    def format_request_message(self, input_result):
        pass
        input_str = ''

        for i in input_result:
            input_str = "%s=%s\n%s" % (i,input_result[i],input_str)

       

        return input_str

    def format_request_output(self,output):
        self.debit_body_output = etree_to_dict(output)
        return True

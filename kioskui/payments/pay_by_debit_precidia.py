__author__ = 'caleb'

# -*- coding: utf-8 -*-
from decimal import Decimal
import logging
# from kioskui.utils import check_generic_products

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

from django.template import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.conf.urls import patterns, url
# from kioskui.signals import *
from shop.models.ordermodel import Order, OrderItem
from shop.util.decorators import on_method, shop_login_required
from shop.models import Order
from shop.util.cart import get_or_create_cart
from shop.util.order import add_order_to_request
from kioskui.models import UserCashFlow
from kioskui.tasks import check_completed_order
from shop.models import OrderPayment, Cart
from shop.order_signals import completed

from django import forms

from precidia import PrecidiaGateway
import json
import redis,time
r = redis.StrictRedis(host='localhost', port=6379, db=0)


class GetDebitCardTrackDataForm(forms.Form):

    order_amount = forms.DecimalField(
        max_digits=12,
        decimal_places=2,
        widget=forms.TextInput(attrs={'readonly': 'readonly'}),
        label="Order Amount",
    )  # order_form
# class GetCreditCardTrackDataForm()


class PrecidiaBackend(object):

    """
    Derived from https://github.com/divio/django-shop/blob/master/shop/payment/backends/pay_on_delivery.py
    """
    backend_name = "Debit CARD"
    url_namespace = "precidia_payment"
    name = url_namespace

    def __init__(self, shop):
        self.shop = shop

    def make_payment(self, request):

        the_order = self.shop.get_order(request)
        

        order_amount = self.shop.get_order_total(the_order)

        if request.user.is_authenticated():
            user = request.user
            logger.info({"msg":"User is logged in", "user":getattr(request.user, 'username', None)})


        else:
            logger.info({"msg":"Anonymous user. Not logged-in.", "user":getattr(request.user, 'username', None)})

            
        # if-else

        # talk to the MyVirtualMerchant endpoint
        precidia_response = self.authorize_transaction(order_amount)

        if not precidia_response:
            logger.info({"msg":"Credit-card transaction not authorized. Cancelling this order.", "user":getattr(request.user, 'username', None)})
            
            
            
            # extract the error messages and act accordingly
            the_order.status = Order.CANCELLED
            the_order.save()

            logger.critical({"msg":"Could Not connect To Credit Card Server","user":getattr(request.user, 'username', None) })

            response = {
                "status": "error", "msg": "Could Not connect To Credit Card Server"}

            return HttpResponse(json.dumps(response))

        # append precidia_response to this order (as a string)
        # FIXME: this should ideally be it's own query-able object
        logger.info({"msg":"Appending precidia_response to this order '{0}'".format(the_order), "user":getattr(request.user, 'username', None)})

        
        self.shop.add_extra_info(
            the_order,
            "'%s' payment-backend precidia_response: \n\n%s" % (
                self.backend_name, precidia_response)
        )

        # process the endpoint precidia_response to determine the authorization
        # status of this transaction

        logger.info({"msg":"Check if Transaction was approved by precidia", "user":getattr(request.user, 'username', None)})
        

        txn_approved = self.is_transaction_approved(precidia_response)

        logger.debug("TRANSACTION STATUS: '%s'" % txn_approved)
        


        # if the transaction was been approved...
        if txn_approved:
            # get the transaction number
            transaction_id = self.get_transaction_id()
            logger.info({"msg":"Transaction was approved", "user":getattr(request.user, 'username', None)})



            if request.user.is_authenticated():
                logger.info({"msg":"incrementing user expenses >>", "user":getattr(request.user, 'username', None)})
                
                user = request.user
                user.increment_credit_card_expense(
                    order_amount, precidia_response['PLResponse']['CardNumber'].rjust(16, '*'), the_order.id)

            else:
                logger.warn({"msg":"Anonymous user. Skipping incrementing expense totals.", "user":getattr(request.user, 'username', None)})

                
                UserCashFlow.objects.create(
                    user=None,
                    status=UserCashFlow.CREDITEXPENCES,
                    amount=order_amount,
                    pre=None,
                    post=None,
                    description='Bought products via Credit Card',
                    extra_field=precidia_response['PLResponse'][
                        'CardNumber'].rjust(16, '*'),
                    order_id = the_order.id    
                )
            # if-else

            # mark the amount as payed and save this transaction

            printing_args = {
                'order_id':the_order.id,
                'card':precidia_response['PLResponse']['CardNumber']
                }
            request.session["printing_args"] = printing_args
            
            logger.info({"msg":"Marking this transaction as paid and save it.", "user":getattr(request.user, 'username', None)})



            OrderPayment.objects.create(
                order=the_order,
                # How much was paid with this particular transfer
                amount=Decimal(order_amount),
                transaction_id=transaction_id,
                payment_method=self.backend_name)
            order = the_order
            if(order.is_paid()):
                # Set the order status:
                order.status = Order.COMPLETED
                order.save()

                # empty the related cart
                try:
                    cart = Cart.objects.get(pk=order.cart_pk)
                    cart.empty()
                except Cart.DoesNotExist:
                    pass

                completed.send(sender=self, order=order)

            # redirect to the next step in this checkout process
            # currently this redirects to the "Thank You View" (view
            # 'thank_you_for_your_order')
            finished_url = self.shop.get_finished_url()

            response = {
                "status": "success", "msg": "transaction was completed", "url": finished_url}
            return HttpResponse(json.dumps(response))

        # if the transaction was denied...
        else:

            logger.info({"msg":"Credit-card transaction not authorized. Canceling this order.", "user":getattr(request.user, 'username', None)})

            
            # extract the error messages and act accordingly
            the_order.status = Order.CANCELLED
            the_order.save()
            response = {
                "status": "error", "msg": "Credit-card transaction not authorized"}

            return HttpResponse(json.dumps(response))
            # if-else
        # if form.is_valid()

    # ... else if the form has NOT been submitted...
    # .. display it with order_amount

    def precidia_payment_view(self, request):
        """
        Process the transaction
        """
        # Get the order object
        logger.info({"msg":"Starting '{0}' payment processing...".format(self.backend_name), "user":getattr(request.user, 'username', None)})
        
        logger.info({"msg":"Get or Create cart", "user":getattr(request.user, 'username', None)})
        cart = get_or_create_cart(request)

        cart.update(request)
        logger.info({"msg":"Cart Total {0}".format(cart.current_total),"user":getattr(request.user, 'username', None)})


        the_order = Order.objects.create_from_cart(cart, request)
        logger.info({"msg":"Order '{0}' Created from Cart".format(the_order.id), "user":getattr(request.user, 'username', None)})

        
        #print the_order
        add_order_to_request(request, the_order)

        logger.info({"msg":"Order amount '{0}' will be charged from account".format(the_order.order_total), "user":getattr(request.user, 'username', None)})

        try:
            check_completed_order.delay(the_order.id)
        except:
            pass    

        ctx = RequestContext(request)

        ctx.update({"amount": the_order.order_total})
        return render_to_response('checkout/pay_by_debit.html', ctx)

    # myvirtualmerchant_payment_view()

    def authorize_transaction(self, order_amount):
        """
        Talk to MyVirtualMerchant.com
        """
        # create a new payment gateway object ## TODO: should I re-use these
        # objects?
        self.gateway = PrecidiaGateway(order_amount)

        # set the gateway with order amount, track data, etc etc
        # self.gateway.set_amount_to_authorize(order_amount)
        # self.gateway.additional_input(items_list)

        # authorize the transaction
        logger.debug("Authorizing credit-card transaction via gateway.")
        gateway_precidia_response = self.gateway.authorize_transaction()

        # return the raw gateway precidia_response (for database persistence)
        return gateway_precidia_response
    # authorize_transaction()

    def is_transaction_approved(self, precidia_response):
        """
        """
        """
        Returns True if the transaction in this transaction_receipt object has been approved
        i.e. return transaction_receipt.get('Approved', None)

        """

        approval_tag = precidia_response['PLResponse']['Result']

        if approval_tag == 'APPROVED':
            return True

        else:
            return False
    # is_transaction_approved()

    def get_transaction_id(self):
        """
        Returns the ID associated with this transaction
        """
        return self.gateway.get_authorization_code()
    # get_transaction_id()

    def get_urls(self):
        urlpatterns = patterns('',
                               url(r'^$', self.precidia_payment_view,
                                   name=self.name),
                               url(r'^make_payment/$', self.make_payment,
                                   name="make_payment"),
                               )
        return urlpatterns
    # get_urls()

# class MyVirtualMerchantBackend()

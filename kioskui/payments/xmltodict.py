from collections import defaultdict
from xml.etree import cElementTree as ET

def etree_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(etree_to_dict, children):
            for k, v in dc.iteritems():
                dd[k].append(v)
        
        temp1 = {}
        

        for k, v in dd.iteritems():
            
            if len(v) == 1:
                temp1.update(
                    {
                k:v[0]
                })
            else:
                temp1.update({
                    k:v
                    })
  
        d = {t.tag:temp1}   
        # d = {t.tag: {k:v[0] if len(v) == 1 else v for k, v in dd.iteritems()}}
        #print 'd',d
    if t.attrib:
        d[t.tag].update(('@' + k, v) for k, v in t.attrib.iteritems())
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
              d[t.tag]['#text'] = text
        else:
            d[t.tag] = text
    return d



          


from django.contrib import admin
from kioskui.forms import KioskUserChangeForm, KioskUserCreationForm
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from kioskui.models import *
from shop_simplecategories.admin import ProductWithCategoryForm

# Register your models here.

class KioskUserAdmin(UserAdmin):
    # The forms to add and change user instances

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference the removed 'username' field
    fieldsets = (
        (None, {'fields': ('username','email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', )}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'is_cashbox_manager',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),

        (_('Payment Fields'),{'fields':('cash_deposits', 'credit_card_deposits', 'debit_card_deposits','account_expenses','debit_card_expenses', 'admin_deposits', 'admin_deducts')}),
        
        (_('Location'),{'fields':('location','company')}),
        (_('Important Fields'), {'fields': ('account_pin', 'account_id')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')}
        ),
    )
    form = KioskUserChangeForm
    add_form = KioskUserCreationForm
    list_display = ('username','email', 'first_name', 'last_name', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    ordering = ('username',)


class CashFlowAdmin(admin.ModelAdmin):
    """docstring for CashFlowAdmin"""
    
    list_display = ('get_username_or_none', 'pre', 'post', 'amount', 'status', 'description')
    search_fields = ('user__username','status')

    def get_username_or_none(self, obj):
        return obj.user.username if obj.user else None

    get_username_or_none.short_description = 'User'    



class BarcodeProductForm(ProductWithCategoryForm):
    class Meta(object):
        model = BarcodeProduct
    ##class Meta
##class BarcodeProducttForm
        
class BarcodeProductAdmin(admin.ModelAdmin):
    form = BarcodeProductForm
    search_fields = ('name','barcode')
##class BarcodeProductAdmin
    

class NoBarcodeProductForm(ProductWithCategoryForm):
    class Meta(object):
        model = NoBarcodeProduct
    ##class Meta
##class NoBarcodeProducttForm
        
class NoBarcodeProductAdmin(admin.ModelAdmin):
    form = NoBarcodeProductForm
##class NoBarcodeProductAdmin

admin.site.register(BarcodeProduct, BarcodeProductAdmin)
admin.site.register(NoBarcodeProduct, NoBarcodeProductAdmin)


        


admin.site.register(KioskUser, KioskUserAdmin)
admin.site.register(UserCashFlow, CashFlowAdmin)

admin.site.register(Tax)
admin.site.register(CashBox)
admin.site.register(QuantityMovement)

admin.site.register(MissingBarcodeOrKeycard, admin_class=None)
admin.site.register(ChoicesPlaceHolder, admin_class=None)
admin.site.register(RestockCategoryChoice, admin_class=None)
admin.site.register(RestockInventoryReport, admin_class=None)
admin.site.register(RestockInventoryReportitems, admin_class=None)
admin.site.register(LocationInfo, admin_class=None)
admin.site.register(PayRollPaymentReport, admin_class=None)
admin.site.register(PayRollItem, admin_class=None)

admin.site.register(Feedback)




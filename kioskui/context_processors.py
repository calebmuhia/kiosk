from shop.util.cart import get_or_create_cart
import redis
r = redis.StrictRedis(host='localhost', port=6379, db=0)
from kioskui.models import LocationInfo


def get_cart_items(request):
    cart = get_or_create_cart(request)
    

    return {"cart_quantity":cart.total_quantity}


def checkInternet(request):

    #return {"is_online":True}

    try:
        info = LocationInfo.objects.get()[0]
    except:
        info = None    

    return {"is_online":r.get("internet"), "info":info}
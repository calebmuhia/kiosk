from shop.cart.cart_modifiers_base import BaseCartModifier
from shop.models import CartItem
from kioskui.models import Tax
import logging
logger = logging.getLogger("debug")
from decimal import Decimal
from shop.models import Cart, CartItem

TWOPLACES = Decimal(10) ** -2



class HstTaxesOnCartItems(BaseCartModifier):

    

    def __init__(self, *args, **kwargs):
        super(HstTaxesOnCartItems, self).__init__(*args, **kwargs)
        self.tax = Decimal("0")
        logger.info("tax during init {0}".format(self.tax))
        self.count = 0
        self.fields = []

    

    def process_cart(self, cart, request):
        """
        This will be called once per Cart, after every line item was treated
        The subtotal for the cart is already known, but the Total is 0.
        Like for the line items, total is expected to be calculated in the
        cart's update() method.

        Line items should be complete by now, so all of their fields are
        accessible.

        Subtotal is accessible, but total is still 0.0. Overrides are expected
        to update cart.current_total.

        The ``request`` object is used to let implementations determine their
        prices according to session, and other request information. Use the
        Python dict ``request.cart_modifier_state`` to pass arbitrary data between
        cart_item_modifers and cart_modifiers.
        """
        

            

        field = self.get_extra_cart_price_field(cart, request)
        if field is not None:
            price = field[1]
            logger.info("hst before cart total {0}".format(cart.current_total))
            cart.current_total = cart.current_total + price
            logger.info("hst current cart total {0}".format(cart.current_total))
            cart.extra_price_fields.append(field)
        self.tax = Decimal("0")    
        return cart

    

    def get_extra_cart_price_field(self, cart, request):
        """
        Get an extra price field tuple for the current cart:

        This allows to modify the price easily, simply return a
        ('Label', Decimal('amount')) from an override. This is expected to be
        a tuple. The decimal should be the amount that should get added to the
        current subtotal. It can be a negative value.

        In case your modifier is based on the current price (for example in
        order to compute value added tax for the whole current price) your
        override can access that price via ``cart.current_total``. That is the
        subtotal, updated with all cart modifiers so far)

        >>> return ('Taxes total', 19.00)
        """

        tax_object = Decimal('0')

        items = CartItem.objects.filter(cart=cart).order_by('pk')

        for item in items:
            try:
                tax = Tax.objects.get(name__icontains='hst')
                hst_tax = Decimal(str(tax.value)) / Decimal('100')

            except Exception, e:
                logger.info(e)
                
                tax_object += Decimal("0.00")

            try:
                if item.product.hst:
                    product_price = item.product.unit_price

                    tax_object += (hst_tax * product_price*item.quantity).quantize(TWOPLACES)


                    
                else:
                    tax_object += Decimal("0.00")

            except Exception, e:
                logger.info(e)

                
                tax_object += Decimal("0.00")

        return ('Gst', tax_object)



# class PstTaxesOnCartItems(BaseCartModifier):

    
class PstTaxesOnCartItems(BaseCartModifier):

    

    def __init__(self, *args, **kwargs):
        super(PstTaxesOnCartItems, self).__init__(*args, **kwargs)
        self.tax = Decimal("0")
        logger.info("tax during init {0}".format(self.tax))
        self.count = 0
        self.fields = []

    

    def process_cart(self, cart, request):
        """
        This will be called once per Cart, after every line item was treated
        The subtotal for the cart is already known, but the Total is 0.
        Like for the line items, total is expected to be calculated in the
        cart's update() method.

        Line items should be complete by now, so all of their fields are
        accessible.

        Subtotal is accessible, but total is still 0.0. Overrides are expected
        to update cart.current_total.

        The ``request`` object is used to let implementations determine their
        prices according to session, and other request information. Use the
        Python dict ``request.cart_modifier_state`` to pass arbitrary data between
        cart_item_modifers and cart_modifiers.
        """
        

            

        field = self.get_extra_cart_price_field(cart, request)
        if field is not None:
            price = field[1]
            logger.info("hst before cart total {0}".format(cart.current_total))
            cart.current_total = cart.current_total + price
            logger.info("hst current cart total {0}".format(cart.current_total))
            cart.extra_price_fields.append(field)
        self.tax = Decimal("0")    
        return cart

    

    def get_extra_cart_price_field(self, cart, request):
        """
        Get an extra price field tuple for the current cart:

        This allows to modify the price easily, simply return a
        ('Label', Decimal('amount')) from an override. This is expected to be
        a tuple. The decimal should be the amount that should get added to the
        current subtotal. It can be a negative value.

        In case your modifier is based on the current price (for example in
        order to compute value added tax for the whole current price) your
        override can access that price via ``cart.current_total``. That is the
        subtotal, updated with all cart modifiers so far)

        >>> return ('Taxes total', 19.00)
        """

        tax_object = Decimal('0')

        items = CartItem.objects.filter(cart=cart).order_by('pk')

        for item in items:
            try:
                tax = Tax.objects.get(name__icontains='pst')
                pst_tax = Decimal(str(tax.value)) / Decimal('100')

            except Exception, e:
                logger.info(e)
                
                tax_object += Decimal("0.00")

            try:
                if item.product.pst:
                    product_price = item.product.unit_price

                    tax_object += (pst_tax * product_price*item.quantity).quantize(TWOPLACES)


                    
                else:
                    tax_object += Decimal("0.00")

            except Exception, e:
                logger.info(e)

                
                tax_object += Decimal("0.00")

        return ('Pst', tax_object)

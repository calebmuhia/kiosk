__author__ = 'caleb'

# -*- coding: utf-8 -*-
from django import template

from classytags.helpers import InclusionTag
from classytags.core import Options
from classytags.arguments import Argument

from shop.util.cart import get_or_create_cart
from shop.models.productmodel import Product
from classytags.core import Tag


register = template.Library()


class Cart_tags(InclusionTag):
    """
    Inclusion tag for displaying cart summary.
    """
    template = '_cart_tags.html'

    def get_context(self, context):
        cart = get_or_create_cart(context['request'])
        cart.update()
        return {
            'cart': cart
        }

register.tag(Cart_tags)

import logging
from django.contrib.auth.models import AnonymousUser

from kiosk import settings
logger = logging.getLogger(__name__)
import time
from django_cups.models import Printer
from shop.models.ordermodel import Order, OrderItem








def word_wrap(string, width=80, ind1=0, ind2=0, prefix=''):
    """ word wrapping function.
        string: the string to wrap
        width: the column number to wrap at
        prefix: prefix each line with this string (goes before any indentation)
        ind1: number of characters to indent the first line
        ind2: number of characters to indent the rest of the lines
    """
    string = prefix + ind1 * " " + string
    newstring = ""
    while len(string) > width:
        # find position of nearest whitespace char to the left of "width"
        marker = width - 1
        while not string[marker].isspace():
            marker = marker - 1

        # remove line from original string and add it to the new string
        newline = string[0:marker] + "\n"
        newstring = newstring + newline
        string = prefix + ind2 * " " + string[marker + 1:]

    return newstring + string


def printing_receipt_def(request):
    
    print 'starting printing'
    msgs = []
    msgs.append('starting printing')
    try:
        
        print "this is A post"
        order = None
        # Set the order status:
        if request.user and not isinstance(request.user, AnonymousUser):
            print request.user

            order = Order.objects.get_latest_for_user(request.user)

        else :
            print "user not available",
            logger.debug("user not available")

            order = Order.objects.order_by("-created")[0]
            print "order", order


        logger.debug("printing receipt for order id %d" % (order.id ))
        order_id = order.id
        print order_id

        filename = "order %s" % order.id
        path = (settings.BASE_DIR+'/static/files/'+ filename + '.txt')
        print path
        try:
            curr_path = open(path, "wb+")
        except Exception, e:
            print e
        image = ''
        thankyou = ''
        try:
            image = settings.BASE_DIR+"/static/images/qualityfont.jpg"
            thankyou = settings.BASE_DIR+"/static/images/thankyou.png"
        except Exception, e:
            print e
        print "image",image
        print "thankyou",thankyou

        pagesize=(180,600)



        #c = canvas.Canvas(curr_path, pagesize=((180,600)))
        #c.drawImage(image,30,550,width=100, height=30)
        
        string = "Canada Micro Markets".center(70)
        string+='\n'

        string+='\n'
        string+='\n'
       

        string += '{0:20}{1}{2:10}{3}'.format('','Order id'.ljust(10),'',order_id)
        string +='\n'
        if order.user and not isinstance(order.user, AnonymousUser):
             string += '{0:20}{1}{2:10}{3}'.format('','User'.ljust(10),'', order.user.first_name)
             string +='\n'

        else:
             string +='{0:20}{1}{2:10}{3}'.format('','User'.ljust(10),'','None')
             string+='\n'


        string += '{0:20}{1}{2:10}{3}'.format('','Date'.ljust(10),'',order.created.strftime('%X  %d:%m:%y'))
        string +='\n'

        orderpayment = order.orderpayment_set.get()

        z = 500
        x = 0
        y = 0
        try:
            if orderpayment.payment_method == "MY ACCOUNT":
                string += '{0:20}Paid on Account-Balance:{1:5}{2}'.format('','',str(order.user.balance))
                string +='\n'

            elif orderpayment.payment_method == "CREDIT CARD":

                extrainfor = order.extra_info.get()
                lines = extrainfor.text.split("\r\n")
                for line in lines:
                    if "ssl_card_number" in line or "ssl_amount" in line or "ssl_result_message" in line:
                        line = line.replace("ssl_", '').split('=')
                        string += '{0:20}Card_Number{1:15}{2}'.format(str(line[1]))

            
            string+='{0:15}{1}'.format('',''.ljust(45,'_'),)
            string+='\n'

        except Exception, e:         
            print e

        try:
           order_items = OrderItem.objects.filter(order = order_id)
           



           for item in order_items:
              #c.drawString(5,z+y, str(item.quantity))
              string1 = item.product_name
              width = 30
              newstring = ''
              a = z

              if len(string1) < width:
                  #c.drawString(10, a+y, string)
                  pass

              else:

                  while len(string1) > width:
                        # find position of nearest whitespace char to the left of "width"
                        marker = width - 1
                        while not string1[marker].isspace():
                            marker = marker - 1

                        # remove line from original string and add it to the new string
                        newline = string1[0:marker]
                        newstring = newstring + newline
                        string1 = string1[marker + 1:]


                        #c.drawString(10, a+y, newstring)
                        a -= 10
                        newstring = ''


              #c.drawString(140,z+y, str(item.unit_price))
              


              if item.product.pst and item.product.hst:
                  type_tax='GP'
              elif item.product.hst:
                  type_tax= 'G'

              elif item.product.pst:
                  type_tax='P'

              else:
                  type_tax=''
              
              
                  
              string+='{0:15}{1}{2:5}{3}{4:10}{5}{6:3}{7}'.format('',item.quantity,'',item.product_name[:20].ljust(20)   ,'',item.unit_price,'',type_tax)
              string+='\n'

        except Exception, e:
            print e
       
       
        string+='\n'    

        string+='{0:40}{1}{2}{3}'.format('','Subtotal'.ljust(15),'',str(order.order_subtotal))
        string+='\n'

        extra = order.extraorderpricefield_set.filter()
        z -=10
        for item in extra:
            if item.label == "Gst":
                string+="{0:40}{1}{2}{3}".format('','Gst'.ljust(15),'',str(item.value))
                string+='\n'
            elif item.label == "Pst":
                string+="{0:40}{1}{2}{3}".format('','PST'.ljust(15),'',str(item.value))
                string+='\n'


        string+='{0:40}{1}'.format('',''.ljust(20,'_'),)
        string+='\n'
        string+='{0:40}{1}{2}{3}'.format('','Total'.ljust(15),'',str(order.order_total))
        string+='\n' 
        string+='\n'
        string+= "Thank You".center(70)           
        curr_path.write(string)

        curr_path.close()
        time.sleep(0.3)
        printers = Printer.objects.filter()
        for printer in printers:

            printer.printFile(str(path),"servomax" )
        return  "printing completed" 

            


           
    except Exception, e:
        return e
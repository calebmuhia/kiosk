__author__ = 'caleb'
from django.shortcuts import render, render_to_response
from django.template.context import RequestContext
from django.views.generic import TemplateView
from django.views.generic.base import RedirectView

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse
from django.views.decorators.http import require_GET
from django.http import HttpResponse, HttpResponseRedirect
from kioskui.models import KioskUser as User, BarcodeProduct, NoBarcodeProduct, MissingBarcodeOrKeycard, CashBox, PrintingObject
from shop.util.cart import get_or_create_cart
from django.contrib.auth import login, authenticate
from shop.views.checkout import CheckoutSelectionView, ThankYouView
from shop.views import ShopTemplateView
from forms import PaymentSelectionForm, BillValuesRequestForm
from decimal import Decimal
from kioskui.payments.precidia import PrecidiaGateway
from kioskui.payments.bill_acceptor import BillAcceptor
from shop.models.ordermodel import Order, OrderItem
from datetime import datetime

from kioskui.forms import EmailForm
import ast
from django.core.mail import EmailMultiAlternatives
from django.template.loader import get_template
from django.template import Context
from provider.oauth2.models import Client
from kioskui.printing import printing_receipt_def
from kioskui.auth.fingerprint import FingerprintAuth

from kioskui.signals import zm_signal
from django.conf import settings

from kioskui.billcollector import BillCollector

import json

import logging
logger = logging.getLogger(__name__)
# Create your views here.
import os
from subprocess import call

try:
    from zm.models import Monitors
except:
    pass    



def server_error_500_view(request, template_name='kioskui/500.html'):
    """
    500 error handler.

    Templates: `500.html`
    Context: None
    """
    return render_to_response(template_name,
        context_instance=RequestContext(request)
    )  # server_error_500_view()

##server_error_500_view()

class Home(TemplateView):

    '''
     front page : welcome page
     does not return any keypress at the moment
    '''

    template_name = 'kioskui/home.html'

    def get(self, request, *args, **kwargs):
        ctx = super(Home, self).get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)

class ExtraActionMixin(TemplateView):
    action = None

    def dispatch(self, request, *args, **kwargs):
        """
        Submitting form works only for "GET" and "POST".
        If `action` is defined use it dispatch request to the right method.
        """
        if not self.action:
            return super(ExtraActionMixin, self).dispatch(request, *args,
                **kwargs)
        if self.action in self.http_method_names:
            handler = getattr(self, self.action, self.http_method_not_allowed)
        else:
            handler = getattr(self, self.action, None)
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)


class SelectPaymentCheckoutView(CheckoutSelectionView):

    template_name = "checkout/selection.html"

    def get_payment_selection_form(self):
        """
        Get (and cache) the PaymentSelectionForm instance
        """
        form = getattr(self, '_payment_selection_form', None)
        if not form:
            if self.request.method == 'POST':
                form = PaymentSelectionForm(self.request.POST)
            else:
                form = PaymentSelectionForm()
            self._payment_selection_form = form
        ##if
        return form
    ##get_payment_selection_form()kiosk

    def post(self, *args, **kwargs):
        """ Called when view is POSTed """
        payment_selection_form = \
            self.get_payment_selection_form()

        if payment_selection_form.is_valid():
            self.request.session['payment_backend'] = \
                payment_selection_form.cleaned_data['payment_method']

            checkout_payment_url = reverse('checkout_payment')

            return HttpResponseRedirect(checkout_payment_url)
        ##if

        return self.get(self, *args, **kwargs)
    ##post()

    def get_context_data(self, **kwargs):
        """
        This overrides the context from the normal template view
        """
        ctx = super(SelectPaymentCheckoutView, self).get_context_data(**kwargs)






        payment_selection_form = self.get_payment_selection_form()

        ## delete un-needed billing- and shipping-address forms
        if 'shipping_address' in ctx:
            del ctx['shipping_address']
        ##if

        if 'billing_address' in ctx:
            del ctx['billing_address']
        ##if

        if 'billing_shipping_form' in ctx:
            del ctx['billing_shipping_form']
        ##if

        ctx.update({
            'payment_selection_form': payment_selection_form,
            #'order': order,
        })

        return ctx
    ##get_context_data()

##class SelectPaymentCheckoutView()

class InsufficientFundsView(ShopTemplateView):
    """
    A custom Insufficent Funds view for My-Account payment option.
    All it does for now is point to a template (no context provided yet)
    """

    template_name = "checkout/insufficient_funds.html"


class UserAccount(ExtraActionMixin):

    
    template_name = 'kioskui/user_account.html'

    
    
    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
        return render_to_response(self.template_name, ctx)

    @method_decorator(login_required)    
    def get_oauth_keys(self, request, *args, **kwargs):
        user = self.request.user
        url = getattr(settings, 'MOBILE_API_ENDPOINT', '')

        client = Client(user=user, name="kioskapp client", client_type=1, url=url)  
        client.save()
        keys = {
        'id':client.client_id,
        'secret':client.client_secret,
        'location':'grenville',
        'user':user.username,
        'api':url

        } 

        return HttpResponse(json.dumps(keys))

            
class CashBoxAuth(ExtraActionMixin):
    template_name = "kioskui/cashboxauth.html"

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)
        currentBox = CashBox.objects.filter(emptied=False)
        history = CashBox.objects.filter(emptied=True).order_by("-created")
        ctx.update({
            "current":currentBox,
            "history":history
            })
        print history


        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        return render_to_response(self.template_name, ctx)

    def empty_current_cashbox(self, request, *args, **kwargs):
        user = self.request.user
        cashbox = CashBox()
        cashbox.empty_cashbox(user)

        return HttpResponseRedirect(reverse('cashboxauth'))    


def triggerzm(sender = None, **kwargs):

    cause = kwargs["cause"]
    camera = kwargs["camera"]
    path = os.path.join(settings.BASE_DIR, "kioskui/triggers.sh")
    call("bash %s %s %s" % (path, camera, cause), shell=True)


zm_signal.connect(triggerzm, dispatch_uid="my_unique_trigger_identifier")




@require_GET
def processBrowserKeypressInput(request):

    """
    process user keypress e.g keytag/product barcode 

    """

    if getattr(settings, 'INCLUDE_ZM', False):

        try:
            triggered = request.session["triggered"]
            pass
        except Exception as e:
            print e
            
            try:

                monitors = [monitor.id for monitor in Monitors.objects.filter(name__in = ["Camera4","camera5", "screen"])]


                for monitor in monitors:
                    zm_signal.send(sender=None,camera = monitor, cause="scanning_items")


                    request.session["triggered"] = True
                    print monitor



            except Exception as e:
                print "not sent", e
                pass

    keypress = request.GET.get('keypress')

    logger.info({'msg':"Received keypress from browser: '%s'" % keypress, 'user':getattr(request.user, 'username', None)})

    response = "Neither a username nor a product"
    keypress = keypress.replace('+', '').replace(' ', '').lower()

    if keypress.startswith("4lf"):
        print
        report_id = str(int(keypress[3:9]))

        logger.info({'msg':'keypress is a warehouse to kiosk report','user':getattr(request.user, 'username', None)})
        


        url = "/dashboard/inventory/reports/driver_prekitting/"+report_id
        response = {
                        "type":"redirect",

                        "url":url,

                        ## FIXME: try-catch non-existent user profiles
                    }
        return HttpResponse(json.dumps(response), mimetype="application/json")
    
    
    if User.objects.filter(username=keypress).exists():
        logger.info({'msg':'keypress {0} matched a user'.format(keypress), 'user':getattr(request.user, 'username', None)})
        user = User.objects.get(username=keypress)

        

        if request.user.is_authenticated():
            logger.info({'msg':'user is already authenticate', 'user':getattr(request.user, 'username', None)})
            
            response = {
                    "type": "USER",
                    "username": request.user.username,
                    "logged_in":"true",
                    "balance":str(request.user.balance)
                    
                    }
            logger.info({'msg':"Sending back json response to browser: '{0}'".format(response), 'user':getattr(request.user, 'username', None)})
            
            return HttpResponse(json.dumps(response), mimetype="application/json")

        else:
            logger.info({'msg':"User is not logged in", 'user':getattr(request.user, 'username', None)})
            

            user = authenticate(username=keypress, password=keypress)

            if user:
                ## authenticated active user
                if user.is_active:
                    logger.info({'msg':"user is active", 'user':getattr(request.user, 'username', None)})


                    if user.is_cashbox_manager:
                        login(request, user)
                        logger.info({'msg':"User is now logged in and is a cashbox manager", 'user':getattr(request.user, 'username', None)})

                        url = reverse('cashboxauth')
                        response = {
                            "type":"redirect",

                            "url":url,

                            
                        }
                        return HttpResponse(json.dumps(response))
                    
                    login(request, user)
                    logger.info({'msg':"User is now logged", 'user':getattr(request.user, 'username', None)})
                    logger.info({'msg':"check if the current user has items in the cart, else fetch from database", 'user':getattr(request.user, 'username', None)})

                    cart = get_or_create_cart(request)
                    cart.update(request)

                    items = []
                    try:
                        if cart.total_quantity > 0:
                            logger.info({'msg':"User Has some Unpaid items on the cart from the previos session ", 'user':getattr(request.user, 'username', None)})

                            request.session["has_items"] = True
                            for item in cart.items.all():
                                product = {
                                    "name":item.product.name,
                                     "quantity":str(item.quantity)+"= $"+str(item.product.unit_price)
                                     }
                                items.append(product)
                                


                    except Exception,e :
                        print e
                        pass


                    
                    
                    print "building response"
                    response = {
                        "type": "USER",
                        "logged_in":"false",
                        "username": user.username,
                        "items":items,
                        "balance": float(user.balance),    ## FIXME: try-catch non-existent user profiles
                    }##response
                    print response
                    
                    
                ## authenticated inactive user
                else:
                    ## build the response
                    response = {
                        "type": "USER",
                        "username": user.username,
                        "error": "Your account has been disabled!",

                    }##response
                    
                    

                ##if-else
                
            ## un-authenticated user
            else:
                    ## build the response
                    response = {
                        "type": "USER",
                        "error": "Incorrect username and password.",
                    }##response
                    logger.warn({'msg':"Incorrect username/password provided.", 'user':getattr(request.user, 'username', None)})
                    
                    

        
        logger.info({'msg':"Sending back json response to browser: '{}'".format(response), 'user':getattr(request.user, 'username', None)})
        
        return HttpResponse(json.dumps(response), mimetype="application/json")


        

    elif BarcodeProduct.objects.filter(barcode=keypress).exists():
        try:


            product = BarcodeProduct.objects.get(barcode=keypress)
            logger.info({'msg':"Keypress matched a product", 'user':getattr(request.user, 'username', None),'product':product})
            
            add_to_cart_url = reverse('cart_item_add')
            print "addtocart"
            response = {
                "type": "PRODUCT",
                "name": product.get_name(),
                "item_id": product.id,
                "add_to_cart_url": add_to_cart_url,
                "unit_price": float(product.unit_price),
            }##response
            logger.info({'msg':"Adding Product into cart", 'user':getattr(request.user, 'username', None),'product':product})

        except:
            logger.error({'msg':"More than 1 item matched this keypress", 'user':getattr(request.user, 'username', None)})

            
            pass

            

        
    if response == "Neither a username nor a product" and keypress.startswith("stcx"):

        url = "/register_user/"+keypress

        response = {
                    "type":"redirect",

                    "url":url,

                    ## FIXME: try-catch non-existent user profiles
                }
        return HttpResponse(json.dumps(response), mimetype="application/json")

    if response == "Neither a username nor a product":
        logger.error({'msg':"keypress {0} is Neither a username nor a product".format(keypress), 'user':getattr(request.user, 'username', None)})

        #send_email_signal.send(sender = None, msg = "Problem detecting when barcode %s was scanned" % keypress )
        MissingBarcodeOrKeycard.objects.create(
                barcode = keypress,
                date = datetime.now())
        
        pass


    logger.info({'msg':"Sending back json response to browser: '{0}'".format(response), 'user':getattr(request.user, 'username', None)})
    return HttpResponse(json.dumps(response), mimetype="application/json")


class ChooseAmountToDepositViaCreditcard(TemplateView):
    def get_context_data(self, **kwargs):
        context = super(ChooseAmountToDepositViaCreditcard, self).get_context_data(**kwargs)
        context.update({"bill_values": [5, 10, 20, 30, 40, 50]})
        return context



class AddCashByDebitCardView(TemplateView):
    """
    """
    template_name = "add_cash_by_debit_card.html"
    action = None

    def dispatch(self, request, *args, **kwargs):
        """
        Submitting form works only for "GET" and "POST".
        If `action` is defined use it dispatch request to the right method.
        """
        if not self.action:
            return super(AddCashByDebitCardView, self).dispatch(request, *args,
                **kwargs)
        if self.action in self.http_method_names:
            handler = getattr(self, self.action, self.http_method_not_allowed)
        else:
            handler = getattr(self, self.action, None)
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

    @method_decorator(login_required)
    def updateaccount(self, request, *args, **kwargs):
        try:
            amount = self.kwargs['bill_value']

            ## Get the user object
            user = request.user

            gateway_response = self.authorize_transaction(amount)
            
            if not gateway_response:
                logger.critical({"msg":"Could Not connect To Credit Card Server", "user":getattr(request.user, 'username', None)})


                response = {'status':'denied', 'msg':'Could Not connect To Credit Card Server'}
                return HttpResponse(json.dumps(response))


            ## FIXME: append MyVirtualMerchant's response somewhere (!!)

            logger.info({"msg":"'{0}' payment-backend response: \n\n{1}".format(self.__class__.__name__, gateway_response), "user":getattr(request.user, 'username', None)})

            txn_approved = self.is_transaction_approved(gateway_response)

            

            ## if the transaction was been approved...
            if txn_approved:
                logger.info({"msg":"Transaction Approved", "user":getattr(request.user, 'username', None)})


                ## get the transaction number
                #transaction_id = self.get_transaction_id()

                ## FIXME: for now, assume they were approved for the full amount they chose
                ## FIXME: we're assuming the payment provider declines partial approvals

                balance_pre_update = user.balance

                ## increment appropriate totals
                logger.debug("Incrementing user expense totals...")

                              
                user.increment_credit_card_deposits(Decimal(amount), gateway_response['PLResponse']['CardNumber'].rjust(16, '*') )
                user.save()

                
                balance_post_update = user.balance

                balance_diff = Decimal(balance_post_update) - Decimal(balance_pre_update)
                cash_amount_decimal = Decimal(amount)

                logger.info({"msg":"Post: '%s' | Pre: '%s' | Cash amount: '%s'" %
                        (balance_post_update, balance_pre_update, cash_amount_decimal), "user":getattr(request.user, 'username', None)}
                )  # logger.info

                if (cash_amount_decimal != balance_diff):
                    logger.critical({"msg":"Pre- and post-update balances don't match up to added cash (!!)", "user":getattr(request.user, 'username', None)})

                response = {
                'status':"success",'msg':"Card Approved", "balance":str(user.balance),"amount":str(cash_amount_decimal)
                }    

                return HttpResponse(json.dumps(response))

            ## if the transaction was denied...
            else:
                logger.info({"msg":"Card Declined","user":getattr(request.user, 'username', None)})
                response = {'status':'denied', 'msg':"Your Card Was Declined"}
                return HttpResponse(json.dumps(response))
                ##if-else
            ##if form.is_valid()
        ##post()
        except Exception as e:
            print e
            response = {'status':'denied','msg':"System Error %s" % e }
            return HttpResponse(json.dumps(response))


    @method_decorator(login_required)        
    def get(self, request, *args, **kwargs):

        logger.info({"msg":"Add Money to account via Credit Card", "user":getattr(request.user, 'username', None)})

        cash_amount = kwargs['bill_value']  # FIXME: make sure cash_amount is a digit/numeric value (!!)
        
        logger.info({"msg":"Selected BILL VALUE: '{0}'".format(cash_amount), "user":getattr(request.user, 'username', None)})


        ## if the form has NOT been submitted...
        ## .. display it with cash_amount
        ctx = self.get_context_data(**kwargs)
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)
        

        ctx.update({"amount":cash_amount})
        return render_to_response(self.template_name, ctx)
    ##get()

    def authorize_transaction(self, order_amount):

        self.gateway = PrecidiaGateway(order_amount)

        ## set the gateway with order amount, track data, etc etc
        # self.gateway.set_amount_to_authorize(order_amount)
        #self.gateway.additional_input(items_list)


        ## authorize the transaction
        logger.debug("Authorizing credit-card transaction via gateway.")
        gateway_response = self.gateway.authorize_transaction()

        ## return the raw gateway response (for database persistence)
        return gateway_response

    def is_transaction_approved(self, response):
        """
        """
        print response['PLResponse']['Result']

        approval_tag = response['PLResponse']['Result']
        print "approval_tag", approval_tag

        if approval_tag == 'APPROVED':
            return True

        else:
            return False
    ##is_transaction_approved()

    def get_transaction_id(self):
        """
        Returns the ID associated with this transaction
        """
        return self.gateway.get_authorization_code()
    ##get_transaction_id()

##class AddCashByCreditCardView()

class AcceptBillsView(TemplateView):
    """
    """
    template_name = "kioskui/insert_bills.html"
    action = None

    def dispatch(self, request, *args, **kwargs):
        """
        Submitting form works only for "GET" and "POST".
        If `action` is defined use it dispatch request to the right method.
        """
        if not self.action:
            return super(AcceptBillsView, self).dispatch(request, *args,
                **kwargs)
        if self.action in self.http_method_names:
            handler = getattr(self, self.action, self.http_method_not_allowed)
        else:
            handler = getattr(self, self.action, None)
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

   

    @method_decorator(login_required)
    def get(self, request, *args, **kwargs):
        user = request.user
        logger.info({"msg":"Adding money to account via the bill acceptor", "user":getattr(request.user, 'username', None)})


        #open_billacceptor_signal.send(sender=None, user = user)
        print "user", user.username
        ctx  = self.get_context_data(**kwargs)
        
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        ctx.update({'username': user.username})
        

        return render_to_response(self.template_name, ctx)
    ##get()

    @method_decorator(login_required)
    def updateaccount(self, request, *args, **kwargs):
        """
        Process the transaction
        """
        client_data = self.kwargs['username']

        ## Get the user object
        user = request.user

        
        

        ## FIXME: TODO: make sure we're adding data for the right user
        username = user.username
        if client_data != username:
            logger.critical({"msg":"Client data '%s' to be sent to bill acceptor not the same as current username '%s' " % (client_data, username), "user":getattr(request.user, 'username', None)})
            response = {
            "status":"error", 
            "msg":"An Error Occured, Your Bill Was Not Accepted"
            }
            return HttpResponse(json.dumps(response))
        ##if

        ## query the bill-acceptor for the inserted bills
        self.bill_acceptor = BillAcceptor()
        self.bill_acceptor_response = \
            self.bill_acceptor.sendCommand(self.bill_acceptor.GET_BILL_VALUE, client_data)

        logger.info({"msg":"Bill Acceptor response: '%s'" % self.bill_acceptor_response,"user":getattr(request.user, 'username', None)})

        ## parse the bill-acceptor response and determine the bill-value
        ## FIXME: also make sure the bill has been stacked
        bill_value = self.get_bill_value(check_bit=client_data)
        # logger.debug("bills thats we got {0}".format(bill_value))



        ## if didn't or can't get bills...
        if not bill_value:
            response = {"status":"error","msg":"No Bill was Put in"}
            logger.info({"msg":"Error getting bill value, Bailing Out", "user":getattr(request.user, 'username', None)})

            return HttpResponse(json.dumps(response))
        ##if

        balance_pre_update = user.balance

        ## increment appropriate totals
        logger.info({"msg":"Incrementing user cash deposits", "user":getattr(request.user, 'username', None)})

        user.increment_cash_deposits(Decimal(bill_value))

        user.save()

        balance_post_update = user.balance

        balance_diff = Decimal(balance_post_update) - Decimal(balance_pre_update)
        bill_value_decimal = Decimal(bill_value)
        logger.info({"msg":"Post: '%s' | Pre: '%s' | Bill value: '%s'" %
                (balance_post_update, balance_pre_update, bill_value), "user":getattr(request.user, 'username', None)})

        assert (bill_value_decimal == balance_diff), \
            "Pre- and post-update balances don't match up to added cash (!!)"
        response = {"status":"success", "msg":"success", "balance":str(user.balance),"amount":bill_value}
        ## thank you for your business
        return HttpResponse(json.dumps(response))
        ##if form.is_valid()
    ##post()
    def get_bill_value(self, response=None, check_bit=None):
        """
        Parse the bill-acceptor response and determine the  bill-value
        Also make sure the bill has been stacked
        """
        if (not response) and not(self.bill_acceptor_response):
            return None
        ##if

        try:
            bill_acceptor_dict = json.loads(self.bill_acceptor_response)
        except Exception, e:
            logger.info({"msg":e})
            
            return None
        ##try-exception

        client_data = bill_acceptor_dict.get("data", None)

        logger.info({"client_data":client_data , "check_bit":check_bit})

        if client_data != check_bit:
            logger.critical({"msg":"Data sent to bill acceptor doesn't match what was echoed back! Bailing out..."})

            return None
        ##if

        bill_value = bill_acceptor_dict.get("billValue", None)
        logger.info({"msg":"Inserted bill value: '%s'" % bill_value})

        return bill_value
    ##get_bill_value()

##class AcceptBillsView()

class CustomThankYouView(ThankYouView):
    """
    Added the user's current balance to the default context
    """

    template_name = "checkout/thank_you.html"

#    @method_decorator(login_required)
    def get_context_data(self, **kwargs):
        """
        This overrides the context from the normal template view
        """
        ctx = super(CustomThankYouView, self).get_context_data(**kwargs)
        # order = ctx["order"]

        # orderitems = OrderItem.objects.filter(order = order)

        balance = None

        ctx.update({
            'balance': balance,

        })

        return ctx
    ##get_context_data()

##class CustomThankYouView()


class UserOrdersView(ExtraActionMixin):
    template_name= "kioskui/user_orders.html"

    def get(self, request, *args, **kwargs):
        orders = Order.objects.filter(user = self.request.user).order_by('-created')

        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request,ctx)
        ctx.update({"orders":orders})
        
        return render_to_response(self.template_name, ctx)  

    def order_details(self, request, *args, **kwargs):
        order_id = self.kwargs['order_id']
        order = Order.objects.get(id = order_id)

        ctx = self.get_context_data(**kwargs)

        ctx.update({'order':order})

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request,ctx)

        return render_to_response(self.template_name, ctx)  

class RegisterUserView(ExtraActionMixin):
    template_name= "kioskui/register_user.html"

    def get(self, request, *args, **kwargs):
        user_keypress = self.kwargs["keypress"]
        logger.info({"msg":"Registering new user for keypress {0}".format(user_keypress), "user":getattr(request.user, 'username', None)})


        

        ctx = self.get_context_data(**kwargs)

        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request,ctx)

        ctx.update({"keypress":user_keypress})    
        
        
        return render_to_response(self.template_name, ctx)  

    def post(self, request, *args, **kwargs):
        user_object = json.loads(self.request.body)
        logger.info({"msg":"receing user object".format(user_object), "user":getattr(request.user, 'username', None)})


        fullname = user_object['full_name']
        username = user_object['keypress']

        try:
            first_name, last_name = fullname.split()
        except Exception, err:
            logger.error({"err":err})
            logger.error({"msg":"Error splitting fullname '%s' into first- and last-names." % fullname, "user":getattr(request.user, 'username', None)})
            logger.warn({"msg":"Assigning incomplete fullname as firstname, lastname left as blank.", "user":getattr(request.user, 'username', None)})
            first_name = fullname
            last_name = ""
        try:
            user, created = User.objects.get_or_create(username=username)
            if created:
                user.set_password(username)
                user.first_name = first_name.title()
                user.last_name = last_name.title()
                user.save()


            logger.info({"msg":"Authenticating the newly-created user.", "user":getattr(request.user, 'username', None)})
            user = authenticate(username=username, password=username)

            if user is not None:
                if user.is_active:
                    logger.info({"msg":"Logging-in the newly-created user '%s'" % username , "user":getattr(request.user, 'username', None)})
                    login(request, user)
                    logger.info({"msg":"Successfully logged-in!", "user":getattr(request.user, 'username', None)})

                    response = {
                        "status":"success",

                        } 
                    # Redirect to a success page.
                else:
                    # FIXME: Return a 'disabled account' error message
                    logger.warn({"msg":"Account for user '%s' is disabed." % username, "user":getattr(request.user, 'username', None)})
                    response = {
                        "status":"error",
                        "msg":"Account Disabled"

                        } 
       
        except Exception, e:
            logger.error({"msg":"unable to create user %s" % e, "user":getattr(request.user, 'username', None)})
            response = {
                    "status":"error",
                    "msg":"Unable to Loggin User"
                    }

        return HttpResponse(json.dumps(response))

class PrintViaPrinter(TemplateView):
    template_name = None

    def get(self, request, *args, **kwargs):

        files = printing_receipt_def(self.request)
        return HttpResponse(json.dumps({'status':files}))


class PrintReceiptsView(TemplateView):
    template_name = "kioskui/printing.html"

    action = None



    def dispatch(self, request, *args, **kwargs):
        """
        Submitting form works only for "GET" and "POST".
        If `action` is defined use it dispatch request to the right method.
        """
        if not self.action:
            return super(PrintReceiptsView, self).dispatch(request, *args,
                **kwargs)
        if self.action in self.http_method_names:
            handler = getattr(self, self.action, self.http_method_not_allowed)
        else:
            handler = getattr(self, self.action, None)
        self.request = request
        self.args = args
        self.kwargs = kwargs
        return handler(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        
        printing_args = self.request.session.get("printing_args", None)
        ctx = self.get_context_data(**kwargs)
        if not isinstance(ctx, RequestContext):
            ctx = RequestContext(self.request, ctx)

        initial = {}  
        email = ''


        

        if printing_args:

            order = Order.objects.get(id = printing_args["order_id"])  
            
            if "card" in printing_args:
                printing_object, created = PrintingObject.objects.get_or_create(card = printing_args["card"])
                if created:
                    ctx.update({"obj":printing_object})

                    initial = {"card":printing_args["card"], "email":"empty", "order_id":order.id}

                else:
                    email = printing_object.email
                    ctx.update({"obj":printing_object})
                    initial =  {"card":printing_args["card"], "email":printing_object.email, "order_id":order.id, "extra":"card"}   

            elif "account" in printing_args:
                email = order.user.email

                initial = {"email":order.user.email, "order_id":order.id, "extra":"account" }




                    

        form = EmailForm(initial)
        ctx.update({"form":form, "email":email})
        return render_to_response(self.template_name,ctx)  


    def post(self, request, *args, **kwargs):

        form  = EmailForm(self.request.POST)
        
        context ={}

        if form.is_valid():
            
            email = form.cleaned_data["email"]
            order_id = form.cleaned_data["order_id"]
            extra = form.cleaned_data["extra"]
            print email
            



            if "card" in extra:
                card = form.cleaned_data["card"]
                printing_object=PrintingObject.objects.get(card = card)
                printing_object.email = email
                printing_object.save()

                context = self.formatCardTransations(order_id, context)

                
            elif  "account" in extra:
                order = Order.objects.get(id = order_id)
                user = order.user
                if user:
                    user.email = email
                    user.save()
                context = self.formatAccountTransaction(order_id, context)

                

            self.sendemail(context, email)

        return HttpResponseRedirect("/accounts/logout")   

    def formatCardTransations(self, order_id, context):
        order = Order.objects.get(id = order_id)
        taxes = order.extraorderpricefield_set.filter()
        for tax in taxes:
            if (tax.label=='Gst'):
                context.update({"Hst":tax.value})
            elif (tax.label=='Pst'):
                context.update({"Pst":tax.value})   

        extra_info = order.extra_info.filter()[0]
        
        products = OrderItem.objects.filter(order = order)
        context.update({"order":order, "type":"card", "transaction_type":"Credit Card Payment", "products":products})
        
        


        if extra_info:
            text = extra_info.text.split("\n\n")
            logger.info(text)
            if text:
                receipt = ast.literal_eval(text[1])
                customer_receipt = receipt['PLResponse']['ReceiptCustomer']

                transaction = customer_receipt["Receipt10"].split("REFERENCE #")[1]
                created_on = customer_receipt["Receipt7"]
                card_type = receipt['PLResponse']["CardType"]
                card_number = customer_receipt["Receipt6"].split('CARD NUMBER     ')[1]

                merchantid = receipt['PLResponse']["MerchantId"]
                order_total = receipt['PLResponse']['AuthAmt']
                context.update({"transaction":transaction, "created_on":created_on, "card_type":card_type, "card_number": card_number, 
                    "merchantid":merchantid, "order_total":order_total})
        return context

    def formatAccountTransaction(self, order_id, context):
        order = Order.objects.get(id = order_id)
        taxes = order.extraorderpricefield_set.filter()
        for tax in taxes:
            if (tax.label=='Gst'):
                context.update({"Hst":tax.value})
            elif (tax.label=='Pst'):
                context.update({"Pst":tax.value})   

        
        space = "&nbps;"
        products = OrderItem.objects.filter(order = order)
        context.update({"order":order, "type":"account", "transaction_type":"Account Payment", "products":products, "balance":order.user.balance})
        
        return context
      

    def sendemail(self, context, to):
        print "to ", to
        subject, from_email, to = 'Order Checkout', 'canadamicromarkets@gmail.com', to
        html = get_template('kioskui/email.html')
        d = Context(context)
        text_content = 'This is an important message.'

        html_content = html.render(d)
        msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
        msg.attach_alternative(html_content, "text/html")
        try:

            msg.send()
            print "message sent"
        except Exception, e:
            print "message not sent ", e

class FingerprintEnrollmentView(TemplateView):
    """
    Register individual fingers as part of the account-creation process
    """

    template_name = None

    def get(self, request, *args, **kwargs):
        ctx = self.get_context_data(**kwargs)

        username = self.request.user.username
        logger.debug({"msg":"response.username = '%s'" % username, "user":getattr(request.user, 'username', None)})

        if not username:
            logger.error({"msg":"Failed to determine username of user.", "user":getattr(request.user, 'username', None)})
            return HttpResponse(json.dumps({'status':'failed'}))
   
        fp = FingerprintAuth()

        logger.info({"msg":"your username '%s'" % username, "user":getattr(request.user, 'username', None)})

        
        response = fp.enroll_finger(username)


        if not response:
            logger.error({"msg":"Server Failed","user":getattr(request.user, 'username', None)})
            return HttpResponse(json.dumps({'status':'failed'}))
        
        else:
            logger.info({"msg":response,"user":getattr(request.user, 'username', None)})
            return HttpResponse(json.dumps({'status':response}))    


##class FingerprintEnrollmentView()




    






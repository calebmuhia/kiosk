__author__ = 'caleb'
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from kioskui.models import KioskUser

from django import forms

from shop.forms import get_billing_backends_choices

class PaymentSelectionForm(forms.Form):
    """
    Derived from shop.forms.BillingShippingForm https://github.com/divio/django-shop/blob/master/shop/forms.py
    
    A form displaying all available payment (the ones defined in settings.SHOP_PAYMENT_BACKENDS)
    """
    payment_method = forms.ChoiceField(choices=get_billing_backends_choices())
    
    ## specify whether this payment transaction is being used to top-up an Account
    ## (credit purchases are configured as Products which increase the balance of one's Account)
    ## default = False
#    account_credit_purchase = forms.BooleanField(
#                                 initial=False,
#                                 widget = forms.CheckboxInput(attrs={'hidden':'hidden', 'readonly':'readonly'}),
#                                 label = "",
#                             )##account_credit_purchase
##class PaymentSelectionForm()


class BillValuesRequestForm(forms.Form):
    """
    """
    data = forms.CharField(
                max_length=255,
                required=True,
                label="client data",
            )##data
    
##class BillValuesRequestForm()




class KioskUserCreationForm(UserCreationForm):
    """
    A form that creates a user, with no privileges, from the given email and
    password.
    """

    def __init__(self, *args, **kargs):
        super(KioskUserCreationForm, self).__init__(*args, **kargs)
        del self.fields['username']

    class Meta:
        model = KioskUser
        fields = ("email",)

class KioskUserChangeForm(UserChangeForm):
    """A form for updating users. Includes all the fields on
    the user, but replaces the password field with admin's
    password hash display field.
    """

    def __init__(self, *args, **kargs):
        super(KioskUserChangeForm, self).__init__(*args, **kargs)


    class Meta:
        model = KioskUser

class EmailForm(forms.Form):
    email = forms.EmailField(max_length=225, label="Email", required=True)
    card = forms.CharField(max_length=225, label="card", required=False)
    order_id = forms.CharField(max_length=225, label="Order", required=True)
    extra = forms.CharField(max_length=225, label="extra", required=True)        
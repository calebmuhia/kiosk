from django.conf.urls import patterns, include, url
from django.views.generic.base import TemplateView
from kioskui.views import *
from kioskui.api import OrdersResource, FeedbackResource
from shop.views.cart import CartDetails

from shop import urls as shop_urls
from shop.views.cart import CartDetails
from shop.views.order import OrderListView
from shop.views import ShopListView
from django.contrib.auth.views import login as login_user, logout as logout_user

from django.contrib import admin
admin.autodiscover()

from tastypie.api import Api
v4_api = Api(api_name='v4')
v4_api.register(FeedbackResource())



urlpatterns = patterns('kioskui.views',
   # Home
    url(r'^$', Home.as_view(), name='kiosk_home'),

   # Cart
    url(r'^cart/$',
        CartDetails.as_view(template_name="kioskui/cart.html"),
        name='cart'
    ),
    # process cart keypress
    url(r'^keypress/$',
        processBrowserKeypressInput,
        name='keypress'
    ),

    #checkout
    url(r'^checkout/$',
        SelectPaymentCheckoutView.as_view(
            template_name="checkout/selection.html"
        ),
        name="checkout_selection"
    ),
    # Auto Select payment options during checkout
    url(r'^auto_checkout/$',
        SelectPaymentCheckoutView.as_view(
            template_name="checkout/auto_selection.html"
        ),
        name="auto_checkout_selection"
    ),

    # Auto Select payment options during checkout
    url(r'^auto_checkout_payroll/$',
        SelectPaymentCheckoutView.as_view(
            template_name="checkout/auto_selection_payroll.html"
        ),
        name="auto_checkout_selection"
    ),

    # insufficient funds view
    url(r'^insufficient_funds$',
        InsufficientFundsView.as_view(
            template_name="checkout/insufficient_funds.html"
        ),
        name='insufficient_funds'
    ),
    

    #include all shop urls
    url(r'^shop/', include('shop.urls')),

    # logout
    url(r'^accounts/logout/$', logout_user, {'next_page': '/'}, name="logout"),

    ## login
    url(r'^accounts/login/$', login_user, name="login"),

    url(r'^user_account/$', UserAccount.as_view(), name="user_account"),
    url(r'^authenticate_app/$', UserAccount.as_view(action="get_oauth_keys"), name="authenticate_app"),
    # bill-values displayed for users adding money via credit/debit card
    url(r'^choose_cash_to_deposit_via_credit_card/$',
        ChooseAmountToDepositViaCreditcard.as_view(template_name='kioskui/choose_cash_to_deposit_via_credit_card.html'),
        
        name='choose_cash_to_deposit_via_credit_card',
    ),

    #initiate add cash to debit with a bill value
    url(r'^add_cash_by_debit_card/(?P<bill_value>\d+)/$',
        AddCashByDebitCardView.as_view(
            template_name="kioskui/add_cash_by_debit_card.html"
        ),
        name="add_cash_by_debit_card"
    ),

    # actual account update with bill value via precidia
    url(r'^add_cash_by_debit_card/updateaccount/(?P<bill_value>\d+)/$',
        AddCashByDebitCardView.as_view(
            action='updateaccount'
        ),
        name="updateaccount"
    ),
    #initial bills insert
    url(r'^insert_bills/$',
        AcceptBillsView.as_view(
            
        ),
        name="insert_bills"
    ),
    #actual account update via bill acceptor
    url(r'^insert_bills/updateaccount/(?P<username>\w+)/$',
        AcceptBillsView.as_view(
            action='updateaccount'
        ),
        name="insert_bills_updateaccount"
    ),
    # Thank-You for shopping
    url(r'^checkout/thank_you/$',
        CustomThankYouView.as_view(
            template_name="checkout/thank_you.html"
        ),
        name='thank_you_for_your_order'
    ),
    #fetch orders for loggedin user 
    url(r'^userorders/$',
        UserOrdersView.as_view(
            
        ),
        name='userorders'
    ),
    # details for an order
    url(r'^userorders/order_details/(?P<order_id>\d+)$',
        UserOrdersView.as_view(action='order_details',
            template_name="kioskui/order_details.html"
            
        ),
        name='userordersdetails'
    ),
    # no-barcodes Product List
    url(r'^products_no_barcodes/$',
        ShopListView.as_view(
            model=NoBarcodeProduct,
            template_name="kioskui/product_no_barcodes_list.html",
            context_object_name="product_no_barcodes_list"
        ),
        name='product_no_barcodes'
    ),

    #users can use this page to enter barcodes manually
    url(r'^key_in_code/$',
        TemplateView.as_view(
            
            template_name="kioskui/key_in_code.html",
            
        ),
        name='key_in_code'
    ),

    #Register the user
    url(r'^register_user/(?P<keypress>\w+)$',
        RegisterUserView.as_view(

        ),
        name='register_user'
    ),
    url(r'^register_user/create_user/$',
        RegisterUserView.as_view(

        ),
        name='create_user'
    ),
    url(r'^cashboxauth/$',
        CashBoxAuth.as_view(),
        name='cashboxauth'
    ),
    url(r'^emptycashbox/$',
        CashBoxAuth.as_view(action="empty_current_cashbox", template_name=None),
        name='emptycashbox'
    ),

    url(r'^printingreceiptsview/$',
        PrintReceiptsView.as_view(),
        name="printingreceiptsview"
    ),
    url(r'^printingviaprinter/$',
        PrintViaPrinter.as_view(),
        name="printingviaprinter"
    ),

    url(r'^register_fingerprint/$',
        TemplateView.as_view(
            
            template_name="kioskui/register_fingerprint.html",
            
        ),
        name='register_fingerprint'
    ),

    url(r'^enroll_fingerprint/$',
        FingerprintEnrollmentView.as_view(
    
        ),
        name='enroll_fingerprint'
    ),
    #api

    url(r'^api/', include(v4_api.urls)),  


)


'''
Created on 2012-05-05

@author: saidimu
'''
import logging
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

import requests
import json
import time


class FingerprintAuth(object):
    """
    """

    API_ENDPOINT = "http://127.0.0.1:8444/fingerprint/scanfinger"
    API_DELETE_ENDPOINT = "http://127.0.0.1:8444/fingerprint/deleteEnrollment"
    HEADERS = {'Content-Type': 'application/json'}

    INIT_ENROLL = {"action": "init_enroll"}
    CLIENT_ID = "client_id"
    CLIENT_PIN = "client_pin"
    ENROLL_FINGER = {"action": "enroll_finger"}
    ENROLL_CANCEL = {"action": "enroll_cancel"}

    ENROLL_STATUS = "enroll_status"
    AUTH_STATUS = "auth_status"
    ALREADY_ENROLLED = "already_enrolled"
    ENROLL_INIT = "initialized"
    ENROLL_FAIL = "failed"
    ENROLL_IN_PROGRESS = "inprogress"
    ENROLL_SUCCESS = "success"
    AUTH_SUCCESS = "success"

    SCANS_REQUIRED = "scans_required"

    # Wait this long to give the fingerprint server time to switch contexts
    ENFORCED_WAIT_TIME = 0.1   # in seconds

    def __init__(self):
        pass
    ##__init__()

    def auth_user(self):
        """
        Sends a GET request to the server to authenticate a user's fingerprint
        On success: returns 'username' of authenticated user
        On failure: returns None.
        """
        logger.info("Starting the fingerprint authentication process...")
        response = self.send_get_message()

        if not response:
            return None
        ##if

        if response[self.AUTH_STATUS] == self.AUTH_SUCCESS:
            username = response[self.CLIENT_ID]
            logger.info("Successfully authenticated username '%s'" % username)
            return username
        else:
            return None
        ##if-else
    ##auth_user()

    def start_enrollment(self, username=None, pin=None):
        """
        Sends a POST request to the server to initialize the enrollment process
        On success: returns the number of scans required
        On failure: returns None.
        """
        payload = None
        logger.info("Starting the fingerprint enrollment process...")
        response = self.send_post_message(self.INIT_ENROLL, username, pin)

        if not response:
            return None
        ##if

        if response[self.ENROLL_STATUS] == self.ALREADY_ENROLLED:
            logger.info("Username '%s' fingerprints have already been enrolled." % username)
            

            return response[self.ALREADY_ENROLLED]


            



            
        elif response[self.ENROLL_STATUS] == self.ENROLL_INIT:
            if response[self.CLIENT_ID] != username:
                logger.critical(
                    "Username '%s' echoed by fingerprint server is not the same as username '%s' sent." % (response[self.CLIENT_ID], username)
                )  # logger.critical
                logger.critical("Aborting fingerprint enrollment...")
                return None
            ##if

            return response[self.SCANS_REQUIRED]
        ##if-elif
    ###start_enrollment()

    def enroll_finger(self, username=None, pin=None):
        """
        Sends a POST request to start enrolling fingerprints
        On success: returns the number of required scans remaining (or 0 for successfull enrollment of all fingers)
        On failure: returns None.


        """

        # time.sleep(20)

        logger.info("About to enroll one finger...")
        response = self.send_post_message(self.ENROLL_FINGER, username)

        # response = {"enroll_status":"success"}

        if not response:
            return None
        ##if

        return response["enroll_status"]
        
    ##enroll_finger()

    def cancel_enrollment(self, username=None):
        """
        Inform the fingerprint server to cancel the already initiated enrollment process.
        """
        response = self.send_post_message(self.ENROLL_CANCEL, username)
        return response
    ##cancel_enrollment()

    def send_post_message(self, payload=None, username=None, pin=None):
        if (not payload) or (not username):
            return None
        ##if

        user_id = {"client_id": username, "client_pin":pin}

        try:
            payload.update(user_id)
        except Exception, err:
            logger.critical(err)
            return None
        ##try-except

        logger.info(
            "Sleeping for '%f' seconds to give the fingerprint server time to switch contexts" % \
            self.ENFORCED_WAIT_TIME
        )  # logger.info

        time.sleep(self.ENFORCED_WAIT_TIME)

        try:
            r = requests.post(
                    self.API_ENDPOINT,
                    data=json.dumps(payload),
                    headers=self.HEADERS,
            )  # requests.post
        except requests.ConnectionError, ce:
            logger.error(ce)
            return None
        ##try-except

        logger.debug("Server response: %s:\n\t" % r.text)

        try:
            response = json.loads(r.text)
        except Exception, e:
            logger.critical(e)
            return None
        ##try-except

        return response
    ##send_post_message()

    def send_get_message(self):
        logger.info(
            "Sleeping for '%f' seconds to give the fingerprint server time to switch contexts" % \
            self.ENFORCED_WAIT_TIME
        )  # logger.info

        time.sleep(self.ENFORCED_WAIT_TIME)

        try:
            r = requests.get(
                    self.API_ENDPOINT
            )  # requests.get
        except requests.ConnectionError, ce:
            logger.error(ce)
            return None
        ##try-except

        logger.debug("Server response: %s:\n\t" % r.text)

        try:
            response = json.loads(r.text)
        except Exception, e:
            logger.critical(e)
            return None
        ##try-except

        return response
    ##send_get_message()

##class FingeprintAuth()

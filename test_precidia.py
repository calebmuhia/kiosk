import xml.etree.ElementTree as ET
import socket

import time

start_prepare_time = time.time()
default_input = """
                <PLRequest> 
                <Command>CCSALE</Command> 
                <Input>EXTERNAL</Input> 
                <Id>1000</Id>                                
                <ClientId>01</ClientId>
                <Amount>0.00</Amount> 
                <KeepAlive>Y</KeepAlive> 
                <ClientMAC>place passphrase here</ClientMAC> 
                </PLRequest>
                """
HOST= '192.168.1.100'
PORT = 9998 
ClientMAC = 6 
PRECIDIA_PARAPHASE = '00011e0b2599'                    

debit_body_input = ET.fromstring(default_input)
debit_body_input[ClientMAC].text = PRECIDIA_PARAPHASE
debit_body_input[4].text = str(10.00)

request_message = ET.tostring(debit_body_input)
end_prepare_time = time.time()

print "time taken to prepare the request message", (end_prepare_time-start_prepare_time)

print "request message", request_message

start_send_message = time.time()
try:


    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST,PORT))
    start_time = time.time()
    data = ''
    s.send(request_message)
    data = []
    while True:
        
        request_response = s.recv(1024)


        data.append(request_response)
        if '</PLResponse>' in request_response:
            break
       
    s.close()
    request_response = ''.join(data)
except Exception, e:
    print "unable to connect to the server"    

end_send_message = time.time()

print "request processed in ", end_send_message-start_send_message
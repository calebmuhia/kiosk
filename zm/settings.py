
__author__='caleb'
from django.conf import settings


class Settings(object):
    """docstring for Settings"""
    def __init__(self, _locals):
        super(Settings, self).__init__()
        self._locals = _locals

    def set_zm(self):
        if getattr(settings, 'INCLUDE_ZM', False):

            self._locals['DATABASE_ROUTERS'].insert(0,'zm.zm_route.ZmRouter') 
            self._locals["INSTALLED_APPS"] += ('zm',)

            
            self._locals['DATABASES'].update({'zm_db':{
                                                'ENGINE': 'django.db.backends.mysql', 
                                                'NAME': 'zm',                      
                                                'USER': 'root',                     
                                                'PASSWORD': 'phoneboth',                 
                                                'HOST': '',                     
                                                'PORT': '',                     
                                            }})
            
            print self._locals['DATABASE_ROUTERS']                                
    

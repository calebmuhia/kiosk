# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines for those models you wish to give write DB access
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
from __future__ import unicode_literals

from django.db import models

class Config(models.Model):
    id = models.IntegerField(db_column='Id') # Field name made lowercase.
    name = models.CharField(db_column='Name', primary_key=True, max_length=32) # Field name made lowercase.
    value = models.TextField(db_column='Value') # Field name made lowercase.
    type = models.TextField(db_column='Type') # Field name made lowercase.
    defaultvalue = models.TextField(db_column='DefaultValue', blank=True) # Field name made lowercase.
    hint = models.TextField(db_column='Hint', blank=True) # Field name made lowercase.
    pattern = models.TextField(db_column='Pattern', blank=True) # Field name made lowercase.
    format = models.TextField(db_column='Format', blank=True) # Field name made lowercase.
    prompt = models.TextField(db_column='Prompt', blank=True) # Field name made lowercase.
    help = models.TextField(db_column='Help', blank=True) # Field name made lowercase.
    category = models.CharField(db_column='Category', max_length=32) # Field name made lowercase.
    readonly = models.IntegerField(db_column='Readonly') # Field name made lowercase.
    requires = models.TextField(db_column='Requires', blank=True) # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Config'
        app_label = "zm"

class Controlpresets(models.Model):
    monitorid = models.IntegerField(db_column='MonitorId') # Field name made lowercase.
    preset = models.IntegerField(db_column='Preset') # Field name made lowercase.
    label = models.CharField(db_column='Label', max_length=64) # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'ControlPresets'
        app_label = "zm"

class Controls(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True) # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64) # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=6) # Field name made lowercase.
    protocol = models.CharField(db_column='Protocol', max_length=64, blank=True) # Field name made lowercase.
    canwake = models.IntegerField(db_column='CanWake') # Field name made lowercase.
    cansleep = models.IntegerField(db_column='CanSleep') # Field name made lowercase.
    canreset = models.IntegerField(db_column='CanReset') # Field name made lowercase.
    canzoom = models.IntegerField(db_column='CanZoom') # Field name made lowercase.
    canautozoom = models.IntegerField(db_column='CanAutoZoom') # Field name made lowercase.
    canzoomabs = models.IntegerField(db_column='CanZoomAbs') # Field name made lowercase.
    canzoomrel = models.IntegerField(db_column='CanZoomRel') # Field name made lowercase.
    canzoomcon = models.IntegerField(db_column='CanZoomCon') # Field name made lowercase.
    minzoomrange = models.IntegerField(db_column='MinZoomRange', blank=True, null=True) # Field name made lowercase.
    maxzoomrange = models.IntegerField(db_column='MaxZoomRange', blank=True, null=True) # Field name made lowercase.
    minzoomstep = models.IntegerField(db_column='MinZoomStep', blank=True, null=True) # Field name made lowercase.
    maxzoomstep = models.IntegerField(db_column='MaxZoomStep', blank=True, null=True) # Field name made lowercase.
    haszoomspeed = models.IntegerField(db_column='HasZoomSpeed') # Field name made lowercase.
    minzoomspeed = models.IntegerField(db_column='MinZoomSpeed', blank=True, null=True) # Field name made lowercase.
    maxzoomspeed = models.IntegerField(db_column='MaxZoomSpeed', blank=True, null=True) # Field name made lowercase.
    canfocus = models.IntegerField(db_column='CanFocus') # Field name made lowercase.
    canautofocus = models.IntegerField(db_column='CanAutoFocus') # Field name made lowercase.
    canfocusabs = models.IntegerField(db_column='CanFocusAbs') # Field name made lowercase.
    canfocusrel = models.IntegerField(db_column='CanFocusRel') # Field name made lowercase.
    canfocuscon = models.IntegerField(db_column='CanFocusCon') # Field name made lowercase.
    minfocusrange = models.IntegerField(db_column='MinFocusRange', blank=True, null=True) # Field name made lowercase.
    maxfocusrange = models.IntegerField(db_column='MaxFocusRange', blank=True, null=True) # Field name made lowercase.
    minfocusstep = models.IntegerField(db_column='MinFocusStep', blank=True, null=True) # Field name made lowercase.
    maxfocusstep = models.IntegerField(db_column='MaxFocusStep', blank=True, null=True) # Field name made lowercase.
    hasfocusspeed = models.IntegerField(db_column='HasFocusSpeed') # Field name made lowercase.
    minfocusspeed = models.IntegerField(db_column='MinFocusSpeed', blank=True, null=True) # Field name made lowercase.
    maxfocusspeed = models.IntegerField(db_column='MaxFocusSpeed', blank=True, null=True) # Field name made lowercase.
    caniris = models.IntegerField(db_column='CanIris') # Field name made lowercase.
    canautoiris = models.IntegerField(db_column='CanAutoIris') # Field name made lowercase.
    canirisabs = models.IntegerField(db_column='CanIrisAbs') # Field name made lowercase.
    canirisrel = models.IntegerField(db_column='CanIrisRel') # Field name made lowercase.
    caniriscon = models.IntegerField(db_column='CanIrisCon') # Field name made lowercase.
    minirisrange = models.IntegerField(db_column='MinIrisRange', blank=True, null=True) # Field name made lowercase.
    maxirisrange = models.IntegerField(db_column='MaxIrisRange', blank=True, null=True) # Field name made lowercase.
    minirisstep = models.IntegerField(db_column='MinIrisStep', blank=True, null=True) # Field name made lowercase.
    maxirisstep = models.IntegerField(db_column='MaxIrisStep', blank=True, null=True) # Field name made lowercase.
    hasirisspeed = models.IntegerField(db_column='HasIrisSpeed') # Field name made lowercase.
    minirisspeed = models.IntegerField(db_column='MinIrisSpeed', blank=True, null=True) # Field name made lowercase.
    maxirisspeed = models.IntegerField(db_column='MaxIrisSpeed', blank=True, null=True) # Field name made lowercase.
    cangain = models.IntegerField(db_column='CanGain') # Field name made lowercase.
    canautogain = models.IntegerField(db_column='CanAutoGain') # Field name made lowercase.
    cangainabs = models.IntegerField(db_column='CanGainAbs') # Field name made lowercase.
    cangainrel = models.IntegerField(db_column='CanGainRel') # Field name made lowercase.
    cangaincon = models.IntegerField(db_column='CanGainCon') # Field name made lowercase.
    mingainrange = models.IntegerField(db_column='MinGainRange', blank=True, null=True) # Field name made lowercase.
    maxgainrange = models.IntegerField(db_column='MaxGainRange', blank=True, null=True) # Field name made lowercase.
    mingainstep = models.IntegerField(db_column='MinGainStep', blank=True, null=True) # Field name made lowercase.
    maxgainstep = models.IntegerField(db_column='MaxGainStep', blank=True, null=True) # Field name made lowercase.
    hasgainspeed = models.IntegerField(db_column='HasGainSpeed') # Field name made lowercase.
    mingainspeed = models.IntegerField(db_column='MinGainSpeed', blank=True, null=True) # Field name made lowercase.
    maxgainspeed = models.IntegerField(db_column='MaxGainSpeed', blank=True, null=True) # Field name made lowercase.
    canwhite = models.IntegerField(db_column='CanWhite') # Field name made lowercase.
    canautowhite = models.IntegerField(db_column='CanAutoWhite') # Field name made lowercase.
    canwhiteabs = models.IntegerField(db_column='CanWhiteAbs') # Field name made lowercase.
    canwhiterel = models.IntegerField(db_column='CanWhiteRel') # Field name made lowercase.
    canwhitecon = models.IntegerField(db_column='CanWhiteCon') # Field name made lowercase.
    minwhiterange = models.IntegerField(db_column='MinWhiteRange', blank=True, null=True) # Field name made lowercase.
    maxwhiterange = models.IntegerField(db_column='MaxWhiteRange', blank=True, null=True) # Field name made lowercase.
    minwhitestep = models.IntegerField(db_column='MinWhiteStep', blank=True, null=True) # Field name made lowercase.
    maxwhitestep = models.IntegerField(db_column='MaxWhiteStep', blank=True, null=True) # Field name made lowercase.
    haswhitespeed = models.IntegerField(db_column='HasWhiteSpeed') # Field name made lowercase.
    minwhitespeed = models.IntegerField(db_column='MinWhiteSpeed', blank=True, null=True) # Field name made lowercase.
    maxwhitespeed = models.IntegerField(db_column='MaxWhiteSpeed', blank=True, null=True) # Field name made lowercase.
    haspresets = models.IntegerField(db_column='HasPresets') # Field name made lowercase.
    numpresets = models.IntegerField(db_column='NumPresets') # Field name made lowercase.
    hashomepreset = models.IntegerField(db_column='HasHomePreset') # Field name made lowercase.
    cansetpresets = models.IntegerField(db_column='CanSetPresets') # Field name made lowercase.
    canmove = models.IntegerField(db_column='CanMove') # Field name made lowercase.
    canmovediag = models.IntegerField(db_column='CanMoveDiag') # Field name made lowercase.
    canmovemap = models.IntegerField(db_column='CanMoveMap') # Field name made lowercase.
    canmoveabs = models.IntegerField(db_column='CanMoveAbs') # Field name made lowercase.
    canmoverel = models.IntegerField(db_column='CanMoveRel') # Field name made lowercase.
    canmovecon = models.IntegerField(db_column='CanMoveCon') # Field name made lowercase.
    canpan = models.IntegerField(db_column='CanPan') # Field name made lowercase.
    minpanrange = models.IntegerField(db_column='MinPanRange', blank=True, null=True) # Field name made lowercase.
    maxpanrange = models.IntegerField(db_column='MaxPanRange', blank=True, null=True) # Field name made lowercase.
    minpanstep = models.IntegerField(db_column='MinPanStep', blank=True, null=True) # Field name made lowercase.
    maxpanstep = models.IntegerField(db_column='MaxPanStep', blank=True, null=True) # Field name made lowercase.
    haspanspeed = models.IntegerField(db_column='HasPanSpeed') # Field name made lowercase.
    minpanspeed = models.IntegerField(db_column='MinPanSpeed', blank=True, null=True) # Field name made lowercase.
    maxpanspeed = models.IntegerField(db_column='MaxPanSpeed', blank=True, null=True) # Field name made lowercase.
    hasturbopan = models.IntegerField(db_column='HasTurboPan') # Field name made lowercase.
    turbopanspeed = models.IntegerField(db_column='TurboPanSpeed', blank=True, null=True) # Field name made lowercase.
    cantilt = models.IntegerField(db_column='CanTilt') # Field name made lowercase.
    mintiltrange = models.IntegerField(db_column='MinTiltRange', blank=True, null=True) # Field name made lowercase.
    maxtiltrange = models.IntegerField(db_column='MaxTiltRange', blank=True, null=True) # Field name made lowercase.
    mintiltstep = models.IntegerField(db_column='MinTiltStep', blank=True, null=True) # Field name made lowercase.
    maxtiltstep = models.IntegerField(db_column='MaxTiltStep', blank=True, null=True) # Field name made lowercase.
    hastiltspeed = models.IntegerField(db_column='HasTiltSpeed') # Field name made lowercase.
    mintiltspeed = models.IntegerField(db_column='MinTiltSpeed', blank=True, null=True) # Field name made lowercase.
    maxtiltspeed = models.IntegerField(db_column='MaxTiltSpeed', blank=True, null=True) # Field name made lowercase.
    hasturbotilt = models.IntegerField(db_column='HasTurboTilt') # Field name made lowercase.
    turbotiltspeed = models.IntegerField(db_column='TurboTiltSpeed', blank=True, null=True) # Field name made lowercase.
    canautoscan = models.IntegerField(db_column='CanAutoScan') # Field name made lowercase.
    numscanpaths = models.IntegerField(db_column='NumScanPaths') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Controls'
        app_label = "zm"

class Devices(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True) # Field name made lowercase.
    name = models.TextField(db_column='Name') # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=3) # Field name made lowercase.
    keystring = models.CharField(db_column='KeyString', max_length=32) # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Devices'
        app_label = "zm"

class Events(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True) # Field name made lowercase.
    monitorid = models.IntegerField(db_column='MonitorId', primary_key=True) # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64) # Field name made lowercase.
    cause = models.CharField(db_column='Cause', max_length=32) # Field name made lowercase.
    starttime = models.DateTimeField(db_column='StartTime') # Field name made lowercase.
    endtime = models.DateTimeField(db_column='EndTime', blank=True, null=True) # Field name made lowercase.
    width = models.IntegerField(db_column='Width') # Field name made lowercase.
    height = models.IntegerField(db_column='Height') # Field name made lowercase.
    length = models.DecimalField(db_column='Length', max_digits=10, decimal_places=2) # Field name made lowercase.
    frames = models.IntegerField(db_column='Frames', blank=True, null=True) # Field name made lowercase.
    alarmframes = models.IntegerField(db_column='AlarmFrames', blank=True, null=True) # Field name made lowercase.
    totscore = models.IntegerField(db_column='TotScore') # Field name made lowercase.
    avgscore = models.IntegerField(db_column='AvgScore', blank=True, null=True) # Field name made lowercase.
    maxscore = models.IntegerField(db_column='MaxScore', blank=True, null=True) # Field name made lowercase.
    archived = models.IntegerField(db_column='Archived') # Field name made lowercase.
    videoed = models.IntegerField(db_column='Videoed') # Field name made lowercase.
    uploaded = models.IntegerField(db_column='Uploaded') # Field name made lowercase.
    emailed = models.IntegerField(db_column='Emailed') # Field name made lowercase.
    messaged = models.IntegerField(db_column='Messaged') # Field name made lowercase.
    executed = models.IntegerField(db_column='Executed') # Field name made lowercase.
    notes = models.TextField(db_column='Notes', blank=True) # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Events'
        app_label = "zm"

class Filters(models.Model):
    name = models.CharField(db_column='Name', primary_key=True, max_length=64) # Field name made lowercase.
    query = models.TextField(db_column='Query') # Field name made lowercase.
    autoarchive = models.IntegerField(db_column='AutoArchive') # Field name made lowercase.
    autovideo = models.IntegerField(db_column='AutoVideo') # Field name made lowercase.
    autoupload = models.IntegerField(db_column='AutoUpload') # Field name made lowercase.
    autoemail = models.IntegerField(db_column='AutoEmail') # Field name made lowercase.
    automessage = models.IntegerField(db_column='AutoMessage') # Field name made lowercase.
    autoexecute = models.IntegerField(db_column='AutoExecute') # Field name made lowercase.
    autoexecutecmd = models.TextField(db_column='AutoExecuteCmd', blank=True) # Field name made lowercase.
    autodelete = models.IntegerField(db_column='AutoDelete') # Field name made lowercase.
    background = models.IntegerField(db_column='Background') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Filters'
        app_label = "zm"

class Frames(models.Model):
    eventid = models.IntegerField(db_column='EventId', primary_key=True) # Field name made lowercase.
    frameid = models.IntegerField(db_column='FrameId', primary_key=True) # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=6) # Field name made lowercase.
    timestamp = models.DateTimeField(db_column='TimeStamp') # Field name made lowercase.
    delta = models.DecimalField(db_column='Delta', max_digits=8, decimal_places=2) # Field name made lowercase.
    score = models.IntegerField(db_column='Score') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Frames'
        app_label = "zm"

class Groups(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True) # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64) # Field name made lowercase.
    monitorids = models.TextField(db_column='MonitorIds') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Groups'
        app_label = "zm"

class Logs(models.Model):
    timekey = models.DecimalField(db_column='TimeKey', max_digits=16, decimal_places=6) # Field name made lowercase.
    component = models.CharField(db_column='Component', max_length=32) # Field name made lowercase.
    pid = models.IntegerField(db_column='Pid', blank=True, null=True) # Field name made lowercase.
    level = models.IntegerField(db_column='Level') # Field name made lowercase.
    code = models.CharField(db_column='Code', max_length=3) # Field name made lowercase.
    message = models.TextField(db_column='Message') # Field name made lowercase.
    file = models.CharField(db_column='File', max_length=255, blank=True) # Field name made lowercase.
    line = models.IntegerField(db_column='Line', blank=True, null=True) # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Logs'
        app_label = "zm"

class Monitorpresets(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True) # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64) # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=6) # Field name made lowercase.
    device = models.TextField(db_column='Device', blank=True) # Field name made lowercase.
    channel = models.TextField(db_column='Channel', blank=True) # Field name made lowercase.
    format = models.IntegerField(db_column='Format', blank=True, null=True) # Field name made lowercase.
    protocol = models.CharField(db_column='Protocol', max_length=16, blank=True) # Field name made lowercase.
    method = models.CharField(db_column='Method', max_length=16, blank=True) # Field name made lowercase.
    host = models.CharField(db_column='Host', max_length=64, blank=True) # Field name made lowercase.
    port = models.CharField(db_column='Port', max_length=8, blank=True) # Field name made lowercase.
    path = models.CharField(db_column='Path', max_length=255, blank=True) # Field name made lowercase.
    subpath = models.CharField(db_column='SubPath', max_length=64, blank=True) # Field name made lowercase.
    width = models.IntegerField(db_column='Width', blank=True, null=True) # Field name made lowercase.
    height = models.IntegerField(db_column='Height', blank=True, null=True) # Field name made lowercase.
    palette = models.IntegerField(db_column='Palette', blank=True, null=True) # Field name made lowercase.
    maxfps = models.DecimalField(db_column='MaxFPS', max_digits=5, decimal_places=2, blank=True, null=True) # Field name made lowercase.
    controllable = models.IntegerField(db_column='Controllable') # Field name made lowercase.
    controlid = models.CharField(db_column='ControlId', max_length=16, blank=True) # Field name made lowercase.
    controldevice = models.CharField(db_column='ControlDevice', max_length=255, blank=True) # Field name made lowercase.
    controladdress = models.CharField(db_column='ControlAddress', max_length=255, blank=True) # Field name made lowercase.
    defaultrate = models.IntegerField(db_column='DefaultRate') # Field name made lowercase.
    defaultscale = models.IntegerField(db_column='DefaultScale') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'MonitorPresets'
        app_label = "zm"

class Monitors(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True) # Field name made lowercase.
    name = models.CharField(db_column='Name', max_length=64) # Field name made lowercase.
    type = models.CharField(db_column='Type', max_length=6) # Field name made lowercase.
    function = models.CharField(db_column='Function', max_length=7) # Field name made lowercase.
    enabled = models.IntegerField(db_column='Enabled') # Field name made lowercase.
    linkedmonitors = models.CharField(db_column='LinkedMonitors', max_length=255) # Field name made lowercase.
    triggers = models.CharField(db_column='Triggers', max_length=3) # Field name made lowercase.
    device = models.CharField(db_column='Device', max_length=64) # Field name made lowercase.
    channel = models.IntegerField(db_column='Channel') # Field name made lowercase.
    format = models.IntegerField(db_column='Format') # Field name made lowercase.
    v4lmultibuffer = models.IntegerField(db_column='V4LMultiBuffer', blank=True, null=True) # Field name made lowercase.
    v4lcapturesperframe = models.IntegerField(db_column='V4LCapturesPerFrame', blank=True, null=True) # Field name made lowercase.
    protocol = models.CharField(db_column='Protocol', max_length=16) # Field name made lowercase.
    method = models.CharField(db_column='Method', max_length=16) # Field name made lowercase.
    host = models.CharField(db_column='Host', max_length=64) # Field name made lowercase.
    port = models.CharField(db_column='Port', max_length=8) # Field name made lowercase.
    subpath = models.CharField(db_column='SubPath', max_length=64) # Field name made lowercase.
    path = models.CharField(db_column='Path', max_length=255) # Field name made lowercase.
    options = models.CharField(db_column='Options', max_length=255) # Field name made lowercase.
    user = models.CharField(db_column='User', max_length=64) # Field name made lowercase.
    pass_field = models.CharField(db_column='Pass', max_length=64) # Field name made lowercase. Field renamed because it was a Python reserved word.
    width = models.IntegerField(db_column='Width') # Field name made lowercase.
    height = models.IntegerField(db_column='Height') # Field name made lowercase.
    colours = models.IntegerField(db_column='Colours') # Field name made lowercase.
    palette = models.IntegerField(db_column='Palette') # Field name made lowercase.
    orientation = models.CharField(db_column='Orientation', max_length=4) # Field name made lowercase.
    deinterlacing = models.IntegerField(db_column='Deinterlacing') # Field name made lowercase.
    brightness = models.IntegerField(db_column='Brightness') # Field name made lowercase.
    contrast = models.IntegerField(db_column='Contrast') # Field name made lowercase.
    hue = models.IntegerField(db_column='Hue') # Field name made lowercase.
    colour = models.IntegerField(db_column='Colour') # Field name made lowercase.
    eventprefix = models.CharField(db_column='EventPrefix', max_length=32) # Field name made lowercase.
    labelformat = models.CharField(db_column='LabelFormat', max_length=64) # Field name made lowercase.
    labelx = models.IntegerField(db_column='LabelX') # Field name made lowercase.
    labely = models.IntegerField(db_column='LabelY') # Field name made lowercase.
    imagebuffercount = models.IntegerField(db_column='ImageBufferCount') # Field name made lowercase.
    warmupcount = models.IntegerField(db_column='WarmupCount') # Field name made lowercase.
    preeventcount = models.IntegerField(db_column='PreEventCount') # Field name made lowercase.
    posteventcount = models.IntegerField(db_column='PostEventCount') # Field name made lowercase.
    streamreplaybuffer = models.IntegerField(db_column='StreamReplayBuffer') # Field name made lowercase.
    alarmframecount = models.IntegerField(db_column='AlarmFrameCount') # Field name made lowercase.
    sectionlength = models.IntegerField(db_column='SectionLength') # Field name made lowercase.
    frameskip = models.IntegerField(db_column='FrameSkip') # Field name made lowercase.
    motionframeskip = models.IntegerField(db_column='MotionFrameSkip') # Field name made lowercase.
    maxfps = models.DecimalField(db_column='MaxFPS', max_digits=5, decimal_places=2, blank=True, null=True) # Field name made lowercase.
    alarmmaxfps = models.DecimalField(db_column='AlarmMaxFPS', max_digits=5, decimal_places=2, blank=True, null=True) # Field name made lowercase.
    fpsreportinterval = models.IntegerField(db_column='FPSReportInterval') # Field name made lowercase.
    refblendperc = models.IntegerField(db_column='RefBlendPerc') # Field name made lowercase.
    alarmrefblendperc = models.IntegerField(db_column='AlarmRefBlendPerc') # Field name made lowercase.
    controllable = models.IntegerField(db_column='Controllable') # Field name made lowercase.
    controlid = models.IntegerField(db_column='ControlId') # Field name made lowercase.
    controldevice = models.CharField(db_column='ControlDevice', max_length=255, blank=True) # Field name made lowercase.
    controladdress = models.CharField(db_column='ControlAddress', max_length=255, blank=True) # Field name made lowercase.
    autostoptimeout = models.DecimalField(db_column='AutoStopTimeout', max_digits=5, decimal_places=2, blank=True, null=True) # Field name made lowercase.
    trackmotion = models.IntegerField(db_column='TrackMotion') # Field name made lowercase.
    trackdelay = models.IntegerField(db_column='TrackDelay') # Field name made lowercase.
    returnlocation = models.IntegerField(db_column='ReturnLocation') # Field name made lowercase.
    returndelay = models.IntegerField(db_column='ReturnDelay') # Field name made lowercase.
    defaultview = models.CharField(db_column='DefaultView', max_length=7) # Field name made lowercase.
    defaultrate = models.IntegerField(db_column='DefaultRate') # Field name made lowercase.
    defaultscale = models.IntegerField(db_column='DefaultScale') # Field name made lowercase.
    signalcheckcolour = models.CharField(db_column='SignalCheckColour', max_length=32) # Field name made lowercase.
    webcolour = models.CharField(db_column='WebColour', max_length=32) # Field name made lowercase.
    sequence = models.IntegerField(db_column='Sequence', blank=True, null=True) # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Monitors'
        app_label = "zm"

class States(models.Model):
    name = models.CharField(db_column='Name', primary_key=True, max_length=64) # Field name made lowercase.
    definition = models.TextField(db_column='Definition') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'States'
        app_label = "zm"

class Stats(models.Model):
    monitorid = models.IntegerField(db_column='MonitorId') # Field name made lowercase.
    zoneid = models.IntegerField(db_column='ZoneId') # Field name made lowercase.
    eventid = models.IntegerField(db_column='EventId') # Field name made lowercase.
    frameid = models.IntegerField(db_column='FrameId') # Field name made lowercase.
    pixeldiff = models.IntegerField(db_column='PixelDiff') # Field name made lowercase.
    alarmpixels = models.IntegerField(db_column='AlarmPixels') # Field name made lowercase.
    filterpixels = models.IntegerField(db_column='FilterPixels') # Field name made lowercase.
    blobpixels = models.IntegerField(db_column='BlobPixels') # Field name made lowercase.
    blobs = models.IntegerField(db_column='Blobs') # Field name made lowercase.
    minblobsize = models.IntegerField(db_column='MinBlobSize') # Field name made lowercase.
    maxblobsize = models.IntegerField(db_column='MaxBlobSize') # Field name made lowercase.
    minx = models.IntegerField(db_column='MinX') # Field name made lowercase.
    maxx = models.IntegerField(db_column='MaxX') # Field name made lowercase.
    miny = models.IntegerField(db_column='MinY') # Field name made lowercase.
    maxy = models.IntegerField(db_column='MaxY') # Field name made lowercase.
    score = models.IntegerField(db_column='Score') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Stats'
        app_label = "zm"

class Triggersx10(models.Model):
    monitorid = models.IntegerField(db_column='MonitorId', primary_key=True) # Field name made lowercase.
    activation = models.CharField(db_column='Activation', max_length=32, blank=True) # Field name made lowercase.
    alarminput = models.CharField(db_column='AlarmInput', max_length=32, blank=True) # Field name made lowercase.
    alarmoutput = models.CharField(db_column='AlarmOutput', max_length=32, blank=True) # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'TriggersX10'
        app_label = "zm"

class Users(models.Model):
    id = models.IntegerField(db_column='Id', primary_key=True) # Field name made lowercase.
    username = models.CharField(db_column='Username', unique=True, max_length=32) # Field name made lowercase.
    password = models.CharField(db_column='Password', max_length=64) # Field name made lowercase.
    language = models.CharField(db_column='Language', max_length=8) # Field name made lowercase.
    enabled = models.IntegerField(db_column='Enabled') # Field name made lowercase.
    stream = models.CharField(db_column='Stream', max_length=4) # Field name made lowercase.
    events = models.CharField(db_column='Events', max_length=4) # Field name made lowercase.
    control = models.CharField(db_column='Control', max_length=4) # Field name made lowercase.
    monitors = models.CharField(db_column='Monitors', max_length=4) # Field name made lowercase.
    devices = models.CharField(db_column='Devices', max_length=4) # Field name made lowercase.
    system = models.CharField(db_column='System', max_length=4) # Field name made lowercase.
    maxbandwidth = models.CharField(db_column='MaxBandwidth', max_length=16) # Field name made lowercase.
    monitorids = models.TextField(db_column='MonitorIds') # Field name made lowercase.
    class Meta:
        managed = False
        db_table = 'Users'
        app_label = "zm"






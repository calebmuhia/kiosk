-- MySQL dump 10.13  Distrib 5.5.38, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: kiosk
-- ------------------------------------------------------
-- Server version	5.5.38-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addressmodel_address`
--

DROP TABLE IF EXISTS `addressmodel_address`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressmodel_address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_shipping_id` int(11) DEFAULT NULL,
  `user_billing_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `zip_code` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `state` varchar(255) NOT NULL,
  `country_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_shipping_id` (`user_shipping_id`),
  UNIQUE KEY `user_billing_id` (`user_billing_id`),
  KEY `addressmodel_address_d860be3c` (`country_id`),
  CONSTRAINT `country_id_refs_id_990a2669` FOREIGN KEY (`country_id`) REFERENCES `addressmodel_country` (`id`),
  CONSTRAINT `user_billing_id_refs_id_357e5150` FOREIGN KEY (`user_billing_id`) REFERENCES `kioskui_kioskuser` (`id`),
  CONSTRAINT `user_shipping_id_refs_id_357e5150` FOREIGN KEY (`user_shipping_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addressmodel_address`
--

LOCK TABLES `addressmodel_address` WRITE;
/*!40000 ALTER TABLE `addressmodel_address` DISABLE KEYS */;
/*!40000 ALTER TABLE `addressmodel_address` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `addressmodel_country`
--

DROP TABLE IF EXISTS `addressmodel_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addressmodel_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addressmodel_country`
--

LOCK TABLES `addressmodel_country` WRITE;
/*!40000 ALTER TABLE `addressmodel_country` DISABLE KEYS */;
/*!40000 ALTER TABLE `addressmodel_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_5f412f9a` (`group_id`),
  KEY `auth_group_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `group_id_refs_id_f4b32aac` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `permission_id_refs_id_6ba0f519` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_d043b34a` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add supplier',1,'add_supplier'),(2,'Can change supplier',1,'change_supplier'),(3,'Can delete supplier',1,'delete_supplier'),(4,'Can add user',2,'add_kioskuser'),(5,'Can change user',2,'change_kioskuser'),(6,'Can delete user',2,'delete_kioskuser'),(7,'Can add user cash flow',3,'add_usercashflow'),(8,'Can change user cash flow',3,'change_usercashflow'),(9,'Can delete user cash flow',3,'delete_usercashflow'),(10,'Can add Products WITH barcodes',4,'add_barcodeproduct'),(11,'Can change Products WITH barcodes',4,'change_barcodeproduct'),(12,'Can delete Products WITH barcodes',4,'delete_barcodeproduct'),(13,'Can add Products WITHOUT barcodes',5,'add_nobarcodeproduct'),(14,'Can change Products WITHOUT barcodes',5,'change_nobarcodeproduct'),(15,'Can delete Products WITHOUT barcodes',5,'delete_nobarcodeproduct'),(16,'Can add Tax',6,'add_tax'),(17,'Can change Tax',6,'change_tax'),(18,'Can delete Tax',6,'delete_tax'),(19,'Can add missing barcode or keycard',7,'add_missingbarcodeorkeycard'),(20,'Can change missing barcode or keycard',7,'change_missingbarcodeorkeycard'),(21,'Can delete missing barcode or keycard',7,'delete_missingbarcodeorkeycard'),(22,'Can add cash box',8,'add_cashbox'),(23,'Can change cash box',8,'change_cashbox'),(24,'Can delete cash box',8,'delete_cashbox'),(25,'Can add Product Quantity Movement',9,'add_quantitymovement'),(26,'Can change Product Quantity Movement',9,'change_quantitymovement'),(27,'Can delete Product Quantity Movement',9,'delete_quantitymovement'),(28,'Can add Location Information',10,'add_locationinfo'),(29,'Can change Location Information',10,'change_locationinfo'),(30,'Can delete Location Information',10,'delete_locationinfo'),(31,'Can add Restock frequency',11,'add_restockfrequency'),(32,'Can change Restock frequency',11,'change_restockfrequency'),(33,'Can delete Restock frequency',11,'delete_restockfrequency'),(34,'Can add Category Fields',12,'add_extracategoryfields'),(35,'Can change Category Fields',12,'change_extracategoryfields'),(36,'Can delete Category Fields',12,'delete_extracategoryfields'),(37,'Can add Choices Place Holder',13,'add_choicesplaceholder'),(38,'Can change Choices Place Holder',13,'change_choicesplaceholder'),(39,'Can delete Choices Place Holder',13,'delete_choicesplaceholder'),(40,'Can add Restock Category Choice',14,'add_restockcategorychoice'),(41,'Can change Restock Category Choice',14,'change_restockcategorychoice'),(42,'Can delete Restock Category Choice',14,'delete_restockcategorychoice'),(43,'Can add restock inventory report',15,'add_restockinventoryreport'),(44,'Can change restock inventory report',15,'change_restockinventoryreport'),(45,'Can delete restock inventory report',15,'delete_restockinventoryreport'),(46,'Can add restock inventory reportitems',16,'add_restockinventoryreportitems'),(47,'Can change restock inventory reportitems',16,'change_restockinventoryreportitems'),(48,'Can delete restock inventory reportitems',16,'delete_restockinventoryreportitems'),(49,'Can add printing object',17,'add_printingobject'),(50,'Can change printing object',17,'change_printingobject'),(51,'Can delete printing object',17,'delete_printingobject'),(52,'Can add log entry',18,'add_logentry'),(53,'Can change log entry',18,'change_logentry'),(54,'Can delete log entry',18,'delete_logentry'),(55,'Can add permission',19,'add_permission'),(56,'Can change permission',19,'change_permission'),(57,'Can delete permission',19,'delete_permission'),(58,'Can add group',20,'add_group'),(59,'Can change group',20,'change_group'),(60,'Can delete group',20,'delete_group'),(61,'Can add content type',21,'add_contenttype'),(62,'Can change content type',21,'change_contenttype'),(63,'Can delete content type',21,'delete_contenttype'),(64,'Can add session',22,'add_session'),(65,'Can change session',22,'change_session'),(66,'Can delete session',22,'delete_session'),(67,'Can add Cart',23,'add_cart'),(68,'Can change Cart',23,'change_cart'),(69,'Can delete Cart',23,'delete_cart'),(70,'Can add Cart item',24,'add_cartitem'),(71,'Can change Cart item',24,'change_cartitem'),(72,'Can delete Cart item',24,'delete_cartitem'),(73,'Can add Product',25,'add_product'),(74,'Can change Product',25,'change_product'),(75,'Can delete Product',25,'delete_product'),(76,'Can add Order',26,'add_order'),(77,'Can change Order',26,'change_order'),(78,'Can delete Order',26,'delete_order'),(79,'Can add Order item',27,'add_orderitem'),(80,'Can change Order item',27,'change_orderitem'),(81,'Can delete Order item',27,'delete_orderitem'),(82,'Can add Order extra info',28,'add_orderextrainfo'),(83,'Can change Order extra info',28,'change_orderextrainfo'),(84,'Can delete Order extra info',28,'delete_orderextrainfo'),(85,'Can add Extra order price field',29,'add_extraorderpricefield'),(86,'Can change Extra order price field',29,'change_extraorderpricefield'),(87,'Can delete Extra order price field',29,'delete_extraorderpricefield'),(88,'Can add Extra order item price field',30,'add_extraorderitempricefield'),(89,'Can change Extra order item price field',30,'change_extraorderitempricefield'),(90,'Can delete Extra order item price field',30,'delete_extraorderitempricefield'),(91,'Can add Order payment',31,'add_orderpayment'),(92,'Can change Order payment',31,'change_orderpayment'),(93,'Can delete Order payment',31,'delete_orderpayment'),(94,'Can add migration history',32,'add_migrationhistory'),(95,'Can change migration history',32,'change_migrationhistory'),(96,'Can delete migration history',32,'delete_migrationhistory'),(97,'Can add Country',33,'add_country'),(98,'Can change Country',33,'change_country'),(99,'Can delete Country',33,'delete_country'),(100,'Can add Address',34,'add_address'),(101,'Can change Address',34,'change_address'),(102,'Can delete Address',34,'delete_address'),(103,'Can add category',35,'add_category'),(104,'Can change category',35,'change_category'),(105,'Can delete category',35,'delete_category'),(106,'Can add api access',36,'add_apiaccess'),(107,'Can change api access',36,'change_apiaccess'),(108,'Can delete api access',36,'delete_apiaccess'),(109,'Can add api key',37,'add_apikey'),(110,'Can change api key',37,'change_apikey'),(111,'Can delete api key',37,'delete_apikey'),(112,'Can add client',38,'add_client'),(113,'Can change client',38,'change_client'),(114,'Can delete client',38,'delete_client'),(115,'Can add grant',39,'add_grant'),(116,'Can change grant',39,'change_grant'),(117,'Can delete grant',39,'delete_grant'),(118,'Can add access token',40,'add_accesstoken'),(119,'Can change access token',40,'change_accesstoken'),(120,'Can delete access token',40,'delete_accesstoken'),(121,'Can add refresh token',41,'add_refreshtoken'),(122,'Can change refresh token',41,'change_refreshtoken'),(123,'Can delete refresh token',41,'delete_refreshtoken');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_taskmeta`
--

DROP TABLE IF EXISTS `celery_taskmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_taskmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL,
  `result` longtext,
  `date_done` datetime NOT NULL,
  `traceback` longtext,
  `hidden` tinyint(1) NOT NULL,
  `meta` longtext,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `celery_taskmeta_2ff6b945` (`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_taskmeta`
--

LOCK TABLES `celery_taskmeta` WRITE;
/*!40000 ALTER TABLE `celery_taskmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `celery_taskmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `celery_tasksetmeta`
--

DROP TABLE IF EXISTS `celery_tasksetmeta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `celery_tasksetmeta` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskset_id` varchar(255) NOT NULL,
  `result` longtext NOT NULL,
  `date_done` datetime NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `taskset_id` (`taskset_id`),
  KEY `celery_tasksetmeta_2ff6b945` (`hidden`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `celery_tasksetmeta`
--

LOCK TABLES `celery_tasksetmeta` WRITE;
/*!40000 ALTER TABLE `celery_tasksetmeta` DISABLE KEYS */;
/*!40000 ALTER TABLE `celery_tasksetmeta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_6340c63c` (`user_id`),
  KEY `django_admin_log_37ef4eb4` (`content_type_id`),
  CONSTRAINT `content_type_id_refs_id_93d2d1f8` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `user_id_refs_id_1b35dfe9` FOREIGN KEY (`user_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `app_label` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'supplier','kioskui','supplier'),(2,'user','kioskui','kioskuser'),(3,'user cash flow','kioskui','usercashflow'),(4,'Products WITH barcodes','kioskui','barcodeproduct'),(5,'Products WITHOUT barcodes','kioskui','nobarcodeproduct'),(6,'Tax','kioskui','tax'),(7,'missing barcode or keycard','kioskui','missingbarcodeorkeycard'),(8,'cash box','kioskui','cashbox'),(9,'Product Quantity Movement','kioskui','quantitymovement'),(10,'Location Information','kioskui','locationinfo'),(11,'Restock frequency','kioskui','restockfrequency'),(12,'Category Fields','kioskui','extracategoryfields'),(13,'Choices Place Holder','kioskui','choicesplaceholder'),(14,'Restock Category Choice','kioskui','restockcategorychoice'),(15,'restock inventory report','kioskui','restockinventoryreport'),(16,'restock inventory reportitems','kioskui','restockinventoryreportitems'),(17,'printing object','kioskui','printingobject'),(18,'log entry','admin','logentry'),(19,'permission','auth','permission'),(20,'group','auth','group'),(21,'content type','contenttypes','contenttype'),(22,'session','sessions','session'),(23,'Cart','shop','cart'),(24,'Cart item','shop','cartitem'),(25,'Product','shop','product'),(26,'Order','shop','order'),(27,'Order item','shop','orderitem'),(28,'Order extra info','shop','orderextrainfo'),(29,'Extra order price field','shop','extraorderpricefield'),(30,'Extra order item price field','shop','extraorderitempricefield'),(31,'Order payment','shop','orderpayment'),(32,'migration history','south','migrationhistory'),(33,'Country','addressmodel','country'),(34,'Address','addressmodel','address'),(35,'category','shop_simplecategories','category'),(36,'api access','tastypie','apiaccess'),(37,'api key','tastypie','apikey'),(38,'client','oauth2','client'),(39,'grant','oauth2','grant'),(40,'access token','oauth2','accesstoken'),(41,'refresh token','oauth2','refreshtoken');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_b7b81f0c` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_crontabschedule`
--

DROP TABLE IF EXISTS `djcelery_crontabschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_crontabschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `minute` varchar(64) NOT NULL,
  `hour` varchar(64) NOT NULL,
  `day_of_week` varchar(64) NOT NULL,
  `day_of_month` varchar(64) NOT NULL,
  `month_of_year` varchar(64) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_crontabschedule`
--

LOCK TABLES `djcelery_crontabschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_crontabschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_crontabschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_intervalschedule`
--

DROP TABLE IF EXISTS `djcelery_intervalschedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_intervalschedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `every` int(11) NOT NULL,
  `period` varchar(24) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_intervalschedule`
--

LOCK TABLES `djcelery_intervalschedule` WRITE;
/*!40000 ALTER TABLE `djcelery_intervalschedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_intervalschedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictask`
--

DROP TABLE IF EXISTS `djcelery_periodictask`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictask` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `task` varchar(200) NOT NULL,
  `interval_id` int(11) DEFAULT NULL,
  `crontab_id` int(11) DEFAULT NULL,
  `args` longtext NOT NULL,
  `kwargs` longtext NOT NULL,
  `queue` varchar(200) DEFAULT NULL,
  `exchange` varchar(200) DEFAULT NULL,
  `routing_key` varchar(200) DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `last_run_at` datetime DEFAULT NULL,
  `total_run_count` int(10) unsigned NOT NULL,
  `date_changed` datetime NOT NULL,
  `description` longtext NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `djcelery_periodictask_8905f60d` (`interval_id`),
  KEY `djcelery_periodictask_7280124f` (`crontab_id`),
  CONSTRAINT `crontab_id_refs_id_286da0d1` FOREIGN KEY (`crontab_id`) REFERENCES `djcelery_crontabschedule` (`id`),
  CONSTRAINT `interval_id_refs_id_1829f358` FOREIGN KEY (`interval_id`) REFERENCES `djcelery_intervalschedule` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictask`
--

LOCK TABLES `djcelery_periodictask` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictask` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictask` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_periodictasks`
--

DROP TABLE IF EXISTS `djcelery_periodictasks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_periodictasks` (
  `ident` smallint(6) NOT NULL,
  `last_update` datetime NOT NULL,
  PRIMARY KEY (`ident`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_periodictasks`
--

LOCK TABLES `djcelery_periodictasks` WRITE;
/*!40000 ALTER TABLE `djcelery_periodictasks` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_periodictasks` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_taskstate`
--

DROP TABLE IF EXISTS `djcelery_taskstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_taskstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(64) NOT NULL,
  `task_id` varchar(36) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `tstamp` datetime NOT NULL,
  `args` longtext,
  `kwargs` longtext,
  `eta` datetime DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  `result` longtext,
  `traceback` longtext,
  `runtime` double DEFAULT NULL,
  `retries` int(11) NOT NULL,
  `worker_id` int(11) DEFAULT NULL,
  `hidden` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_id` (`task_id`),
  KEY `djcelery_taskstate_5654bf12` (`state`),
  KEY `djcelery_taskstate_4da47e07` (`name`),
  KEY `djcelery_taskstate_abaacd02` (`tstamp`),
  KEY `djcelery_taskstate_cac6a03d` (`worker_id`),
  KEY `djcelery_taskstate_2ff6b945` (`hidden`),
  CONSTRAINT `worker_id_refs_id_6fd8ce95` FOREIGN KEY (`worker_id`) REFERENCES `djcelery_workerstate` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_taskstate`
--

LOCK TABLES `djcelery_taskstate` WRITE;
/*!40000 ALTER TABLE `djcelery_taskstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_taskstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `djcelery_workerstate`
--

DROP TABLE IF EXISTS `djcelery_workerstate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `djcelery_workerstate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hostname` varchar(255) NOT NULL,
  `last_heartbeat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  KEY `djcelery_workerstate_11e400ef` (`last_heartbeat`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `djcelery_workerstate`
--

LOCK TABLES `djcelery_workerstate` WRITE;
/*!40000 ALTER TABLE `djcelery_workerstate` DISABLE KEYS */;
/*!40000 ALTER TABLE `djcelery_workerstate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_barcodeproduct`
--

DROP TABLE IF EXISTS `kioskui_barcodeproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_barcodeproduct` (
  `product_ptr_id` int(11) NOT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `barcode` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `unit_cost` double NOT NULL,
  `case_upc` varchar(255) DEFAULT NULL,
  `case_qty` int(11) NOT NULL,
  `min_stock` int(11) DEFAULT NULL,
  `max_stock` int(11) DEFAULT NULL,
  `demand` int(11) DEFAULT NULL,
  `demand_two` int(11) DEFAULT NULL,
  `lead_time` double DEFAULT NULL,
  `bottle_deposit` tinyint(1) NOT NULL,
  `pst` tinyint(1) NOT NULL,
  `hst` tinyint(1) NOT NULL,
  PRIMARY KEY (`product_ptr_id`),
  UNIQUE KEY `barcode` (`barcode`),
  KEY `kioskui_barcodeproduct_272520ac` (`supplier_id`),
  CONSTRAINT `product_ptr_id_refs_id_7c33b541` FOREIGN KEY (`product_ptr_id`) REFERENCES `shop_product` (`id`),
  CONSTRAINT `supplier_id_refs_id_9fa74a3d` FOREIGN KEY (`supplier_id`) REFERENCES `kioskui_supplier` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_barcodeproduct`
--

LOCK TABLES `kioskui_barcodeproduct` WRITE;
/*!40000 ALTER TABLE `kioskui_barcodeproduct` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_barcodeproduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_cashbox`
--

DROP TABLE IF EXISTS `kioskui_cashbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_cashbox` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `amount` decimal(30,2) NOT NULL,
  `emptied` tinyint(1) NOT NULL,
  `created` datetime DEFAULT NULL,
  `emptied_at` datetime DEFAULT NULL,
  `emptied_by_id` int(11) DEFAULT NULL,
  `extra_info` longtext,
  PRIMARY KEY (`id`),
  KEY `kioskui_cashbox_d57af92a` (`emptied_by_id`),
  CONSTRAINT `emptied_by_id_refs_id_14221b7c` FOREIGN KEY (`emptied_by_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_cashbox`
--

LOCK TABLES `kioskui_cashbox` WRITE;
/*!40000 ALTER TABLE `kioskui_cashbox` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_cashbox` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_choicesplaceholder`
--

DROP TABLE IF EXISTS `kioskui_choicesplaceholder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_choicesplaceholder` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `slug` varchar(254) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `schedule` tinyint(1) NOT NULL,
  `scheduled_date` datetime,
  `email` varchar(75),
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_choicesplaceholder`
--

LOCK TABLES `kioskui_choicesplaceholder` WRITE;
/*!40000 ALTER TABLE `kioskui_choicesplaceholder` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_choicesplaceholder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_extracategoryfields`
--

DROP TABLE IF EXISTS `kioskui_extracategoryfields`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_extracategoryfields` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `days_of_product` int(11) NOT NULL,
  `hard_max` int(11) DEFAULT NULL,
  `round_to_case` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_id` (`category_id`),
  CONSTRAINT `category_id_refs_id_152b3833` FOREIGN KEY (`category_id`) REFERENCES `shop_simplecategories_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_extracategoryfields`
--

LOCK TABLES `kioskui_extracategoryfields` WRITE;
/*!40000 ALTER TABLE `kioskui_extracategoryfields` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_extracategoryfields` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_kioskuser`
--

DROP TABLE IF EXISTS `kioskui_kioskuser`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_kioskuser` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  `location` varchar(255) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL,
  `cash_deposits` decimal(30,2) NOT NULL,
  `credit_card_deposits` decimal(30,2) NOT NULL,
  `debit_card_deposits` decimal(30,2) NOT NULL,
  `account_expenses` decimal(30,2) NOT NULL,
  `credit_card_expenses` decimal(30,2) NOT NULL,
  `debit_card_expenses` decimal(30,2) NOT NULL,
  `account_pin` int(11) NOT NULL,
  `account_id` varchar(100) NOT NULL,
  `is_cashbox_manager` tinyint(1) NOT NULL,
  `admin_deposits` decimal(30,2) NOT NULL,
  `admin_deducts` decimal(30,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_kioskuser`
--

LOCK TABLES `kioskui_kioskuser` WRITE;
/*!40000 ALTER TABLE `kioskui_kioskuser` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_kioskuser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_kioskuser_groups`
--

DROP TABLE IF EXISTS `kioskui_kioskuser_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_kioskuser_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kioskuser_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kioskuser_id` (`kioskuser_id`,`group_id`),
  KEY `kioskui_kioskuser_groups_bb400414` (`kioskuser_id`),
  KEY `kioskui_kioskuser_groups_5f412f9a` (`group_id`),
  CONSTRAINT `group_id_refs_id_fd5869a7` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `kioskuser_id_refs_id_d575e1cb` FOREIGN KEY (`kioskuser_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_kioskuser_groups`
--

LOCK TABLES `kioskui_kioskuser_groups` WRITE;
/*!40000 ALTER TABLE `kioskui_kioskuser_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_kioskuser_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_kioskuser_user_permissions`
--

DROP TABLE IF EXISTS `kioskui_kioskuser_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_kioskuser_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kioskuser_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `kioskuser_id` (`kioskuser_id`,`permission_id`),
  KEY `kioskui_kioskuser_user_permissions_bb400414` (`kioskuser_id`),
  KEY `kioskui_kioskuser_user_permissions_83d7f98b` (`permission_id`),
  CONSTRAINT `kioskuser_id_refs_id_ff63cc97` FOREIGN KEY (`kioskuser_id`) REFERENCES `kioskui_kioskuser` (`id`),
  CONSTRAINT `permission_id_refs_id_b4f10cbf` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_kioskuser_user_permissions`
--

LOCK TABLES `kioskui_kioskuser_user_permissions` WRITE;
/*!40000 ALTER TABLE `kioskui_kioskuser_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_kioskuser_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_locationinfo`
--

DROP TABLE IF EXISTS `kioskui_locationinfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_locationinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `location_name` varchar(255) NOT NULL,
  `address` varchar(255),
  `contact_name` varchar(255),
  `contact_number` varchar(255),
  `notes` longtext,
  `weekday_from` int(11),
  `weekday_to` int(11),
  `from_hour` int(11),
  `to_hour` int(11),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_locationinfo`
--

LOCK TABLES `kioskui_locationinfo` WRITE;
/*!40000 ALTER TABLE `kioskui_locationinfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_locationinfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_missingbarcodeorkeycard`
--

DROP TABLE IF EXISTS `kioskui_missingbarcodeorkeycard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_missingbarcodeorkeycard` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `barcode` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_missingbarcodeorkeycard`
--

LOCK TABLES `kioskui_missingbarcodeorkeycard` WRITE;
/*!40000 ALTER TABLE `kioskui_missingbarcodeorkeycard` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_missingbarcodeorkeycard` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_nobarcodeproduct`
--

DROP TABLE IF EXISTS `kioskui_nobarcodeproduct`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_nobarcodeproduct` (
  `product_ptr_id` int(11) NOT NULL,
  `image` varchar(100) NOT NULL,
  `quantity` int(11) NOT NULL,
  `ordering` int(11) NOT NULL,
  `unit_cost` double NOT NULL,
  `pst` tinyint(1) NOT NULL,
  `hst` tinyint(1) NOT NULL,
  PRIMARY KEY (`product_ptr_id`),
  CONSTRAINT `product_ptr_id_refs_id_efaaab43` FOREIGN KEY (`product_ptr_id`) REFERENCES `shop_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_nobarcodeproduct`
--

LOCK TABLES `kioskui_nobarcodeproduct` WRITE;
/*!40000 ALTER TABLE `kioskui_nobarcodeproduct` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_nobarcodeproduct` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_printingobject`
--

DROP TABLE IF EXISTS `kioskui_printingobject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_printingobject` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_printingobject`
--

LOCK TABLES `kioskui_printingobject` WRITE;
/*!40000 ALTER TABLE `kioskui_printingobject` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_printingobject` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_quantitymovement`
--

DROP TABLE IF EXISTS `kioskui_quantitymovement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_quantitymovement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(100) NOT NULL,
  `product_barcode` varchar(100) DEFAULT NULL,
  `pre_quantity` varchar(100) NOT NULL,
  `post_quantity` varchar(100) NOT NULL,
  `amount` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  `status` int(11) NOT NULL,
  `slug` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kioskui_quantitymovement_f52cfca0` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_quantitymovement`
--

LOCK TABLES `kioskui_quantitymovement` WRITE;
/*!40000 ALTER TABLE `kioskui_quantitymovement` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_quantitymovement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_restockcategorychoice`
--

DROP TABLE IF EXISTS `kioskui_restockcategorychoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_restockcategorychoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `place_holder_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kioskui_restockcategorychoice_6363a991` (`place_holder_id`),
  KEY `kioskui_restockcategorychoice_6f33f001` (`category_id`),
  CONSTRAINT `category_id_refs_id_91ae9d16` FOREIGN KEY (`category_id`) REFERENCES `shop_simplecategories_category` (`id`),
  CONSTRAINT `place_holder_id_refs_id_579e0532` FOREIGN KEY (`place_holder_id`) REFERENCES `kioskui_choicesplaceholder` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_restockcategorychoice`
--

LOCK TABLES `kioskui_restockcategorychoice` WRITE;
/*!40000 ALTER TABLE `kioskui_restockcategorychoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_restockcategorychoice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_restockfrequency`
--

DROP TABLE IF EXISTS `kioskui_restockfrequency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_restockfrequency` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restock_frequency` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_restockfrequency`
--

LOCK TABLES `kioskui_restockfrequency` WRITE;
/*!40000 ALTER TABLE `kioskui_restockfrequency` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_restockfrequency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_restockinventoryreport`
--

DROP TABLE IF EXISTS `kioskui_restockinventoryreport`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_restockinventoryreport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(256) NOT NULL,
  `created` datetime NOT NULL,
  `prekitting_completed` tinyint(1) NOT NULL,
  `isUpdated` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_restockinventoryreport`
--

LOCK TABLES `kioskui_restockinventoryreport` WRITE;
/*!40000 ALTER TABLE `kioskui_restockinventoryreport` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_restockinventoryreport` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_restockinventoryreportitems`
--

DROP TABLE IF EXISTS `kioskui_restockinventoryreportitems`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_restockinventoryreportitems` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_name` varchar(256) NOT NULL,
  `upc` varchar(255) NOT NULL,
  `proposed_restock_quantity` int(11) NOT NULL,
  `categories` varchar(100) NOT NULL,
  `report_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `kioskui_restockinventoryreportitems_ae85b46e` (`report_id`),
  CONSTRAINT `report_id_refs_id_8b6d9d67` FOREIGN KEY (`report_id`) REFERENCES `kioskui_restockinventoryreport` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_restockinventoryreportitems`
--

LOCK TABLES `kioskui_restockinventoryreportitems` WRITE;
/*!40000 ALTER TABLE `kioskui_restockinventoryreportitems` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_restockinventoryreportitems` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_supplier`
--

DROP TABLE IF EXISTS `kioskui_supplier`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_supplier`
--

LOCK TABLES `kioskui_supplier` WRITE;
/*!40000 ALTER TABLE `kioskui_supplier` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_supplier` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_tax`
--

DROP TABLE IF EXISTS `kioskui_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_tax` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_tax`
--

LOCK TABLES `kioskui_tax` WRITE;
/*!40000 ALTER TABLE `kioskui_tax` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_tax` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kioskui_usercashflow`
--

DROP TABLE IF EXISTS `kioskui_usercashflow`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kioskui_usercashflow` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `pre` decimal(30,2) DEFAULT NULL,
  `post` decimal(30,2) DEFAULT NULL,
  `date` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `amount` decimal(30,2) NOT NULL,
  `extra_field` varchar(256),
  `description` longtext,
  `order_id` varchar(10),
  PRIMARY KEY (`id`),
  KEY `kioskui_usercashflow_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_6a6bbdfc` FOREIGN KEY (`user_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kioskui_usercashflow`
--

LOCK TABLES `kioskui_usercashflow` WRITE;
/*!40000 ALTER TABLE `kioskui_usercashflow` DISABLE KEYS */;
/*!40000 ALTER TABLE `kioskui_usercashflow` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2_accesstoken`
--

DROP TABLE IF EXISTS `oauth2_accesstoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_accesstoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `client_id` int(11) NOT NULL,
  `expires` datetime NOT NULL,
  `scope` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth2_accesstoken_6340c63c` (`user_id`),
  KEY `oauth2_accesstoken_4fea5d6a` (`client_id`),
  KEY `oauth2_accesstoken_e0a0c5a7` (`token`),
  CONSTRAINT `client_id_refs_id_dffc817d` FOREIGN KEY (`client_id`) REFERENCES `oauth2_client` (`id`),
  CONSTRAINT `user_id_refs_id_b4d62186` FOREIGN KEY (`user_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2_accesstoken`
--

LOCK TABLES `oauth2_accesstoken` WRITE;
/*!40000 ALTER TABLE `oauth2_accesstoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2_accesstoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2_client`
--

DROP TABLE IF EXISTS `oauth2_client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11),
  `url` varchar(200) NOT NULL,
  `redirect_uri` varchar(200) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `client_secret` varchar(255) NOT NULL,
  `client_type` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth2_client_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_fbb4cc3f` FOREIGN KEY (`user_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2_client`
--

LOCK TABLES `oauth2_client` WRITE;
/*!40000 ALTER TABLE `oauth2_client` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2_client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2_grant`
--

DROP TABLE IF EXISTS `oauth2_grant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_grant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL,
  `expires` datetime NOT NULL,
  `redirect_uri` varchar(255) NOT NULL,
  `scope` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth2_grant_6340c63c` (`user_id`),
  KEY `oauth2_grant_4fea5d6a` (`client_id`),
  CONSTRAINT `client_id_refs_id_098c2f19` FOREIGN KEY (`client_id`) REFERENCES `oauth2_client` (`id`),
  CONSTRAINT `user_id_refs_id_d64eacab` FOREIGN KEY (`user_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2_grant`
--

LOCK TABLES `oauth2_grant` WRITE;
/*!40000 ALTER TABLE `oauth2_grant` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2_grant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oauth2_refreshtoken`
--

DROP TABLE IF EXISTS `oauth2_refreshtoken`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oauth2_refreshtoken` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `token` varchar(255) NOT NULL,
  `access_token_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `expired` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `access_token_id` (`access_token_id`),
  KEY `oauth2_refreshtoken_6340c63c` (`user_id`),
  KEY `oauth2_refreshtoken_4fea5d6a` (`client_id`),
  CONSTRAINT `access_token_id_refs_id_b5577697` FOREIGN KEY (`access_token_id`) REFERENCES `oauth2_accesstoken` (`id`),
  CONSTRAINT `client_id_refs_id_3730d4ce` FOREIGN KEY (`client_id`) REFERENCES `oauth2_client` (`id`),
  CONSTRAINT `user_id_refs_id_585b8038` FOREIGN KEY (`user_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oauth2_refreshtoken`
--

LOCK TABLES `oauth2_refreshtoken` WRITE;
/*!40000 ALTER TABLE `oauth2_refreshtoken` DISABLE KEYS */;
/*!40000 ALTER TABLE `oauth2_refreshtoken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_cart`
--

DROP TABLE IF EXISTS `shop_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `date_created` datetime NOT NULL,
  `last_updated` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  CONSTRAINT `user_id_refs_id_bfc2bc40` FOREIGN KEY (`user_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_cart`
--

LOCK TABLES `shop_cart` WRITE;
/*!40000 ALTER TABLE `shop_cart` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_cartitem`
--

DROP TABLE IF EXISTS `shop_cartitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_cartitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cart_id` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shop_cartitem_8a7b7230` (`cart_id`),
  KEY `shop_cartitem_7f1b40ad` (`product_id`),
  CONSTRAINT `cart_id_refs_id_2335207e` FOREIGN KEY (`cart_id`) REFERENCES `shop_cart` (`id`),
  CONSTRAINT `product_id_refs_id_fd98e281` FOREIGN KEY (`product_id`) REFERENCES `shop_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_cartitem`
--

LOCK TABLES `shop_cartitem` WRITE;
/*!40000 ALTER TABLE `shop_cartitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_cartitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_extraorderitempricefield`
--

DROP TABLE IF EXISTS `shop_extraorderitempricefield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_extraorderitempricefield` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_item_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `value` decimal(30,2) NOT NULL,
  `data` longtext,
  PRIMARY KEY (`id`),
  KEY `shop_extraorderitempricefield_f7db0357` (`order_item_id`),
  CONSTRAINT `order_item_id_refs_id_ce77bcad` FOREIGN KEY (`order_item_id`) REFERENCES `shop_orderitem` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_extraorderitempricefield`
--

LOCK TABLES `shop_extraorderitempricefield` WRITE;
/*!40000 ALTER TABLE `shop_extraorderitempricefield` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_extraorderitempricefield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_extraorderpricefield`
--

DROP TABLE IF EXISTS `shop_extraorderpricefield`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_extraorderpricefield` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `value` decimal(30,2) NOT NULL,
  `data` longtext,
  `is_shipping` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shop_extraorderpricefield_68d25c7a` (`order_id`),
  CONSTRAINT `order_id_refs_id_b455b649` FOREIGN KEY (`order_id`) REFERENCES `shop_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_extraorderpricefield`
--

LOCK TABLES `shop_extraorderpricefield` WRITE;
/*!40000 ALTER TABLE `shop_extraorderpricefield` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_extraorderpricefield` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_order`
--

DROP TABLE IF EXISTS `shop_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `order_subtotal` decimal(30,2) NOT NULL,
  `order_total` decimal(30,2) NOT NULL,
  `shipping_address_text` longtext,
  `billing_address_text` longtext,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `cart_pk` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `shop_order_6340c63c` (`user_id`),
  CONSTRAINT `user_id_refs_id_f58b5c8b` FOREIGN KEY (`user_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_order`
--

LOCK TABLES `shop_order` WRITE;
/*!40000 ALTER TABLE `shop_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_orderextrainfo`
--

DROP TABLE IF EXISTS `shop_orderextrainfo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderextrainfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `text` longtext NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shop_orderextrainfo_68d25c7a` (`order_id`),
  CONSTRAINT `order_id_refs_id_7a284893` FOREIGN KEY (`order_id`) REFERENCES `shop_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_orderextrainfo`
--

LOCK TABLES `shop_orderextrainfo` WRITE;
/*!40000 ALTER TABLE `shop_orderextrainfo` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_orderextrainfo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_orderitem`
--

DROP TABLE IF EXISTS `shop_orderitem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `product_reference` varchar(255) NOT NULL,
  `product_name` varchar(255) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `unit_price` decimal(30,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `line_subtotal` decimal(30,2) NOT NULL,
  `line_total` decimal(30,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shop_orderitem_68d25c7a` (`order_id`),
  KEY `shop_orderitem_7f1b40ad` (`product_id`),
  CONSTRAINT `order_id_refs_id_53b51ae1` FOREIGN KEY (`order_id`) REFERENCES `shop_order` (`id`),
  CONSTRAINT `product_id_refs_id_5e350bc1` FOREIGN KEY (`product_id`) REFERENCES `shop_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_orderitem`
--

LOCK TABLES `shop_orderitem` WRITE;
/*!40000 ALTER TABLE `shop_orderitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_orderitem` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_orderpayment`
--

DROP TABLE IF EXISTS `shop_orderpayment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_orderpayment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) NOT NULL,
  `amount` decimal(30,2) NOT NULL,
  `transaction_id` varchar(255) NOT NULL,
  `payment_method` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shop_orderpayment_68d25c7a` (`order_id`),
  CONSTRAINT `order_id_refs_id_a2292bb5` FOREIGN KEY (`order_id`) REFERENCES `shop_order` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_orderpayment`
--

LOCK TABLES `shop_orderpayment` WRITE;
/*!40000 ALTER TABLE `shop_orderpayment` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_orderpayment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_product`
--

DROP TABLE IF EXISTS `shop_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `polymorphic_ctype_id` int(11) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL,
  `last_modified` datetime NOT NULL,
  `unit_price` decimal(30,2) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`),
  KEY `shop_product_0022f76a` (`polymorphic_ctype_id`),
  CONSTRAINT `polymorphic_ctype_id_refs_id_9520ea5e` FOREIGN KEY (`polymorphic_ctype_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_product`
--

LOCK TABLES `shop_product` WRITE;
/*!40000 ALTER TABLE `shop_product` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_simplecategories_category`
--

DROP TABLE IF EXISTS `shop_simplecategories_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_simplecategories_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `slug` varchar(50) NOT NULL,
  `parent_category_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `shop_simplecategories_category_f52cfca0` (`slug`),
  KEY `shop_simplecategories_category_5639c68d` (`parent_category_id`),
  CONSTRAINT `parent_category_id_refs_id_2ff9ae81` FOREIGN KEY (`parent_category_id`) REFERENCES `shop_simplecategories_category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_simplecategories_category`
--

LOCK TABLES `shop_simplecategories_category` WRITE;
/*!40000 ALTER TABLE `shop_simplecategories_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_simplecategories_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shop_simplecategories_category_products`
--

DROP TABLE IF EXISTS `shop_simplecategories_category_products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shop_simplecategories_category_products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `shop_simplecategories_categor_category_id_44f11e07a4d5c13c_uniq` (`category_id`,`product_id`),
  KEY `shop_simplecategories_category_products_6f33f001` (`category_id`),
  KEY `shop_simplecategories_category_products_7f1b40ad` (`product_id`),
  CONSTRAINT `category_id_refs_id_e05371aa` FOREIGN KEY (`category_id`) REFERENCES `shop_simplecategories_category` (`id`),
  CONSTRAINT `product_id_refs_id_0de15982` FOREIGN KEY (`product_id`) REFERENCES `shop_product` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shop_simplecategories_category_products`
--

LOCK TABLES `shop_simplecategories_category_products` WRITE;
/*!40000 ALTER TABLE `shop_simplecategories_category_products` DISABLE KEYS */;
/*!40000 ALTER TABLE `shop_simplecategories_category_products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `south_migrationhistory`
--

DROP TABLE IF EXISTS `south_migrationhistory`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `south_migrationhistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_name` varchar(255) NOT NULL,
  `migration` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `south_migrationhistory`
--

LOCK TABLES `south_migrationhistory` WRITE;
/*!40000 ALTER TABLE `south_migrationhistory` DISABLE KEYS */;
/*!40000 ALTER TABLE `south_migrationhistory` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tastypie_apiaccess`
--

DROP TABLE IF EXISTS `tastypie_apiaccess`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tastypie_apiaccess` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `identifier` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `request_method` varchar(10) NOT NULL,
  `accessed` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tastypie_apiaccess`
--

LOCK TABLES `tastypie_apiaccess` WRITE;
/*!40000 ALTER TABLE `tastypie_apiaccess` DISABLE KEYS */;
/*!40000 ALTER TABLE `tastypie_apiaccess` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tastypie_apikey`
--

DROP TABLE IF EXISTS `tastypie_apikey`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tastypie_apikey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `key` varchar(128) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `tastypie_apikey_c0d4be93` (`key`),
  CONSTRAINT `user_id_refs_id_a20fa439` FOREIGN KEY (`user_id`) REFERENCES `kioskui_kioskuser` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tastypie_apikey`
--

LOCK TABLES `tastypie_apikey` WRITE;
/*!40000 ALTER TABLE `tastypie_apikey` DISABLE KEYS */;
/*!40000 ALTER TABLE `tastypie_apikey` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-30 17:18:16

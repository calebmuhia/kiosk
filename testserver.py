import SocketServer
import time
import random


data = """<?xml version="1.0" standalone="yes" ?>
<PLResponse>
<Result>APPROVED</Result>
<Authorization>OK7855</Authorization>
<RecNum>011001</RecNum>
<RefData>140904000021</RefData>
<ErrorCode>0000</ErrorCode>
<AuthAmt>10.00</AuthAmt>
<InputMethod>Manual</InputMethod>
<CardNumber>0010</CardNumber>
<CardType>Visa</CardType>
<CardInfo>6C5BEF2D01293F678EB5671E70D30E045D826FB021F0AC031714B89A256742AC5E7E5273A760AE46A204907F767752E5335B2CD3E261DF85C0D8F56F3E5B0136</CardInfo>
<TransactionDate>140904</TransactionDate>
<TransactionTime>140951</TransactionTime>
<Command>CCSALE</Command>
<ResultText>APPROVAL        </ResultText>
<MerchantId>000082025020017</MerchantId>
<TerminalId>01244971</TerminalId>
<Id>1000</Id>
<ClientId>01</ClientId>
<Receipt>
<Receipt1>                                      </Receipt1>
<Receipt2>MERCHANT #       000082025020017      </Receipt2>
<Receipt3>TERM #           01244971             </Receipt3>
<Receipt4>TYPE             PURCHASE             </Receipt4>
<Receipt5>ACCOUNT TYPE     Visa                 </Receipt5>
<Receipt6>CARD NUMBER      ************0010     </Receipt6>
<Receipt7>DATE/TIME        14/09/04 14:09:51    </Receipt7>
<Receipt8>REC #            011001               </Receipt8>
<Receipt9>INVOICE          1000                 </Receipt9>
<Receipt10>REFERENCE #      140904000021 M       </Receipt10>
<Receipt11>SEQUENCE #       000021               </Receipt11>
<Receipt12>AUTHOR. #        OK7855               </Receipt12>
<Receipt13>AMOUNT           $10.00               </Receipt13>
<Receipt14>                 --------------       </Receipt14>
<Receipt15>TOTAL            CAD $10.00           </Receipt15>
<Receipt16>                 --------------       </Receipt16>
<Receipt17>                                      </Receipt17>
<Receipt18>APPROVED - THANK YOU                  </Receipt18>
<Receipt19>00                                    </Receipt19>
<Receipt20>                                      </Receipt20>
<Receipt21>IMPORTANT -- retain this copy for your</Receipt21>
<Receipt22>records.                              </Receipt22>
<Receipt23>                                      </Receipt23>
<Receipt24>MERCHANT COPY                         </Receipt24>
<Receipt25>                                      </Receipt25>
<Receipt26>                                      </Receipt26>
</Receipt>
<ReceiptCustomer>
<Receipt1>                                      </Receipt1>
<Receipt2>MERCHANT #       000082025020017      </Receipt2>
<Receipt3>TERM #           01244971             </Receipt3>
<Receipt4>TYPE             PURCHASE             </Receipt4>
<Receipt5>ACCOUNT TYPE     Visa                 </Receipt5>
<Receipt6>CARD NUMBER      ************0010     </Receipt6>
<Receipt7>DATE/TIME        14/09/04 14:09:51    </Receipt7>
<Receipt8>REC #            011001               </Receipt8>
<Receipt9>INVOICE          1000                 </Receipt9>
<Receipt10>REFERENCE #      140904000021 M       </Receipt10>
<Receipt11>SEQUENCE #       000021               </Receipt11>
<Receipt12>AUTHOR. #        OK7855               </Receipt12>
<Receipt13>AMOUNT           $10.00               </Receipt13>
<Receipt14>                 --------------       </Receipt14>
<Receipt15>TOTAL            CAD $10.00           </Receipt15>
<Receipt16>                 --------------       </Receipt16>
<Receipt17>                                      </Receipt17>
<Receipt18>APPROVED - THANK YOU                  </Receipt18>
<Receipt19>00                                    </Receipt19>
<Receipt20>                                      </Receipt20>
<Receipt21>IMPORTANT -- retain this copy for your</Receipt21>
<Receipt22>records.                              </Receipt22>
<Receipt23>                                      </Receipt23>
<Receipt24>CUSTOMER COPY                         </Receipt24>
<Receipt25>                                      </Receipt25>
<Receipt26>                                      </Receipt26>
</ReceiptCustomer>
</PLResponse>"""

data2="""<?xml version="1.0" standalone="yes" ?>
<PLResponse>
<Result>DECLINED</Result>
<ErrorCode>0001</ErrorCode>
<InputMethod>Chip</InputMethod>
<CardNumber>7870</CardNumber>
<CardType>Debit</CardType>
<CardInfo>84E8AAC3E69D96768A9873C043994DA98BA8633DE2F35B8F32C006936415226A7C1A83AAB8D731916107F5748A560101E3F37579F5E52BF199A5465C28789A30</CardInfo>
<TransactionDate>140904</TransactionDate>
<TransactionTime>130805</TransactionTime>
<Command>CCSALE</Command>
<ResultText>CHIP DECLINED</ResultText>
<MerchantId>000082025020017</MerchantId>
<TerminalId>01244971</TerminalId>
<Id>1000</Id>
<ClientId>01</ClientId>
<Receipt>
<Receipt1>                                      </Receipt1>
<Receipt2>MERCHANT #       000082025020017      </Receipt2>
<Receipt3>TERM #           01244971             </Receipt3>
<Receipt4>TYPE             PURCHASE             </Receipt4>
<Receipt5>ACCOUNT TYPE     INTERAC CHEQUING     </Receipt5>
<Receipt6>CARD NUMBER      ***************7870  </Receipt6>
<Receipt7>DATE/TIME        14/09/04 13:08:05    </Receipt7>
<Receipt8>REC #                                 </Receipt8>
<Receipt9>INVOICE          1000                 </Receipt9>
<Receipt10>REFERENCE #      000071000011 C       </Receipt10>
<Receipt11>SEQUENCE #       000011               </Receipt11>
<Receipt12>AMOUNT           $0.10                </Receipt12>
<Receipt13>                 --------------       </Receipt13>
<Receipt14>TOTAL            CAD $0.10            </Receipt14>
<Receipt15>                 --------------       </Receipt15>
<Receipt16>APP              INTERAC              </Receipt16>
<Receipt17>AID              A0000002771010       </Receipt17>
<Receipt18>TVR              4000008040           </Receipt18>
<Receipt19>[9F0D] FCF8FCF8F0                     </Receipt19>
<Receipt20>[9F0E] 0000180000                     </Receipt20>
<Receipt21>[9F0F] FC78E4F880                     </Receipt21>
<Receipt22>[ 5A ] ************7870               </Receipt22>
<Receipt23>[5F2A] 0124                           </Receipt23>
<Receipt24>[5F34] 01                             </Receipt24>
<Receipt25>[ 82 ] 5C00                           </Receipt25>
<Receipt26>[ 84 ] A0000002771010                 </Receipt26>
<Receipt27>[ 95 ] 4000008040                     </Receipt27>
<Receipt28>[ 9A ] 140904                         </Receipt28>
<Receipt29>[ 9B ] F800                           </Receipt29>
<Receipt30>[ 9C ] 00                             </Receipt30>
<Receipt31>[9F02] 000000000010                   </Receipt31>
<Receipt32>[9F03] 000000000000                   </Receipt32>
<Receipt33>[9F10] 068A0A032C2000                 </Receipt33>
<Receipt34>[9F1A] 124                            </Receipt34>
<Receipt35>[9F26] FD725B522667086A               </Receipt35>
<Receipt36>[9F27] 00                             </Receipt36>
<Receipt37>[9F34] 010302                         </Receipt37>
<Receipt38>[9F36] 026A                           </Receipt38>
<Receipt39>[9F37] 89CD4DB4                       </Receipt39>
<Receipt40>                                      </Receipt40>
<Receipt41>TRANSACTION NOT APPROVED              </Receipt41>
<Receipt42>CHIP DECLINED                         </Receipt42>
<Receipt43>00/00                                 </Receipt43>
<Receipt44>                                      </Receipt44>
<Receipt45>IMPORTANT -- retain this copy for your</Receipt45>
<Receipt46>records.                              </Receipt46>
<Receipt47>                                      </Receipt47>
<Receipt48>MERCHANT COPY                         </Receipt48>
<Receipt49>                                      </Receipt49>
<Receipt50>                                      </Receipt50>
</Receipt>
<ReceiptCustomer>
<Receipt1>                                      </Receipt1>
<Receipt2>MERCHANT #       000082025020017      </Receipt2>
<Receipt3>TERM #           01244971             </Receipt3>
<Receipt4>TYPE             PURCHASE             </Receipt4>
<Receipt5>ACCOUNT TYPE     INTERAC CHEQUING     </Receipt5>
<Receipt6>CARD NUMBER      ***************7870  </Receipt6>
<Receipt7>DATE/TIME        14/09/04 13:08:05    </Receipt7>
<Receipt8>REC #                                 </Receipt8>
<Receipt9>INVOICE          1000                 </Receipt9>
<Receipt10>REFERENCE #      000071000011 C       </Receipt10>
<Receipt11>SEQUENCE #       000011               </Receipt11>
<Receipt12>AMOUNT           $0.10                </Receipt12>
<Receipt13>                 --------------       </Receipt13>
<Receipt14>TOTAL            CAD $0.10            </Receipt14>
<Receipt15>                 --------------       </Receipt15>
<Receipt16>APP              INTERAC              </Receipt16>
<Receipt17>AID              A0000002771010       </Receipt17>
<Receipt18>TVR              4000008040           </Receipt18>
<Receipt19>[9F0D] FCF8FCF8F0                     </Receipt19>
<Receipt20>[9F0E] 0000180000                     </Receipt20>
<Receipt21>[9F0F] FC78E4F880                     </Receipt21>
<Receipt22>[ 5A ] ************7870               </Receipt22>
<Receipt23>[5F2A] 0124                           </Receipt23>
<Receipt24>[5F34] 01                             </Receipt24>
<Receipt25>[ 82 ] 5C00                           </Receipt25>
<Receipt26>[ 84 ] A0000002771010                 </Receipt26>
<Receipt27>[ 95 ] 4000008040                     </Receipt27>
<Receipt28>[ 9A ] 140904                         </Receipt28>
<Receipt29>[ 9B ] F800                           </Receipt29>
<Receipt30>[ 9C ] 00                             </Receipt30>
<Receipt31>[9F02] 000000000010                   </Receipt31>
<Receipt32>[9F03] 000000000000                   </Receipt32>
<Receipt33>[9F10] 068A0A032C2000                 </Receipt33>
<Receipt34>[9F1A] 124                            </Receipt34>
<Receipt35>[9F26] FD725B522667086A               </Receipt35>
<Receipt36>[9F27] 00                             </Receipt36>
<Receipt37>[9F34] 010302                         </Receipt37>
<Receipt38>[9F36] 026A                           </Receipt38>
<Receipt39>[9F37] 89CD4DB4                       </Receipt39>
<Receipt40>                                      </Receipt40>
<Receipt41>TRANSACTION NOT APPROVED              </Receipt41>
<Receipt42>CHIP DECLINED                         </Receipt42>
<Receipt43>00/00                                 </Receipt43>
<Receipt44>                                      </Receipt44>
<Receipt45>IMPORTANT -- retain this copy for your</Receipt45>
<Receipt46>records.                              </Receipt46>
<Receipt47>                                      </Receipt47>
<Receipt48>CUSTOMER COPY                         </Receipt48>
<Receipt49>                                      </Receipt49>
<Receipt50>                                      </Receipt50>
</ReceiptCustomer>
</PLResponse>"""

class MyTCPHandler(SocketServer.BaseRequestHandler):
    """
    The RequestHandler class for our server.

    It is instantiated once per connection to the server, and must
    override the handle() method to implement communication to the
    client.
    """
    
    def handle(self):
        # self.request is the TCP socket connected to the client
 
        self.data = self.request.recv(1024).strip()
        print "{} wrote:".format(self.client_address[0])
        #num = random.randrange(1,3)
        num=1
        if num==1:
            self.request.sendall(data)
        else:
            self.request.sendall(data2)    


if __name__ == "__main__":
    HOST, PORT = "localhost", 9998

    # Create the server, binding to localhost on port 9999
    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)

    # Activate the server; this will keep running until you
    # interrupt the program with Ctrl-C
    server.serve_forever()
